<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
if ( get_field('show_events_schedule', get_option('page_on_front') ) ) {
	wp_safe_redirect(home_url('events'));
}
get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();
			$has_nycjw_block = false;
			if ( has_blocks( $post->post_content ) ) {
					$blocks = parse_blocks( $post->post_content );
					foreach($blocks as $block) {
						if (strpos($block['blockName'], 'nycjw') !== false) {
							$has_nycjw_block = true;
						}
					}
			} ?>
			<section class="section">
				<div class="section-wrapper full-width-page">
					<div id="page-content" class="<?php echo $has_nycjw_block ? '' : 'inner-content'; ?>">
						<?php the_content();
						// global $post;
						// $blocks = parse_blocks( $post->post_content );
						// print_r($blocks); ?>
					</div>
				</div>
			</section>
		<?php
		endwhile; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
