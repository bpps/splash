<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();
				$gallery = get_field('vendor_gallery'); ?>
				<section class="section">
					<div class="section-wrapper">
						<div id="page-content">
							<div class="vendor-single-page">
								<div class="vendor-content">
									<div class="vendor-gallery-container">
										<div class="vendor-gallery">
											<?php
											if($image = get_field('vendor_image')) { ?>
												<img src="<?php echo $image['sizes']['small-medium']; ?>"/>
											<?php
											}
											if($gallery) {
												foreach($gallery as $galImg) { ?>
													<img src="<?php echo $galImg['sizes']['medium']; ?>"/>
												<?php
												}
											} ?>
										</div>
										<?php
										if($gallery) { ?>
											<div class="vendor-gallery-arrows">
												<div class="vendor-arrow-left vendor-arrow"></div>
												<div class="vendor-arrow-right vendor-arrow"></div>
											</div>
										<?php
										} ?>
									</div>
									<div class="vendor-info">
										<h1><?php the_title(); ?></h1>
										<?php
										if($locations = get_the_terms( get_the_id(), 'vendor-location' )) { ?>
											<p class="vendor-location"><?php echo $locations[0]->name; ?></p>
										<?php
										}
										$twitter = get_field('vendor_twitter');
										$instagram = get_field('vendor_instagram');
										$facebook = get_field('vendor_facebook');
										if($twitter || $instagram || $facebook) { ?>
											<div class="vendor-social-links flex-grid">
												<?php
												if($twitter) { ?>
													<a target="_blank" href="https://twitter.com/<?php echo $twitter; ?>" class="vendor-social-link">
														<img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"/>
													</a>
												<?php
												}
												if($instagram) { ?>
													<a target="_blank" href="https://instagram.com/<?php echo $instagram; ?>" class="vendor-social-link flex-grid flex-align-center">
														<img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/>
														<span>@<?php echo $instagram; ?></span>
													</a>
												<?php
												}
												if($facebook) { ?>
													<a target="_blank" href="<?php echo $facebook; ?>" class="vendor-social-link">
														<img src="<?php echo get_template_directory_uri(); ?>/images/fb.png"/>
													</a>
												<?php
												} ?>
											</div>
										<?php }
										the_field('vendor_description');
										if($url = get_field('vendor_url')) { ?>
											<p><a target="_blank" href="<?php echo $url; ?>">website</a></p>
										<?php
										}
										if($address = get_field('vendor_address')) { ?>
											<p><?php echo $address; ?></p>
										<?php
										}
										if( $content = get_field('vendor_content') ) { ?>
											<div class="vendor-content-text">
												<?php echo $content; ?>
											</div>
										<?php
										} ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
