<?php
/**
 * The template for displaying author
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

if( is_user_logged_in() ) {
	if ( !current_user_can('administrator') ) {
		$user = wp_get_current_user();
		$author_id = get_query_var( 'author' );
		if ( $user->ID != $author_id ) {
			wp_safe_redirect( home_url('wp-login.php') );
		}
		$business_name = get_field('business_name', 'user_' . $author_id);
		if ( !$business_name && ( !isset($_GET['page']) || $_GET['page'] != 'account' ) ) {
			wp_safe_redirect( get_author_posts_url( $author_id ) . '?page=account&edit_id=' . 'user_' . $author_id );
		}
	}
} else {
	wp_safe_redirect( home_url('wp-login.php') );
}
// session_start();
$events_disabled = true;
$discover_disabled = false;
$retail_disabled = false;
$discover_retail_disabled = false;
if ( !$events_disabled || !$discover_disabled || !$discover_retail_disabled ) {
	acf_form_head();
}
get_header();
if ( $discover_disabled && $events_disabled ) { ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section flex flex-align-center" style="min-height: 70vh;">
				<div class="section-wrapper text-center py5">
					<h2>Event registration is currently closed.</h2>
				</div>
			</section>
		</main>
	</div>
<?php
} else {
	$author_id = get_query_var( 'author' );
	$author = get_user_by( 'id', $author_id );
	$edit_id = isset($_GET['edit_id']) ? $_GET['edit_id'] : null;
	$discover_edit_id = null;
	$access_array = get_field('field_6117d70b70724', 'user_' . $author_id);
	$access = array_map( function($arr) {
		return trim($arr['value']);
	}, $access_array);
	$page_type = isset($_GET['page']) ? $_GET['page'] : $access[0];
	$page_type = preg_replace('/\s+/', '', $page_type);
	$form_label = '';

	foreach( $access_array as $access ) {
		if ( $access['value'] == $page_type ) {
			$form_label = $access['label'];
		}
	}

	// 'field_groups' => ['group_6101620abd8c7'],
	// 'post_id' 	   => 'user_' . $author_id,
	// 'submit_value' => 'Update Business Information',
	// 'return'       => home_url( $wp->request ),
	$form_details = [
		'account' => [
			'group' => 'group_6101620abd8c7',
			'label' => 'Account',
			'disabled' => false,
			'limit' => 1,
			'edit_label' => 'business information',
			'label_single' => 'Business Information',
			'show_preview' => false
		],
		'event' => [
			'group' => 'group_5b57491693783',
			'label' => 'My Events',
			'disabled' => false,
			'limit' => 5,
			'edit_label' => 'an event',
			'label_single' => 'Event',
			'show_preview' => true
		],
		'discover' => [
			'group' => 'group_610c47ed98e8d',
			'label' => 'My Discover Events',
			'disabled' => false,
			'limit' => 5,
			'edit_label' => 'a discover event',
			'label_single' => 'Discover Event',
			'show_preview' => true
		],
		'discover-retail' => [
			'group' => 'group_632874d050820',
			'label' => 'My Retail Application',
			'disabled' => false,
			'limit' => 1,
			'edit_label' => 'your Discover Retail application',
			'label_single' => 'Discover Retail',
			'show_preview' => false
		]
	];
	
	$query_args = [
		'post_id' => $edit_id ? $edit_id : 'new_post',
		'id' => $edit_id 
			? $page_type . '-form-' . $edit_id 
			: 'new-' . $page_type,
		'post_title' => $page_type != 'account',
		'new_post' => $edit_id
			? null
			: array(
				'post_type'		=> $page_type,
				'post_status'	=> 'draft'
			),
		'html_before_fields' => $edit_id 
			? '<h2 class="edit-event-title text-centered">' . get_the_title($edit_id) . '</h2>'
			: '<h2 class="edit-event-title text-centered">New ' . $form_details[$page_type]['label_single'] . '</h2>',
		'updated_message' => $edit_id 
			? __("{$form_details[$page_type]['label_single']} Updated", 'acf')
			: __("Your " . $form_details[$page_type]['label_single'] . " has been submitted", 'acf'),
		'submit_value' => $edit_id 
			? __("Update {$form_details[$page_type]['label_single']}", 'acf')
			: __("Submit {$form_label}", 'acf'),
		'uploader' => 'basic',
		'field_groups' => [$form_details[$page_type]['group']]
	];

	$current_view = [ 
		'label' => $form_details[$page_type]['label'], 
		'view' => $page_type,
		'has_access' => in_array($page_type, $access),
		'query_args' => $query_args
	];

	$submitted_posts = array_map( function($arr) use ($author_id) {
		$posts = get_posts([
			'post_type' => $arr['value'],
			'numberposts' => -1,
			'author' => $author_id,
			'post_status' => ['publish', 'draft'],
		]);
		// If this is the current page type and the limit is 1 and the user has 1 post, set that as the edit post
		return [ "label" => $arr['label'], "slug" => $arr['value'], 'posts' => $posts ];
	}, $access_array);

	foreach( $submitted_posts as $sub_post ) {
		if ( $sub_post['slug'] == $page_type && $form_details[$sub_post['slug']]['limit'] == count($sub_post['posts']) && $form_details[$sub_post['slug']]['limit'] == 1 ) {
			$current_view['query_args'] = [
				'post_id' => $sub_post['posts'][0]->ID,
				'id' => $sub_post['posts'][0]->ID,
				'post_title' => $page_type != 'account',
				'new_post' => null,
				'html_before_fields' => '<h2 class="edit-event-title text-centered">' . get_the_title($sub_post['posts'][0]->ID) . '</h2>',
				'updated_message' => __("{$form_details[$page_type]['label_single']} Updated", 'acf'),
				'submit_value' => __("Update {$form_details[$page_type]['label_single']}", 'acf'),
				'uploader' => 'basic',
				'field_groups' => [$form_details[$page_type]['group']]
			];
		}
	} ?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<section id="event-owner-events" class="section">
					<div class="section-wrapper">
						<?php
						$obj_id = get_queried_object_id();
						$current_url = get_author_posts_url( $obj_id ); ?>
						<div id="page-header">
							<?php
							$titleWidth = get_title_length('My Events'); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php 
								echo $current_view['label']; ?>
							</h1>
							<hr>
							<div class="sub-nav">
								<ul>
									<?php
									foreach ( $submitted_posts as $menu_item ) {
										if ( $menu_item['slug'] != 'account' && !$business_name ) {
											continue;
										} ?>
										<li>
											<a href="<?php echo get_author_posts_url( $author_id ); ?>?page=<?php echo $menu_item['slug']; ?>">
												<?php echo $menu_item['label']; ?>
											</a>
										</li>
									<?php 
									} ?>
									<li>
										<a href="<?php echo get_author_posts_url( $author_id ) . '?page=account&edit_id=user_' . $author_id; ?>">
											Account
										</a>
									</li>
									<li>
										<a href="<?php echo wp_logout_url(); ?>">
											Log Out
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="row justify-center">
							<?php 
							if ( $page_type != 'account' ) { ?>
								<div class="col-4">
									<div id="event-owner-events-list">
										<?php
										$event_count = 0;
										foreach ( $submitted_posts as $post ) { ?>
											<h3 style="text-transform: uppercase; border-bottom: 3px solid black; margin-top: 0; margin-bottom: 1rem;"><?php echo $form_details[$post['slug']]['label']; ?></h3>
											<?php 
											if ( count($post['posts']) > 0 ) {
												if ( !$form_details[$post['slug']]['disabled'] ) { ?>
													<p style="margin-top: 1rem;">Click <?php echo $form_details[$post['slug']]['edit_label']; ?> to edit</p>
													<?php
													foreach ( $post['posts'] as $form_post ) { ?>
														<div class="event-owner-list-item event-list-item-<?php echo $form_post->ID; ?>">
															<div class="row align-center">
																<div class="col-12">
																	<div class="event-row" data-id="<?php echo $form_post->ID;?>">
																		<a href="<?php echo $current_url . '?page=' . $post['slug'] . '&edit_id=' . $form_post->ID; ?>" class="edit-event-button justify-space-between flex" data-event-id="<?php echo $form_post->ID; ?>">
																			<span><?php echo $form_post->post_title; ?></span>
																			<img alt="edit event" src="<?php echo get_template_directory_uri(); ?>/images/edit.png"/>
																		</a>
																	</div>
																</div>
															</div>
														</div>
													<?php 
													}
												} else { ?>
													<p><?php echo $post['label']; ?> submission is closed</p>
												<?php 
												}
											}
											if ( $form_details[$post['slug']]['limit'] > count($post['posts']) && !$form_details[$post['slug']]['disabled'] ) { ?>
												<a href="<?php echo $current_url; ?>?page=<?php echo $post['slug']; ?>" style="margin-bottom: 3rem; margin-top: 1rem; width: 100%;" class="mt2 create-an-event btn list-create-an-event">
													Create a new <?php echo strtolower($post['slug']); ?>
												</a>
											<?php 
											}
										} ?>
									</div>
								</div>
							<?php
							}
							// $current_view = [ 
							// 	'label' => $form_details[$page_type]['label'], 
							// 	'view' => $page_type,
							// 	'field_group' => $form_details[$page_type]['group'],
							// 	'has_access' => in_array($page_type, $access),
							// 	'query_args' => $query_args
							// ]; ?>
							<div class="col-8">
								<?php
								if ( $form_details[$current_view['view']]['disabled'] ) { ?>
									<section class="section flex flex-align-center">
										<div class="text-center" style="width: 100%;">
											<h2>Event registration is currently closed</h2>
											<?php
											if ( in_array('discover', $access) ) { ?>
												<p>There's still time to <?php echo $discover_edit_id ? 'edit ' : 'create '; ?> your Discover event</p>
												<?php
												if ( $discover_edit_id ) { ?>
													<a href="<?php echo $current_url . '?edit_discover=' . $discover_edit_id; ?>" class="mt1 create-an-event btn list-create-an-event" data-event-id="<?php echo $discover_edit_id; ?>">
														Edit your Discover event
													</a>
												<?php
												} else { ?>
													<a href="<?php echo $current_url . '?discover=submit'; ?>" style="margin-top: 1rem; width: 100%;" class="mt1 create-an-event btn list-create-an-event">
														Create a Discover event
													</a>
												<?php
												}
											} ?>
										</div>
									</section>
								<?php
								} else {
									// Show event views ?>
									<div class="event-owner-event event-form event-<?php echo get_the_ID(); ?>">
										<div class="row edit-event-form">
											<?php
											// if ( !current_user_can('administrator') ) {
											// 	$update_event_form_args['field_groups'] = ['group_5b57491693783'];
											// }
											acf_form($current_view['query_args']); ?>
										</div>
										<?php 
										if ( $form_details[$page_type]['show_preview'] ) { ?>
											<div id="preview-wrapper"></div>
										<?php 
										} ?>
									</div>
								<?php
								} ?>
							</div>
						</div>
					</div>
				</section>

			</main><!-- #main -->
		</div><!-- #primary -->
	<?php
	}
get_footer();
