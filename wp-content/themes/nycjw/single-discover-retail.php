<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();
                $background_image = get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>
                <div id="featured-articles">
                    <div class="lazy featured-article-wrapper" style="background-image: url('<?php echo $background_image; ?>');">
                        <div class="thumbnail-wrapper">
                            <h2 class="entry-title font-larger"><span><?php echo get_the_title(); ?></span></h2>
                            <?php 
                            if ( $store_url = get_field('discover_retail_url') ) {
                                $store_url = preg_replace( "#^[^:/.]*[:/]+#i", "", $store_url ); ?>
                                <a class="category nunito text-decoration-none" href="<?php echo $store_url; ?>" target="_blank">
                                    <?php echo $store_url; ?>
                                </a>
                            <?php 
                            }
                            if ( $address = get_field('discover_retail_address') ) { ?>
                                <div class="discover-address">
                                    <?php 
                                    if ( $address['street'] ) { ?>
                                        <span><?php echo $address['street']; ?></span>
                                    <?php 
                                    }
                                    if ( $address['city_state_zip'] ) { ?>
                                        <span><?php echo $address['city_state_zip']; ?></span>
                                    <?php 
                                    } ?>
                                </div>
                            <?php
                            } ?>
                        </div>
                    </div>
                </div>
                <?php 
                if ( get_the_content() ) { ?>
                    <div class="discover-content inner-content">
                        <?php the_content(); ?>
                    </div>
                <?php 
                } 
                if ( $products = get_field('discover_retail_products') ) { ?>
                    <div id="filterable-items" class="items-per-row-3">
                        <div class="item-grid-wrapper" style="display: flex; flex-wrap: wrap;">
                            <?php 
                            foreach ( $products as $product_index => $product ) { ?>
                                <div class="item">
                                    <div class="item-content">
                                        <button class="open-discover-modal" data-index="<?php echo $product_index; ?>">
                                            <div class="image-wrapper ratio-7">
                                                <img src="<?php echo $product['image']['sizes']['medium']; ?>" />
                                            </div>
                                            <div class="item-info">
                                                <div class="item-name">
                                                    <h3><a href="<?php echo $product['external_link'];?>" target="_blank"><?php echo $product['title']; ?></a></h3>
                                                </div>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            <?php 
                            } ?>
                        </div>
                    </div>
                    <div role="dialog" id="discover-modal" aria-label="Product Images" aria-modal="true" class="hidden">
                        <div id="discover-modal__wrapper">
                            <div id="discover-modal__close" onclick="closeDialog(this)"></div>
                            <div id="discover-modal__carousel">
                                <?php 
                                foreach ( $products as $product ) { ?>
                                    <div class="discover-modal">
                                        <div class="flex w-100 flex-align-top">
                                            <div class="discover-modal__image">
                                                <div class="image-wrapper bg-centered detail-view" style="background-image: url('<?php echo $product['image']['sizes']['large']; ?>');">
                                                </div>
                                            </div>
                                            <div class="discover-modal__content">
                                                <span class="category"><?php echo get_the_title(); ?></span>
                                                <h3><?php echo $product['title']; ?></h3>
                                                <?php 
                                                if ( $product['description'] ) { ?>
                                                    <div class="discover-modal__desc">
                                                        <?php echo $product['description']; ?>
                                                    </div>
                                                <?php 
                                                }
                                                if ( $product['external_link'] ) { ?>
                                                    <a class="btn" target="_blank" href="<?php echo $product['external_link']; ?>">View Product</a>
                                                <?php 
                                                } ?> 
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                } ?>
                            </div>
                            <div class="arrow-prev">
                                <svg width="31" height="53" viewBox="0 0 31 53" fill="none">
                                <path d="M27.0119 53L31 49.0123L8.00456 25.9909L30.01 4.01601L25.9936 -4.37672e-07L2.36121e-06 25.9909L27.0119 53Z" fill="#000"/>
                                </svg>
                            </div>
                            <div class="arrow-next">
                                <svg width="31" height="53" viewBox="0 0 31 53" fill="none">
                                <path d="M3.98814 0L0 3.98773L22.9954 27.0091L0.989963 48.984L5.00639 53L31 27.0091L3.98814 0Z" fill="#000"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                <?php
                }
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
