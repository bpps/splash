<?php
/**
 * The template for displaying discover single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$post_id = null;
			while ( have_posts() ) :
				the_post();
				$gallery = get_field('image_carousel'); ?>
				<section class="section">
					<div class="section-wrapper">
						<div id="page-content">
							<div class="vendor-single-page">
								<span class="vendor-location bebas">Discover</span>
								<h1><?php the_title(); ?></h1>
								<div class="vendor-content">
									<?php
									if ( $gallery ) { ?>
										<section id="featured-articles">
									    <div class="featured_article arrow-prev">
									      <svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
									        <path d="M27.0119 53L31 49.0123L8.00456 25.9909L30.01 4.01601L25.9936 -4.37672e-07L2.36121e-06 25.9909L27.0119 53Z" fill="#FCF250"/>
									      </svg>
									    </div>
									    <div class="article-repeater">
									      <?php
												foreach($gallery as $gal_img_id ) {
													 	$gal_img = wp_get_attachment_image_url($gal_img_id, 'large'); ?>
									          <div class="featured-article-wrapper">
									            <div class="thumbnail-wrapper">
																<img src="<?php echo $gal_img; ?>"/>
									            </div>
									          </div>
									        <?php
													} ?>
									    </div>
									    <div class="featured_article arrow-next">
									      <svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
									        <path d="M3.98814 0L0 3.98773L22.9954 27.0091L0.989963 48.984L5.00639 53L31 27.0091L3.98814 0Z" fill="#FCF250"/>
									      </svg>
									    </div>
									  </section>
									<?php
									} ?>
									<div class="vendor-info">
										<h2><?php the_title(); ?></h2>
										<?php
										if ( $about = get_field('about_section') ) { ?>
											<div class="about-section">
												<?php echo $about; ?>
											</div>
										<?php
										}
										if( $url = get_field('booth_website_url') ) { ?>
											<p><a target="_blank" href="<?php echo $url; ?>">website</a></p>
										<?php
										}
										if ( $social = get_field('social') ) { ?>
											<div class="vendor-social-links flex-grid">
												<?php
												foreach ( $social as $channel => $link ) {
													if ( $link ) { ?>
														<a target="_blank" href="<?php echo $link; ?>" class="vendor-social-link">
															<img src="<?php echo get_template_directory_uri() . '/images/' . $channel . '.png'; ?>"/>
														</a>
													<?php
													}
												} ?>
											</div>
										<?php
										}
										if ( $logo = get_field('logo') ) { ?>
											<div class="booth-logo">
												<img src="<?php echo $logo['sizes']['small-medium']; ?>"/>
											</div>
										<?php
										}
										// NOTE: Get contact info from vendor single if we're adding it back in. ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			<?php
			$post_id = get_the_id();
			endwhile;
			wp_reset_postdata(); // End of the loop.
			$all_discover = get_posts( [
				'post_type' => 'discover',
				'orderby'=>'post_date',
				'order'=> 'DESC',
				'posts_per_page' => -1
			] ); ?>
			<div class="post-nav section">
				<div class="section-wrapper">
					<?php
					foreach( $all_discover as $index => $discover ) {
						if ( $discover->ID == $post_id ) { ?>
							<div class="nav-post">
								<?php
								if ( isset($all_discover[$index - 1]) && is_object($all_discover[$index - 1]) ) {
									$nav_post_id = $all_discover[$index - 1]->ID;
									echo nav_post_link($nav_post_id, 'prev');
								} ?>
							</div>
							<div class="nav-post">
							<?php
								if ( isset($all_discover[$index + 1]) && is_object($all_discover[$index - 1]) ) {
									$nav_post_id = $all_discover[$index + 1]->ID;
									echo nav_post_link($nav_post_id, 'next');
								} ?>
							</div>
						<?php
						}
					} ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

function nav_post_link ($nav_post_id, $dir='prev') {
	ob_start();
		$nav_post_image = get_the_post_thumbnail_url($nav_post_id, 'small-medium');
		if ( !$nav_post_image ) {
			if ( $gallery = get_field('image_carousel', $nav_post_id ) ) {
				$nav_post_image = wp_get_attachment_image_url($gallery[0], 'small-medium');
			}
		} ?>
		<a class="post-nav-arrow-link bg-centered" href="<?php echo get_permalink($nav_post_id); ?>" style="background-image: url(<?php echo $nav_post_image; ?>);">
			<div class="post-nav-link-content">
				<div class="post-nav-arrow arrow-<?php echo $dir; ?>">
					<?php
					switch ( $dir ) {
						case 'next': ?>
							<span>
								<?php echo get_the_title($nav_post_id); ?>
								<svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
									<path d="M3.98814 0L0 3.98773L22.9954 27.0091L0.989963 48.984L5.00639 53L31 27.0091L3.98814 0Z" fill="#FCF250"/>
								</svg>
							</span>
						<?php
						break;
						default: ?>
							<span>
								<svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
									<path d="M27.0119 53L31 49.0123L8.00456 25.9909L30.01 4.01601L25.9936 -4.37672e-07L2.36121e-06 25.9909L27.0119 53Z" fill="#FCF250"/>
								</svg>
								<?php echo get_the_title($nav_post_id); ?>
							</span>
						<?php
						break;
					} ?>
				</div>
			</div>
		</a>
	<?php
	return ob_get_clean();
}

get_footer();
