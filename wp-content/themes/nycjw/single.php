<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
			<section class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php $titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
					</div>
					<div id="page-content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
