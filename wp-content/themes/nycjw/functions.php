<?php
/**
 * NYCJW functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NYCJW
 */

/**
	* Export events.
	*/
require get_template_directory() . '/inc/export-events.php';

if ( ! function_exists( 'nycjw_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nycjw_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on NYCJW, use a find and replace
		 * to change 'nycjw' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nycjw', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_image_size( 'small', 400 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( [
			'menu-1' => esc_html__( 'Primary', 'nycjw' ),
		] );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		] );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'nycjw_custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		] ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', [
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		] );
		if (current_user_can('administrator') ) {
			// user can view admin bar
			show_admin_bar(true); // this line isn't essentially needed by default...
		} else {
			// hide admin bar
			show_admin_bar(false);
		}
	}
endif;
add_action( 'after_setup_theme', 'nycjw_setup' );

add_post_type_support( 'page', 'excerpt' );

add_action('init', 'wp55290310_rewrite_rules');

function sponsor_to_sponsors () {
	$args = [
		'post_type'      => ['event'],
		'posts_per_page' => -1,
		'post_status' => ['publish', 'draft'],
		'order' => 'ASC',
	];
	$events_query = new WP_Query( $args );
	if ( $events_query->have_posts() ) :
		while ( $events_query->have_posts() ) : $events_query->the_post();
			$post_id = get_the_ID();
			if ( !get_field('field_63569bb490dc7', $post_id) ) {
				$sponsor_name = get_post_meta($post_id, 'sponsor_sponsor_name', true);
				$sponsor_link = get_post_meta($post_id, 'sponsor_sponsor_link', true);
				$sponsor_logo_id = get_post_meta($post_id, 'sponsor_sponsor_logo', true);
				$row = array();
				if ( $sponsor_name ) {
					$row['field_63569bc390dc8'] = $sponsor_name;
				}
				if ( $sponsor_link ) {
					$row['field_63569bd190dc9'] = $sponsor_link;
				}
				if ( $sponsor_logo_id ) {
					$row['field_63569bd890dca'] = $sponsor_logo_id;
				}
				if ( $sponsor_name || $sponsor_link || $sponsor_logo_id ) {
					add_row('field_63569bb490dc7', $row);
				}
			}
		endwhile;
		wp_reset_postdata();
	endif;
}

// add_action('init', 'sponsor_to_sponsors');

function wp55290310_rewrite_rules() {
    add_rewrite_rule('^events/(.+)?', 'index.php?pagename=events', 'top');
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nycjw_widgets_init() {
	register_sidebar( [
		'name'          => esc_html__( 'Sidebar', 'nycjw' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nycjw' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	] );
}
add_action( 'widgets_init', 'nycjw_widgets_init' );

function myguten_enqueue() {
    wp_enqueue_script(
        'gutenberg-scripts',
        get_template_directory_uri() . '/js/gutenberg-scripts.js',
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
        filemtime( get_template_directory_uri() . '/js/gutenberg-scripts.js' )
    );
}
// add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );

/**
 * Enqueue scripts and styles.
 */
function nycjw_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'lazy', get_template_directory_uri() .'/js/jquery.lazy.min.js', ['jquery'], null, true);
	wp_enqueue_script( 'aria-modal', get_template_directory_uri() .'/js/aria-modal.js', ['jquery'], null, true);

	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'nycjw-style', get_template_directory_uri() . $main->css, false, null );

	if ( is_page_template( 'templates/map.php' ) || is_page_template( 'templates/events.php' ) ) {

		wp_enqueue_script( 'map-box-script', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js', [], null, true );

		wp_enqueue_style( 'map-box-style', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css', false, null);

		wp_enqueue_script('nycjw-map-js', get_template_directory_uri() . $manifest->map->js, ['jquery'], null, true);

  }

	if ( is_page_template( 'templates/events.php') || has_block('acf/nycjw-events') ) {

		wp_enqueue_script('nycjw-events-js', get_template_directory_uri() . $manifest->events->js, ['jquery'], null, true);
	}

	wp_enqueue_script( 'images-loaded', get_template_directory_uri() .'/js/imagesloaded.min.js', ['jquery'], null, true);

	wp_enqueue_script( 'packery', get_template_directory_uri() .'/js/packery-mode.pkgd.min.js', ['jquery'], null, true);


	// wp_enqueue_script( 'jquery-masonry' );

	wp_enqueue_script( 'isotope', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js' );

	if ( has_block('acf/nycjw-filterable-content') || is_tax() ) {

		wp_enqueue_script('nycjw-filterable-js', get_template_directory_uri() . $manifest->vendors->js, ['jquery'], null, true);

	}

	wp_enqueue_script('nycjw-js', get_template_directory_uri() . $main->js, ['jquery', 'masonry'], null, true);

	//wp_enqueue_script( 'nycjw-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	//wp_enqueue_script( 'nycjw-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nycjw_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * ACF Block Functions.
 */
require get_template_directory() . '/inc/acf-block-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
	* NYCJ Shortcodes.
	*/
require get_template_directory() . '/inc/nycjw-shortcodes.php';

/**
 * ACF Blocks
 */
require get_template_directory() . '/inc/acf-blocks.php';

/**
	* Vendor Functions.
	*/
require get_template_directory() . '/inc/vendors.php';

/**
	* Event Submission Functions.
	*/
require get_template_directory() . '/inc/event-submission.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page([
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'NYCJW Settings',
		'menu_slug' 	=> 'nycjw-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	]);

	acf_add_options_sub_page([
		'page_title' 	=> 'Testimonials',
		'menu_title'	=> 'Testimonials',
		'parent_slug'	=> 'nycjw-settings',
	]);

	acf_add_options_sub_page([
		'page_title' 	=> 'Event submission',
		'menu_title'	=> 'Event submission',
		'parent_slug'	=> 'nycjw-settings',
	]);

	acf_add_options_sub_page(array(
		'page_title'  => __('Event Settings'),
		'menu_title'  => __('Event Settings'),
		'parent_slug' => 'edit.php?post_type=event',
	));

}



function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function register_custom_post_types() {

	$labels = [
    'name' => _x('Team', 'post type general name'),
    'singular_name' => _x('Team Member', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New Team Member'),
    'edit_item' => __('Edit Team Member'),
    'new_item' => __('New Team Member'),
    'view_item' => __('View Team Member'),
    'search_items' => __('Search Team'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-groups',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'],
    'taxonomies' => ['member-type'],
  ];

  register_post_type( 'team' , $args );

	$labels = [
    'name' => _x('HWA honorees', 'post type general name'),
    'singular_name' => _x('HWA', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New HWA'),
    'edit_item' => __('Edit HWA'),
    'new_item' => __('New HWA'),
    'view_item' => __('View HWA'),
    'search_items' => __('Search HWA honorees'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
	'menu_icon'   => 'dashicons-heading',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'maker' , $args );

	$labels = [
    'name' => _x('Explore Jewelers', 'post type general name'),
    'singular_name' => _x('Explore Jewelers', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New Explore Jewelers'),
    'edit_item' => __('Edit Explore Jewelers'),
    'new_item' => __('New Explore Jewelers'),
    'view_item' => __('View Explore Jewelers'),
    'search_items' => __('Search Explore Jewelers'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
	'menu_icon'   => 'dashicons-superhero-alt',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'jewelry' , $args );

	$labels = [
    'name' => _x('Full Circle Makers', 'post type general name'),
    'singular_name' => _x('Full Circle Maker', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New Full Circle Maker'),
    'edit_item' => __('Edit Full Circle Maker'),
    'new_item' => __('New Full Circle Maker'),
    'view_item' => __('View Full Circle Maker'),
    'search_items' => __('Search Full Circle Makers'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 6,
	'menu_icon'   => 'dashicons-marker',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes']
  ];

  register_post_type( 'fc-maker' , $args );

	$labels = [
    'name' => _x('Advisors', 'post type general name'),
    'singular_name' => _x('Advisor', 'post type singular name'),
    'add_new' => _x('Add New', 'advisor'),
    'add_new_item' => __('Add New Advisor'),
    'edit_item' => __('Edit Advisor'),
    'new_item' => __('New Advisor'),
    'view_item' => __('View Advisor'),
    'search_items' => __('Search Advisors'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'],
    'taxonomies' => ['advisor-type'],
  ];

  register_post_type( 'advisor' , $args );

	$labels = [
    'name' => _x('Mentors', 'post type general name'),
    'singular_name' => _x('Mentor', 'post type singular name'),
    'add_new' => _x('Add New', 'mentor'),
    'add_new_item' => __('Add New Mentor'),
    'edit_item' => __('Edit Mentor'),
    'new_item' => __('New Mentor'),
    'view_item' => __('View Mentor'),
    'search_items' => __('Search Mentors'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => 'Parent Page'
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => true,
	'menu_icon'   => 'dashicons-universal-access',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'],
    'taxonomies' => [],
  ];

  register_post_type( 'mentor' , $args );

	$labels = [
    'name' => _x('Press', 'post type general name'),
    'singular_name' => _x('Press', 'post type singular name'),
    'add_new' => _x('Add New', 'press'),
    'add_new_item' => __('Add New Press'),
    'edit_item' => __('Edit Press'),
    'new_item' => __('New Press'),
    'view_item' => __('View Press'),
    'search_items' => __('Search Press'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-megaphone',
    'supports' => ['title', 'editor', 'page-attributes'],
  ];

  register_post_type( 'press' , $args );

	$labels = [
    'name' => _x('Events', 'post type general name'),
    'singular_name' => _x('Event', 'post type singular name'),
    'add_new' => _x('Add New', 'event'),
    'add_new_item' => __('Add New Event'),
    'edit_item' => __('Edit Event'),
    'new_item' => __('New Event'),
    'view_item' => __('View Event'),
    'search_items' => __('Search Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-calendar',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'author', 'page-attributes'],
    'taxonomies' => ['event-type', 'event-theme', 'event-category' ],
  ];

  register_post_type( 'event' , $args );

	$labels = [
    'name' => _x('Discover', 'post type general name'),
    'singular_name' => _x('Discover Event', 'post type singular name'),
    'add_new' => _x('Add New', 'virtual-booth'),
    'add_new_item' => __('Add New Discover Event'),
    'edit_item' => __('Edit Discover Event'),
    'new_item' => __('New Discover Event'),
    'view_item' => __('View Discover Event'),
    'search_items' => __('Search Discover Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'discover', 'with_front' => false),
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-visibility',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'author', 'page-attributes'],
    'taxonomies' => ['virtual-booth-type' ],
  ];

  register_post_type( 'virtual-booth' , $args );

  $labels = [
    'name' => _x('Discover', 'post type general name'),
    'singular_name' => _x('Discover Event', 'post type singular name'),
    'add_new' => _x('Add New', 'virtual-booth'),
    'add_new_item' => __('Add New Discover Event'),
    'edit_item' => __('Edit Discover Event'),
    'new_item' => __('New Discover Event'),
    'view_item' => __('View Discover Event'),
    'search_items' => __('Search Discover Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'discover', 'with_front' => false),
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-visibility',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'author', 'page-attributes'],
    'taxonomies' => ['virtual-booth-type' ],
  ];

  register_post_type( 'discover' , $args );

  $labels = [
    'name' => _x('Discover Retail', 'post type general name'),
    'singular_name' => _x('Discover Retail', 'post type singular name'),
    'add_new' => _x('Add New', 'discover-retail'),
    'add_new_item' => __('Add New Discover Retail'),
    'edit_item' => __('Edit Discover Retail'),
    'new_item' => __('New Discover Retail'),
    'view_item' => __('View Discover Retail'),
    'search_items' => __('Search Discover Retail'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'discover-retail', 'with_front' => false),
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
	'menu_icon'   => 'dashicons-products',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'author', 'page-attributes']
  ];

  register_post_type( 'discover-retail' , $args );

	$labels = [
    'name' => _x('OFTF Creatives', 'post type general name'),
    'singular_name' => _x('OFTF Creative', 'post type singular name'),
    'add_new' => _x('Add New', 'emerging-creatives'),
    'add_new_item' => __('Add New OFTF Creative'),
    'edit_item' => __('Edit OFTF Creative'),
    'new_item' => __('New OFTF Creative'),
    'view_item' => __('View OFTF Creative'),
    'search_items' => __('Search OFTF Creatives'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
	'menu_icon'   => 'dashicons-hammer',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'],
  ];

  register_post_type( 'emerging-creatives' , $args );

	$labels = [
    'name' => _x('Vendors', 'post type general name'),
    'singular_name' => _x('Vendor', 'post type singular name'),
    'add_new' => _x('Add New', 'vendor'),
    'add_new_item' => __('Add New Vendor'),
    'edit_item' => __('Edit Vendor'),
    'new_item' => __('New Vendor'),
    'view_item' => __('View Vendor'),
    'search_items' => __('Search Vendor'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
	'menu_icon'   => 'dashicons-money-alt',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'author'],
    'taxonomies' => ['jewelry-type', 'vendor-location'],
		'map_meta_cap' => true,
		'capabilities' => [
		  'edit_post'          => 'edit_vendor',
		  'publish_posts'      => 'publish_vendor',
			'edit_posts'         => 'edit_vendors',
			'upload_files'			 => 'upload_vendor_files'
		],
  ];

  register_post_type( 'vendor' , $args );

	$labels = [
    'name' => _x('Shop The Week', 'post type general name'),
    'singular_name' => _x('Shop The Week', 'post type singular name'),
    'add_new' => _x('Add New', 'vendor'),
    'add_new_item' => __('Add New Shop The Week'),
    'edit_item' => __('Edit Shop The Week'),
    'new_item' => __('New Shop The Week'),
    'view_item' => __('View Shop The Week'),
    'search_items' => __('Search Shop The Week'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  ];

  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
	'menu_icon'   => 'dashicons-cart',
    'has_archive' => false,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'author'],
    'taxonomies' => ['jewelry-type', 'price-range']
  ];

  register_post_type( 'jeweler-products' , $args );
}

function role_set (){
	global $wp_roles;

	$role = get_role( 'shop_manager' );
}
// add_action('init', 'role_set');

add_action('init', 'register_custom_post_types');

function give_vendor_edit_capabilities() {
  $vendor = get_role('vendor');
	$admin = get_role('administrator');
  $addCaps = [
    'edit_vendor',
    'publish_vendor',
		'edit_vendors',
		'upload_vendor_files'
  ];
  foreach($addCaps as $cap) {
    if(!isset($vendor->capabilities[ $cap ]) || $vendor->capabilities[ $cap ] == false) {
      $vendor->add_cap( $cap, true );
    }
  }
	foreach($addCaps as $cap) {
    if(!isset($admin->capabilities[ $cap ]) || $admin->capabilities[ $cap ] == false) {
      $admin->add_cap( $cap, true );
    }
  }
}
add_action( 'admin_init', 'give_vendor_edit_capabilities' );

function register_custom_taxonomies() {

	$labels = [
		'name'                       => _x( 'Member Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Member Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Member Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Member Types', 'nycjw' ),
		'all_items'                  => __( 'All Member Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Member Type', 'nycjw' ),
		'update_item'                => __( 'Update Member Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Member Type', 'nycjw' ),
		'new_item_name'              => __( 'New Member Type Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate member types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove member types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used member types', 'nycjw' ),
		'not_found'                  => __( 'No member types found.', 'nycjw' ),
		'menu_name'                  => __( 'Member Types', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'advisor-type' ],
	];

	register_taxonomy( 'member-type', ['team'], $args );
	// register_taxonomy_for_object_type( 'member-type', ['team'] );

	$labels = [
		'name'                       => _x( 'Advisor Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Advisor Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Advisor Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Advisor Types', 'nycjw' ),
		'all_items'                  => __( 'All Advisor Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Advisor Type', 'nycjw' ),
		'update_item'                => __( 'Update Advisor Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Advisor Type', 'nycjw' ),
		'new_item_name'              => __( 'New Advisor Type Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate advisor types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove advisor types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used advisor types', 'nycjw' ),
		'not_found'                  => __( 'No advisor types found.', 'nycjw' ),
		'menu_name'                  => __( 'Advisor Types', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'advisor-type' ],
	];

	register_taxonomy( 'advisor-type', ['advisor'], $args );
	// register_taxonomy_for_object_type( 'advisor-type', ['advisor'] );

	$labels = [
		'name'                       => _x( 'Event Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Types', 'nycjw' ),
		'all_items'                  => __( 'All Event Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Type', 'nycjw' ),
		'update_item'                => __( 'Update Event Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Type', 'nycjw' ),
		'new_item_name'              => __( 'New Event Type', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event types', 'nycjw' ),
		'not_found'                  => __( 'No event types found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Types', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'has_archive'						=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'event-type' ],
	];

	register_taxonomy( 'event-type', ['event'], $args );

	$labels = [
		'name'                       => _x( 'Discover Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Discover Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Discover Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Discover Types', 'nycjw' ),
		'all_items'                  => __( 'All Discover Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Discover Type', 'nycjw' ),
		'update_item'                => __( 'Update Discover Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Discover Type', 'nycjw' ),
		'new_item_name'              => __( 'New Discover Type', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate Discover Types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove Discover Types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used Discover Types', 'nycjw' ),
		'not_found'                  => __( 'No event types found.', 'nycjw' ),
		'menu_name'                  => __( 'Discover Types', 'nycjw' ),
	];

	$args = [
    	'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'rewrite'				=> ['slug' => 'discover-type', 'with_front' => true],
		'public'								=> true,
		'has_archive'						=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
	];

	register_taxonomy( 'virtual-booth-type', ['virtual-booth'], $args );
	// register_taxonomy_for_object_type( 'event-type', ['event'] );

	$labels = [
		'name'                       => _x( 'Supporting Event Categories', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Supporting Event Category', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Supporting Event Categories', 'nycjw' ),
		'popular_items'              => __( 'Popular Supporting Event Categories', 'nycjw' ),
		'all_items'                  => __( 'All Supporting Event Categories', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Supporting Event Category', 'nycjw' ),
		'update_item'                => __( 'Update Supporting Event Category', 'nycjw' ),
		'add_new_item'               => __( 'Add New Supporting Event Category', 'nycjw' ),
		'new_item_name'              => __( 'New Supporting Event Category', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate supporting event categories with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove supporting event categories', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used supporting event categories', 'nycjw' ),
		'not_found'                  => __( 'No supporting event categories found.', 'nycjw' ),
		'menu_name'                  => __( 'Supporting Event Categories', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'supporting-event-category' ],
	];

	register_taxonomy( 'supporting-event-category', ['event'], $args );

	$labels = [
		'name'                       => _x( 'Event Locations', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Location', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Locations', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Locations', 'nycjw' ),
		'all_items'                  => __( 'All Event Locations', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Location', 'nycjw' ),
		'update_item'                => __( 'Update Event Location', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Location', 'nycjw' ),
		'new_item_name'              => __( 'New Event Location', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event locations with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event locations', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event locations', 'nycjw' ),
		'not_found'                  => __( 'No event locations found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Locations', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'event-location' ],
	];

	// register_taxonomy( 'event-location', ['event'], $args );
		// register_taxonomy_for_object_type( 'event-location', ['event'] );

	$labels = [
		'name'                       => _x( 'Event Themes', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Theme', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Themes', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Themes', 'nycjw' ),
		'all_items'                  => __( 'All Event Themes', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Theme', 'nycjw' ),
		'update_item'                => __( 'Update Event Theme', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Theme', 'nycjw' ),
		'new_item_name'              => __( 'New Event Theme', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event themes with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event themes', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event themes', 'nycjw' ),
		'not_found'                  => __( 'No event themes found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Themes', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'has_archive'						=> false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'event-themes' ],
	];

	// register_taxonomy( 'event-theme', ['event'], $args );

	$labels = [
		'name'                       => _x( 'Event Days', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Day', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Days', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Days', 'nycjw' ),
		'all_items'                  => __( 'All Event Days', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Day', 'nycjw' ),
		'update_item'                => __( 'Update Event Day', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Day', 'nycjw' ),
		'new_item_name'              => __( 'New Event Day', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event days with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event days', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event days', 'nycjw' ),
		'not_found'                  => __( 'No event days found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Days', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'has_archive'						=> false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		// 'rewrite'               => [ 'slug' => 'event-themes' ],
	];

	register_taxonomy( 'event-day', ['event'], $args );

	$labels = [
		'name'                       => _x( 'Jewelry Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Jewelry Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search jewelry Types', 'nycjw' ),
		'popular_items'              => __( 'Popular jewelry Types', 'nycjw' ),
		'all_items'                  => __( 'All jewelry Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit jewelry Type', 'nycjw' ),
		'update_item'                => __( 'Update jewelry Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New jewelry Type', 'nycjw' ),
		'new_item_name'              => __( 'New jewelry Type Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate jewelry types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove jewelry types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used jewelry types', 'nycjw' ),
		'not_found'                  => __( 'No jewelry types found.', 'nycjw' ),
		'menu_name'                  => __( 'Jewelry Types', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'jewelry-type' ],
	];

	register_taxonomy( 'jewelry-type', ['vendor', 'event'], $args );
	// register_taxonomy_for_object_type( 'jewelry-type', ['vendor', 'event'] );

	$labels = [
		'name'                       => _x( 'Locations', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Location', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Locations', 'nycjw' ),
		'popular_items'              => __( 'Popular Locations', 'nycjw' ),
		'all_items'                  => __( 'All Locations', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Location', 'nycjw' ),
		'update_item'                => __( 'Update Location', 'nycjw' ),
		'add_new_item'               => __( 'Add New Location', 'nycjw' ),
		'new_item_name'              => __( 'New Location Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate locations with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove locations', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used locations', 'nycjw' ),
		'not_found'                  => __( 'No locations found.', 'nycjw' ),
		'menu_name'                  => __( 'Locations', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'search_in'     => true,
		'public'								=> false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'location' ],
	];

	register_taxonomy( 'vendor-location', ['vendor'], $args );

	$labels = [
		'name'                       => _x( 'Price ranges', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Price range', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Price ranges', 'nycjw' ),
		'popular_items'              => __( 'Popular Price ranges', 'nycjw' ),
		'all_items'                  => __( 'All Price ranges', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Price range', 'nycjw' ),
		'update_item'                => __( 'Update Price range', 'nycjw' ),
		'add_new_item'               => __( 'Add New Price range', 'nycjw' ),
		'new_item_name'              => __( 'New Price range Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate locations with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove locations', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used locations', 'nycjw' ),
		'not_found'                  => __( 'No locations found.', 'nycjw' ),
		'menu_name'                  => __( 'Price ranges', 'nycjw' ),
	];

	$args = [
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'search_in'     => true,
		'public'								=> false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'location' ],
	];

	register_taxonomy( 'price-range', ['vendor', 'jeweler-products'], $args );
	// register_taxonomy_for_object_type( 'vendor-location', ['vendor'] );
}

add_action('init', 'register_custom_taxonomies', 0);

function filter_get_terms_orderby( $orderby, $this_query_vars, $this_query_vars_taxonomy ) {
    // make filter magic happen here...
    return 'slug';
};

// add the filter
add_filter( 'get_terms_orderby', 'filter_get_terms_orderby', 10, 3 );

function remove_protected_title_prefix() {
	return '%s';
}

add_filter( 'protected_title_format', 'remove_protected_title_prefix', 10, 3);

function custom_password_form($post) {
  $post = get_post( $post );
  $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
  $output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
  <p>' . __( 'This content is password protected. To view it please enter your password below' ) . '</p>
  <div id="password-form-fields"><input name="post_password" id="' . $label . '" placeholder="password" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr_x( 'Enter', 'post password form' ) . '" /></div></form>
  ';
  return $output;
}

add_filter( 'the_password_form', 'custom_password_form', 10, 3);


function order_acf_event_date_taxonomy( $args, $field )
{
    // run the_content filter on all textarea values
		$args['orderby'] = 'slug';
		//$args['order'] = 'DESC';

    return $args;
}

// acf/load_value - filter for every value load
add_filter('acf/fields/taxonomy/query/name=event_date', 'order_acf_event_date_taxonomy', 10, 3);

function add_acf_columns ( $columns ) {
	return array_merge ( $columns, array (
   'publication' => __ ( 'Publication' )
 	) );
}

add_filter ( 'manage_press_posts_columns', 'add_acf_columns' );

function press_custom_column ( $column, $post_id ) {
	switch ( $column ) {
		case 'publication':
			echo get_post_meta ( $post_id, 'publication', true );
			break;
	}
}
add_action ( 'manage_press_posts_custom_column', 'press_custom_column', 10, 2 );

function get_next_post_id($id) {
	if( get_post_ancestors($id) ) {
		global $post;
		$post = get_post($id);
		setup_postdata( $post );
			if($next_post = get_next_post()) {
				return get_next_post_id($next_post->ID);
			} else {
				return false;
			}
		wp_reset_postdata();
	} else {
		return $id;
	}
}

function get_previous_post_id($id) {
	if( get_post_ancestors($id) ) {
		global $post;
		$post = get_post($id);
		setup_postdata( $post );
			if($next_post = get_previous_post()) {
				return get_previous_post_id($next_post->ID);
			} else {
				return false;
			}
		wp_reset_postdata();
	} else {
		return $id;
	}
}

function acf_load_menu_choices( $field ) {

    // reset choices
    // $field['choices'] = [];

		$menus = get_terms( 'nav_menu' );

		foreach ( $menus as $menu ) {
			if ( !in_array( $menu->name, $field['choices'] ) ) {
				$field['choices'][ $menu->name ] = $menu->name;
			}
		}

    // return the field
    return $field;

}

add_filter('acf/load_field/name=sub_menu', 'acf_load_menu_choices');
add_filter('acf/load_field/name=events_sub_menu', 'acf_load_menu_choices');

// Redirects for various user types

function custom_users_login_redirect( $user_login, WP_User $user ) {
	$role = $user->roles;
	switch ($role[0]) {
		case 'vendor':
			$vendorPage = get_user_meta($user->ID, 'vendor_post');
			$redirect = get_the_permalink($vendorPage[0]).'edit-profile';
			wp_safe_redirect( $redirect );
			exit;
		break;
		case 'event_owner':
			global $wp_rewrite;
			$wp_rewrite->author_base = 'profile';
			$event_owner_url = get_author_posts_url( $user->ID );
			wp_safe_redirect( $event_owner_url );
			exit;
		break;
		default:
			wp_safe_redirect( home_url('wp-admin') );
			exit;
		break;
	}
}

// add the filter
add_action( 'wp_login', 'custom_users_login_redirect', 10, 2 );

function iconic_login_redirect( $redirect, $user ) {
	$redirect_page_id = url_to_postid( $redirect );
	$checkout_page_id = wc_get_page_id( 'checkout' );

	if( $redirect_page_id == $checkout_page_id ) {
		return $redirect;
	}

	return wc_get_page_permalink( 'shop' );
}

if ( class_exists( 'woocommerce' ) ) {
	add_filter( 'woocommerce_login_redirect', 'custom_users_login_redirect', 10, 2 );
}

function edit_user_notification_email( $wp_new_user_notification_email, $user, $blogname ) {
  $user_login = stripslashes($user->user_login);
  $user_email = stripslashes($user->user_email);

  $message  = sprintf(__('New user registration on your blog %s:'), get_option('blogname')) . "\r\n\r\n";
  $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
  $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";

  @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);

	$role = $user->roles;
	$message = '';
	$headers = array('Content-Type: text/html; charset=UTF-8');
	switch ($role[0]) {
		case 'event_owner':
			global $wp_rewrite;
			$wp_rewrite->author_base = 'profile';
			$event_owner_url = get_author_posts_url( $user->ID );
			$message = '<h2>Thank you for creating a login on <a style="color: black;" href="' . wp_login_url() . '"><strong>nycjewelryweek.com</strong></a>.</h2>';
			$message .= '<p>You can set your password with the top link below and begin uploading your information to the NYCJW website.</p>';
			$message .= '<p>Save this email and use the bottom link below to login in at anytime while the website is open for uploads.</p>';
			$message .= '<p><a style="margin-top: 1rem; margin-bottom: 1rem; display: inline-block; text-transform: uppercase; text-decoration: none; color: white; background-color: black; padding: .75rem 1rem; font-weight: bold;" href="' . wp_login_url() . '?action=rp&key=' . get_password_reset_key($user) . '&login=' . $user->user_login . '">Create a password to log in</a></p>';
			$message .= '<p><strong>Save this URL: <a style="color: black;" href="' . wp_login_url() . '">' . wp_login_url() . '</a></strong></p>';
		break;
		default:
			$message  = __('Hi there,') . "<br><br>";
			$message .= 'Welcome to NYC Jewelry week! \r\nLog in ' . '<a href="' . wp_login_url() . '">here</a> with password: ' . $user->user_pass;
		break;
	}
	$wp_new_user_notification_email['headers'] = $headers;
  $wp_new_user_notification_email['message'] = $message;
	$wp_new_user_notification_email['subject'] = 'Welcome to NYC Jewelry Week!';
	return $wp_new_user_notification_email;

}

add_filter( 'wp_new_user_notification_email' , 'edit_user_notification_email', 10, 3 );

function nycjw_new_mail_from_name( $name ) {
	$name = 'NYC Jewelry Week';

	return $name;
}
add_filter( 'wp_mail_from_name', 'nycjw_new_mail_from_name' );

// function custom_login_page() {
//   $new_login_page_url = home_url('login'); // new login page
//   global $pagenow;
//   if( $pagenow == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
//     log_it('logging in');
//     wp_redirect($new_login_page_url);
//     exit;
//   }
// }
//
// if(!is_user_logged_in()){
//  add_action('init','custom_login_page');
// }
//
// // add_filter( 'authenticate', function( $user, $username, $password ) {
// //     // forcefully capture login failed to forcefully open wp_login_failed action,
// //     // so that this event can be captured
// //     if ( empty( $username ) || empty( $password ) ) {
// //         do_action( 'wp_login_failed', $user );
// //     }
// //     return $user;
// // }, 10, 3 );
//
// function my_front_end_login_fail( $username ) {
// 	log_it('failed');
// 	wp_redirect( 'login?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
// 	exit;
// }
//
// add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function my_login_logo() { ?>
  <style type="text/css">
		* {
			box-sizing: border-box;
		}
		.event-login-message {
			text-align: center;
			margin-bottom: 1rem!important;
			display: block;
		}
		#login_error {
			width: 80%;
			margin: 0 auto 1.5rem!important;
		}
		.event-login-message p {
			font-size: 1rem;
		}
		body.login {
			background-color: #FEF200;
			box-sizing: border-box;
			display: flex;
			justify-content: center;
		}
		/* body.login:after {
			content: '';
			position: fixed;
			bottom: -10vh;
			left: -10vh;
			width: 120vh;
			height: 120vh;
			background-image: url(<?php echo get_template_directory_uri(); ?>/images/circle_logo_2019.png);
			background-size: contain;
			background-repeat: no-repeat;
			z-index: -1;
		} */
		/* body.login::after {
			content: '';
	    position: fixed;
	    top: 50%;
	    left: 50%;
	    width: 100vh;
	    height: 100%;
	    background-color: transparent;
	    border-radius: 50%;
	    transform: translate(-50%, -50%) scale(1.5)!important;
	    box-shadow: 0px -200px 0px 300px #FEF200;
		}
		@media only screen and (min-width: 31em)  {
			body.login::after {
		    transform: translate(-50%, -50%) scale(1.5)!important;
			}
		}
		@media only screen and (max-width: 768px) and (orientation: landscape) {
			body.login::after {
		    transform: translate(-50%, -50%) scale(2);
			}
		}
		@media only screen and (min-width: 43em) and (orientation:landscape) {
			body.login::after {
		    transform: translate(-50%, -50%) scale(1.4);
			}
		}
		@media only screen and (min-width: 43em) and (orientation:portrait) {
			body.login::after {
		    transform: translate(-50%, -50%) scale(1.4);
			}
		}
		@media only screen and (min-width: 48em) {
			body.login::after {
		    transform: translate(-50%, -50%) scale(2.2)!important;
			}
		}
		@media only screen and (min-width: 48em) and (orientation:portrait){
			body.login::after {
		    transform: translate(-50%, -50%) scale(1.5)!important;
			}
		} */
		#login {
			padding: 30px 0!important;
			height: 100%;
			display: flex;
			justify-content: center;
			flex-direction: column;
			margin: 0!important;
			width: 40%!important;
		}

		#login form {
			overflow: auto;
		}
		/* #login h1 {
			display: none;
		} */
		#login form, #login p {
			width: 80%;
			max-width: 450px;
			margin: 0 auto!important;
		}
		#login .message {
			margin-bottom: 1rem!important;
			border-left-color: black;
		}
		#login form p {
			width: auto;
		}
		#login form input[type="text"]:focus, #login form input[type="password"]:focus {
			box-shadow: 5px 5px rgba(0,0,0,.2);
			outline: none!important;
			border-color: black;
		}
		#login form #wp-submit {
			border-radius: 0!important;
			background-color: black!important;
			text-shadow: none;
			border: none;
		}
		#login #nav, #login #backtoblog {
			padding: 5px 0!important;
		}
    #login h1 a, .login h1 a {
      background-image: url(<?php echo get_template_directory_uri(); ?>/images/logo.png);
			height: 60px;
			width: 320px;
			background-size: contain;
			background-repeat: no-repeat;
			margin: 0 auto 30px;
    }
		@media only screen and (max-width: 1090px) {
		  #login h1 {
		  	display: block;
			}
			#login {
				width: 320px!important;
			}
			#login form {
				box-shadow: none;
				border: 1px solid black;
				width: 320px;
			}
			#login #registerform {

			}
			body.login {
				justify-content: center;
				background-color: white;
			}
			body.login::after {
				display: none;
			}
		}
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

global $post_ids;

function get_processed_image_ids () {
	global $pagenow;
	if ( $pagenow == 'upload.php') {
		global $wpdb;
		global $post_ids;
		$post_ids = [];
		$args = array(
			'post_type' => 'attachment', 
			// 'meta_key' => 'my_meta_key', 
			'order'  => 'ASC',
			// 'meta_value' => 'my_meta_value', 
			'post_status' => 'inherit',
			'posts_per_page' => -1
		);
		$any_files = new WP_Query( $args );
		$post_ids = [];
		if ( $any_files->have_posts() ):
			// log_it('hi');
			while ( $any_files->have_posts() ) : $any_files->the_post();
			// log_it(get_the_ID());
				$results = $wpdb->get_results($wpdb->prepare('SELECT source_id FROM wp_as3cf_items WHERE source_id = %s', get_the_ID()));
				if ( !count($results) ) {
					array_push($post_ids, get_the_id());
					// log_it(get_the_id());
				}
			endwhile;
		endif;
	}
}

add_action('admin_init', 'get_processed_image_ids');

// The next two functions filter the media queries by only attachments that were not uploaded to S3
// function restrict_media_images_per_post_type($query) {
// 	global $post_ids;
// 	global $pagenow;
// 	if ( $pagenow == 'upload.php') {
// 		if ( isset($query->query['post_type']) && $query->query['post_type'] == 'attachment' ) {
// 			$query->query_vars['post__in'] = $post_ids;
// 		}
// 	}
//     return $query;
// }

// add_action('pre_get_posts', 'restrict_media_images_per_post_type', 10, 1);


// function nycjw_get_attachment_meta () {
// 	$args = array(
// 		'post_type' => 'attachment', 
// 		// 'meta_key' => 'my_meta_key', 
// 		'order'  => 'DESC',
// 		// 'meta_value' => 'my_meta_value', 
// 		'post_status' => 'inherit',
// 		'posts_per_page' => 200 
// 	);
// 	$any_files = new WP_Query( $args );
// 	if($any_files->have_posts()) :
// 		// log_it('hi');
// 		while ( $any_files->have_posts() ) : $any_files->the_post();
// 		// log_it(get_the_ID());
// 			$meta_data = wp_get_attachment_metadata(get_the_ID());
// 			// log_it($meta_data);
// 		endwhile;
// 	endif;
// }

// add_action( 'admin_init', 'nycjw_get_attachment_meta' );