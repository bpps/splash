<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */
$event_id = get_the_ID();
$event_url = '';
$event_length = get_field('event_length', $event_id);
if ( get_field('virtual_event', $event_id) == 'virtual' ) {
	$event_url = 'virtual/';
} else {
	switch ($event_length) {
		case 'single':
			$event_url = 'one-time/' . get_field('event_date', $event_id) . '/';
		break;
		case 'multiple':
			$event_dates = get_field('event_dates', $event_id);
			if ( count($event_dates) > 2 ) {
				$event_cats = get_the_terms( $event_id, 'event-type' );
				$event_url = 'on-going/' . $event_cats[0]->slug . '/';
			} else {
				$event_url = 'one-time/' . get_field('event_dates', $event_id)[0]['date'] . '/';
			}
		break;
		case 'range':
			$event_cats = get_the_terms( $event_id, 'event-type' );
			$event_url = 'on-going/' . $event_cats[0]->slug . '/';
		break;
	}
}
$event_url .= basename(get_the_permalink($event_id));
wp_safe_redirect(home_url('events') . '/' . $event_url);
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			print_r( get_field('event_date'));
			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
