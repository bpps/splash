<?php
/**
 * Block Name: NYCJW Content Block
 *
 */
$title = get_field('title');
$content = get_field('content');
$image = get_field('image');
$image_srcset = $image ? wp_get_attachment_image_srcset( $image['id'], 'medium' ) : null;
$layout = get_field('layout');
$cta = get_field('cta');
$layout_class = 'row-centered flex-column';
switch ( $layout ) {
  case 'left':
    $layout_class = 'row-left';
  break;
  case 'right':
    $layout_class = 'row-right flex-reverse';
  break;
}
if ( get_field('layout_top') ) {
  $layout_class .= ' flex-align-top';
} ?>
<section class="nycjw-content-block nycjw-block py2">
  <div class="content-container row <?php echo $layout_class; ?>">
    <?php
    if ( $image ) { ?>
      <div class="col-6 nycjw-header-image animate animate-from-<?php echo $layout; ?>">
        <img <?php if ( $image_srcset ) { ?>srcset="<?php echo $image_srcset; ?>"<?php } else { ?>src="<?php echo $image['sizes']['medium_large']; ?>"<?php } ?>/>
      </div>
    <?php
    } ?>
    <div class="col-<?php echo $image ? '6' : '8'; ?> animate animate-from-bottom animate-delay-2-5">
      <div class="nycjw-content-wrapper text-justified <?php echo $layout == 'center' ? '' : 'px5'; ?> relative">
        <div class="nycjw-header-content">
          <h2 class="section-title text-centered" style="margin-bottom: 1rem;"><?php echo $title; ?></h2>
          <div class="content">
          <?php
            if ( $content ) {
              echo $content;
            }
            if ( $cta ) {
              $image_suffixes = ['.png', '.jpg', '.jpeg', '.gif', '.tif'];
              $url_is_image = false;
              foreach ($image_suffixes as $suffix) {
                if (strpos($cta['url'], $suffix) !== FALSE) {
                  $url_is_image = true;
                  break;
                }
              } ?>
              <div class="cta-wrapper text-centered">
                <a class="<?php echo is_front_page() ? '' : 'btn'; ?>" target="<?php echo $cta['target']; ?>" href="<?php echo $cta['url']; ?>" <?php if ( $url_is_image ) { ?>download="<?php echo $cta['url']; ?>" title="download zoom background"<?php } ?>>
                  <?php if ( is_front_page() ) { ?>
                    <svg width="136" height="31" viewBox="0 0 136 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M120.527 0L118.243 2.33244L129.697 14.0277H0V17.3362H129.908L118.81 28.651L121.11 31L136 15.7978L120.527 0Z" fill="#FAF013"/>
                    </svg>
                  <?php
                  } else { ?>
                    <span><?php echo $cta['title']; ?></span>
                  <?php
                  } ?>
                </a>
              </div>
            <?php
            } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
