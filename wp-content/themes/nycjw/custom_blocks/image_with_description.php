<?php
/**
 * Block Name: Image with Description Block
 *
 */
?>
<section class="image-description-block">
  <div class="content-container">
    <div class="team-member-container">
      <div class="team-member-content">
        <?php
        if ( $image = get_field('image') ) { ?>
          <div class="member-image-container">
            <div class="member-image bg-centered" style="background-image:url('<?php echo $image['sizes']['small-medium']; ?>')"></div>
          </div>
        <?php
        } ?>
        <div class="member-content">
          <?php
          if ( $title = get_field('title') ) { ?>
            <h3 class="member-name"><?php echo $title; ?></h3>
          <?php
          }
          if ( $content = get_field('content') ) { ?>
            <div class="member-content-text"><?php echo $content; ?></div>
          <?php
          } ?>
        </div>
      </div>
    </div>
  </div>
</section>
