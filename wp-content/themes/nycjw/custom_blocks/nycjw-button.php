<?php
/**
 * Block Name: NYCJW Button Block
 *
 */

$button = get_field('button');
$align = get_field('button_alignment');
$icon = get_field('button_icon');
if ( $button ) { ?>
  <div class="cta-wrapper nycjw-button text-<?php echo $align; if ( $icon ) { echo ' has-icon'; } ?>">
    <a class="btn" target="<?php echo $button['target']; ?>" href="<?php echo $button['url']; ?>">
      <?php
      if ( $icon ) { ?>
        <div class="button-icon <?php echo $icon; ?>-icon"></div>
      <?php
      } ?>
      <span><?php echo $button['title']; ?></span>
    </a>
  </div>
<?php
}
