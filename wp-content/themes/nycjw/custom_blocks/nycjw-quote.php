<?php
/**
 * Block Name: NYCJW Quote Block
 *
 */
$title = get_field('title');
$content = get_field('content');
$author = get_field('author'); ?>
<section class="nycjw-quote-block nycjw-block px5">
  <div class="content-container py1 border-top-yellow border-bottom-yellow">
    <div class="inner-content">
      <?php
      if ( $title ) { ?>
        <h2 class="section-title"><?php echo $title; ?></h2>
      <?php
      }
      if ( $content ) { ?>
        <div class="nycjw-quote-content text-justified">
          <?php echo $content; ?>
        </div>
      <?php
      }
      if ( $author ) { ?>
        <div class="nycjw-quote-author text-right">
          <span class="h2">-<?php echo $author; ?></span>
        </div>
      <?php
      } ?>
    </div>
  </div>
</section>
