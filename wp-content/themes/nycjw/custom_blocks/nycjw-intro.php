<?php
/**
 * Block Name: NYCJW Intro Block
 *
 */
$header = get_field('header');
$sections = get_field('sections'); ?>
<section class="nycjw-grid nycjw-block py2 px5">
  <?php
  if ( $header ) { ?>
    <div class="nycjw-grid-header nycjw-intro-header py1">
      <h1 class="section-title text-centered animate animate-from-top"><?php echo $header; ?></h1>
    </div>
  <?php
  }
  if ( $sections ) {
    foreach ( $sections as $section ) { ?>
      <div class="nycjw-intro-section">
        <?php
        if ( $section['title'] ) { ?>
          <h2><span class="underlined"><?php echo $section['title']; ?></span></h2>
        <?php
        }
        if ( $section['links'] ) { ?>
          <div class="nycjw-intro-links content-container row">
            <?php
            foreach( $section['links'] as $link ) {
              $link_item = [
                'image' => $link['image']['sizes']['small-medium'],
                'title' => $link['link']['title'],
                'link' => $link['link'],
                'varied_pos' => true
              ];
              $col_count = count($section['links']) > 3 ? 3 : 4;
              echo display_grid_item($link_item, $col_count, $section['display_as_buttons']);
            } ?>
          </div>
        <?php
        } ?>
      </div>
    <?php
    }
  } ?>
</section>

<?php
