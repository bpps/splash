<?php
/**
 * Block Name: NYCJW Events
 *
 */ ?>
<section class="section page-events-block">
	<div>
		<div id="root"></div>
		<?php
		$days = get_terms( 'event-day', array(
				'hide_empty' => false,
		));
		$event_days = [];
		foreach( $days as $day ) {
			array_push($event_days, get_field('event_date', $day));
		}
		$event_settings = [
			'is_admin' => false,
			'content' => null,
			'full_schedule_image' => get_field('full_schedule_image', 'option'),
			'selected_event_type' => get_field('starting_event_type'),
			'watch_live_link' => get_field('watch_live_link', 'option')
		];
		$event_cats = get_terms( 'event-type', [
			'hide_empty' => false
		]);
		foreach ( $event_cats as $key => $type ) {
			// if not in hoods array, add it
			if ( $color = get_field('color', $type) ) {
				$event_cats[$key]->color = $color;
			}
			if ( $plural = get_field('plural', $type) ) {
				$event_cats[$key]->plural = $plural;
			}
			if ( $sort_by_date = get_field('sort_by_post_date', $type) ) {
				$event_cats[$key]->sort_by_date = true;
			}
		}
		wp_localize_script( 'nycjw-events-js', 'nycjwEventSettings', $event_settings );
		wp_localize_script( 'nycjw-events-js', 'eventIntros', $event_days );
		wp_localize_script( 'nycjw-events-js', 'eventCats', $event_cats ); ?>
	</div>
</section>
