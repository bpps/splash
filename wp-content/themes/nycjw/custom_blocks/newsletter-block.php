<?php
/**
 * Block Name: Member Preview Block
 *
 */
?>
<section class="newsletter-block newsletter-container">
  <div class="content-container">
    <div class="newsletter-signup">
      <div class="newsletter-signup-text">
        <p>Sign up to receive the latest updates  from NYC Jewelry Week in your inbox.</p>
      </div>
      <form>
        <input class="jw-email" type="email" placeholder="email"/>
        <div class="jw-submit-wrapper">
          <div class="jw-submit-loader">
            <div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
          </div>
          <input class="jw-submit" type="submit" value="sign up"/>
        </div>
      </form>
    </div>
  </div>
</section>
