<?php
/**
 * Block Name: NYCJW Content Block
 *
 */
  $title = get_field('title');
  $block_type = get_field('block_type');
  $post_type = 'discover';
  $tax_term = null;
  $order_by = 'alpha';
  $post_count = 9;
  if (!isset($block_type) || (isset($block_type) && $block_type == 'type1')) {
    $content = get_field('content');
    $cta = get_field('cta');
    $auto = get_field('auto_select');
    $grid_items = $auto ? get_field('grid_items') : get_field('manual_grid_items');
  } else if (isset($block_type) && $block_type == 'type2') {
    $post_count = get_field('post_count') ? get_field('post_count') : 1000;
    $post_type = get_field('post_type');
    $tax_term = get_field('tax_term');
    $order_by = get_field('order_by');
    // if (empty($title)) {
    //   $title = get_post_type_object($post_type)->label;
    // }
  }
?>
<section class="nycjw-grid nycjw-block py2 px5 <?php if (isset($block_type) && $block_type == 'type2') {echo 'alt-design';} ?>" >
  <?php
  if ( $title || $content ) { ?>
    <div class="nycjw-grid-header py1">
      <?php
      if ( $title ) { ?>
        <h2 class="section-title text-centered animate animate-from-top h1"><?php echo $title; ?></h2>
      <?php
      }
      if ( $content !== '' ) { ?>
        <div class="content"><?php echo $content; ?></div>
      <?php
      } ?>
    </div>
  <?php
  } ?>

  <?php
  if (!isset($block_type) || (isset($block_type) && $block_type == 'type1')) { ?>
    <div class="content-container row py2">
      <?php
      foreach ( $grid_items as $item ) {
        $item_obj = [];
        if ( $auto ) { // If auto, populate item_obj
          $item_obj['image'] = get_the_post_thumbnail_url( $item, 'small');
          $item_obj['title'] = get_the_title($item);
          $item_obj['link'] = [ 'url' => get_the_permalink($item), 'target' => '_self' ];
        } else { // If manual
          $item_obj['image'] = $item['image']['sizes']['small'];
          $item_obj['image_srcset'] = wp_get_attachment_image_srcset($item['image']['id'], 'small');
          $item_obj['title'] = $item['title'];
          $item_obj['link'] = $item['link'];
        }
        echo display_grid_item($item_obj);
      } ?>
    </div>

  <?php
  } else if (isset($block_type) && $block_type == 'type2') { ?>
    <div id="filterable-items" class="items-per-row-3">
      <div class="item-grid-wrapper" style="display: flex; flex-wrap: wrap;">
        <?php
        $args = array(
          'post_type' => $post_type,
          'posts_per_page' => -1,
          'fields' => 'ids'
        );
        if ($tax_term) {
          $term_id = explode('|', $tax_term)[1];
          $term_tax = explode('|', $tax_term)[0];
          $args['tax_query'] = [
            [
              'field' => 'term_id',
              'terms' => [$term_id],
              'taxonomy' => $term_tax
            ]
          ];
        }

        $grid_posts = get_posts($args);
        if ( $order_by == 'random' ) {
          shuffle($grid_posts);
        }
        foreach( $grid_posts as $key => $grid_post_id ) {
          if ( $key < $post_count ) {
            $item_obj = [];
            $post_id = $grid_post_id;
            $item_obj['image'] = get_the_post_thumbnail_url( $post_id, 'small');
            $item_obj['title'] = get_the_title($post_id);
            $item_obj['link'] = [ 'url' => get_the_permalink($post_id), 'target' => '_self' ];
            ?>
            <div class="item">
              <div class="item-content">
                <?php
                if ( $gallery = get_field('image_carousel', $post_id) ) { ?>
                  <div class="item-carousel-wrapper">
                    <div class="item-carousel">
              				<?php
                      foreach( $gallery as $image ) {
                        $image_id = is_array($image) ? $image['id'] : $image; ?>
                        <div class="item-image-container item-image">
                          <a target="_blank" href="<?php echo $item_obj['link']['url']; ?>" class="item-image ratio-1">
                            <div data-background-image="<?php echo wp_get_attachment_image_src( $image_id, 'small')[0]; ?>" class="lazy-item">
                            </div>
                          </a>
                        </div>
                      <?php
                      } ?>
                    </div>
                  </div>
                <?php
                } else if ( $item_obj['image'] ) { ?>
                  <div class="image-wrapper">
                    <img src="<?php echo $item_obj['image'];?>" />
                  </div>
                <?php
                } ?>
                <div class="item-info">
                  <div class="item-name">
                    <h3><a href="<?php echo get_the_permalink($post_id);?>" target="_blank"><?php echo $item_obj['title']; ?></a></h3>
                  </div>
                </div>
              </div>
            </div>
          <?php
          } else {
            break;
          }
        } ?>
      </div>
    </div>
  <?php
  }
  if (isset($block_type) && $block_type == 'type2' && $tax_term):
    $more_post_link_text = get_field('more_posts_link_text');
    $term_id = explode('|', $tax_term)[1];
    $term_tax = explode('|', $tax_term)[0];
    $term = get_term($term_id, $term_tax); ?>
    <div class="more-link">
      <a href="<?php echo get_term_link($term); ?>" class="btn" target="_blank">
        <?php echo $more_post_link_text; ?>
      </a>
    </div>
  <?php
  endif; ?>
</section>

<?php
