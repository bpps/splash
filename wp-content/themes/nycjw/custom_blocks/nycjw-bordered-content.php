<?php
/**
 * Block Name: NYCJW Bordered Content Block
 *
 */

$borderColor = get_field('border_color');
$content = get_field('content');
?>

<section class="nycjw-content-block nycjw-block bordered-content py4">
  <div class="content-container border-<?php echo $borderColor; ?>">
    <div class="content">
      <?php
        if ( $content ) {
          echo $content;
        }
      ?>
    </div>
  </div>
</section>
