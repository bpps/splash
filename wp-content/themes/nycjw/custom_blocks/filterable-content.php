<?php
/**
 * Block Name: Filterable Content Block
 *
 */

$post_type = get_field('post_type') ? get_field('post_type') : 'vendor';
$items_per_row = get_field('items_per_row') ? get_field('items_per_row') : 2;
$random_order = get_field('random_order');
$show_filters = get_field('show_filters');
$show_price_range = get_field('show_price_range');
$alt_design = get_field('alt_design');
$view_type = get_field('view_type');
$filter_by_title = get_field('filter_by_title');
$settings = [
  'post_type' => $post_type,
  'items_per_row' => $items_per_row,
  'order_random' => $random_order,
  'show_filters' => $show_filters,
  'show_price_range' => $show_price_range,
  'alt_design' => $alt_design,
  'view_type' => strtolower($view_type),
  'isFilterByTitle' => $filter_by_title
];
if ( $post_type ) {
  if ( $post_type_object = get_post_type_object($post_type) ) {
    $settings['post_type_plural'] = $post_type_object->labels->name;
  }
}?>
<section class="filtered-content<?php echo $alt_design ? ' alt-design' : ''; ?><?php echo ' '.strtolower($view_type).' items-'.$items_per_row;?>">
  <div id="filtered-content-container">
  </div>
  <?php
    if (!function_exists('check_term_posts')):
      function check_term_posts($tax_slug, $term_id, $post_type = false) {
        $args = array(
            'status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => $tax_slug,
                    'field' => 'term_id',
                    'terms' => $term_id
                )
            )
        );
        if ( $post_type ) {
          $args['post_type'] = $post_type;
        }
        $term_query =  new WP_Query($args);
        $term_posts_count = $term_query->found_posts;
        if( $term_posts_count>0 ){
            return true;
        } else {
            return false;
        }
      }
    endif;
  $filterObjs = [];
  if ( $taxes = get_object_taxonomies( $post_type ) ) {
    foreach($taxes as $tax) {
      $filterObj = (object)[];
      $taxonomy = get_taxonomy( $tax );
      $terms = get_terms(['taxonomy' => $tax]);
      $termsObj = [];
      foreach($terms as $term) {
        $slug = $term->slug;
        $name = $term->name;
        if ( function_exists('check_term_posts') && check_term_posts($tax, $term->term_id, $post_type ) ) {
          $termsObj[$slug] = [ 'name' => $name, 'slug' => $slug ];
        }
      }
      $filterObj->tax = $tax;
      $filterObj->name = strtolower($taxonomy->labels->singular_name);
      $filterObj->search = isset($taxonomy->search_in) ? 'search-in' : 'search-start';
      $filterObj->terms = $termsObj;
      $filterObj->selected = null;
      $filterObjs[$tax] = $filterObj;
    }
  }

  wp_localize_script( 'nycjw-filterable-js', 'filterObjs', $filterObjs );
  wp_localize_script( 'nycjw-filterable-js', 'filterSettings', $settings ); ?>
</section>
