<?php
/**
 * Block Name: NYCJW Buttons Block (multiple)
 *
 */

$buttons = get_field('buttons');

if ( $buttons ) { ?>
  <div class="nycjw-buttons">
    <?php
    foreach( $buttons as $button ) { ?>
      <div class="button-wrapper">
        <div class="cta-wrapper nycjw-button">
          <a class="btn" target="<?php echo $button['button_link']['target']; ?>" href="<?php echo $button['button_link']['url']; ?>">
            <span><?php echo $button['button_link']['title']; ?></span>
          </a>
        </div>
      </div>
    <?php
    } ?>
  </div>
<?php
}
