<?php
/**
 * Block Name: NYCJW Header Block
 *
 */
global $post;
?>
<section class="nycjw-page-hero nycjw-block">
  <div class="content-container row">
    <?php
    $image = get_the_post_thumbnail_url( $post->ID, 'medium' );
    $title_field = get_field('header_title');
    $title = $title_field ? $title_field : get_the_title();
    $sponsor_title = get_field('sponsor_title');
    $sponsor_logo = get_field('sponsor_logo');
    $sponsor_link = get_field('sponsor_link'); ?>
    <div class="col-6 animate animate-from-top animate-delay-2-5">
      <div class="nycjw-hero-left text-justified px5 py3 relative">
        <div class="nycjw-hero-content">
          <h1 class="section-title text-centered"><?php echo $title; ?></h1>
          <?php
          if ( $content = get_field('header_content') ) { ?>
            <div class="content">
              <?php echo $content; ?>
            </div>
          <?php
          } ?>
        </div>
        <?php
        if ( $sponsor_logo or $sponsor_title ) { ?>
          <div class="text-centered nycjw-hero-sponsor mt4">
            <?php
            if ( $sponsor_title ) { ?>
              <p><?php echo $sponsor_title; ?></p>
            <?php
            }
            if ( $sponsor_logo ) {
              if ( $sponsor_link ) { ?>
                <a target="<?php echo $sponsor_link['target']; ?>" href="<?php echo $sponsor_link['url']; ?>">
              <?php
              } ?>
                <img src="<?php echo $sponsor_logo['sizes']['small']; ?>"/>
              <?php
              if ( $sponsor_link ) { ?>
                </a>
              <?php
              }
            } ?>
          </div>
        <?php
        } ?>
      </div>
    </div>
    <div class="col-6 nycjw-hero-image animate animate-from-right">
      <img src="<?php echo $image; ?>"/>
    </div>
  </div>
</section>
