<?php
/**
 * Block Name: NYCJW Header Block
 *
 */

$header = get_field('header_text');
$text_align = get_field('header_text_alignment');
$subtext = get_field('header_subtext'); ?>
<section class="nycjw-custom-heading nycjw-block">
  <div class="content-container px5">
    <h2 class="text-<?php echo $text_align; ?>"><?php echo $header ? $header . ' ' : ''; ?><?php echo $subtext ? '<span>' . $subtext. '</span>' : ''; ?></h2>
  </div>
</section>
