<?php
/**
 * Block Name: NYCJW Featured Slideshow
 *
 */
$hide_carousel = get_field('hide_carousel');
if ( !$hide_carousel ) {
  if ( $featured_post_ids = get_field('selected_posts') ) {
    $args = array(
      'post__in' => $featured_post_ids,
      'post_type' => ['event', 'discover-retail', 'virtual-booth', 'emerging-creatives', 'jeweler-products', 'discover', 'page', 'vendor', 'press', 'mentor', 'advisor', 'fc-maker', 'jewelry', 'maker', 'team']
    );

    $featured_posts = get_posts($args); ?>
    <section id="featured-articles">
      <div class="featured_article arrow-prev">
        <svg width="31" height="53" viewBox="0 0 31 53" fill="none">
          <path d="M27.0119 53L31 49.0123L8.00456 25.9909L30.01 4.01601L25.9936 -4.37672e-07L2.36121e-06 25.9909L27.0119 53Z" fill="#FCF250"/>
        </svg>
      </div>
      <div class="article-repeater">
        <?php
        foreach ($featured_posts as $f_post) :
          $post_id = $f_post->ID;
          $post_type = get_post_type($post_id);
          $carousel_images = get_field('image_carousel', $post_id);
          $featured_image = has_post_thumbnail($post_id) ? get_the_post_thumbnail_url( $post_id, 'medium' ) : null;
          if ( !has_post_thumbnail($post_id) && count($carousel_images) > 0 ) {
            $image_id = is_array($carousel_images[0]) ? $carousel_images[0]['id'] : $carousel_images[0];
            $featured_image = wp_get_attachment_image_src( $image_id, 'large')[0];
            if ( !$featured_image ) {
              $featured_image = wp_get_attachment_image_src( $image_id, 'medium')[0];
            }
          } ?>
          <div class="lazy featured-article-wrapper" data-background-image="<?php echo $featured_image; ?>">
            <div class="thumbnail-wrapper">
              <?php
              $terms = get_the_terms($post_id, 'discover-type');
              if ( !$terms->errors ) { ?>
                <span class="category">
                  <?php echo $terms[0]->name; ?>
                </span>
                <?php
              } ?>
              <h2 class="entry-title"><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h2>
            </div>
          </div>
        <?php
        endforeach; ?>
      </div>
      <div class="featured_article arrow-next">
        <svg width="31" height="53" viewBox="0 0 31 53" fill="none">
          <path d="M3.98814 0L0 3.98773L22.9954 27.0091L0.989963 48.984L5.00639 53L31 27.0091L3.98814 0Z" fill="#FCF250"/>
        </svg>
      </div>
    </section>
  <?php
  }
} ?>
