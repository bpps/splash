<?php
/**
 * Block Name: NYCJW Slideshow
 *
 */
if ( $images = get_field('slideshow_images') ) { ?>
  <section class="nycjw-slideshow-block nycjw-block">
    <div class="content-container">
      <div class="nycjw-slideshow">
        <?php foreach( $images as $image ) {
          $image_srcset = wp_get_attachment_image_srcset( $image['id'], 'medium'); ?>
          <div class="nycjw-slide">
            <div class="slide-content bg-centered" style="-webkit-image-set('<?php echo $image_srcset; ?>'); background-image: url(<?php echo $image['sizes']['medium']; ?>);">

            </div>
          </div>
        <?php
        } ?>
      </div>
			<div class="gallery-arrow-prev">
				<i></i>
			</div>
			<div class="gallery-arrow-next">
				<i></i>
			</div>
    </div>
  </section>
<?php
} ?>
