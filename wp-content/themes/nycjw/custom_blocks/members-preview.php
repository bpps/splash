<?php
/**
 * Block Name: Member Preview Block
 *
 */
?>
<section class="members-preview-block">
  <div class="content-container">
    <div>
      <?php
      if ( $content = get_field('members_preview_content') ) {
        echo $content;
      }
      if ( $members = get_field('members') ) { ?>
        <div id="sponsors_list">
          <div class="sponsor_type_list">
            <?php
            if ( $header = get_field('members_preview_header') ) { ?>
              <h2><?php echo $header; ?></h2>
            <?php
            } ?>
            <div class="sponsor_type_sponsors">
              <?php
              $archive_page = get_field('archive_page') ? get_the_permalink(get_field('archive_page')) : home_url();
              foreach($members as $member) { ?>
                <div class="sponsor no-logo">
                  <a href="<?php echo $archive_page . '#' . basename(get_the_permalink($member)); ?>">
                    <p><?php echo get_the_title($member); ?></p>
                  </a>
                </div>
              <?php
              } ?>
            </div>
          </div>
        </div>
      <?php
      } ?>
    </div>
  </div>
</section>
