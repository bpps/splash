<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NYCJW
 */

?>

	</div><!-- #content -->
	<div class="mail-icon">
		<svg width="62" height="38" viewBox="0 0 62 38" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M30.9654 37.8579C21.5068 37.8579 12.0481 37.8579 2.58951 37.8579C1.78894 37.8579 1.0872 37.6407 0.533716 37.0385C0.197673 36.6732 0 36.2289 0 35.7254C0 25.6258 0 15.5261 0 5.4264C0 5.1006 0.138371 4.87353 0.424996 4.72544C0.751156 4.54774 1.0872 4.57736 1.38371 4.79455C1.81859 5.12035 2.24358 5.45602 2.67846 5.78181C5.70285 8.10188 8.73713 10.4121 11.7615 12.7321C14.0348 14.4796 16.308 16.227 18.5812 17.9646C20.8347 19.6923 23.098 21.42 25.3515 23.1379C26.4782 23.9968 27.5951 24.8656 28.7317 25.7146C30.0858 26.7315 31.9735 26.7019 33.3276 25.6751C35.1956 24.2436 37.0537 22.8219 38.9217 21.4003C42.6874 18.5175 46.4531 15.6248 50.2088 12.742C53.0652 10.5602 55.9117 8.36844 58.7681 6.18659C59.3512 5.74232 59.9343 5.28818 60.5175 4.83404C60.8535 4.57736 61.1994 4.51812 61.5849 4.72544C61.8616 4.87353 61.9704 5.12035 61.9901 5.4264C62 5.51525 61.9901 5.60411 61.9901 5.69296C61.9901 15.5754 61.9802 25.4678 62 35.3503C62 37.0089 60.7547 37.848 59.4994 37.848C56.2774 37.848 53.0553 37.848 49.8333 37.848C43.5473 37.8579 37.2613 37.8579 30.9654 37.8579Z" fill="black"/>
			<path d="M4.34885 4.53841C3.26165 3.69923 2.16456 2.86006 1.07737 2.02089C0.494231 1.57662 0.494231 0.846049 1.07737 0.391909C1.65062 -0.0424858 16.3871 0.105603 31.005 0.0957308C45.702 0.0858582 60.2704 -0.190575 60.8338 0.24382C61.5158 0.767068 61.5059 1.55688 60.8239 2.08013C58.9658 3.50178 57.1175 4.92344 55.2594 6.35497C55.2397 6.37471 38.0224 19.4757 32.705 23.3556C31.9439 23.9085 30.6986 23.9381 29.5521 23.4445C24.3533 20.4629 5.09012 5.10114 4.34885 4.53841Z" fill="black"/>
		</svg>
	</div>
	<footer id="colophon" class="site-footer">
		<div id="footer-wrapper">
			<div id="contact-info-wrapper">
				<div id="contact-info">
					<!-- <div class="row-1">
						<a class="volunteer-link" href="<?php echo home_url('shop'); ?>">SHOP</a>
					</div> -->
					<div class="row-2">
						<!-- <a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a> -->
						<?php // echo newsletter_popup(null); ?>
						<div class="newsletter-container">
							<div class="newsletter-signup-text"></div>
							<form class="jw-newsletter-form">
								<input class="jw-email" type="email" placeholder="email"/>
								<div class="jw-submit-wrapper">
									<div class="jw-submit-loader">
										<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
									</div>
									<input class="jw-submit uppercase" type="submit" value="join our mailing list"/>
								</div>
							</form>
						</div>
						<!-- <a class="volunteer-link" href="<?php echo home_url('event-submission'); ?>">SUBMIT AN EVENT</a> -->
						<div class="social-icons">
							<div class="social-icon">
								<a target="_blank" href="http://instagram.com/nycjewelryweek"><img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer-right">
				<span>©<?php echo date('Y'); ?> NYC Jewelry Week</span>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php

if ( $GLOBALS['pagenow'] !== 'wp-login.php' && !is_author() ) { ?>
	<section class="newsletter-container">
		<div class="newsletter-wrapper">
			<div class="newsletter-content">
				<a href="http://" id="close-newsletter">
				</a>
				<div class="newsletter-signup">
					<div class="newsletter-signup-text">
						<?php
						if ( $newsletter_header = get_field('newsletter_header', 'option') ) { ?>
							<h2><span><?php echo $newsletter_header; ?></span></h2>
						<?php
						}
						if ( $newsletter_text = get_field('newsletter_text', 'option') ) {
							echo $newsletter_text;
						} ?>
					</div>
					<form class="jw-newsletter-form">
						<input class="jw-email" type="email" placeholder="email"/>
						<div class="jw-submit-wrapper">
							<div class="jw-submit-loader">
								<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
							</div>
							<input class="jw-submit uppercase" type="submit" value="Join our mailing list"/>
						</div>
					</form>
					<!-- <?php echo do_shortcode('[caldera_form id="CF5c4f5bc748d31"]'); ?> -->
				</div>

			</div>
		</div>
	</section>
<?php
} ?>

<?php wp_footer(); ?>
<script>

jQuery(document).ready(function() {
//   console.log($(".item-grid-wrapper"));
    jQuery("body.page").lazyScrollLoading({
			lazyItemSelector : ".lazy-item",
			onLazyItemFirstVisible : function(e, $lazyItems, $firstVisibleLazyItems) {
				$firstVisibleLazyItems.each(function() {
					let src = jQuery(this).attr("data-background-image");
					jQuery(this).css('background-image', `url(${src})`);
				});
			}
    });
});
</script>
</body>
</html>
