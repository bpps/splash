<?php
/**
 * Template Name: Events Page
 *
 * The template for displaying the events page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div id="events-banner"><img src="<?php echo get_template_directory_uri() . '/images/events-banner.png'; ?>"/></div>
			<section class="section">
				<div class="section-wrapper full-width-page">

					<?php
					while ( have_posts() ) :
						the_post();
						$pw = post_password_required();
						if ( $pw ) {
							$post = get_post( $post );
						  $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID ); ?>
						  <form action="<?php echo esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ); ?>" class="post-password-form" method="post">
						  	<p><?php echo __( 'This content is password protected. To view it please enter your password below' ); ?></p>
						  	<div id="password-form-fields">
									<input name="post_password" id="<?php echo $label; ?>" placeholder="password" type="password" size="20" />
									<input type="submit" name="Submit" value="<?php echo esc_attr_x( 'Enter', 'post password form' ); ?>" />
								</div>
							</form>
						<?php
						} else {
							$event_content = get_the_content(); ?>
							<div id="root"></div>
							<?php
							$days = get_terms( 'event-day', array(
							    'hide_empty' => false,
							));
							$event_days = [];
							foreach( $days as $day ) {
								array_push($event_days, get_field('event_date', $day));
							}
							$event_settings = [
								'is_admin' => current_user_can( 'edit_posts' ),
								'content' => $event_content,
								'full_schedule_image' => get_field('full_schedule_image', 'option'),
								'selected_event_type' => 'scheduled',
								'watch_live_link' => get_field('watch_live_link', 'option')
							];
							$event_cats = get_terms( 'event-type', [
						    'hide_empty' => false
							]);
							foreach ( $event_cats as $key => $type ) {
								// if not in hoods array, add it
								if ( $color = get_field('color', $type) ) {
									$event_cats[$key]->color = $color;
								}
								if ( $plural = get_field('plural', $type) ) {
									$event_cats[$key]->plural = $plural;
								}
								if ( $sort_by_date = get_field('sort_by_post_date', $type) ) {
									$event_cats[$key]->sort_by_date = true;
								}
							}
							wp_localize_script( 'nycjw-events-js', 'nycjwEventSettings', $event_settings );
							wp_localize_script( 'nycjw-events-js', 'eventIntros', $event_days );
							wp_localize_script( 'nycjw-events-js', 'eventCats', $event_cats );
							$sub_menu = get_field('events_sub_menu', 'options') ? get_field('events_sub_menu', 'options') : [];
							wp_localize_script( 'nycjw-events-js', 'eventsSubMenu', wp_get_nav_menu_items($sub_menu) ); 
						}
					endwhile; ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
