<?php
/**
 * Template Name: Coming Soon
 *
 * The template for displaying the events page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper full-width-page coming-soon">
					<div class="inner-content">
						<?php
						while ( have_posts() ) :
							the_post(); ?>
							<h1 class="coming-soon-title"><?php the_title(); ?></h1>
							<div class="coming-soon-body"><?php the_content(); ?></div>
						<?php
						endwhile; ?>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
