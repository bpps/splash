<?php
/**
 * Template Name:  Events Login
 *
 * The template for displaying vendors page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

if( is_user_logged_in() ) {
  $user = wp_get_current_user();
  $role = $user->roles;
  if($role[0] == 'event_owner') {
    $wp_rewrite->author_base = 'profile';
    $event_owner_url = get_author_posts_url( $user->ID );
    wp_safe_redirect( $event_owner_url );
    exit;
  } 
} else {
  wp_safe_redirect(home_url('login'));
}

get_header(); ?>
  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section class="section">
        <div class="section-wrapper">
          <div id="page-header">
            <?php
            $titleWidth = get_title_length(get_the_title()); ?>
            <h1 class="section-title <?php echo $titleWidth; ?>">
              <?php the_title(); ?>
            </h1>
            <hr>
          </div>
          <div id="page-content">
            <div class="row justify-space-between">
              <div class="col-6">
                <?php wp_login_form(); ?>
              </div>
              <div class="col-5">
                <img alt="events image" src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' ); ?>"/>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main><!-- #main -->
  </div><!-- #primary -->
<?php
get_footer();
