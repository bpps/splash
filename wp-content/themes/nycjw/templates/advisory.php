<?php
/**
 * Template Name:  Advisory Page
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
      $args = array(
        'post_type' => 'advisor',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
      );
      $advisors = new WP_Query($args);
      if($advisors->have_posts()) : ?>
        <section class="section">
          <div class="section-wrapper">
						<div id="page-header">
							<?php
							$titleWidth = get_title_length(get_the_title()); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php the_title(); ?>
							</h1>
							<hr>
							<?php
							if ( $menu = get_field('sub_menu') ) { ?>
								<div class="sub-nav">
									<?php echo wp_nav_menu(['menu' => $menu]); ?>
								</div>
							<?php
							} ?>
						</div>
						<div id="page-content">
							<?php
							the_content();
							table_of_contents_query(); ?>
						</div>
            <div id="team-container" class="advisory-committee">
              <?php
              while($advisors->have_posts()):
                $advisors->the_post();
								if(get_the_content()) {
	                $advisorImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium');
									if(!$advisorImage || $advisorImage == '') {
										$advisorImage = get_template_directory_uri().'/images/logo-small.png';
									}
	                $slug = slugify(get_the_title()); ?>
	                <div class="team-member-container" name="<?php echo $slug; ?>">
										<a class="team-member-anchor" name="<?php echo $slug; ?>"></a>
										<div class="team-member-content">
		                  <div class="member-image-container">
												<?php if($advisorImage) { ?>
			                    <div class="member-image bg-centered" style="background-image:url(<?php echo $advisorImage; ?>);">
			                    </div>
												<?php } ?>
		                  </div>
		                  <div class="member-content">
		                    <h3 class="member-name"><?php the_title(); ?></h3>
		                    <?php if($memberTypes = get_the_terms(get_the_ID(), 'advisor-type')) { ?>
		                      <span class="member-title"><?php echo $memberTypes[0]->name; ?></span>
		                    <?php } ?>
												<div class="member-content-text">
													<?php
													$content = get_post_field( 'post_content', get_the_ID() );
													// Get content parts
													$content_parts = get_extended( $content );
													echo apply_filters('the_content', $content_parts['main']);
													if ( $content_parts['extended'] ) { ?>
														<div class="extended-content-wrapper">
															<div class="extended-content-container">
																<div class="extended-content">
																	<?php echo apply_filters('the_content', $content_parts['extended']); ?>
																</div>
															</div>
															<button class="extend-content text-button">read more</button>
														</div>
													<?php
													} ?>
												</div>
		                  </div>
										</div>
	                </div>
	              <?php
								}
              endwhile;
							remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
            </div>
          </div>
        </section>
      <?php
      endif;
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
