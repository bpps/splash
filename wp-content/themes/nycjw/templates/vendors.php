<?php
/**
 * Template Name:  vendors Page
 *
 * The template for displaying vendors page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <?php while ( have_posts() ) : the_post(); ?>
        <section class="section">
          <div class="section-wrapper">
            <div id="page-header">
              <?php
              $titleWidth = get_title_length(get_the_title()); ?>
              <h1 class="section-title <?php echo $titleWidth; ?>">
                <?php the_title(); ?>
              </h1>
              <hr>
              <?php
  						if ( $menu = get_field('sub_menu') ) { ?>
  							<div class="sub-nav">
  								<?php echo wp_nav_menu(['menu' => $menu]); ?>
  							</div>
  						<?php
  						} ?>
            </div>
            <div id="page-content">
              <?php the_content(); ?>
            </div>
          </div>
        </section>
      <?php endwhile; ?>
    </main><!-- #main -->
  </div><!-- #primary -->
<?php
get_footer();
