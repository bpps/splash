<?php
/**
 * Template Name:  Full width page
 *
 * The template for temporarily displayed the new default page layout
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header();
$pw = post_password_required(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
      while ( have_posts() ) :
  			the_post(); ?>
        <section class="section">
  				<div class="section-wrapper full-width-page<?php echo $pw ? ' password-protected' : ''; ?>">
            <div id="page-content">
              <?php the_content();
              // global $post;
    					// $blocks = parse_blocks( $post->post_content );
    					// print_r($blocks); ?>
            </div>
          </div>
  			</section>
      <?php
      endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
