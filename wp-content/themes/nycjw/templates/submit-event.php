<?php
/**
 * Template Name:  Submit Event
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
if( is_user_logged_in() ) {
 	if ( !current_user_can('administrator') ) {
		global $wp_query;
	  $wp_query->set_404();
	  status_header( 404 );
	  get_template_part( 404 ); exit();
 	}
} else {
 	global $wp_query;
 	$wp_query->set_404();
 	status_header( 404 );
 	get_template_part( 404 ); exit();
}
session_start();
acf_form_head();
get_header();
$events_args = array(
	'post_type' => 'event',
	'posts_per_page' => -1,
	'post_status' => ['publish', 'draft']
);
$form_id = isset($_SESSION['form-id']) ? $_SESSION['form-id'] : ''; ?>
	<style>
		.event-form .acf-field[data-name="event_image"] {
			opacity: 1;
			pointer-events: auto;
		}
		.event-form .acf-field[data-name="premium_event_membership"] {
			display: none;
		}
	</style>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section id="event-owner-events" class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length('Events admin'); ?>
						<h1 class="section-title">
							Events admin
						</h1>
						<hr>
					</div>
					<div class="row">
						<div class="col-4">
							<a href="#" class="create-an-event list-create-an-event">
								Create a new event
							</a>
							<?php
							$events_query = new WP_Query($events_args);
							if( $events_query->have_posts() ) : ?>
								<div id="event-owner-events-list">
									<?php
									while( $events_query->have_posts() ): $events_query->the_post(); ?>
										<div class="event-owner-list-item event-list-item-<?php echo get_the_ID(); ?>">
											<div class="row align-center">
												<div class="col-10">
													<a href="#" class="edit-event" data-event-id="<?php echo get_the_ID(); ?>">
														<?php the_title(); ?>
													</a>
												</div>
												<div class="col-2">
													<a href="#" class="edit-event" data-event-id="<?php echo get_the_ID(); ?>">
														<img alt="edit event" src="<?php echo get_template_directory_uri(); ?>/images/edit.png"/>
													</a>
												</div>
											</div>
										</div>
									<?php
									endwhile;
									wp_reset_postdata(); ?>
								</div>
							<?php
							else: ?>
								<p>No Events</p>
								<a href="#" class="create-an-event list-create-an-event">
									Create a new event
								</a>
							<?php
							endif; ?>
						</div>
						<div class="col-8">

							<?php
							$_SESSION['form-id'] = null; ?>
							<div id="new-event-form" class="event-form<?php echo $form_id == 'new' ? ' active' : ''; ?>">
								<?php
								$new_event_form_args = [
									'post_id'		=> 'new_post',
									'id' => 'new-event',
									'new_post'		=> array(
										'post_type'		=> 'event',
										'post_status'	=> 'draft'
									),
									'html_before_fields' => '<h3 class="edit-event-title">New Event</h3>',
									'updated_message' => __("Your event has been submitted", 'acf'),
									'submit_value' => 'Create Event'
								];
								acf_form($new_event_form_args); ?>
							</div>

							<?php
							$events_query = new WP_Query($events_args);
							if( $events_query->have_posts() ) :
								while( $events_query->have_posts() ): $events_query->the_post(); ?>
									<div class="event-owner-event event-form event-<?php echo get_the_ID(); echo $form_id == get_the_ID() ? ' active' : ''; ?>">
										<div class="row edit-event-form">
											<?php
											$update_event_form_args = [
												'post_id'		=> get_the_ID(),
												'id' => 'event-form-' . get_the_ID(),
												'submit_value' => 'Update Event',
												'updated_message' => __("Event Updated", 'acf'),
												'html_before_fields' => '<h3 class="edit-event-title">' . get_the_title() . '</h3>',
												'uploader' => 'basic'
											];
											acf_form($update_event_form_args); ?>
										</div>
									</div>
								<?php
								endwhile;
								wp_reset_postdata();
							else:
							endif; ?>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
	<script>
		jQuery.noConflict();
		(function( $ ) {
			$(function() {
				$('.acf-field[data-name="paid_event"] input[type="checkbox"]').prop('checked', true)
			});
		})(jQuery);
	</script>
<?php
get_footer();
