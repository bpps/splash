<?php
/**
 * Template Name:  Press Page
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
				<section class="section">
					<div class="section-wrapper">
						<div id="page-header">
							<?php
							$titleWidth = get_title_length(get_the_title()); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php the_title(); ?>
							</h1>
							<hr>
							<?php
							if ( $menu = get_field('sub_menu') ) { ?>
								<div class="sub-nav">
									<?php echo wp_nav_menu(['menu' => $menu]); ?>
								</div>
							<?php
							} ?>
						</div>
						<div id="page-content">
							<?php
							if ($press_items = get_field('press') ) { ?>
								<section class="section press-2019">
									<h2 class="custom-header">2019 Press</h2>
									<div class="col-md">
										<?php foreach( $press_items as $press_item ) { ?>
											<div class="col-item">
												<?php echo $press_item['url'] ? '<a class="press-image" target="_blank" href="' . $press_item['url'] . '">' : '<div class="press-image">'; ?>
													<img src="<?php echo $press_item['image']['sizes']['medium']; ?>"/>
												<?php echo $press_item['url'] ? '</a>' : '</div>'; ?>
												<?php
												if ( $press_item['title'] ) { ?>
													<div class="press-title-container">
														<h3><span><?php echo $press_item['title']; ?></span></h3>
													</div>
												<?php
												} ?>
											</div>
										<?php
										} ?>
									</div>
								</section>
							<?php
							}
							if ( $additional_press = get_field('additional_press') ) { ?>
								<section class="section additional-press">
									<h2 class="custom-header">Additional Press</h2>
									<?php
									foreach ( $additional_press as $press ) { ?>
										<div class="press-item">
											<div class="press-item-content">
												<div class="press-content">
													<span><h4><?php echo $press['publication']; ?></h4></span>
													<a target="_blank" href="<?php echo $press['link']; ?>">
														<span><?php echo $press['title']; ?></span>
													</a>
												</div>
											</div>
										</div>
									<?php
									} ?>
								</section>
							<?php
							}
							$args = array(
						    'post_type' => 'press',
						    'posts_per_page' => -1,
								'orderby' => 'menu_order',
								'order' => 'ASC'
						  );
						  $press = new WP_Query($args);
						  if($press->have_posts()) : ?>
						    <div id="press-preview">
									<h2 class="custom-header">2018 Press</h2>
						      <?php
						      while($press->have_posts()): $press->the_post();
						        $logo = get_field('press_logo'); ?>
						        <div class="press-item">
											<div class="press-item-content">
							          <div class="press-logo">
							            <img src="<?php echo $logo['sizes']['small']; ?>"/>
							          </div>
							          <div class="press-content">
							            <span><h4><?php echo get_field('publication'); ?></h4></span>
							            <a target="_blank" href="<?php echo get_field('press_link'); ?>">
							              <span><?php the_title(); ?></span>
							            </a>
							          </div>
											</div>
											<?php if(get_the_content()) { ?>
												<div class="press-additional-content">
													<?php the_content(); ?>
												</div>
											<?php } ?>
						        </div>
						      <?php
						      endwhile;
									wp_reset_postdata(); ?>
						    </div>
							<?php
							endif; ?>
						</div>
					</div>
				</section>
		  <?php

			if ( current_user_can( 'manage_options' ) ) {

			}
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
