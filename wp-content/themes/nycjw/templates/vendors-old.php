<?php
$filterObjs = [];
if($taxes = get_object_taxonomies( 'vendor' )) { ?>
  <div id="vendor-filters">
    <?php
    foreach($taxes as $tax) {
      $filterObj = (object)[];
      $taxonomy = get_taxonomy( $tax );
      $terms = get_terms(['taxonomy' => $tax]);
      ?>
      <div class="vendor-filter filter" data-filter="<?php echo $tax; ?>" class="filter-<?php echo $tax; ?>">
        <div class="filter-header">
          <div class="filter-value">
          </div>
          <div class="filter-clear">
          </div>
          <div class="filter-placeholder">
            <span>Choose a <?php echo strtolower($taxonomy->labels->singular_name); ?></span>
          </div>
          <div class="filter-input">
            <input class="<?php echo isset($taxonomy->search_in) ? 'search-in' : 'search-start'; ?>" />
          </div>
        </div>
        <div class="filter-options">
          <div style="display: none;" class="filter-option-none">
            No Results Found
          </div>
          <?php
          $termsArray = [];
          foreach($terms as $term) {
            $termObj = (object) ['slug' => $term->slug, 'name' => $term->name];
            array_push($termsArray, $termObj); ?>
            <div class="filter-option option-<?php echo $term->slug; ?>" data-value="<?php echo $term->name; ?>" data-slug="<?php echo $term->slug; ?>">
              <?php echo $term->name; ?>
            </div>
          <?php
          }
          $filterObj->tax = $tax;
          $filterObj->name = strtolower($taxonomy->labels->singular_name);
          $filterObj->search = isset($taxonomy->search_in) ? 'search-in' : 'search-start';
          $filterObj->terms = $termsArray;
          $filterObj->selected = null;
          array_push($filterObjs, $filterObj); ?>
        </div>
      </div>
    <?php
    }
    wp_localize_script( 'nycjw-js', 'filterObjs', $filterObjs ); ?>
  </div>
<?php
} ?>
<div id="vendors">
  <div class="vendors-gutter"></div>
  <?php
  $args = array(
    'post_type' => 'vendor',
    'posts_per_page' => 10,
    'paged' => 1,
    'post_parent' => 0,
    'meta_query' => [
      [
        'key'     => 'vendor_active',
        'value'   => true,
        'compare' => '='
      ],
    ],
  );
  if($vendors = get_vendors($args)) {
    echo $vendors->vendors;
  }
  ?>
</div>
<div class="no-vendors" style="display:none; margin-top:2em;">
  Sorry, we have a lot of great vendors but your search may be too specific!
</div>
<a style="display:none" data-more="<?php echo $vendors->more_vendors; ?>" class="more-vendors" href="#" data-count="<?php echo $args['posts_per_page']; ?>" data-page="1"></a>
