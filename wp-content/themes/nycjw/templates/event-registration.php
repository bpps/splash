<?php
/**
 * Template Name: Event Registration Page
 *
 * The template for displaying the events page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 * Built by Xiaofeng
 */
  acf_form_head();
  get_header();

?>


<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper full-width-page">
          <div id="event-registration-wrappe"></div>
          <?php
          $options = [
            'field_groups' => ['group_6101620abd8c7'],
            'post_id' 	   => 'new_user',
            'submit_value' => 'Register',
            'return'       => get_permalink(9769),
          ];
          acf_form($options);
          // $event_form_fields = acf_get_fields('group_60f86fbace62f');
          // $event_fields = [];
          // foreach( $event_form_fields as $field ) {
          //   $field_obj = $field;
          //   foreach( $field['sub_fields'] as $key => $sub_field ) {
          //     if ( $sub_field['type'] == 'taxonomy' ) {
          //       $terms = get_terms( $sub_field['taxonomy'], [
          //         'hide_empty' => false,
          //       ]);
          //       $term_objs = [];
          //       foreach ( $terms as $term ) {
          //         array_push($term_objs, [ 'name' => $term->name, 'id' => $term->term_id ]);
          //       }
          //       $field_obj['sub_fields'][$key]['terms'] = $term_objs;
          //     }
          //   }
          //   array_push($event_fields, $field_obj);
          // }
          // wp_localize_script( 'nycjw-js', 'eventFormFields', $event_fields ); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
