<?php
/**
 * Template Name:  Mentor Archive Page
 *
 * The template for displaying mentors page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
      $args = array(
        'post_type' => 'mentor',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
      );
      $mentors = new WP_Query($args);
      if($mentors->have_posts()) : ?>
        <section class="section">
          <div class="section-wrapper">
						<div id="page-header">
							<?php
							$titleWidth = get_title_length(get_the_title()); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php the_title(); ?>
							</h1>
							<hr>
							<?php
							if ( $menu = get_field('sub_menu') ) { ?>
								<div class="sub-nav">
									<?php echo wp_nav_menu(['menu' => $menu]); ?>
								</div>
							<?php
							} ?>
						</div>
						<div id="page-content">
							<?php
							the_content();
							table_of_contents_query('mentor'); ?>
						</div>
            <div id="team-container" class="advisory-committee">
              <?php
              while($mentors->have_posts()):
                $mentors->the_post();
								if(get_the_content()) {
	                $mentor_image = get_the_post_thumbnail_url(get_the_ID(), 'small-medium');
									if(!$mentor_image || $mentor_image == '') {
										$mentor_image = get_template_directory_uri().'/images/logo-small.png';
									}
	                $slug = basename(get_the_permalink()); ?>
	                <div class="team-member-container" name="<?php echo $slug; ?>">
										<a class="team-member-anchor" name="<?php echo $slug; ?>"></a>
										<div class="team-member-content">
		                  <div class="member-image-container">
												<?php if($mentor_image) { ?>
			                    <div class="member-image bg-centered" style="background-image:url(<?php echo $mentor_image; ?>);">
			                    </div>
												<?php } ?>
		                  </div>
		                  <div class="member-content">
		                    <h3 class="member-name"><?php the_title(); ?></h3>
												<div class="member-content-text">
													<?php
													$content = get_post_field( 'post_content', get_the_ID() );
													// Get content parts
													$content_parts = get_extended( $content );
													echo apply_filters('the_content', $content_parts['main']);
													if ( $content_parts['extended'] ) { ?>
														<div class="extended-content-wrapper">
															<div class="extended-content-container">
																<div class="extended-content">
																	<?php echo apply_filters('the_content', $content_parts['extended']); ?>
																</div>
															</div>
															<button class="extend-content text-button">read more</button>
														</div>
													<?php
													} ?>
												</div>
		                  </div>
										</div>
	                </div>
	              <?php
								}
              endwhile;
							remove_filter( 'posts_orderby' , 'posts_orderby_lastname' ); ?>
            </div>
          </div>
        </section>
      <?php
      endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
