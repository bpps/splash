<?php
/**
 * Template Name:  Vendor Registration
 *
 * The template for displaying vendors page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
wp_safe_redirect(home_url('login'));
get_header(); ?>
  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section class="section">
        <div class="section-wrapper">
          <div id="page-header">
            <?php
            $titleWidth = get_title_length(get_the_title()); ?>
            <h1 class="section-title <?php echo $titleWidth; ?>">
              <?php the_title(); ?>
            </h1>
            <hr>
            <?php
						if ( $menu = get_field('sub_menu') ) { ?>
							<div class="sub-nav">
								<?php echo wp_nav_menu(['menu' => $menu]); ?>
							</div>
						<?php
						} ?>
          </div>
          <div id="page-content">
            <?php echo do_shortcode('[woocommerce_my_account]'); ?>
          </div>
        </div>
      </section>
    </main><!-- #main -->
  </div><!-- #primary -->
<?php
get_footer();
