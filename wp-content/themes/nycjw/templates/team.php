<?php
/**
 * Template Name:  Team Page
 *
 * The template for displaying the team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
						<hr>
						<?php
						if ( $menu = get_field('sub_menu') ) { ?>
							<div class="sub-nav">
								<?php echo wp_nav_menu(['menu' => $menu]); ?>
							</div>
						<?php
						} ?>
					</div>
					<div id="page-content">
						<?php the_content(); ?>
					</div>
					<div id="team-container-list">
						<?php
						$args_terms_id_list = [59,88, 89];
						foreach ($args_terms_id_list as $current_term_id) {
					    // Get term by id (''term_id'') in Categories taxonomy.
							$m_type = get_term_by('id', $current_term_id , 'member-type');
							$args = [
								'post_type' => 'team',
								'posts_per_page' => -1,
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'tax_query' => [
									[
										'taxonomy' => 'member-type',
										'field' => 'slug',
										'terms' => $m_type->slug,
										'operator' => 'IN',
									]
								]
							];
							$team_query = new WP_Query($args);
							if( $team_query->have_posts() ) :
								while( $team_query->have_posts() ): $team_query->the_post();
									$slug = slugify(get_the_title());
									$image1 = get_field('image_1'); ?>
									<div class="team-member-container more-info">
										<a class="team-member-anchor" name="<?php echo $slug; ?>"></a>
										<div class="team-member-content">
											<div class="member-image-container">
												<div class="member-image bg-centered" style="background-image:url(<?php echo isset($image1['sizes']) ? $image1['sizes']['medium'] : ''; ?>);">
												</div>
												<?php
												if( $image2 = get_field('image_2') ) { ?>
													<div class="image-2 bg-centered" style="background-image:url(<?php echo $image2['sizes']['medium']; ?>);">
													</div>
												<?php
												} ?>
											</div>
											<div class="member-content">
												<h3 class="member-name"><?php the_title(); ?></h3>
												<?php
												if( $title = get_field('member_title') ) { ?>
													<span class="member-title"><?php echo $title; ?></span>
												<?php
												} ?>
												<div id="ec-links">
													<?php
													if($website = get_field('website')) { ?>
														<a target="_blank" href="<?php echo $website; ?>">
															<img src="<?php echo get_template_directory_uri(); ?>/images/website.png"/>
														</a>
													<?php
													}
													if($email = get_field('email')) { ?>
														<a href="mailto:<?php echo $email; ?>">
															<img src="<?php echo get_template_directory_uri(); ?>/images/email.png"/>
														</a>
													<?php
													}
													if($insta = get_field('instagram')) { ?>
														<a target="_blank" href="https://www.instagram.com/<?php echo $insta; ?>">
															<img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/>
															<span>@<?php echo $insta; ?></span>
														</a>
													<?php
													}
													?>
												</div>
												<?php
												if( get_the_content() ) { ?>
													<div class="member-content-text">
														<?php the_content(); ?>
													</div>
												<?php
												} ?>
											</div>
										</div>
									</div>
								<?php
								endwhile;
								wp_reset_postdata();
							endif;
						} ?>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
