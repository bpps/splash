<?php
/**
 * Template Name:  About Page
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$pw = post_password_required(); ?>
			<section class="section">
				<div class="section-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
						<hr>
						<div class="sub-nav">
							<?php echo wp_nav_menu(['menu' => 'about-menu']); ?>
						</div>
					</div>
					<div id="page-content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
