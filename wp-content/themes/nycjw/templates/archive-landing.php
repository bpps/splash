<?php
/**
 * Template Name:  Archive Landing
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$pw = post_password_required(); ?>
			<section class="section">
				<div class="section-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
						<hr>
						<?php
						if ( $menu = get_field('sub_menu') ) { ?>
							<div class="sub-nav">
								<?php echo wp_nav_menu(['menu' => $menu]); ?>
							</div>
						<?php
						} ?>
					</div>
					<div id="page-content">
						<?php
						if( $pages = get_field('archive_pages') ) { ?>
							<div class="circle-links flex-grid flex-gutter-medium">
								<?php
								foreach($pages as $page) {
									$image = $page['image'] ? $page['image']['sizes']['medium'] : get_template_directory_uri() . '/images/logo-large.png'; ?>
									<div class="flex-grid-item large-one-third">
										<div class="circle-link-container">
											<div class="circle-link-image">
												<a target="_blank" href="<?php echo $page['link']; ?>">
													<img src="<?php echo $image; ?>"/>
												</a>
											</div>
											<div class="circle-link">
												<a target="_blank" href="<?php echo $page['link']; ?>">
													<?php echo $page['title']; ?>
												</a>
											</div>
										</div>
									</div>
								<?php
								} ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
