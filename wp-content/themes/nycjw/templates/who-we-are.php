<?php
/**
 * Template Name:  Who We Are
 *
 * The template for displaying the team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
						<hr>
						<?php
						if ( $menu = get_field('sub_menu') ) { ?>
							<div class="sub-nav">
								<?php echo wp_nav_menu(['menu' => $menu]); ?>
							</div>
						<?php
						} ?>
					</div>
					<div id="page-content">
						<?php the_content(); ?>
					</div>
					<div id="team-container">
						<?php
						echo get_team_members(); ?>
					</div>
					<?php
					if( $bottom_content = get_field('bottom_content') ) { ?>
						<div id="bottom-content" style="margin-top: 40px;">
							<?php echo $bottom_content; ?>
						</div>
					<?php
					} ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
