<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

?>

<article id="post-<?php the_ID(); ?>" class="post-thumb">
	<div class="thumbnail">
		<?php
			$categories = get_the_category();
			if (sizeof($categories) > 0):
		?>
			<span class="category">
				<?php
					echo $categories[0]->name;
				?>
			</span>
		<?php
			endif;
		?>
		<div class="thumbnail-wrapper">
			<?php nycjw_post_thumbnail(); ?>
		</div>
	</div>
	<header class="entry-header">
		<?php
		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php echo get_the_date(); ?>
			</div><!-- .entry-meta -->
		<?php
		endif;
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
	</header><!-- .entry-header -->
</article><!-- #post-<?php the_ID(); ?> -->
