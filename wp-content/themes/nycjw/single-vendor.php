<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */
$editor = false;
$parent = wp_get_post_parent_id( get_the_ID() );
$editPage = false;
if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$role = $user->roles;
	if($parent != 0) { // Edit page
		$editPage = true;
		$vendorUser = get_users(array('meta_key' => 'vendor_post', 'meta_value' => $parent));
		if( $role[0] == 'administrator' || ( $vendorUser && $vendorUser[0]->ID == $user->ID ) ) {
			$editor = true;
			acf_form_head();
		} else {
			wp_safe_redirect( get_the_permalink($parent) );
		}
	} else { // Vendor Page
		if( get_field('vendor_active') == false ) {
			if( $role[0] == 'administrator' || ( $vendorUser && $vendorUser[0]->ID == $user->ID )) {
				// Allow post to be shown
			} else {
				// Redirect everyone else back to vendors
				$vendorPage = get_pages(array(
			    'meta_key' => '_wp_page_template',
			    'meta_value' => 'vendors.php'
				));
				wp_safe_redirect( get_the_permalink($vendorPage[0]->ID) );
			}
		}
	}
} else if($parent != 0){
	wp_safe_redirect( get_the_permalink($parent) );
}
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post(); ?>
				<section class="section">
					<div class="section-wrapper">
						<?php
						if(!$editor) { ?>
							<a class="rolodex-single-header" href="<?php echo home_url('placement'); ?>">Placement</a></h2>
						<?php
						} ?>
						<div id="page-header">
							<?php
							if($editPage) {
								$title = $parent == 0 ? get_the_title() : get_the_title($parent);
								$titleWidth = get_title_length( $title ); ?>
								<h1 class="section-title <?php echo $titleWidth; ?>">
									<?php echo $title; ?>
								</h1>
								<hr>
							<?php
							}
							if($editor) { ?>
								<div class="vendor-links">
									<a href="<?php echo get_permalink( $parent ); ?>">
										View Listing
									</a>
									<a href="<?php echo wp_logout_url(); ?>">
										Log Out
									</a>
								</div>
							<?php
							} ?>
						</div>
						<div id="page-content">
							<?php
							if($editPage) { ?>
								<div class="vendor-single-page">
									<?php
									acf_form([
										'updated_message' => __("Your edits have been submitted to NYCJW for review!", 'acf'),
										'field_groups' => [1583]
									]); ?>
								</div>
							<?php
							} else { // Main page
								$gallery = get_field('vendor_gallery'); ?>
								<div class="vendor-single-page">
									<div class="vendor-content">
										<div class="vendor-gallery-container">
											<div class="vendor-gallery">
												<?php
												if($image = get_field('vendor_image')) { ?>
													<img src="<?php echo $image['sizes']['small-medium']; ?>"/>
												<?php
												}
												if($gallery) {
													foreach($gallery as $galImg) { ?>
														<img src="<?php echo $galImg['sizes']['medium']; ?>"/>
													<?php
													}
												} ?>
											</div>
											<?php
											if($gallery) { ?>
												<div class="vendor-gallery-arrows">
													<div class="vendor-arrow-left vendor-arrow"></div>
													<div class="vendor-arrow-right vendor-arrow"></div>
												</div>
											<?php
											} ?>
										</div>
										<div class="vendor-info">
											<h1><?php the_title(); ?></h1>
											<?php
											if($locations = get_the_terms( get_the_id(), 'vendor-location' )) { ?>
												<p class="vendor-location"><?php echo $locations[0]->name; ?></p>
											<?php
											}
											$twitter = get_field('vendor_twitter');
											$instagram = get_field('vendor_instagram');
											$facebook = get_field('vendor_facebook');
											if($twitter || $instagram || $facebook) { ?>
												<div class="vendor-social-links flex-grid">
													<?php
													if($twitter) { ?>
														<a target="_blank" href="https://twitter.com/<?php echo $twitter; ?>" class="vendor-social-link">
															<img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"/>
														</a>
													<?php
													}
													if($instagram) { ?>
														<a target="_blank" href="https://instagram.com/<?php echo $instagram; ?>" class="vendor-social-link flex-grid flex-align-center">
															<img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/>
															<span>@<?php echo $instagram; ?></span>
														</a>
													<?php
													}
													if($facebook) { ?>
														<a target="_blank" href="<?php echo $facebook; ?>" class="vendor-social-link">
															<img src="<?php echo get_template_directory_uri(); ?>/images/fb.png"/>
														</a>
													<?php
													} ?>
												</div>
											<?php }
											the_field('vendor_description');
											if($url = get_field('vendor_url')) { ?>
												<p><a target="_blank" href="<?php echo $url; ?>">website</a></p>
											<?php
											}
											if($address = get_field('vendor_address')) { ?>
												<p><?php echo $address; ?></p>
											<?php
											}
											if( $content = get_field('vendor_content') ) { ?>
												<div class="vendor-content-text">
													<?php echo $content; ?>
												</div>
											<?php
											} ?>
										</div>
									</div>
								</div>
							<?php
							} ?>
						</div>
						<?php
						if(!$editor) { ?>
							<div id="post-navigation">
								<?php
								if($prev_post = get_previous_post()) {
									if ( $prev_post_id = get_previous_post_id($prev_post->ID) ) { ?>
										<a class="nycjw-nav-item nycjw-prev-post" href="<?php echo get_the_permalink($prev_post_id); ?>"><?php echo get_the_title($prev_post_id); ?></a>
									<?php
									} else { ?>
										<div></div>
									<?php
									}
								}
								if($next_post = get_next_post()) {
									if ( $next_post_id = get_next_post_id($next_post->ID) ) { ?>
										<a class="nycjw-nav-item nycjw-next-post" href="<?php echo get_the_permalink($next_post_id); ?>"><?php echo get_the_title($next_post_id); ?></a>
									<?php
									}
								} ?>
							</div>
						<?php
						} ?>
					</div>
				</section>
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
