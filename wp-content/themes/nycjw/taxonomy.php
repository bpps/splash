<?php
/**
 * The template for displaying taxonomy term pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
      $post_type = 'disocver';
      $items_per_row = 3;
      $random_order = false;
      $show_filters = false;
      $alt_design = true;
      $view_type = 'grid';
      $show_price_range = false;
      $settings = [
        'post_type' => $post_type,
        'tax' => get_queried_object()->taxonomy,
        'term' => get_queried_object()->slug,
        'term_label' => get_queried_object()->name,
        'items_per_row' => $items_per_row,
        'order_random' => $random_order,
        'show_filters' => $show_filters,
        'show_price_range' => $show_price_range,
        'alt_design' => $alt_design,
        'title_filter_only' => false,
        'isFilterByTitle' => false,
        'view_type' => strtolower($view_type)
      ];
      if ( $post_type ) {
        if ( $post_type_object = get_post_type_object($post_type) ) {
          $settings['post_type_plural'] = $post_type_object->labels->name;
        }
      }?>
      <section class="filtered-content<?php echo $alt_design ? ' alt-design' : ''; ?><?php echo ' '.strtolower($view_type).' items-'.$items_per_row;?>">
        <div id="filtered-content-container">
        </div>
        <?php
          if (!function_exists('check_term_posts')):
            function check_term_posts($tax_slug, $term_id, $post_type = false) {
              $args = array(
                  'status' => 'publish',
                  'tax_query' => array(
                      array(
                          'taxonomy' => $tax_slug,
                          'field' => 'term_id',
                          'terms' => $term_id
                      )
                  )
              );
              if ( $post_type ) {
                $args['post_type'] = $post_type;
              }
              $term_query =  new WP_Query($args);
              $term_posts_count = $term_query->found_posts;
              if( $term_posts_count>0 ){
                  return true;
              } else {
                  return false;
              }
            }
          endif;
        $filterObjs = [];
        if ( $taxes = get_object_taxonomies( $post_type ) ) {
          foreach($taxes as $tax) {
            $filterObj = (object)[];
            $taxonomy = get_taxonomy( $tax );
            $terms = get_terms(['taxonomy' => $tax]);
            $termsObj = [];
            foreach($terms as $term) {
              $slug = $term->slug;
              $name = $term->name;
              if ( function_exists('check_term_posts') && check_term_posts($tax, $term->term_id, $post_type ) ) {
                $termsObj[$slug] = [ 'name' => $name, 'slug' => $slug ];
              }
            }
            $filterObj->tax = $tax;
            $filterObj->name = strtolower($taxonomy->labels->singular_name);
            $filterObj->search = isset($taxonomy->search_in) ? 'search-in' : 'search-start';
            $filterObj->terms = $termsObj;
            $filterObj->selected = null;
            $filterObjs[$tax] = $filterObj;
          }
        }

        wp_localize_script( 'nycjw-filterable-js', 'filterObjs', $filterObjs );
        wp_localize_script( 'nycjw-filterable-js', 'filterSettings', $settings ); ?>
      </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
