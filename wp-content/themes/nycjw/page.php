<?php
/**
 * Template Name:  Temp default page
 *
 * The template for temporarily displayed the new default page layout
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header();
$pw = post_password_required(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
      while ( have_posts() ) :
  			the_post();
				$has_nycjw_block = false;
				if ( has_blocks( $post->post_content ) ) {
				    $blocks = parse_blocks( $post->post_content );
				    foreach($blocks as $block) {
							if (strpos($block['blockName'], 'nycjw') !== false) {
						    $has_nycjw_block = true;
							}
						}
				} ?>
        <section class="section">
  				<div class="section-wrapper full-width-page<?php echo $pw ? ' password-protected' : ''; ?>">
						<div id="page-header">
							<div class="inner-content">
								<?php
								$titleWidth = get_title_length(get_the_title()); ?>
								<h1 class="section-title default">
									<?php the_title(); ?>
								</h1>
								<hr>
								<?php
								if ( $sub_menu = get_field('sub_menu') ) { ?>
									<div class="sub-nav">
										<?php echo wp_nav_menu(['menu' => $sub_menu]); ?>
									</div>
								<?php
								} ?>
							</div>
						</div>
            <div id="page-content" class="<?php echo $has_nycjw_block ? '' : 'inner-content'; ?>">
              <?php the_content();
              // global $post;
    					// $blocks = parse_blocks( $post->post_content );
    					// print_r($blocks); ?>
            </div>
          </div>
  			</section>
      <?php
      endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
