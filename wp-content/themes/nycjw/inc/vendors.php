<?php
function create_vendor_profile( $user_id ) {
  $user_meta = get_userdata($user_id);
  $user_roles = $user_meta->roles;
  if ( $user_roles[0] == 'vendor' ) {
    $vendorArgs = [
      'post_author' => $user_id,
      'post_title' => $user_meta->user_login,
      'post_type' => 'vendor',
      'post_status' => 'publish'
    ];
    if($vendorPost = wp_insert_post( $vendorArgs )) {
      update_field('vendor_post', $vendorPost, 'user_'.$user_id);
      $editVendorArgs = [
        'post_author' => $user_id,
        'post_title' => 'Edit Profile',
        'post_type' => 'vendor',
        'post_parent' => $vendorPost,
        'post_status' => 'publish'
      ];
      wp_insert_post( $editVendorArgs );
    }
  }
}

add_action( 'user_register', 'create_vendor_profile', 10, 1 );

function add_vendor_role() {

  //add the special customer role
  add_role(
    'vendor',
    "Vendor",
    array(
      'read'         => true,
      'delete_posts' => false,
			'upload_files' => true,
    )
  );

}
add_action('admin_init', 'add_vendor_role');

function register_vendor_user( $user_id ) {
	$user = get_user_by( 'id', $user_id );
	if(isset($_REQUEST['vendor-registration'])) {
		$role = 'vendor';
    $user->set_role($role);
	}
}

if(!function_exists('log_it')){
 function log_it( $message ) {
   if( WP_DEBUG === true ){
     if( is_array( $message ) || is_object( $message ) ){
       error_log( print_r( $message, true ) );
     } else {
       error_log( $message );
     }
   }
 }
}

function add_vendor_update_link($actions, $post) {
	$parent = wp_get_post_parent_id($post->ID);
	if ($post->post_type == 'vendor' && $parent != 0) {
		$parentFields = get_fields($parent);
		$postFields = get_fields($post->ID);
		if($parentFields != $postFields) { ?>
			<strong>UPDATE PENDING</strong>
			<?php
			$url = add_query_arg(
	      [
	        'post_id' => $post->ID,
	        'update_vendor' => true,
					'redirect' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
	      ]
	    );
	    $actions['update_vendor'] = '<a href="' . esc_url( $url ) . '">Update Vendor</a>';
		}
	}
	return $actions;
}
add_filter('page_row_actions', 'add_vendor_update_link', 10, 2);

function vendor_bulk_actions($actions){
  $actions['update_vendors'] = 'Update Vendors';
  return $actions;
}

add_filter('bulk_actions-edit-vendor','vendor_bulk_actions');

function handle_vendor_bulk_action($redirect, $doaction, $items) {
	if($doaction == 'update_vendors') {
		update_vendor_posts($items, $redirect);
	}
}

add_filter('handle_bulk_actions-edit-vendor', 'handle_vendor_bulk_action', 10, 3);

function update_vendor_button(){
  if ( isset( $_REQUEST['post'] ) ) {
    $postID = $_REQUEST['post'];
  	$postType = get_post_type($postID);
  	$parentID = wp_get_post_parent_id($postID);
  	if($postType != 'vendor' || $parentID == 0) {
  		return;
  	}
  	$postFields = get_field_objects($postID);
  	$parentFields = get_field_objects($parentID);
  	if($parentFields != $postFields) {
  		$url = add_query_arg(
  			[
  				'post_id' => $_REQUEST['post'],
  				'update_vendor' => true,
  				'redirect' => admin_url()."post.php?post=".$postID."&action=edit&post_type=vendor"
  			]
  		);
  	  $html  = '<a href="'. esc_url( $url ) .'">Update Vendor</a>';
  	  echo $html;
  	}
  }
}

add_action( 'post_submitbox_misc_actions', 'update_vendor_button' );

function options_instructions_example() {
  global $my_admin_page;
  $screen = get_current_screen();

  if ( is_admin() && ($screen->id == 'vendor') ) {
		global $post;
		$postID = $post->ID;
		$parent = wp_get_post_parent_id($postID);
		if($parent != 0) {
			echo '<a style="text-decoration:none;border-radius:3px;color:white;display:inline-block;background-color:#0085ba;font-size:14px;font-weight:500;margin-top:20px;padding:5px 10px;" href="'.get_edit_post_link( $parent ).'">'.get_the_title($parent).'</a>';
		}
  }
}
add_action( 'admin_notices', 'options_instructions_example' );

function update_vendor_post_on_load(){
  if ( isset( $_REQUEST['update_vendor'] ) ) {
		$postID = $_REQUEST['post_id'];
		$redirect = $_REQUEST['redirect'];

		update_vendor_posts( [$postID], $redirect );
    exit;
  }
}

add_action( 'admin_init', 'update_vendor_post_on_load' );

function update_vendor_posts($ids, $redirect = false) {
	foreach($ids as $id) {
		$parentID = wp_get_post_parent_id($id);
		$parentFields = get_field_objects($parentID);
		$postFields = get_field_objects($id);
		if($parentFields != $postFields) {
			update_vendor_post($id, $parentID);
		}
	}
	if( $redirect ) {
		wp_safe_redirect($redirect);
	}
}

function update_vendor_post($fromID, $toID) {
	$fromFields = get_field_objects($fromID);
	foreach($fromFields as $key=>$field) {
		switch ($field['type']) {
			case 'wysiwyg':
			case 'text':
			case 'textarea':
      case 'url':
			case 'true_false':
				update_field($key, $field['value'], $toID);
			break;
			case 'gallery':
				$galIDs = [];
				if($field['value']) {
					foreach($field['value'] as $galImg) {
						array_push($galIDs, $galImg['ID']);
					}
					update_field($key, $galIDs, $toID);
				}
			break;
			case 'image':
				if($key == 'vendor_image') {
          set_post_thumbnail($fromID, $field['value']['ID']);
					set_post_thumbnail($toID, $field['value']['ID']);
				}
				update_field($key, $field['value']['ID'], $toID);
			break;
			case 'taxonomy':
				$taxes = $field['value'];
				if($field['field_type'] != 'radio') {
					$taxIds = [];
					if($taxes) {
						foreach($taxes as $tax) {
							array_push($taxIds, $tax->term_id);
						}
						wp_set_post_terms( $toID, $taxIds, $field['taxonomy'], false );
						update_field($key, $taxIds, $toID);
					}
				} else {
					wp_set_post_terms( $toID, [$taxes[0]->term_id], $field['taxonomy'], false );
					update_field($key, $taxes[0]->term_id, $toID);
				}
			break;
		}
	}
}

function save_vendor_functions($postID) {
	$postType = get_post_type($postID);
  $user = wp_get_current_user();
	$role = $user->roles;
	if($postType == 'vendor' && $role[0] == 'administrator') {
		$parent = wp_get_post_parent_id($postID);
		if( $parent == 0 ) {
			$children = array(
		    'post_parent'   => $postID,
		    'post_type'         => 'vendor',   //you can use also 'any'
		    'post_status' => 'publish',
				'posts_per_page' => 1
		  );
	    $children_query = new WP_Query( $children );
	    if ( $children_query->have_posts() ) :
				$child = false;
	      while ( $children_query->have_posts() ) : $children_query->the_post();
					$child = get_the_ID();
	      endwhile;
				update_vendor_post($postID, $child);
	    endif;
		} else {
			update_vendor_post($postID, $parent);
		}
	}
}

// add_action( 'acf/save_post', 'save_vendor_functions', 20 );

add_filter('wp_dropdown_users', 'add_vendors_to_author_dropdown');

function add_vendors_to_author_dropdown($output) {
  global $post;
  if ( !$post ) return;
	if ( get_post_type($post->ID) == 'vendor' ) {
		$users = get_users('role=vendor');

    $output = "<select id=\"post_author_override\" name=\"post_author_override\" class=\"\">";

    //Leave the admin in the list
    $output .= "<option value=\"1\">Admin</option>";
    foreach($users as $user) {
      $sel = ($post->post_author == $user->ID)?"selected='selected'":'';
      $output .= '<option value="'.$user->ID.'"'.$sel.'>'.$user->user_login.'</option>';
    }
    $output .= "</select>";
	}

  return $output;
}

// Add columns to vendor edit

function add_vendor_columns ( $columns ) {
  return array_merge ( $columns, [
     'vendor_featured' => __ ( 'Featured' ),
     'vendor_active'   => __ ( 'Active' )
    ] );
  }
add_filter ( 'manage_vendor_posts_columns', 'add_vendor_columns' );

function vendor_custom_column ( $column, $post_id ) {
  switch ( $column ) {
    case 'vendor_featured':
      echo get_post_meta ( $post_id, 'vendor_featured', true ) ? '☑' : '';
      break;
    case 'vendor_active':
      echo get_post_meta ( $post_id, 'vendor_active', true ) ? '☑' : '';
    break;
  }
}
add_action ( 'manage_vendor_posts_custom_column', 'vendor_custom_column', 10, 2 );

function my_pre_save_post( $post_id ) {
  if(get_post_type($post_id) == 'vendor') {
    return alert_admin_to_vendor_update( $post_id );
  }

  return $post_id;
}

// add_filter('acf/pre_save_post' , 'my_pre_save_post', 10, 1 );

function alert_admin_to_vendor_update( $post_id ) {
  $parent = wp_get_post_parent_id($post_id);
  $title = get_the_title( $parent );
  $to = 'hello@nycjewelryweek.com';
  $subject = $title.' updated their profile page';
  $headers = "From: hello@nycjewelryweek.com" . "\r\n";
  $headers .= "Reply-To: hello@nycjewelryweek.com" . "\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
  $message = '<html><body>';
  $message .= '<p>'.$title.' just updated their profile. Click the following link to review their changes. To publish the changes, just update the page.</p>';
  $message .= '<a href="'.get_edit_post_link( $post_id ).'">review profile edits</a>';
  $message .= '</body></html>';
  if(mail($to, $subject, $message, $headers)) {
    //log_it($message);
  } else {
    //log_it('error');
  }
  return $post_id;
}

?>
