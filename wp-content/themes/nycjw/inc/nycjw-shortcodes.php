<?php

function show_testimonials( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  if(get_field('quotes', 'option')) { ?>
    <div class="testimonials">
      <div class="testimonials-wrapper">
        <?php
        foreach(get_field('quotes', 'option') as $testimonial) { ?>
          <div class="testimonial">
            <?php if($testimonial['quote']) { ?>
              <div class="quote">
                <?php echo $testimonial['quote']; ?>
              </div>
            <?php } ?>
            <?php if($testimonial['author']) { ?>
              <div class="quote-author">
                <?php echo $testimonial['author']; ?>
              </div>
            <?php } ?>
            <?php if($testimonial['additional_info']) { ?>
              <div class="quote-info">
                <?php echo $testimonial['additional_info']; ?>
              </div>
            <?php } ?>
          </div>
        <?php
        } ?>
      </div>
      <div class="testimonial-arrows">
				<div class="testimonial-arrow-left">
					<i></i>
				</div>
				<div class="testimonial-arrow-right">
					<i></i>
				</div>
			</div>
    </div>
  <?php
  }
  $content = ob_get_clean();
  return $content;
}
add_shortcode( 'testimonials', 'show_testimonials' );

function show_events_map() { ?>
  <div id="nycjw-map">
    <?php
    $args = [
      'post_type' => 'event',
      'posts_per_page' => -1,
    ];
    $events_query = new WP_Query( $args );
    $events_obj = (object)['type' => 'FeatureCollection'];
    $features_array = [];
    if ( $events_query->have_posts() ) :
      while ( $events_query->have_posts() ) : $events_query->the_post();
        if(!get_field('hide_on_map') && !get_field('private_event')) {
          $eventTypes = get_the_terms(get_the_id(), 'event-type');
          if ( $e_loc = get_field('event_location') ) {
            $eLong = $e_loc['lng'];
            $eLat = $e_loc['lat'];
            $eObj = (object)array(
              'type' => 'Feature',
              'geometry' => (object)array(
                'type' => 'Point',
                'coordinates' => array(floatval($eLong), floatval($eLat))
              ),
              'lat' => $eLong,
              'lng' => $eLat,
              'properties' => (object)array(
                'title' => get_the_title(),
                'description' => get_field('location_text'),
                'address' => get_field('event_location'),
                'eid' => get_the_id(),
                'link' => get_permalink(),
                'image' => '',
                //'mapnumber' => '',
                'icon' => '<img src="'.get_template_directory_uri().'/images/icon-single.png"/>',
              )
            );
            if($mapNumber = get_field('map_number')) {
              $eObj->properties->mapnumber = $mapNumber;
            }
            if($eventTypes) {
              if($icon = get_field('icon', $eventTypes[0])) {
                $eObj->properties->image = $icon['sizes']['small'];
              }
            }
            array_push($features_array, $eObj);
          }
        }
      endwhile;
      wp_reset_postdata();
      $events_obj->features = $features_array;
      wp_localize_script( 'nycjw-js', 'eventsObj', json_encode($events_obj) );
    endif; ?>
  </div>
  <section id="event-drawer-wrapper" class="<?php echo isset($_GET['nycjw-event']) ? 'active' : ''; ?>">
    <div id="event-drawer">
      <div id="close-events-drawer-container">
        <div id="close-events-drawer">
        </div>
      </div>
      <div id="event-drawer-content">
        <div id="event-drawer-inner">

        </div>
      </div>
    </div>
  </section>
  <?php
}
add_shortcode( 'map', 'show_events_map');

function show_press( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
    <div class="press-preview">
      <div class="press-gif">
        <img src=""/>
      </div>
      <h1 class="press-preview-title">
        <a href="<?php echo home_url('press'); ?>">NYCJW</br>PRESS</a>
      </h1>
    </div>
  <?php
  $content = ob_get_clean();
  return $content;
}
add_shortcode( 'press', 'show_press' );

function show_team_members( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  $args = array(
    'post_type' => 'team',
    'posts_per_page' => -1,
    'post__in' => [34,35]
  );
  $team_query = new WP_Query($args);
  if($team_query->have_posts()) : ?>
    <div id="team-container">
      <?php
      while($team_query->have_posts()):
        $team_query->the_post();
        $slug = slugify(get_the_title());
        $memberImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium'); ?>
        <div class="team-member-container">
          <a href="<?php echo home_url('/the-team#'.$slug); ?>" class="team-item" data-name="<?php echo $slug; ?>">
            <div class="member-image-container">
              <div class="member-image bg-centered" style="background-image:url(<?php echo $memberImage; ?>);">
              </div>
            </div>
            <div class="member-content">
              <h3 class="member-name"><?php the_title(); ?></h3>
              <?php if($memberTitle = get_field('member_title')) { ?>
                <span class="member-title"><?php echo $memberTitle; ?></span>
              <?php } ?>
            </div>
          </a>
        </div>
      <?php
      endwhile; ?>
    </div>
    <a class="meet-the-team" href="<?php echo home_url('who-we-are'); ?>">The Team</a>
  <?php
  endif;
  return ob_get_clean();
}
add_shortcode( 'the-team', 'show_team_members' );

function show_rolodex( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
  <div id="filtered-content-container">
  </div>
  <?php
  $filterObjs = [];
  if($taxes = get_object_taxonomies( 'vendor' )) {
    foreach($taxes as $tax) {
      $filterObj = (object)[];
      $taxonomy = get_taxonomy( $tax );
      $terms = get_terms(['taxonomy' => $tax]);
      $termsObj = [];
      foreach($terms as $term) {
        $slug = $term->slug;
        $name = $term->name;
        $termsObj[$slug] = [ 'name' => $name, 'slug' => $slug ];
      }
      $filterObj->tax = $tax;
      $filterObj->name = strtolower($taxonomy->labels->singular_name);
      $filterObj->search = isset($taxonomy->search_in) ? 'search-in' : 'search-start';
      $filterObj->terms = $termsObj;
      $filterObj->selected = null;
      $filterObjs[$tax] = $filterObj;
    }
    print_r($filterObjs);
    wp_localize_script( 'nycjw-vendors-js', 'filterObjs', $filterObjs );
  }
  return ob_get_clean();
}
add_shortcode( 'rolodex', 'show_rolodex' );

function show_emerging_creatives( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
  $args = array(
    'post_type' => 'emerging-creatives',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC'
  );
  $team_query = new WP_Query($args);
  if($team_query->have_posts()) : ?>
    <h2 class="emerging-title">Emerging Creatives</h2>
    <p style="margin: 0;">Check back here for updates on participating Emerging Creatives.</p>
    <div id="team-container" class="emerging-creatives-container">
      <div class="team-container team-container-supporting-staff">
        <?php
        while($team_query->have_posts()): $team_query->the_post();
    			$memberImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium'); ?>
    			<div class="team-member-container more-info">
    				<a href="<?php echo get_the_permalink(); ?>">
      				<div class="team-member-content">
      					<div class="member-image-container">
      						<?php if($memberImage) { ?>
      							<div class="member-image bg-centered" style="background-image:url(<?php echo $memberImage; ?>);">
    									<div class="more-info-cover">
    										<span></span>
    									</div>
      							</div>
      						<?php } ?>
      					</div>
      					<div class="member-content">
      						<h3 class="member-name"><?php the_title(); ?></h3>
      						<?php if($show = get_field('show')) { ?>
      							<span class="member-title"><?php echo $show; ?></span>
      						<?php
                  } ?>
      					</div>
      				</div>
    				</a>
    			</div>
        <?php
        endwhile;
        wp_reset_postdata(); ?>
      </div>
    </div>
  <?php
  endif;
  remove_filter( 'posts_orderby' , 'posts_orderby_lastname' );
  return ob_get_clean();
}
add_shortcode( 'emerging-creatives', 'show_emerging_creatives' );

function list_tiers( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  global $post;
  $postID = $post->ID;
  if( isset($atts['page']) ) {
    $sponsors_page = get_page_by_path($atts['page']);
    $postID = $sponsors_page->ID;
  }

  if($sTypes = get_field('sponsor_types', $postID)) { ?>
    <div id="sponsors_list">
      <?php
      foreach($sTypes as $sType) { ?>
        <div class="sponsor_type_list">
          <?php
          if( $sType['sponsor_type'] ) { ?>
            <h2><?php echo $sType['sponsor_type']; ?></h2>
          <?php
          } ?>
          <div class="sponsor_type_sponsors">
            <?php
            foreach($sType['sponsors'] as $sponsor) { ?>
              <div class="sponsor<?php echo $sponsor['logo'] ? ' with-logo' : ' no-logo'; ?>">
                <?php if($sURL = $sponsor['url']) { ?>
                  <a href="<?php echo $sURL; ?>" target="_blank">
                <?php } ?>
                <?php
                if($sImg = $sponsor['logo']) {
                  $imgType = 'landscape';

                  if($sImg['width'] / $sImg['height'] < 1.4) {
                    $imgType = 'portrait';
                  } ?>
                  <img class="<?php echo $imgType; ?>" width="<?php echo $sImg['width']; ?>" height="<?php echo $sImg['height']; ?>" ratio="<?php echo $sImg['width'] / $sImg['height']; ?>" alt="<?php echo $sponsor['name']; ?>" src="<?php echo $sImg['sizes']['small']; ?>"/>
                <?php
                } else { ?>
                  <p><?php echo $sponsor['name']; ?></p>
                <?php
                } ?>
                <?php if($sURL = $sponsor['url']) { ?>
                  </a>
                <?php } ?>
              </div>
            <?php
            } ?>
          </div>
        </div>
      <?php
      } ?>
    </div>
  <?php
  }

  return ob_get_clean();
}
add_shortcode( 'tiered-content', 'list_tiers' );

function newsletter_popup () {
  ob_start(); ?>
    <a class="join-list" target="_blank" href="http://">Join the mailing list</a>
  <?php
  return ob_get_clean();
}

add_shortcode( 'newsletter-popup', 'newsletter_popup');

function table_of_contents_query($post_type = 'advisor', $home = false) {
  add_filter( 'posts_orderby' , 'posts_orderby_lastname' );
  $args = array(
    'post_type' => $post_type,
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC'
  );
  $toc = new WP_Query($args);
  if($toc->have_posts()) : ?>
    <div id="team-index">
      <?php
      while($toc->have_posts()):
        $toc->the_post();
        $slug = basename(get_the_permalink()); ?>
        <div>
          <?php
          $advURL = '#'.$slug;
          if($home) {
            $advURL = home_url('/advisory-committee#'.$slug);
          }
          if(get_the_content()){ ?>
            <a href="<?php echo $advURL; ?>" class="team-item" data-name="<?php echo $slug; ?>">
          <?php
          } ?>
          <span>
            <?php the_title(); ?>
          </span>
          <?php if($memberTypes = get_the_terms(get_the_ID(), 'advisor-type')) { ?>
            <h6 class="member-title"><?php echo $memberTypes[0]->name; ?></h6>
          <?php } ?>
          <?php if(get_the_content()){ ?>
            </a>
          <?php } ?>
        </div>
      <?php
      endwhile; ?>
    </div>

  <?php
  endif;
  remove_filter( 'posts_orderby' , 'posts_orderby_lastname' );
}

function show_advisory_committee( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  table_of_contents_query(true);
  return ob_get_clean();
}

add_shortcode( 'advisory-committee', 'show_advisory_committee' );

function show_additional_links( $atts, $content, $tags ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
  <div class="show-more-links">
    <?php echo $content; ?>
  </div>
  <?php
  return ob_get_clean();
}

add_shortcode( 'additional_links', 'show_additional_links' );

function show_partners( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  global $post;
  $postID = $post->ID;
  $partnerlists = get_field('partner_list', $postID);
  if(count($partnerlists) > 0) { ?>
    <section class="partner-list-container">
      <div class="partner-list-gutter"></div>
      <?php
      foreach($partnerlists as $list) { ?>
        <div class="partner-list">
          <?php
          if($listTitle = $list['list_title']) { ?>
            <h2><?php echo $listTitle; ?></h2>
          <?php
          }
          if($partners = $list['partners']) {
            usort($partners, "sort_partners"); ?>
            <ul class="partner-list-partners">
              <?php
              foreach($partners as $partner) { ?>
                <li>
                  <?php
                  // Create link if link exists
                  if($partner['partner_link']) { ?>
                    <a href="<?php echo $partner['partner_link']; ?>" target="_blank">
                  <?php
                  }
                      echo $partner['partner_name'];
                  // Close link if link exists
                  if($partner['partner_link']) { ?>
                    </a>
                  <?php
                  }
                  ?>
                </li>
              <?php
              }
              ?>
            </ul>
          <?php
          } ?>
        </div>
      <?php
      } ?>
    </section>
  <?php
  }
  return ob_get_clean();
}
add_shortcode( 'partners', 'show_partners' );

function show_events( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  $args = array(
    'post_type' => 'event',
    'posts_per_page' => 4,
    'orderby' => 'rand'
  );
  $events = new WP_Query($args);
  if($events->have_posts()) : ?>
    <div id="events-preview">
      <?php
      while($events->have_posts()):
      $events->the_post();
      $slug = slugify(get_the_title()); ?>
      <div class="event-item">
        <?php
        if($eventtypes = get_the_terms(get_the_id(), 'event-type')){
  				$eventtype = $eventtypes[0];
  				$eventtypeslug = $eventtype->slug;
          $eventtypetitle = $eventtype->name; ?>
          <h6><?php echo $eventtypetitle; ?></h6>
        <?php
  			} ?>
        <div class="event-title">
          <h2><?php the_title(); ?></h2>
          <?php if($location = get_field('location_text')) { ?>
            <span class="event-loc"><?php echo $location; ?></span>
          <?php } ?>
          <?php if($datetext = get_field('alt_date_text')) { ?>
            <span class="event-date"><?php echo $datetext; ?></span>
          <?php } ?>
          <?php if($desc = get_the_content()) { ?>
            <a class="desc-toggle" href="#">
            </a>
          <?php } ?>
        </div>
        <?php if($desc = get_the_content()) { ?>
          <div class="event-desc">
            <?php echo add_filter( 'the_content' , $desc); ?>
          </div>
        <?php } ?>
      </div>
      <?php
      endwhile;
      $eventspage = get_post(); ?>
      <div class="all-events">
        <a href="https://">All Events</a>
      </div>
    </div>
  <?php
  endif;
  return ob_get_clean();
}
add_shortcode( 'events', 'show_events' );

function sort_partners($a, $b)
{
  return strcmp($a['partner_name'], $b['partner_name']);
}

function posts_orderby_lastname ($orderby_statement)
{
  $orderby_statement = "RIGHT(post_title, LOCATE(' ', REVERSE(post_title)) - 1) ASC";
    return $orderby_statement;
}

function getDay($date){
    $dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    $dow_numeric = date('w', $date);
    return $dowMap[$dow_numeric];
}

// function underline_link( $atts ) {
//   $a = shortcode_atts( array(
//     'link' => '',
//     'text' => ''
//   ), $atts );
//   return "<a class='grey-text nar-underline' style='font-size: 1.2em;' target='_blank' href='".$a['link']."'>".$a['text']."</a>";
// }
// add_shortcode( 'underlinelink', 'underline_link' );
