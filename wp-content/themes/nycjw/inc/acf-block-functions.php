<?php

function display_grid_item ($item, $col_count = 4, $button = false) {
  $random = rand(1, 4);
  $direction = 'left';
  switch ( $random ) {
    case 1:
      $direction = 'left';
    break;
    case 2:
      $direction = 'right';
    break;
    case 3:
      $direction = 'top';
    break;
    case 4:
      $direction = 'bottom';
    break;
  }
  ob_start(); ?>
    <div class="col-<?php echo $col_count; ?> grid-item animate animate-from-<?php echo $direction; if ( !$item['image'] ) { echo ' no-image'; } if ( $button ) { echo ' is-button'; }?>">
      <?php
      if ( $button ) { ?>
        <a class="btn" href="<?php echo $item['link']['url']; ?>" target="<?php echo $item['link']['target']; ?>"><?php echo $item['link']['title']; ?></a>
      <?php
      } else { ?>
        <div class="image-square-container">
          <?php
          if ( $item['link'] ) { ?>
            <a href="<?php echo $item['link']['url']; ?>" target="<?php echo $item['link']['target']; ?>">
          <?php
          }
          if ( $item['image'] ) { ?>
            <div class="image-absolute bg-centered" style="background-image:url('<?php echo $item['image']; ?>');">
            </div>
          <?php
          }
          if ( $item['title'] ) {
            $titleWidth = get_title_length($item['title']); ?>
            <div class="grid-item-title text-centered">
              <span class="<?php echo $titleWidth; ?>"><?php echo $item['title']; ?></span>
            </div>
          <?php
          }
          if ( $item['link'] ) { ?>
            </a>
          <?php
          } ?>
        </div>
      <?php
      } ?>
    </div>
  <?php
  return ob_get_clean();
}
