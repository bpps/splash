<?php
if( function_exists('acf_register_block') ) {

	// Members Button Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-button',
			'title'						=> __('NYCJW Button'),
			'description'			=> 'Display a custom yellow button',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'id-alt',
			'keywords'				=> [ 'button' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-button.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Buttons Multiple
	acf_register_block (
		array(
			'name'						=> 'nycjw-buttons',
			'title'						=> __('NYCJW Buttons (Multiple)'),
			'description'			=> 'Display multiple buttons',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'align-right',
			'keywords'				=> [ 'button' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-buttons.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Members Intro Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-intro',
			'title'						=> __('NYCJW Intro'),
			'description'			=> 'Display the main areas of Jewelry Week',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'id-alt',
			'keywords'				=> [ 'intro' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-intro.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Members Events Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-events',
			'title'						=> __('NYCJW Events'),
			'description'			=> 'Display the event listings',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'id-alt',
			'keywords'				=> [ 'events', 'event' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-events.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Members Preview Block
	acf_register_block (
		array(
			'name'						=> 'members-preview',
			'title'						=> __('NYCJW Members Preview'),
			'description'			=> 'Use this for the setup where you list people and click to another page for more info',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'id-alt',
			'keywords'				=> [ 'preview', 'mentor', 'member' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/members-preview.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Filterable Content Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-filterable-content',
			'title'						=> __('NYCJW Filterable Content'),
			'description'			=> 'Use this for the setup where you list people and click to another page for more info',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'screenoptions',
			'keywords'				=> [ 'filter', 'products', 'jewelers' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/filterable-content.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Newsletter Block
	acf_register_block (
		array(
			'name'						=> 'newsletter-signup',
			'title'						=> __('NYCJW Newsletter Signup'),
			'description'			=> null,
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'email-alt',
			'keywords'				=> [ 'newsletter', 'signup', 'subscribe' ],
			'mode' 						=> 'inline',
			'render_template' => 'custom_blocks/newsletter-block.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

  // Profile with description
  acf_register_block (
		array(
			'name'						=> 'image-with-description',
			'title'						=> __('NYCJW Profile'),
			'description'			=> 'Display a profile image a description',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'id',
			'keywords'				=> [ 'description', 'mentor', 'member' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/image_with_description.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Hero
	acf_register_block (
		array(
			'name'						=> 'nycjw-header',
			'title'						=> __('NYCJW Hero'),
			'description'			=> 'This is for the section at the top with the title, content and an image',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'align-left',
			'keywords'				=> [ 'header' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-hero.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// Custom Heading
	acf_register_block (
		array(
			'name'						=> 'nycjw-heading',
			'title'						=> __('NYCJW Custom Heading'),
			'description'			=> 'Use this for headings that are broken up by bold and non-bold text ie. HERE WE ARE development committee',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'editor-textcolor',
			'keywords'				=> [ 'header' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-custom-heading.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Content Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-content',
			'title'						=> __('NYCJW Content Section'),
			'description'			=> 'Display content and/or image with custom alignment and a CTA',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'align-right',
			'keywords'				=> [ 'content' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-content.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Bordered  Block
	acf_register_block (
		array(
			'name'						=> 'nycjw-bordered-content',
			'title'						=> __('NYCJW Bordered Content Section'),
			'description'			=> 'Display content with custom border color',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'align-right',
			'keywords'				=> [ 'content' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-bordered-content.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Grid
	acf_register_block (
		array(
			'name'						=> 'nycjw-grid',
			'title'						=> __('NYCJW Grid'),
			'description'			=> 'Display any post type in a grid with titles below images',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'screenoptions',
			'keywords'				=> [ 'grid' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-grid.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Quote
	acf_register_block (
		array(
			'name'						=> 'nycjw-quote',
			'title'						=> __('NYCJW Quote'),
			'description'			=> 'Display a custom quote block with a title, content and author',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'format-quote',
			'keywords'				=> [ 'quote' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-quote.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Slideshow
	acf_register_block (
		array(
			'name'						=> 'nycjw-slidshow',
			'title'						=> __('NYCJW Slideshow'),
			'description'			=> 'Display slideshow of images',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'format-quote',
			'keywords'				=> [ 'slideshow' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-slideshow.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);

	// NYCJW Featured slideshow
	acf_register_block (
		array(
			'name'						=> 'nycjw-featured-slidshow',
			'title'						=> __('NYCJW Featured Posts Slideshow'),
			'description'			=> 'Display slideshow of posts',
			'category'				=> 'nycjw-blocks',
			'icon'						=> 'format-image',
			'keywords'				=> [ 'slideshow' ],
			'mode' 						=> 'edit',
			'render_template' => 'custom_blocks/nycjw-featured-slideshow.php',
			'supports' 				=> [
				'align' 					=> [ 'full' ],
			],
		)
	);
}

function nycjw_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'nycjw-blocks',
				'title' => __( 'NYCJW Blocks', 'nycjw-blocks' )
			)
		)
	);
}

add_filter( 'block_categories', 'nycjw_block_category', 10, 2);
