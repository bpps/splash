<?php
class EventsExport {

  /**
   * Constructor
   */
  public function __construct() {
    if (isset($_POST['export'])) {
      header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Cache-Control: private", false);
      header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename=\"nycjw_events.csv\";");
      header("Content-Transfer-Encoding: binary");
      $csv = $this->export_all_events();
      echo $csv;
      exit;
    }
  }
  public function log_it ($message) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
  public function export_all_events () {
    $event_form_fields = acf_get_fields('group_5b57491693783');
    // $this->log_it($event_form_fields);
    $csv_header = '';
    // Row 1
    $csv_header .= 'title,';
    // Row 2
    foreach( $event_form_fields as $field ) {
      $csv_header .= $field['label'] . ',';
    }


    //if( count($referrers_obj) > 0 ) {
    $csv_output = '';
    $csv_output .= $csv_header;
    $csv_output .= "\n";

    $args = array(
      'post_type'         => 'event',
      'posts_per_page'      => -1,
      'post_status' => ['publish', 'draft']
     );
    $events = get_posts( $args );
    foreach( $events as $event ) {
      $csv_output .= str_replace(",", "<comma>", get_the_title($event->ID)) . ',';
      foreach( $event_form_fields as $field ) {
        $field_val = get_field($field['key'], $event->ID);
        switch ( $field['type'] ) {
          case 'image':
            $field_val = $field_val ? $field_val['url'] : '';
          break;
          case 'google_map':
            $field_val = $field_val ? str_replace(",", "<comma>", $field_val['address']) : ' ';
          break;
          case 'repeater':
            $field_val = $field_val ? 'multiple' : '';
          break;
          case 'taxonomy':
            $term = $field_val ? get_term_by('id', $field_val, 'event-type') : false;
            // $this->log_it($term);
            $field_val = $term ? $term->name : '';
          break;
          default:
            $field_val = $field_val ? str_replace(",", "<comma>", $field_val) : '';
          break;
        }
        $csv_output .= str_replace(array("\n", "\t", "\r"), '', $field_val) . ',';
      }
      $csv_output .= "\n";
    }
    return $csv_output;
  }
}

add_action( 'wp_dashboard_setup', 'export_nycjw_events' );
function export_nycjw_events_function() {
  echo '<form action="" method="post" enctype="multipart/form-data">';
  echo '<input type="submit" name="export" value="Export Events">';
  echo '</form>';
}

function export_nycjw_events() {
	wp_add_dashboard_widget(
   'export_nycjw_events', // Widget slug.
   'Export NYCJW Events', // Title.
   'export_nycjw_events_function' // Display function.
  );
}

$csvExport = new EventsExport();
?>
