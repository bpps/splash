<?php
function create_event_owner_profile( $user_id ) {
  $user_meta = get_userdata($user_id);
  $user_roles = $user_meta->roles;
  $event_title = get_field('field_5c65cddfa2852', 'user_' . $user_id);
  if ( $user_roles[0] == 'event_owner' ) {
    $event_submission_args = [
      'post_author' => $user_id,
      'post_title' => $user_meta->user_login,
      'post_type' => 'event',
      'post_status' => 'publish'
    ];
    wp_insert_post( $event_submission_args );
  }
}

//add_action( 'user_register', 'create_event_owner_profile', 10, 1 );

function add_event_owner_role() {

  //add the special customer role
  add_role(
    'event_owner',
    "Event Owner",
    array(
      'read'         => true,
      'delete_posts' => true,
			'upload_files' => true,
    )
  );

}
add_action('admin_init', 'add_event_owner_role');

add_filter('wp_dropdown_users', 'add_event_owners_to_author_dropdown');

function add_event_owners_to_author_dropdown($output) {
  global $post;
  if ( !$post ) return;
	if( get_post_type($post->ID) == 'event' || get_post_type($post->ID) == 'discover') {
		$users = get_users('role=event_owner');

    $output = "<select id=\"post_author_override\" name=\"post_author_override\" class=\"\">";

    //Leave the admin in the list
    $output .= "<option value=\"1\">Admin</option>";
    foreach($users as $user) {
      $sel = ($post->post_author == $user->ID)?"selected='selected'":'';
      $output .= '<option value="'.$user->ID.'"'.$sel.'>'.$user->user_login.'</option>';
    }
    $output .= "</select>";
	}

  return $output;
}

add_action('init', 'change_author_permalink');
function change_author_permalink() {
  if ( is_user_logged_in() ) {
    $user = wp_get_current_user();
    if ( in_array( 'event_owner', $user->roles ) ) {
      global $wp_rewrite;
      $author_slug = 'profile'; // change slug name
      $wp_rewrite->author_base = $author_slug;
    }
    if ( in_array( 'administrator', $user->roles ) ) {
      global $wp_rewrite;
      $author_slug = 'profile'; // change slug name
      $wp_rewrite->author_base = $author_slug;
    }
  }
  // $users = get_users([ 'role' => 'event_owner' ]);
  // foreach( $users as $user ) {
  //   update_field('field_6117d70b70724', ['event'], 'user_' . $user->ID);
  // }
}

function wd_post_title_acf_name( $field ) {
  if( is_author() ) { // if on the vendor page
    if ( $author_id = get_query_var( 'author' ) ) {
      $author = get_user_by( 'id', $author_id );
      if ( in_array( 'event_owner', $author->roles ) ) {
        $field['label'] = 'Event Title';
      }
    }
  }
  return $field;
}
// add_filter('acf/load_field/name=_post_title', 'wd_post_title_acf_name');

// Modify ACF Form Label for Post Content Field
function wd_post_content_acf_name( $field ) {
  if( is_author() ) { // if on the vendor page
    if ( $author_id = get_query_var( 'author' ) ) {
      $author = get_user_by( 'id', $author_id );
      if ( in_array( 'event_owner', $author->roles ) ) {
        $field['label'] = 'Description';
      }
    }
  }
  return $field;
}
add_filter('acf/load_field/name=_post_content', 'wd_post_content_acf_name');

function add_edit_event_save( $post_id, $post, $update ) {

  // check if this is to be a new post
  //session_start();
  if ( $update ) {
    $_POST['form-id'] = $post_id;
  } else {
    $_POST['form-id'] = 'new';
  }
}

// add_filter('save_post' , 'add_edit_event_save', 10, 3 );

function set_event_featured_image( $value, $post_id, $field  ){
  if($value != ''){
    //Add the value which is the image ID to the _thumbnail_id meta data for the current post
    add_post_meta($post_id, '_thumbnail_id', $value);
  }
  return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=event_image', 'set_event_featured_image', 10, 3);

function set_event_title( $value, $post_id, $field  ){
  if($value != ''){
    //Add the value which is the image ID to the _thumbnail_id meta data for the current post
    $content = array(
      'ID' => $post_id,
      'post_title' => $value
    );

    wp_update_post($content);
  }
  return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=event_title', 'set_event_title', 10, 3);

function set_event_description( $value, $post_id, $field  ){
  if($value != ''){
    if ( !current_user_can('administrator') ) {
      global $allowedtags;
      $tags = $allowedtags;
      unset($tags['a']);
      $value = addslashes(wp_kses(stripslashes($value), $tags));
    }

    //Add the value which is the image ID to the _thumbnail_id meta data for the current post
    $content = array(
      'ID' => $post_id,
      'post_content' => $value
    );

    wp_update_post($content);
  }
  return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=event_description', 'set_event_description', 10, 3);

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyCePSm9P7bDGnCc9E4t8K9pDLg8mtwPGuw');
}

add_action('acf/init', 'my_acf_init');

class CSVExport {

  /**
   * Constructor
   */
  public function __construct() {
    if (isset($_GET['event_owner_export'])) {
      $csv = $this->generate_csv();
      header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Cache-Control: private", false);
      header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename=\"event_owner_info.csv\";");
      header("Content-Transfer-Encoding: binary");
      echo $csv;
      exit;
    }

// Add extra menu items for admins
    add_action('admin_menu', array($this, 'admin_menu'));

// Create end-points
    add_filter('query_vars', array($this, 'query_vars'));
    add_action('parse_request', array($this, 'parse_request'));
    add_action('parse_json_request', array($this, 'parse_json_request'));
  }

  /**
   * Add extra menu items for admins
   */
  public function admin_menu() {
    add_submenu_page('users.php', 'Export Event Owner Info', 'Export Event Owner Info', 'manage_options', 'export_event_owner_info', array($this, 'export_event_owner_info'));
  }

  /**
   * Allow for custom query variables
   */
  public function query_vars($query_vars) {
    $query_vars[] = 'export_event_owner_info';
    return $query_vars;
  }

  /**
   * Parse the request
   */
  public function parse_request(&$wp) {
    if (array_key_exists('export_event_owner_info', $wp->query_vars)) {
      $this->export_event_owner_info();
      exit;
    }
  }

  /**
   * Download reports
   */
  public function export_event_owner_info() {
    ob_start(); ?>
    <div class="wrap">
      <div id="icon-tools" class="icon32">
      </div>
      <h2>Export Event Owner Info</h2>
      <p><a href="?event_owner_export=export">Export Event Owner Info</a></p>
    </div>
    <?php
    echo ob_get_clean();
  }

  /**
   * Converting data to CSV
   */
  public function generate_csv () {
    $csv_output = $this->get_event_owner_info();

    return $csv_output;
  }

  public function get_event_owner_info () {
    $csv_output = '';
    $args = array(
    	'role'         => 'event_owner',
    	'order'        => 'ASC',
    	'fields'       => 'all',
     );
    $event_owners = get_users( $args );
    $csv_output .= 'date added, email, contact name, business name, office phone, provided email, partner level';
    $csv_output .= "\n";

    foreach( $event_owners as $eo ) {
      $eo_data = get_userdata( $eo->ID );
      $email = $eo_data->user_email;
      $eo_id = $eo->ID;
      $csv_output .= $eo_data->user_registered . ',';
      $csv_output .= $email . ',';
      // Contact name
      $csv_output .= get_field('field_5d5d7c4b3802e', 'user_' . $eo_id) . ',';
      // Business name
      $csv_output .= get_field('field_5d5d7c7b3802f', 'user_' . $eo_id) . ',';
      // Office phone
      $office_phone = get_field('field_5d5d7c9338030', 'user_' . $eo_id) ? get_field('field_5d5d7c9338030', 'user_' . $eo_id) : 'not provided';
      $csv_output .= $office_phone . ',';
      // Provided Email
      $csv_output .= get_field('field_5d5d7cb338031', 'user_' . $eo_id) . ',';
      // Partner level
      $csv_output .= get_field('field_5d5d7cca38032', 'user_' . $eo_id) . ',';

      $csv_output .= "\n";
    }
    return $csv_output;
  }

}

// Instantiate a singleton of this plugin
$csvExport = new CSVExport();

function save_thumbnail_image_as_featured( $post_id ) {

  // get the thumbnail for event
  if ( $thumbnail = get_field('event_image', $post_id) ) {
    set_post_thumbnail($post_id, $thumbnail['ID']);
  }

  // get the thumbnail for virtual event
  if ( $thumbnails = get_field('booth_images', $post_id) ) {
    set_post_thumbnail($post_id, $thumbnails[0]['ID']);
  }

}

// run after ACF saves the $_POST['acf'] data
add_action('acf/save_post', 'save_thumbnail_image_as_featured');

function login_form_username() {
  if ( $username = $_GET['event_owner_login'] ) {
    global $user_login;
    return $user_login = $username;
  }
}

function check_event_register_access_code( $user_login, $user_email, $errors ) {
  $access_code = $_POST['acf']['field_610185f799e19'];
  $access_codes = ['evt2022*nycjw*', 'discovernycjw*2022*', 'retailnycjw*2022*'];
  if ( $access_code == null || !in_array(strtolower($access_code), $access_codes) ) {
    $errors->add( 'bad_email_domain', 'Please contact NYCJW at <a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a> to receive the correct access code' );
  }
}
add_action( 'register_post', 'check_event_register_access_code', 10, 3 );

// add_action( 'login_head', 'login_form_username' );

function event_login_message( $message ) {
  if ( $username = $_GET['event_owner_login'] ) {
    $message = '<div class="event-login-message"><p>You are already registered as an event owner.</p><p>Please login to create events</p></div>';
  }
  return $message;
}

// add_filter( 'login_message', 'event_login_message' );

// function event_owner_register_fail ($email) {
//   wp_safe_redirect(home_url('wp-login.php') . '?event_owner_login=' . $email );
//   exit();
// }

function event_owner_register_pre_save( $post_id ) {
    // check if this is to be a new post
  $post_type = get_post_type($post_id);

  if ( $post_type != 'event' ) {
    return $post_id;
  }

  if ( $supporting_events = get_field('supporting_events', $post_id) ) {
    foreach( $supporting_events as $event ) {
      $event_title = $event['supporting_event_name'];
      $args = ['post_type' => 'event', 'post_parent' => $post_id, 'post_title' => $event_title];
      if ( $event_id = wp_insert_post($args) ) {
        wp_update_post(
          [
            'ID' => $event_id,
            'post_parent' => $post_id
          ]
        );
        if ( $event['date'] ) {
          $date = [
            [
              'field_6101afb0bb2fb' => $event['date'],
              'field_6101b049bb2fc' => $event['start_time'],
              'field_6101b065bb2fd' => $event['end_time'],
            ]
          ];
          update_field('field_6101af9dbb2fa', $date, $event_id);
        }
        if ( $event['event_category'] ) {
          update_field('field_6101ba4c3816c', $event['event_category']->term_id, $event_id);
        }
      }
    }
  }

  return $post_id;
}

// add_filter('acf/pre_save_post' , 'event_owner_register_pre_save');

//create a hidden field for role
function add_hidden_role_field(){
    if (isset($_GET['role'])){
        echo '<input id="user_email" type="hidden" tabindex="20" size="25" value="'.$_GET['role'].'" name="role"/>';
    }
}

// add_action('register_form','add_hidden_role_field');

//save the the role
function update_role($user_id) {
  $user = get_user_by('id', $user_id);
  $access_code = $_POST['acf']['field_610185f799e19'] ? strtolower($_POST['acf']['field_610185f799e19']) : null;
  if ( $access_code ) {
    $access = [];
    switch ($access_code) {
      case 'discovernycjw*2022*':
        array_push($access, 'discover');
      break;
      case 'evt2022*nycjw*':
        array_push($access, 'event');
      break;
      case 'retailnycjw*2022*':
        array_push($access, 'discover-retail');
      break;
    }
    update_field('field_6117d70b70724', $access, 'user_' . $user_id);
    $user->show_admin_bar_front = false;
    wp_update_user($user);
    wp_set_password( $user->user_pass, $user_id );
  }
}

add_action('user_register', 'update_role', 10, 1);

function my_acf_prepare_field( $field ) {
  if ( is_author() ) {
      $field['label'] = "Program Title (50 Characters Max)";
      $field['maxlength'] = 50;
      if ( (isset($_GET['page']) && $_GET['page'] == 'discover') || isset($_GET['edit_discover']) ) {
        $field['label'] = "Discover Event Title (50 Characters Max)";
        $field['instructions'] = "The name of your brand, studio, company, institution, business, or practice. This should reflect how you wish to be recognized publicly.";
      }
      if ( isset($_GET['page']) && $_GET['page'] == 'retail' || isset($_GET['edit_retail']) ) {
        $field['label'] = "Store Title (50 Characters Max)";
        $field['instructions'] = "The name of your brand, studio, company, institution, business, or practice. This should reflect how you wish to be recognized publicly.";
      }
   }
   return $field;

}
add_filter('acf/prepare_field/name=_post_title', 'my_acf_prepare_field');

?>
