<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package NYCJW
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function nycjw_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'nycjw_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function nycjw_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'nycjw_pingback_header' );

function display_jw_logo() { ?>
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 391.9 261.5" style="enable-background:new 0 0 391.9 261.5;" xml:space="preserve">
		<style type="text/css">
			.st0{stroke:#000000;stroke-miterlimit:10;}
		</style>
		<g>
			<path class="st0" d="M78.3,72.3v51.5L0.5,12V3.1h29.6L78.3,72.3z"/>
			<path class="st0" d="M145.7,64.9V122h-28.5v-52L85.1,3.1h31.3L145.7,64.9z"/>
			<path class="st0" d="M171.2,62.2c0-4.6,0.5-9.3,1.5-14c1-4.7,2.5-9.2,4.4-13.6c1.9-4.4,4.3-8.5,7.1-12.3c2.8-3.8,6.1-7.3,9.7-10.3
				c3.7-3,7.7-5.5,12.1-7.6c4.4-2,9.1-3.3,14.2-3.9c-7.6,2.5-13,9.9-16.1,22.1c-0.8,3.2-1.4,6.6-1.8,10.1c-0.4,3.5-0.7,6.9-0.9,10.4
				c-0.2,3.4-0.3,6.7-0.3,9.9c0,3.2,0,6.1,0,8.7v0.8c0,9.5,0.4,17.8,1.1,25.1c0.7,7.2,1.8,13.4,3.3,18.5c1.5,5.1,3.3,9.2,5.6,12.2
				c2.3,3.1,4.9,5.2,8,6.3c-6.8-0.8-13.1-3-18.9-6.5c-5.8-3.5-10.9-8-15.2-13.5c-4.3-5.5-7.6-11.9-10.1-19.1
				C172.4,78.2,171.2,70.5,171.2,62.2z M235.1,27.2c0-2,0.4-3.8,1.1-5.6c0.7-1.7,1.7-3.2,3-4.5c1.3-1.3,2.8-2.3,4.5-3
				c1.7-0.7,3.6-1.1,5.5-1.1c2,0,3.8,0.4,5.6,1.1c1.7,0.7,3.2,1.7,4.5,3c1.3,1.3,2.3,2.8,3,4.5c0.7,1.7,1.1,3.6,1.1,5.6
				c0,1.9-0.4,3.8-1.1,5.5c-0.7,1.7-1.7,3.2-3,4.5c-1.3,1.3-2.8,2.3-4.5,3c-1.7,0.7-3.6,1.1-5.6,1.1c-1.9,0-3.8-0.4-5.5-1.1
				c-1.7-0.7-3.2-1.7-4.5-3c-1.3-1.3-2.3-2.8-3-4.5C235.5,30.9,235.1,29.1,235.1,27.2z"/>
		</g>
		<g>
			<g>
				<path class="st0" d="M1,240.9c0-1.9,0.4-3.7,1.1-5.4c0.7-1.7,1.7-3.2,3-4.5c1.3-1.3,2.7-2.3,4.4-3c1.7-0.7,3.5-1.1,5.4-1.1
					c2,0,3.8,0.4,5.5,1.1c1.7,0.7,3.2,1.7,4.4,3c1.3,1.3,2.3,2.8,3,4.5c0.7,1.7,1.1,3.5,1.1,5.4c0,2-0.4,3.8-1.1,5.5
					c-0.7,1.7-1.7,3.2-3,4.4c-1.3,1.3-2.7,2.3-4.4,3c-1.7,0.7-3.5,1.1-5.5,1.1c-1.9,0-3.7-0.4-5.4-1.1c-1.7-0.7-3.2-1.7-4.4-3
					c-1.3-1.3-2.3-2.7-3-4.4C1.3,244.7,1,242.8,1,240.9z M66.4,223.7c0,5.6-0.9,10.6-2.8,15c-1.9,4.4-4.5,8.2-8,11.3
					c-3.4,3.1-7.6,5.5-12.4,7.2c-4.9,1.7-10.3,2.5-16.2,2.5h-0.6c3.8-0.5,6.7-2.3,8.8-5.4c2-3.1,3-7.4,3-12.9V139h28.3V223.7z"/>
				<path class="st0" d="M128.4,259.9L83.2,139h31l28,74.4L128.4,259.9z M184.3,259.9l-45.2-121h31l27.9,74.5L184.3,259.9z"/>
			</g>
			<g>
				<path d="M282.1,168.2c0,23.6-16.5,31.4-40.5,40.3c-19.5,7.2-39.2,5.9-41.4,40.3c5.2-17.6,15-23.6,26.7-23.6
					c17.8,0,24.5,13.1,39.3,13.1c8.6,0,17.8-4,17.8-16.9v-0.7h0.3v2.1c0,19-10.2,36.7-27.8,36.7c-23.4,0-26.2-17.6-42.2-17.1
					c-8.6,0.3-14.3,6-14.3,14.6h-0.3v-2.6c0-32.7,13.6-36.7,33.3-51.5c18.1-13.6,20-25,20-38.6c0-18.1-6.2-28.4-19.8-28.4
					c-15,0-30,9.3-31.4,24.6c1.7-6.2,7.4-10.9,14.1-10.9c8.1,0,14.6,6.5,14.6,14.6s-6.6,14.7-14.6,14.7c-9.7,0-14.6-7.6-14.6-15.9
					c0-16.7,15.3-29.1,39.3-29.1C263.1,134.1,282.1,150.1,282.1,168.2z"/>
				<path d="M338.6,259.5c-25,0-53.3-28.4-53.3-62.7s28.3-62.7,53.3-62.7c25,0,53.3,28.4,53.3,62.7S363.6,259.5,338.6,259.5z
					 M338.6,134.4c-16.7,0-23.1,13.8-23.1,62.4c0,48.6,6.4,62.4,23.1,62.4c16.7,0,23.1-13.8,23.1-62.4
					C361.7,148.2,355.4,134.4,338.6,134.4z"/>
			</g>
		</g>
		<g>
			<path d="M286,1h-2v15.9L273.8,2.2v14.4h2v0h-4v0h2V1h-2v0h5.9L284,10V1h-2V1L286,1L286,1z"/>
			<path d="M299.3,16.7h-12.7v0h2V1h-2v0h11.8v5.7h0c0,0-0.7-5.6-5.3-5.6h-0.8v7.6h0.3c2.9,0,3.2-3.7,3.2-3.7h0v7.5h0
				c0,0-0.4-3.7-3.2-3.7h-0.3v8h0.8c5.6,0,6.1-6.1,6.1-6.1h0V16.7z"/>
			<path d="M315.6,1h-1.9l3.7,9.8l2.9-9.8h-2.1v0h4v0h-1.8l-4.8,16.1L311.4,6l-3.3,11.1l-6-16.1h-1.8v0h7.3v0h-1.4l3.7,9.8l1.4-4.9
				L309.5,1h-1.3v0L315.6,1L315.6,1z"/>
			<path d="M286.8,22.2h-1.8l-3.5,8.2v7.5h2v0h-7.7v0h2V31l-4.2-8.8h-1.9v0h8.3v0h-2.2l3.8,8.1l3.5-8.1h-2.3v0L286.8,22.2L286.8,22.2z
				"/>
			<path d="M293.9,38.2c-3.7,0-7.7-3.6-7.7-8.2s4-8.2,7.7-8.2s7.7,3.6,7.7,8.2S297.6,38.2,293.9,38.2z M293.9,21.8
				c-2.6,0-3.7,1.8-3.7,8.2s1.1,8.2,3.7,8.2c2.6,0,3.7-1.8,3.7-8.2S296.5,21.8,293.9,21.8z"/>
			<path d="M318.8,37.2c-0.4,0.8-0.9,1-1.9,1c-2.2,0-4.2-0.9-4.2-4v-1.5c0-2.2-1.3-3-3.3-3h-1.1v8.1h2v0h-7.7v0h2V22.2h-2v0h7.1
				c3.6,0,5.9,1.6,5.9,3.9c0,1.9-1.7,3.5-4.2,3.6c2.3,0.1,5.1,1.3,5.1,4.3v2c0,1.7,0.5,2,1.1,2C318.2,38,318.5,37.7,318.8,37.2
				L318.8,37.2z M309.2,22.2h-0.8v7.5h1.1c1.8,0,2.7-0.4,2.7-3.6C312.1,23.3,311.2,22.2,309.2,22.2z"/>
			<path d="M336,37.9h-8.5v0h1.9l-4.7-7.4v7.4h2v0h-7.7v0h2V22.2h-2v0h7.7v0h-2v8.2l6.4-8.2h-2v0h4.1v0h-2l-3.9,5l6.8,10.7L336,37.9
				L336,37.9z"/>
			<path d="M284.1,45.7c0.1,0.3,0.1,0.5,0.1,0.7c0,1.1-0.7,2.1-1.9,2.1c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9
				c0.9,0,1.6,0.6,1.8,1.4c-0.2-1.3-1.8-3.1-4.7-3.1c-3.4,0-3.3,5.4-3.3,8.2c0,6.4,1.3,8.2,3.1,8.2c2.5,0,4.6-1.8,5.1-5.2h0
				c-0.4,2.8-2.2,5.3-5.2,5.3c-3.9,0-7.1-3.5-7.1-8.2c0-4,2.8-8.2,7.3-8.2C282.1,42.9,283.6,44.4,284.1,45.7z"/>
			<path d="M290.5,58.9h2v0h-7.7v0h2V43.3h-2v0h7.7v0h-2V58.9z"/>
			<path d="M305.8,43.3v6.2h-0.1c0-2.4-1.1-5.9-4.3-6.2v15.6h2v0h-7.7v0h2V43.3c-3.1,0.3-4.3,3.8-4.3,6.2h-0.1v-6.2H305.8z"/>
			<path d="M321.7,43.3H320l-3.5,8.2v7.5h2v0h-7.7v0h2v-6.8l-4.2-8.8h-1.9v0h8.3v0h-2.2l3.8,8.1l3.5-8.1h-2.3v0L321.7,43.3L321.7,43.3
				z"/>
			<path d="M272.8,80c-0.8-0.4-1.3-1.1-1.3-1.9c0-1.2,0.8-2.1,1.9-2.1c1.1,0,1.9,0.9,1.9,1.9c0,1.1-0.9,1.9-1.9,1.9
				c-0.9,0-1.6-0.6-1.8-1.4c0.2,1.2,1.4,1.9,3,1.9c1.2,0,1.8-0.8,1.8-2.4V64.4h-2v0h7.7v0h-2v11.2c0,3-2,4.8-5.2,4.8
				C274.1,80.5,273.3,80.3,272.8,80z"/>
			<path d="M295.2,80.1h-12.7v0h2V64.4h-2v0h11.8v5.7h0c0,0-0.7-5.6-5.3-5.6h-0.8V72h0.3c2.9,0,3.2-3.7,3.2-3.7h0v7.5h0
				c0,0-0.4-3.7-3.2-3.7h-0.3v8h0.8c5.6,0,6.1-6.1,6.1-6.1h0V80.1z"/>
			<path d="M311.5,64.4h-1.9l3.7,9.8l2.9-9.8H314v0h4v0h-1.8l-4.8,16.1l-4.1-11.1L304,80.5l-6-16.1h-1.8v0h7.3v0h-1.4l3.7,9.8l1.4-4.9
				l-1.8-4.9h-1.3v0L311.5,64.4L311.5,64.4z"/>
			<path d="M331.2,80.1h-12.7v0h2V64.4h-2v0h11.8v5.7h0c0,0-0.7-5.6-5.3-5.6h-0.8V72h0.3c2.9,0,3.2-3.7,3.2-3.7h0v7.5h0
				c0,0-0.4-3.7-3.2-3.7h-0.3v8h0.8c5.6,0,6.1-6.1,6.1-6.1h0V80.1z"/>
			<path d="M345.1,80.1h-12.7v0h2V64.4h-2v0h7.7v0h-2v15.6h0.8c5.6,0,6.1-6.1,6.1-6.1h0V80.1z"/>
			<path d="M362.2,79.5c-0.4,0.8-0.9,1-1.9,1c-2.2,0-4.2-0.9-4.2-4V75c0-2.2-1.3-3-3.3-3h-1.1v8.1h2v0H346v0h2V64.4h-2v0h7.1
				c3.6,0,5.9,1.6,5.9,3.9c0,1.9-1.7,3.5-4.2,3.6c2.3,0.1,5.1,1.3,5.1,4.3v2c0,1.7,0.5,2,1.1,2C361.6,80.3,362,80,362.2,79.5
				L362.2,79.5z M352.6,64.4h-0.8v7.5h1.1c1.8,0,2.7-0.4,2.7-3.6C355.5,65.6,354.7,64.4,352.6,64.4z"/>
			<path d="M374.2,64.4h-1.8l-3.5,8.2v7.5h2v0h-7.7v0h2v-6.8l-4.2-8.8H359v0h8.3v0h-2.2l3.8,8.1l3.5-8.1h-2.3v0L374.2,64.4L374.2,64.4
				z"/>
			<path d="M286.9,85.6H285l3.7,9.8l2.9-9.8h-2.1v0h4v0h-1.8l-4.8,16.1l-4.1-11.1l-3.3,11.1l-6-16.1h-1.8v0h7.3v0h-1.4l3.7,9.8
				l1.4-4.9l-1.8-4.9h-1.3v0L286.9,85.6L286.9,85.6z"/>
			<path d="M306.6,101.3h-12.7v0h2V85.6h-2v0h11.8v5.7h0c0,0-0.7-5.6-5.3-5.6h-0.8v7.6h0.3c2.9,0,3.2-3.7,3.2-3.7h0v7.5h0
				c0,0-0.4-3.7-3.2-3.7h-0.3v8h0.8c5.6,0,6.1-6.1,6.1-6.1h0V101.3z"/>
			<path d="M320.5,101.3h-12.7v0h2V85.6h-2v0h11.8v5.7h0c0,0-0.7-5.6-5.3-5.6h-0.8v7.6h0.3c2.9,0,3.2-3.7,3.2-3.7h0v7.5h0
				c0,0-0.4-3.7-3.2-3.7h-0.3v8h0.8c5.6,0,6.1-6.1,6.1-6.1h0V101.3z"/>
			<path d="M338.8,101.3h-8.5v0h1.9l-4.7-7.4v7.4h2v0h-7.7v0h2V85.6h-2v0h7.7v0h-2v8.2l6.4-8.2h-2v0h4.1v0h-2l-3.9,5l6.8,10.7
				L338.8,101.3L338.8,101.3z"/>
			<path d="M282.7,110.8c0,3.1-2.2,4.1-5.3,5.3c-2.5,0.9-5.1,0.8-5.4,5.3c0.7-2.3,2-3.1,3.5-3.1c2.3,0,3.2,1.7,5.1,1.7
				c1.1,0,2.3-0.5,2.3-2.2v-0.1h0v0.3c0,2.5-1.3,4.8-3.6,4.8c-3.1,0-3.4-2.3-5.5-2.2c-1.1,0-1.9,0.8-1.9,1.9h0v-0.3
				c0-4.3,1.8-4.8,4.3-6.7c2.4-1.8,2.6-3.3,2.6-5c0-2.4-0.8-3.7-2.6-3.7c-2,0-3.9,1.2-4.1,3.2c0.2-0.8,1-1.4,1.8-1.4
				c1.1,0,1.9,0.9,1.9,1.9c0,1.1-0.9,1.9-1.9,1.9c-1.3,0-1.9-1-1.9-2.1c0-2.2,2-3.8,5.1-3.8C280.2,106.4,282.7,108.4,282.7,110.8z"/>
			<path d="M291.2,122.7c-3.3,0-6.9-3.7-6.9-8.2s3.7-8.2,6.9-8.2c3.3,0,6.9,3.7,6.9,8.2S294.4,122.7,291.2,122.7z M291.2,106.4
				c-2.2,0-3,1.8-3,8.1s0.8,8.1,3,8.1c2.2,0,3-1.8,3-8.1S293.3,106.4,291.2,106.4z"/>
			<path d="M309.4,110.5c0,3.1-2.2,4.2-5.4,5.4c-2.6,1-5.2,0.8-5.5,5.4c0.7-2.3,2-3.1,3.5-3.1c2.4,0,3.2,1.7,5.2,1.7
				c1.1,0,2.4-0.5,2.4-2.2v-0.1h0v0.3c0,2.5-1.4,4.9-3.7,4.9c-3.1,0-3.5-2.3-5.6-2.3c-1.1,0-1.9,0.8-1.9,1.9h0v-0.3
				c0-4.3,1.8-4.9,4.4-6.8c2.4-1.8,2.7-3.3,2.7-5.1c0-2.4-0.8-3.8-2.6-3.8c-2,0-4,1.2-4.2,3.3c0.2-0.8,1-1.4,1.9-1.4
				c1.1,0,1.9,0.9,1.9,1.9s-0.9,1.9-1.9,1.9c-1.3,0-1.9-1-1.9-2.1c0-2.2,2-3.9,5.2-3.9C306.9,105.9,309.4,108.1,309.4,110.5z"/>
			<path d="M317.5,122.6c-3.3,0-7.1-3.8-7.1-8.3s3.8-8.3,7.1-8.3c3.3,0,7.1,3.8,7.1,8.3S320.9,122.6,317.5,122.6z M317.5,106
				c-2.2,0-3.1,1.8-3.1,8.3c0,6.5,0.8,8.3,3.1,8.3c2.2,0,3.1-1.8,3.1-8.3C320.6,107.8,319.8,106,317.5,106z"/>
		</g>
	</svg>

<?php
}


function display_jw_logo_circle() { ?>
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 488 488" style="enable-background:new 0 0 488 488;" xml:space="preserve">
		<style type="text/css">
			.st1{stroke:#000000;stroke-miterlimit:10;}
			.st2{display:none;}
		</style>
		<g id="Layer_1">
			<g>
				<circle class="st0" style="fill:none;stroke:#000000;stroke-miterlimit:10;" cx="244" cy="244" r="243.5"/>
				<g>
					<path class="st1" style="stroke:#000000;stroke-miterlimit:10;" d="M141.8,176.8v51.5L64,116.5v-8.8h29.6L141.8,176.8z"/>
					<path class="st1" style="stroke:#000000;stroke-miterlimit:10;" d="M209.3,169.4v57.2h-28.5v-52l-32.1-66.9h31.3L209.3,169.4z"/>
					<path class="st1" style="stroke:#000000;stroke-miterlimit:10;" d="M234.7,166.8c0-4.6,0.5-9.3,1.5-14c1-4.7,2.5-9.2,4.4-13.6c1.9-4.4,4.3-8.5,7.1-12.3
						c2.8-3.8,6.1-7.3,9.7-10.3c3.7-3,7.7-5.5,12.1-7.6c4.4-2,9.1-3.3,14.2-3.9c-7.6,2.5-13,9.9-16.1,22.1c-0.8,3.2-1.4,6.6-1.8,10.1
						c-0.4,3.5-0.7,6.9-0.9,10.4c-0.2,3.4-0.3,6.7-0.3,9.9c0,3.2,0,6.1,0,8.7v0.8c0,9.5,0.4,17.8,1.1,25.1c0.7,7.2,1.8,13.4,3.3,18.5
						c1.5,5.1,3.3,9.2,5.6,12.2c2.3,3.1,4.9,5.2,8,6.3c-6.8-0.8-13.1-3-18.9-6.5c-5.8-3.5-10.9-8-15.2-13.5
						c-4.3-5.5-7.6-11.9-10.1-19.1C236,182.7,234.7,175,234.7,166.8z M298.6,131.7c0-2,0.4-3.8,1.1-5.6c0.7-1.7,1.7-3.2,3-4.5
						c1.3-1.3,2.8-2.3,4.5-3c1.7-0.7,3.6-1.1,5.5-1.1c2,0,3.8,0.4,5.6,1.1c1.7,0.7,3.2,1.7,4.5,3c1.3,1.3,2.3,2.8,3,4.5
						c0.7,1.7,1.1,3.6,1.1,5.6c0,1.9-0.4,3.8-1.1,5.5c-0.7,1.7-1.7,3.2-3,4.5c-1.3,1.3-2.8,2.3-4.5,3c-1.7,0.7-3.6,1.1-5.6,1.1
						c-1.9,0-3.8-0.4-5.5-1.1c-1.7-0.7-3.2-1.7-4.5-3c-1.3-1.3-2.3-2.8-3-4.5C299,135.5,298.6,133.6,298.6,131.7z"/>
				</g>
				<g>
					<g>
						<path class="st1" style="stroke:#000000;stroke-miterlimit:10;" d="M64.5,345.4c0-1.9,0.4-3.7,1.1-5.4c0.7-1.7,1.7-3.2,3-4.5c1.3-1.3,2.7-2.3,4.4-3c1.7-0.7,3.5-1.1,5.4-1.1
							c2,0,3.8,0.4,5.5,1.1c1.7,0.7,3.2,1.7,4.4,3c1.3,1.3,2.3,2.8,3,4.5c0.7,1.7,1.1,3.5,1.1,5.4c0,2-0.4,3.8-1.1,5.5
							c-0.7,1.7-1.7,3.2-3,4.4c-1.3,1.3-2.7,2.3-4.4,3c-1.7,0.7-3.5,1.1-5.5,1.1c-1.9,0-3.7-0.4-5.4-1.1c-1.7-0.7-3.2-1.7-4.4-3
							c-1.3-1.3-2.3-2.7-3-4.4C64.9,349.2,64.5,347.4,64.5,345.4z M129.9,328.2c0,5.6-0.9,10.6-2.8,15c-1.9,4.4-4.5,8.2-8,11.3
							c-3.4,3.1-7.6,5.5-12.4,7.2c-4.9,1.7-10.3,2.5-16.2,2.5h-0.6c3.8-0.5,6.7-2.3,8.8-5.4c2-3.1,3-7.4,3-12.9V243.5h28.3V328.2z"/>
						<path class="st1" style="stroke:#000000;stroke-miterlimit:10;" d="M192,364.5l-45.2-121h31l28,74.4L192,364.5z M247.8,364.5l-45.2-121h31l27.9,74.5L247.8,364.5z"/>
					</g>
					<g>
						<path d="M345.6,272.7c0,23.6-16.5,31.4-40.5,40.3c-19.5,7.2-39.2,5.9-41.4,40.3c5.2-17.6,15-23.6,26.7-23.6
							c17.8,0,24.5,13.1,39.3,13.1c8.6,0,17.8-4,17.8-16.9v-0.7h0.3v2.1c0,19-10.2,36.7-27.8,36.7c-23.4,0-26.2-17.6-42.2-17.1
							c-8.6,0.3-14.3,6-14.3,14.6h-0.3v-2.6c0-32.7,13.6-36.7,33.3-51.5c18.1-13.6,20-25,20-38.6c0-18.1-6.2-28.4-19.8-28.4
							c-15,0-30,9.3-31.4,24.6c1.7-6.2,7.4-10.9,14.1-10.9c8.1,0,14.6,6.5,14.6,14.6c0,8.1-6.6,14.7-14.6,14.7
							c-9.7,0-14.6-7.6-14.6-15.9c0-16.7,15.3-29.1,39.3-29.1C326.7,238.6,345.6,254.7,345.6,272.7z"/>
						<path d="M402.2,364.1c-25,0-53.3-28.4-53.3-62.7s28.3-62.7,53.3-62.7c25,0,53.3,28.4,53.3,62.7S427.2,364.1,402.2,364.1z
							 M402.2,239c-16.7,0-23.1,13.8-23.1,62.4c0,48.6,6.4,62.4,23.1,62.4c16.7,0,23.1-13.8,23.1-62.4
							C425.3,252.8,418.9,239,402.2,239z"/>
					</g>
				</g>
			</g>
		</g>
		<g id="Layer_2" class="st2">

				<image style="display:inline;overflow:visible;opacity:0.28;" width="738" height="882" xlink:href="F0BC31BE0695812F.jpg"  transform="matrix(1 0 0 1 -112.8039 -194.5189)">
			</image>
		</g>
	</svg>
<?php
}

function display_mobile_logo() { ?>
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 141 26.8" style="enable-background:new 0 0 141 26.8;" xml:space="preserve">
		<g>
			<path class="st0" style="fill:#FFFFFF;" d="M16.6,15.3v11L0,2.4V0.6h6.3L16.6,15.3z"/>
			<path class="st0" style="fill:#FFFFFF;" d="M30.9,13.7v12.2h-6.1V14.8L18,0.6h6.7L30.9,13.7z"/>
			<path class="st0" style="fill:#FFFFFF;" d="M36.3,13.1c0-1,0.1-2,0.3-3c0.2-1,0.5-2,0.9-2.9c0.4-0.9,0.9-1.8,1.5-2.6c0.6-0.8,1.3-1.6,2.1-2.2
				c0.8-0.6,1.6-1.2,2.6-1.6c0.9-0.4,1.9-0.7,3-0.8c-1.6,0.5-2.8,2.1-3.4,4.7c-0.2,0.7-0.3,1.4-0.4,2.1c-0.1,0.7-0.2,1.5-0.2,2.2
				c0,0.7-0.1,1.4-0.1,2.1c0,0.7,0,1.3,0,1.9v0.2c0,2,0.1,3.8,0.2,5.3c0.2,1.5,0.4,2.9,0.7,3.9c0.3,1.1,0.7,2,1.2,2.6
				c0.5,0.7,1,1.1,1.7,1.3c-1.4-0.2-2.8-0.6-4-1.4c-1.2-0.7-2.3-1.7-3.2-2.9c-0.9-1.2-1.6-2.5-2.1-4.1C36.6,16.5,36.3,14.9,36.3,13.1z
				 M49.9,5.7c0-0.4,0.1-0.8,0.2-1.2c0.2-0.4,0.4-0.7,0.6-1c0.3-0.3,0.6-0.5,1-0.6c0.4-0.2,0.8-0.2,1.2-0.2c0.4,0,0.8,0.1,1.2,0.2
				c0.4,0.2,0.7,0.4,1,0.6c0.3,0.3,0.5,0.6,0.6,1C55.9,4.9,56,5.3,56,5.7c0,0.4-0.1,0.8-0.2,1.2c-0.2,0.4-0.4,0.7-0.6,1
				c-0.3,0.3-0.6,0.5-1,0.6c-0.4,0.2-0.8,0.2-1.2,0.2c-0.4,0-0.8-0.1-1.2-0.2c-0.4-0.2-0.7-0.4-1-0.6c-0.3-0.3-0.5-0.6-0.6-1
				C50,6.5,49.9,6.1,49.9,5.7z"/>
		</g>
		<g>
			<g>
				<path class="st0" style="fill:#FFFFFF;" d="M57.9,22.8c0-0.4,0.1-0.8,0.2-1.2c0.2-0.4,0.4-0.7,0.6-1c0.3-0.3,0.6-0.5,0.9-0.6c0.4-0.2,0.7-0.2,1.1-0.2
					c0.4,0,0.8,0.1,1.2,0.2c0.4,0.2,0.7,0.4,0.9,0.6c0.3,0.3,0.5,0.6,0.6,1c0.2,0.4,0.2,0.8,0.2,1.2c0,0.4-0.1,0.8-0.2,1.2
					c-0.2,0.4-0.4,0.7-0.6,0.9c-0.3,0.3-0.6,0.5-0.9,0.6c-0.4,0.2-0.7,0.2-1.2,0.2c-0.4,0-0.8-0.1-1.1-0.2c-0.4-0.2-0.7-0.4-0.9-0.6
					c-0.3-0.3-0.5-0.6-0.6-0.9C58,23.6,57.9,23.2,57.9,22.8z M71.8,19.1c0,1.2-0.2,2.2-0.6,3.2c-0.4,0.9-1,1.7-1.7,2.4
					c-0.7,0.7-1.6,1.2-2.6,1.5c-1,0.4-2.2,0.5-3.5,0.5h-0.1c0.8-0.1,1.4-0.5,1.9-1.2c0.4-0.7,0.6-1.6,0.6-2.8V1.1h6V19.1z"/>
				<path class="st0" style="fill:#FFFFFF;" d="M85,26.8L75.4,1.1H82l6,15.8L85,26.8z M96.9,26.8L87.3,1.1h6.6l5.9,15.8L96.9,26.8z"/>
			</g>
			<g>
				<path class="st0" style="fill:#FFFFFF;" d="M117.7,7.3c0,5-3.5,6.7-8.6,8.6c-4.1,1.5-8.3,1.2-8.8,8.6c1.1-3.7,3.2-5,5.7-5c3.8,0,5.2,2.8,8.4,2.8
					c1.8,0,3.8-0.8,3.8-3.6v-0.1h0.1v0.4c0,4-2.2,7.8-5.9,7.8c-5,0-5.6-3.7-9-3.6c-1.8,0.1-3,1.3-3,3.1h-0.1v-0.5c0-7,2.9-7.8,7.1-11
					c3.8-2.9,4.2-5.3,4.2-8.2c0-3.8-1.3-6-4.2-6c-3.2,0-6.4,2-6.7,5.2c0.4-1.3,1.6-2.3,3-2.3c1.7,0,3.1,1.4,3.1,3.1s-1.4,3.1-3.1,3.1
					c-2.1,0-3.1-1.6-3.1-3.4c0-3.6,3.3-6.2,8.4-6.2C113.6,0.1,117.7,3.5,117.7,7.3z"/>
				<path class="st0" style="fill:#FFFFFF;" d="M129.7,26.7c-5.3,0-11.3-6-11.3-13.3s6-13.3,11.3-13.3c5.3,0,11.3,6,11.3,13.3S135,26.7,129.7,26.7z
					 M129.7,0.2c-3.6,0-4.9,2.9-4.9,13.3c0,10.3,1.4,13.3,4.9,13.3c3.6,0,4.9-2.9,4.9-13.3C134.6,3.1,133.2,0.2,129.7,0.2z"/>
			</g>
		</g>
	</svg>
<?php
}

add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var vendorsNonce = ' <?php echo wp_create_nonce('ajax_function'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
  switch($_REQUEST['fn']){
		case 'get_event_info':
			echo json_encode(get_event_info($_REQUEST['eid']));
		break;
		case 'get_all_events':
			echo json_encode(get_event_posts());
	  break;
		case 'get_events_by_term':
			echo json_encode(get_event_by_term($_REQUEST['term']));
	  break;
		case 'get_events':
			if ( $_REQUEST['event_type'] == 'scheduled' ) {
				echo json_encode( get_todays_events($_REQUEST['param']) );
			} else {
				echo json_encode( get_events_by_category($_REQUEST['param']) );
			}

		break;
		case 'get_event_by_slug':
			echo json_encode( get_event_content($_REQUEST['event'], filter_var($_REQUEST['intro'], FILTER_VALIDATE_BOOLEAN)) );
		break;

		case 'get_event_content_by_id':
			$supporting_url = isset($_REQUEST['supporting']) ? $_REQUEST['supporting'] : false;
			echo json_encode( get_event_content_by_id($_REQUEST['event'], filter_var($_REQUEST['intro'], FILTER_VALIDATE_BOOLEAN), $supporting_url ));
		break;
		case 'get_event_content':
			$event_id = $_REQUEST['id'];
			$event_map = $_REQUEST['map'];
			echo json_encode(get_event_content($event_id, false, $event_map == 'false' ? false : true));
		break;
		case 'sign_up_user':
      $mc_email = $_REQUEST['email'];
      mailchimp_sign_up($mc_email);
	  break;
		case 'update_paid_user':
			$user = $_REQUEST['user'];
			update_field('paid_event_owner', true, 'user_' . $user);
		break;
		case 'filter_items':
			$filter = isset($_REQUEST['filters']) ? $_REQUEST['filters'] : null;
			$post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : null;
			$random = isset($_REQUEST['random']) ? $_REQUEST['random'] : null;
			$not_in = isset($_REQUEST['not_in']) ? $_REQUEST['not_in'] : null;
			$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : null;
			$posts_per_page = isset($_REQUEST['posts_per_page']) == '-1' ? -1 : null;
			$args = [
				'post_type' => $post_type,
				'posts_per_page' => 9,
				'orderby' => 'title',
				'order' => 'ASC',
				'post_status' => 'publish',
				'post_parent' => 0,
			];
			if ( $page != null ) {
				$args['paged'] = $page;
			}
			if ( $posts_per_page != null ) {
				$args['posts_per_page'] = $posts_per_page;
			}
			if ( $random ) {
				$args['orderby'] = 'rand';
			}
			if ( $not_in ) {
				$args['post__not_in'] = $not_in;
			}
			if ( $post_type == 'vendor' ) {
				$args['meta_query'] = [
					[
						'key'     => 'vendor_active',
						'value'   => true,
						'compare' => '='
					],
				];
			}
			if(!empty($filter)) {
				$taxes = ['relation' => 'AND'];
				foreach($filter as $key=>$f) {
					array_push($taxes, [
						'taxonomy' => $key,
						'field' => 'slug',
						'terms' => $f
					]);
				}
				$args['tax_query'] = $taxes;
			}
			$itemsObj = get_filterable_content($args);
			$itemsObj->filter = $filter;
			$itemsObj->type = gettype($filter);
			echo json_encode($itemsObj);
		break;
		default:
			$output = 'nothing here';
			echo $output;
		break;
	}
	die();
}

function get_event_info($id) {
	$args = [
		'post_type' => 'event',
		'post__in' => [$id],
	];
	// return $id;
	ob_start();
	echo events_posts_query($args, '', 1);
	return ob_get_clean();
}

function get_event_content($event, $intro) {
	if ( $intro ) {
		$day = get_term_by('slug', $event, 'event-day');
		$event_obj = (object)[];
		$event_obj->intro = true;
		if ( $featured_image = get_field( 'small-medium', $day ) ) {
			$event_obj->featured_image = $featured_image['sizes']['small-medium'];
		}
		$event_obj->title = $day->name;
		$event_obj->subtitle = get_field('subtitle', $day);
		$event_obj->content = get_field('content', $day);
		$event_obj->id = $day->term_id;
		$event_obj->date = get_field('event_date', $day);
		$event_obj->permalink = $day->slug;
		$event_obj->durational = false;
		if ( $featured_image = get_field('featured_image', $day) ) {
			$event_obj->featured_image = $featured_image['sizes']['small-medium'];
		}
		return $event_obj;
	} else {
		$args = [
			'post_type' => ['event'],
			'name' => $event,
			'posts_per_page' => 1
		];
		$event_query = new WP_Query( $args );
		if ( $event_query->have_posts() ) :
			$event_obj = [];
			while ( $event_query->have_posts() ) : $event_query->the_post();
				$event_obj = (object)[];
				if ( $featured_image = get_the_post_thumbnail_url( get_the_ID(), 'small-medium' ) ) {
					$event_obj->featured_image = $featured_image;
				}
				$start_date = get_field('start_date');
				$event_length = get_field('event_length');
				$event_obj->durational = has_term('exhibition', 'event-type', $id ) || ($start_date && $event_length == 'range');
				//$formattedDate = date('l, F jS',$realDate);

				$event_obj->title = get_the_title();
				$event_obj->subtitle = get_field('event_subtitle');
				$event_obj->id = get_the_ID();
				$event_obj->permalink = basename(get_the_permalink());
				$event_obj->content = get_field('description');
				$event_obj->virtual = get_field('virtual_event') == 'virtual';
				switch ($event_length) {
					case 'single':
					$event_obj->date = get_field('event_date');
					$event_obj->start_time = get_field('opening_time');
					$event_obj->end_time = get_field('closing_time');
					break;
					case 'multiple':
					break;
					case 'range':
					break;
				}

				// if ( get_post_type() == 'page' ) {
				// 	$event_obj->intro = true;
				// }
				if( $event_types = get_the_terms( get_the_id(), 'event-type' ) ) {
					$event_obj->event_types = $event_types;
				}
				if( $event_images = get_field('event_images') ) {
					$event_obj->images = $event_images;
				}
				if( $participants = get_field('participants') ) {
					$event_obj->participants = $participants;
				}
				if( $event_subtitle = get_field('event_subtitle') ) {
					$event_obj->subtitle = $event_subtitle;
				}
				if( $rsvp_link = get_field('rsvp_link') ) {
					$event_obj->rsvp = $rsvp_link;
				}
				if( $event_link = get_field('event_link') ) {
					$event_obj->link = $event_link;
				}

				$event_obj->instagram_event = get_field('instagram_event');

				if( $partner_name = get_field('partner_name') ) {
					$event_obj->partner_name = $partner_name;
				}
				if( $partner_website = get_field('partner_website') ) {
					$event_obj->partner_website = $partner_website;
				}
				if( $facebook = get_field('facebook') ) {
					$event_obj->facebook = $facebook;
				}
				if( $twitter = get_field('twitter') ) {
					$event_obj->twitter = $twitter;
				}
				if( $instagram = get_field('instagram') ) {
					$event_obj->instagram = $instagram;
				}
			endwhile;
			return $event_obj;
		wp_reset_postdata();
		endif;
	}
}


// Testing Function with Id
function get_event_content_by_id($id, $intro, $supporting_slug) {
	if ( $intro ) {
		$day = get_term_by('slug', $id, 'event-day');
		$event_obj = (object)[];
		$event_obj->intro = true;
		$featured_image = get_field( 'small-medium', $day );
		if ( $featured_image ) {
			$event_obj->featured_image = $featured_image['sizes']['small-medium'];
		}
		$event_obj->title = $day->name;
		$event_obj->subtitle = get_field('subtitle', $day);
		$event_obj->content = get_field('content', $day);
		$event_obj->id = $day->term_id;
		$event_obj->date = get_field('event_date', $day);
		$event_obj->permalink = $day->slug;
		$event_obj->durational = false;
		if ( $featured_image = get_field('featured_image', $day) ) {
			$event_obj->featured_image = $featured_image['sizes']['small-medium'];
		}
		return $event_obj;
	} else {
		$args = array(
			'name'        => $id,
			'post_type'   => 'event',
			'post_status' => 'publish',
			'numberposts' => 1
		);
		$my_posts = get_posts($args);
		$event_query = new WP_Query( $args );
		if ( $event_query->have_posts() ) :
			$event_obj = [];
			while ( $event_query->have_posts() ) : $event_query->the_post();
				$post_id = get_the_ID();
				$event_obj = (object)[];

				$supporting_events = [];
				$supporting_event_index = null;
				$supporting_events_length = intval(get_post_meta($post_id, 'supporting_events', true));
				for($i = 0; $i < $supporting_events_length; $i++) {
					$meta_key_support_event_name = 'supporting_events_'.$i.'_supporting_event_name';
					$meta_key_support_event_type = 'supporting_events_'.$i.'_event_category';
					$meta_key_support_event_date = 'supporting_events_'.$i.'_date';
					$meta_key_support_event_start_time = 'supporting_events_'.$i.'_start_time';
					$meta_key_support_event_end_time =  'supporting_events_'.$i.'_end_time';
					$meta_key_support_event_rsvp =  'supporting_events_'.$i.'_supporting_url';
					$meta_key_support_event_location =  'supporting_events_'.$i.'_location';
					$meta_key_support_event_virtual =  'supporting_events_'.$i.'_virtual';
					$support_event_name = get_post_meta($post_id, $meta_key_support_event_name, true);
					$support_event_type = get_post_meta($post_id, $meta_key_support_event_type, true);
					$support_event_date = get_post_meta($post_id, $meta_key_support_event_date, true);
					$support_start_time = get_post_meta($post_id, $meta_key_support_event_start_time, true);
					$support_end_time = get_post_meta($post_id, $meta_key_support_event_end_time, true);
					$support_rsvp = get_post_meta($post_id, $meta_key_support_event_rsvp, true);
					$support_location = get_post_meta($post_id, $meta_key_support_event_location, true);
					$support_virtual = get_post_meta($post_id, $meta_key_support_event_virtual, true);
					$support_event_term = get_term( $support_event_type );
					array_push($supporting_events, array(
						"event" => $support_event_name,
						"date" =>  strlen($support_event_date) > 5 ? (substr($support_event_date, 0, 4).'-'.substr($support_event_date, 4, 2).'-'.substr($support_event_date, 6, 2)) : '',
						"event_types" => array(
							'id' => $support_event_type,
							'name' => $support_event_term->name
						),
						"start_time" => $support_start_time,
						"end_time" => $support_end_time,
						"location" => $support_location,
						"rsvp" => $support_rsvp,
						"virtual" => $support_virtual
					));
					if ( $supporting_slug && slugify($support_event_name) == $supporting_slug ) {
						$supporting_event_index = $i;
					}
				}
				$event_obj->supportEvents = $supporting_events;
				
				// Images:
				$event_image_id = get_post_meta($post_id, 'event_image', true);
				$listing_image_id = get_post_meta($post_id, 'listing_image', true);
				list($event_image, $e_width, $e_height) = wp_get_attachment_image_src($event_image_id, 'medium');
				list($listing_image, $width, $height) = wp_get_attachment_image_src($listing_image_id, 'medium');
				$event_obj->event_image = $event_image;
				$event_obj->listing_image = $listing_image;

				$images[] = array(
						'src' => $event_image,
						'id' => $event_image_id
				);

				$event_obj->featured_image = array(
					'src' => $event_image,
					'alt' => get_the_title()
				);
				$event_obj->images = $images;
				if ( $supporting_slug) {
					log_it($event_obj->supportEvents);
				}
				$event_obj->title = ($supporting_slug && isset(supportEvents[$supporting_event_index]['event'])) ? $event_obj->supportEvents[$supporting_event_index]['event'] : get_the_title();
				$event_obj->subtitle = $supporting_slug ? null : get_field('event_subtitle');
				$event_obj->content = get_field('description');
				$event_obj->is_discover = get_field('discover');
				$event_date_string =  get_post_meta($post_id, 'event_date', true);
				$event_date_formatted = strlen($event_date_string) > 5 ? (substr($event_date_string, 0, 4).'-'.substr($event_date_string, 4, 2).'-'.substr($event_date_string, 6, 2)) : '';
				$event_obj->event_date = $supporting_slug 
					? $event_obj->supportEvents[$supporting_event_index]['date']
					: $event_date_formatted;

				$virtual_event = get_post_meta($post_id, 'virtual_event', true);
				if ( get_field('instagram_event', $post_id) ) {
					$event_obj->instagram = true;
				}
				$hide_virtual_info = get_post_meta($post_id, 'hide_virtual_event_info', true);
				if (!$virtual_event || strlen($virtual_event) < 1) {
					$virtual_event = 'In-person';
				}
				$event_obj->virtual_event = $supporting_slug 
					? $event_obj->supportEvents[$supporting_event_index]['virtual']
					: $virtual_event;
				$event_obj->covid_messaging = get_field('covid_messaging', 'option');
				$event_obj->hide_virtual = $hide_virtual_info;
				$event_partner_website = get_post_meta($post_id, 'partner_website', true);
				if (!$event_partner_website || strlen($event_partner_website) < 1) {
					$event_partner_website = get_post_meta($post_id, 'project_website_url', true);
				}
				$event_obj->project_url = $event_partner_website;
				$event_type_id = get_post_meta($post_id, 'event_type', true);
				$event_type = get_term( $event_type_id );
				$event_types = [];
				array_push($event_types, [
					'id' => $event_type_id,
					'name' => $event_type->name,
					'slug' => $event_type->slug
				]);
				$start_date = get_field('start_date', $post_id);
				$end_date = get_field('end_date', $post_id);
				$event_length = get_field('event_length', $post_id);
				$is_durational = has_term('exhibition', 'event-type', $post_id ) || ($start_date && $event_length == 'range');
				$event_obj->durational = $is_durational;
				$event_obj->location_name = $supporting_slug 
					? null
					: get_post_meta($post_id, 'location_name', true);
				$event_obj->location_address = $supporting_slug
					? $event_obj->supportEvents[$supporting_event_index]['location']
					: get_post_meta($post_id, 'event_location', true);
				$event_obj->access_instructions = $supporting_slug 
					? null
					: get_post_meta($post_id, 'access_instructions', true);
				$event_obj->event_types = $event_types;
				$event_obj->event_url = $is_durational ? get_post_meta($post_id, 'event_url', true) : get_field('watch_live_link', 'option');

				$event_dates_length = intval(get_post_meta($post_id, 'event_dates', true));
				$virtual_event = get_post_meta($post_id, 'virtual_event', true);
				$project_website_url = get_post_meta($post_id, 'project_website_url', true);
				$event_location = get_post_meta($post_id, 'event_location', true);

				$rsvp_link = $supporting_slug 
				? $event_obj->supportEvents[$supporting_event_index]['rsvp']
				: get_post_meta($post_id, 'rsvp_link', true);
				
				if ( $rsvp_link ) {
					$event_obj->rsvp = array(
						'url' => $rsvp_link['url'],
						'title' => $rsvp_link['title']
					);
				}

				$event_dates = [];
				$start_time = [];
				$end_time = [];
				$event_dates_ = [];
				
				if ( $event_length == 'multiple' ) {
					for($i = 0; $i < $event_dates_length; $i++) {
						$meta_key_date = 'event_dates_'.$i.'_date';
						$meta_key_start_time = 'event_dates_'.$i.'_start_time';
						$meta_key_end_time = 'event_dates_'.$i.'_end_time';
						$event_date = get_post_meta($post_id, $meta_key_date, true);
						$event_start_time = get_post_meta($post_id, $meta_key_start_time, true);
						$event_end_time = get_post_meta($post_id, $meta_key_end_time, true);
						$event_date = strlen($event_date) > 5 ? (substr($event_date, 0, 4).'-'.substr($event_date, 4, 2).'-'.substr($event_date, 6, 2)) : '';
						if (strlen($event_date) > 0) {
							array_push($start_time, $event_start_time);
							array_push($end_time, $event_end_time);
							array_push($event_dates_, $event_date);
							array_push($event_dates, array(
								'event_date' => $event_date,
								'start_time' => $event_start_time,
								'end_time'	=> $event_end_time
							));
						}
					}
				}
				
				if ( $event_length == 'single' ) {
					$event_date = strlen($event_date_string) > 5 ? (substr($event_date_string, 0, 4).'-'.substr($event_date_string, 4, 2).'-'.substr($event_date_string, 6, 2)) : '';
					$event_start_time = get_post_meta($post_id, 'opening_time', true);
					$event_end_time = get_post_meta($post_id, 'closing_time', true);
					$start_time = $event_start_time;
					$end_time = $event_end_time;
					array_push($event_dates_, $event_date);
					array_push($event_dates, array(
						'event_date' => $event_date,
						'start_time' => $event_start_time ? $event_start_time : null,
						'end_time'	=> $event_end_time ? $event_end_time : null
					));
				}

				$event_obj->date = $supporting_slug 
					? $event_obj->supportEvents[$supporting_event_index]['date']
					: $event_dates_;

				if ( $supporting_slug ) {
					if ( $event_obj->supportEvents[$supporting_event_index]['date'] ) {
						$event_obj->dates = [
							array(
								'event_date' => $event_obj->supportEvents[$supporting_event_index]['date'],
								'start_time' => $event_obj->supportEvents[$supporting_event_index]['start_time'],
								'end_time'	=> $event_obj->supportEvents[$supporting_event_index]['end_time']
							)];
					}
				} else {
					$event_obj->dates = $event_dates;
				}

				$event_obj->start_time = $start_time;
				$event_obj->end_time = $end_time;

				if ( $event_length == 'range' ) {
					$event_start_time = get_post_meta($post_id, 'opening_time', true);
					$event_end_time = get_post_meta($post_id, 'closing_time', true);
					$event_obj->start_date = $start_date;
					$event_obj->end_date = $end_date;
					$event_obj->start_time = $event_start_time;
					$event_obj->end_time = $event_end_time;
				}

				if ( $supporting_slug ) {
					$event_obj->start_date = null;
				}

				$participants = [];
				$participants_length = intval(get_post_meta($post_id, 'participants', true));
				for($i = 0; $i < $participants_length; $i++) {
					$meta_key_name = 'participants_'.$i.'_name';
					$meta_key_bio = 'participants_'.$i.'_bio';
					$participant_name = get_post_meta($post_id, $meta_key_name, true);
					$participant_bio = get_post_meta($post_id, $meta_key_bio, true);
					array_push($participants, array(
						'name' => $participant_name,
						'bio' => $participant_bio
					));
				}
				$event_obj->participants = $participants;

				$event_url = get_post_meta($post_id, 'event_url', true);
				$event_obj->link = array(
					'title' => $virtual_event ? 'Watch here' : 'Event link',
					'url' => $event_url,
					'target' => '_blank'
				);
				
				$facebook = get_post_meta($post_id, 'event_social_links_facebook_url', true);
				$instagram = get_post_meta($post_id, 'event_social_links_instagram_url', true);
				$tiktok = get_post_meta($post_id, 'event_social_links_tiktok_url', true);
				$twitter = get_post_meta($post_id, 'event_social_links_twitter_url', true);
				$sponsor_name = get_post_meta($post_id, 'sponsor_sponsor_name', true);
				$sponsor_link = get_post_meta($post_id, 'sponsor_sponsor_link', true);
				$sponsor_logo_id = get_post_meta($post_id, 'sponsor_sponsor_logo', true);
				list($sponsor_logo, $e_width, $e_height) = wp_get_attachment_image_src($sponsor_logo_id, 'medium');
				$event_obj->sponsor = array(
					'name' => $sponsor_name,
					'link' => $sponsor_link,
					'image' => $sponsor_logo
				);

				$sponsors = [];
				$sponsors_length = intval(get_post_meta($post_id, 'sponsors', true));
				for($i = 0; $i < $sponsors_length; $i++) {
					$meta_key_name = 'sponsors_'.$i.'_sponsor_name';
					$meta_key_link = 'sponsors_'.$i.'_sponsor_link';
					$meta_key_logo = 'sponsors_'.$i.'_sponsor_logo';
					$sponsor_name = get_post_meta($post_id, $meta_key_name, true);
					$sponsor_link = get_post_meta($post_id, $meta_key_link, true);
					$sponsor_logo = get_post_meta($post_id, $meta_key_logo, true);
					array_push($sponsors, array(
						'name' => $sponsor_name,
						'link' => $sponsor_link,
						'image' => $sponsor_logo
					));
				}
				if ( count($sponsors) ) {
					$event_obj->sponsors = $sponsors;
				}


				$social = [];
				if (strlen($facebook) > 0) {
					array_push($social, array(
						'type'=>'facebook',
						'link' => $facebook
					));
				}
				if (strlen($instagram) > 0) {
					array_push($social, array(
						'type'=>'instagram',
						'link' => $instagram
					));
				}
				if (strlen($tiktok) > 0) {
					array_push($social, array(
						'type'=>'tiktok',
						'link' => $tiktok
					));
				}
				if (strlen($twitter) > 0) {
					array_push($social, array(
						'type'=>'twitter',
						'link' => $twitter
					));
				}
				$event_obj->social = $social;
				if ( $supporting_slug ) {
					// OMIT CURRENT SUPPORTING EVENT
					$event_obj->supportEvents = array_filter($event_obj->supportEvents, function($v, $k) use ($event_obj) {
						return $v['event'] != $event_obj->title;
					}, ARRAY_FILTER_USE_BOTH);
				}
			endwhile;
			return $event_obj;
		wp_reset_postdata();
		endif;
	}
}

function sort_event_dates($a, $b) {
  return strcmp($a->slug, $b->slug);
}

function get_event_posts() {
	$events = [];
	$days = get_terms( 'event-day', array(
		'hide_empty' => false
	));
	foreach( $days as $day ) {
		$event_date = get_field('event_date', $day);
		$events[$event_date][] = get_the_intro($day);
	}
	// Scheduled
	$args = [
		'post_type'      => ['event'],
		'posts_per_page' => -1,
		'orderby' => 'meta_value',
		'post_status' => ['publish'],
		'order' => 'ASC',
		// 'tax_query' => [
		// 	[
		// 		'taxonomy' => 'event-type',
		// 		'field'    => 'slug',
		// 		'terms'    => ['exhibition'],
		// 		'operator'  => 'NOT IN'
		// 	]
		// ]
	];
	$events_query = new WP_Query( $args );
	if ( $events_query->have_posts() ) :
		while ( $events_query->have_posts() ) : $events_query->the_post();
			$id = get_the_ID();
			$event_length = get_field('event_length');
			if ( $event_length == 'single' ) {
				if ( $event_date = get_field('event_date') ) {
					$event = get_the_event($id);
					$event->durational = false;
					// Add supporting event as event
					if ( isset($event->supporting_events) ) {
						foreach( $event->supporting_events as $supporting_event ) {
							$supporting_event_date = isset($supporting_event['date']) ? $supporting_event['date'] :  $event_date;
							$events[$supporting_event_date][] = $supporting_event;
						}
					}
					$events[$event_date][] = $event;
				}
			}
			
			if ( $event_length == 'multiple' ) {
				if ( $event_dates = get_field('event_dates') ) {
					if ( count($event_dates) > 2 ) {
						// if event has more than 2 dates, put it into durational

						$event = get_the_event($id);
						$event->durational = true;
						if ( $event->event_types ) {
							foreach ( $event->event_types as $event_type ) {
								$events[$event_type->slug][] = $event;	
							}
						}
						if ( isset($event->supporting_events) ) {
							foreach( $event->supporting_events as $supporting_event ) {
								$supporting_event_date = isset($supporting_event['date']) ? $supporting_event['date'] : $event_type->slug;
								$events[$supporting_event_date][] = $supporting_event;
							}
						}
					} else {
						$event->durational = false;
						foreach ( $event_dates as $event_date ) {
							$event = get_the_event($id);
							$event->date = $event_date['date'];
							$event->start_time = $event_date['start_time'] ? format_event_date($event_date['start_time']) : null;
							$event->end_time = $event_date['end_time'] ? format_event_date($event_date['end_time']) : null;
							$events[$event_date['date']][] = $event;
						}
						if ( isset($event->supporting_events) ) {
							foreach( $event->supporting_events as $supporting_event ) {
								$supporting_event_date = isset($supporting_event['date']) ? $supporting_event['date'] : $event_dates[0]['date'];
								$events[$supporting_event_date][] = $supporting_event;
							}
						}
					}
				}
			}
			if ( $event_length == 'range' ) {
				$start_date = get_field('start_date');
				if ( $start_date ) {
					$event = get_the_event($id);
					$event->durational = true;
					if ( $event->event_types ) {
						foreach ( $event->event_types as $event_type ) {
							$events[$event_type->slug][] = $event;	
						}
					}
					if ( isset($event->supporting_events) ) {
						foreach( $event->supporting_events as $supporting_event ) {
							$supporting_event_date = isset($supporting_event['date']) ? $supporting_event['date'] : $event_type->slug;
							$events[$supporting_event_date][] = $supporting_event;
						}
					}
				}
			}
		endwhile;
		wp_reset_postdata();
	endif;

	// // Durational
	// $args = [
	// 	'post_type'      => ['event'],
	// 	'posts_per_page' => -1,
	// 	'post_status' => ['publish'],
	// 	'order' => 'ASC'
	// ];
	// $events_query = new WP_Query( $args );
	// if ( $events_query->have_posts() ) :
	// 	while ( $events_query->have_posts() ) : $events_query->the_post();
	// 		$id = get_the_ID();
	// 		$start_date = get_field('start_date');
	// 		if ( has_term('exhibition', 'event-type', $id ) || $start_date ) {
	// 			$event = get_the_event($id);
	// 			if ( $event->event_types ) {
	// 				foreach ( $event->event_types as $event_type ) {
	// 					$events[$event_type->slug][] = $event;	
	// 				}
	// 			}
	// 		}
	// 	endwhile;
	// 	wp_reset_postdata();
	// endif;

	// usort($events, "sort_events_by_time");
	return $events;
}

function format_event_date ($date) {
	$f_date = date_create($date);
	$hours = $f_date->format('g');
	$minutes = $f_date->format('i');
	$meridien = $f_date->format('a');
	$date_string = $hours;
	$date_string .= ':' . $minutes;
	// if ( $minutes != 0 ) {
	// 	$date_string .= ':' . $minutes;
	// }
	$date_string .= $meridien;
	return $date_string;
}

function get_event_by_term($term) {
	$events = [];
	// Scheduled
	$event_type = get_term_by('id', $term, 'event-type');
	$args = [
		'post_type'      => ['event'],
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'post_status' => 'publish',
		'order' => 'ASC',
		'tax_query' => [
      [
	      'taxonomy' => 'event-type',
	      'field'    => 'id',
	      'terms'    => $term,
      ]
    ]
	];

	$events_query = new WP_Query( $args );
	if ( $events_query->have_posts() ) :
		while ( $events_query->have_posts() ) : $events_query->the_post();
			$id = get_the_ID();
			$event = get_the_event($id);
			if ( $event->event_types ) {
				$events[$event_type->slug][] = $event;
			}
		endwhile;
		wp_reset_postdata();
	endif;

	// usort($events, "sort_events_by_time");
	return $events;
}

function sort_events_by_time($a, $b)
{
    return strcmp($a->start_time, $b->start_time);
}

function get_todays_events ($date) {
	$args = [
		'post_type' => ['event', 'page'],
		'posts_per_page' => -1,
		'meta_query' => [
			'relation' => 'OR',
			[
				'relation' => 'AND',
	      [
	        'key'           => 'event_date',
	        'compare'       => '<=',
	        'value'         => $date,
					'type' 					=> 'date'
	      ],
				[
	        'key'           => 'event_date',
	        'compare'       => '=>',
	        'value'         => $date,
					'type' 					=> 'date'
	      ]
			],
			[
				'key' 						=> 'event_dates_$_date',
				'compare' 				=> '=',
				'value' 					=> $date,
				'type' 						=> 'date'
			]
    ],
		'post_status' => 'publish'
	];
	$event_objs = get_the_events($args);
	usort($event_objs, "sort_events_by_time");
	return $event_objs;
}

function get_events_by_category ($cat) {
	$args = [
		'post_type' => 'event',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'tax_query' => [
			[
				'taxonomy' => 'event-type',
				'field' => 'slug',
				'terms' => $cat,
				'operator' => 'IN',
			]
		],
		'meta_query' => [
      [
        'key'           => 'scheduled_or_durational',
        'operator' 			=> 'IN',
        'value'         => 'durational',
      ]
    ]
	];
	$event_objs = get_the_events($args);
	return $event_objs;
}

function get_the_events ($args) {
	$event_objs = [];
	$events_query = new WP_Query( $args );
	if ( $events_query->have_posts() ) :
		while ( $events_query->have_posts() ) : $events_query->the_post();
			$event_length = get_field('event_length');
			if ( $event_length == 'multiple' ) {
				if ( $event_dates = get_field('event_dates') ) {
					foreach( $event_dates as $event_date ) {
						$event_obj = get_the_event( get_the_id() );
						$event_obj->date = $event_date['date'];
						$event_obj->start_time = $event_date['start_time'] ? $event_date['start_time'] : null;
						$event_obj->end_time = $event_date['end_time'] ? $event_date['end_time'] : null;
						array_push($event_objs, $event_obj);
					}
				}
			}
			if ( $event_length == 'single' ) {
				$event_obj = get_the_event( get_the_id() );
				array_push($event_objs, $event_obj);
			}

		endwhile;
		// usort($ongoing_events, "sort_events_by_time");
	endif;
	return $event_objs;
}

function get_the_event ($id) {
	$post_type = get_post_type($id);
	$event_obj = (object)[];
	$ongoing_events = [];
	$event_hoods_arr = [];
	$featured_image = get_the_post_thumbnail_url( $id, 'small-medium' );
	if ( $featured_image ) {
		$event_obj->featured_image = $featured_image;
	}
	$list_image = get_field('listing_image', $id);
	if ( $list_image ) {
		$event_obj->listing_image = $list_image['sizes']['small-medium'];
	}
	//$formattedDate = date('l, F jS',$realDate);
	if ( $post_type == 'page' ) {
		$event_obj->intro = true;
	}
	$event_excerpt = get_field('event_short_description', $id);
	$event_permalink = basename(get_the_permalink($id));
	$event_obj->title = get_the_title($id);
	$event_obj->id = $id;
	$event_obj->virtual = get_field('virtual_event', $id) == 'virtual';
	if ( get_field('instagram_event', $id) ) {
		$event_obj->instagram = true;
	}
	$event_obj->permalink = $event_permalink;
	$event_obj->hide_from_listing = get_field('hide_from_listing', $id);
	$event_obj->start_time = get_field('opening_time', $id) ? format_event_date(get_field('opening_time', $id)) : null;
	$event_obj->end_time = get_field('closing_time', $id) ? format_event_date(get_field('closing_time', $id)) : null;
	$event_obj->excerpt = $event_excerpt;
	$event_obj->instagram_event = get_field('instagram_event', $id);
	$event_obj->location = get_field('event_location', $id);
	$event_obj->listing_button_label = get_field('event_listing_label', $id) ? get_field('event_listing_label', $id) : false;
	$event_length = get_field('event_length');
	switch ( $event_length ) {
		case 'single':
			$event_obj->date = get_field('event_date', $id);
		break;
		case 'multiple':
			$event_dates_length = intval(get_post_meta($id, 'event_dates', true));
			$event_dates = [];
			for($i = 0; $i < $event_dates_length; $i++) {
				$meta_key_date = 'event_dates_'.$i.'_date';
				$meta_key_start_time = 'event_dates_'.$i.'_start_time';
				$meta_key_end_time = 'event_dates_'.$i.'_end_time';
				$event_date = get_post_meta($id, $meta_key_date, true);
				$event_start_time = get_post_meta($id, $meta_key_start_time, true);
				$event_end_time = get_post_meta($id, $meta_key_end_time, true);
				array_push($event_dates, array(
					'event_date' => strlen($event_date) > 5 ? (substr($event_date, 0, 4).'-'.substr($event_date, 4, 2).'-'.substr($event_date, 6, 2)) : '',
					'start_time' => $event_start_time,
					'end_time'	=> $event_end_time
				));
			}
			$event_obj->dates = $event_dates;
		break;
		case 'range':
			$event_obj->start_date = get_field('start_date', $id);
			$event_obj->end_date = get_field('end_date', $id);
		break;
	}
	if( $rsvp_link = get_field('rsvp_link', $id) ) {
		$event_obj->rsvp = $rsvp_link;
	}
	if( $event_link = get_field('event_link', $id) ) {
		$event_obj->link = $event_link;
	}
	if ( $subtitle = get_field('event_subtitle', $id) ) {
		$event_obj->subtitle = $subtitle;
	}
	$event_obj->scheduled_or_durational = get_field('scheduled_or_durational', $id);
	if( $event_types = get_the_terms( $id, 'event-type' ) ) {
		$event_types_array = array_map( function($event_type) {
			if ( $color = get_field('color', $event_type) ) {
				$event_type->color = $color;
			}
			return $event_type;
		}, $event_types);
		$event_obj->event_types = $event_types_array;
	}
	$supporting_events = [];
	$supporting_events_length = intval(get_post_meta($id, 'supporting_events', true));
	for( $i = 0; $i < $supporting_events_length; $i++ ) {
		$meta_key_support_event_name = 'supporting_events_'.$i.'_supporting_event_name';
		$meta_key_support_event_type = 'supporting_events_'.$i.'_event_type';
		$meta_key_support_event_date = 'supporting_events_'.$i.'_date';
		$meta_key_support_event_start_time = 'supporting_events_'.$i.'_start_time';
		$meta_key_support_event_end_time =  'supporting_events_'.$i.'_end_time';
		$meta_key_support_event_url =  'supporting_events_'.$i.'_supporting_url';
		$meta_key_support_event_category =  'supporting_events_'.$i.'_event_category';
		$meta_key_support_location =  'supporting_events_'.$i.'_location';
		$meta_key_support_virtual =  'supporting_events_'.$i.'_virtual';
		$support_event_name = get_post_meta($id, $meta_key_support_event_name, true);
		$support_event_type = get_post_meta($id, $meta_key_support_event_category, true);
		$support_event_date = get_post_meta($id, $meta_key_support_event_date, true);
		$support_start_time = get_post_meta($id, $meta_key_support_event_start_time, true);
		$support_end_time = get_post_meta($id, $meta_key_support_event_end_time, true);
		$support_url = get_post_meta($id, $meta_key_support_event_url, true);
		$support_category = get_post_meta($id, $meta_key_support_event_category, true);
		$support_location = get_post_meta($id, $meta_key_support_location, true);
		$support_virtual = get_post_meta($id, $meta_key_support_virtual, true);
		$support_event_term = get_term( $support_event_type );
		$support_event_obj = array(
			"title" => $support_event_name,
			"permalink" => $event_permalink,
			"supporting_permalink" => slugify($support_event_name),
			"excerpt" => $event_excerpt,
			"supporting" => true,
			"virtual" => $support_virtual == 'virtual'
		);
		if ( $support_event_date ) {
			$support_event_obj['date'] = strlen($support_event_date) > 5 ? (substr($support_event_date, 0, 4).'-'.substr($support_event_date, 4, 2).'-'.substr($support_event_date, 6, 2)) : '';
		}
		if ( $support_start_time ) {
			$support_event_obj['start_time'] = format_event_date($support_start_time);
		}
		if ( $support_end_time ) {
			$support_event_obj['end_time'] = format_event_date($support_end_time);
		}
		if ( $list_image ) {
			$support_event_obj['listing_image'] = $list_image['sizes']['small-medium'];
		}
		if ( $support_event_term ) {
			if ( $color = get_field('color', $support_event_term) ) {
				$support_event_term['color'] = $color;
			}
			$support_event_obj['event_types'] = [$support_event_term];	
		}

		if ( $support_url ) {
			$support_event_obj['url'] = $support_url;
		}
		if ( $support_location ) {
			$support_event_obj['location'] = $support_location;
		}
		if ( $support_category ) {
			$support_event_obj['category'] = $support_category;
		}
		array_push( $supporting_events, $support_event_obj );
	}
	if ( count($supporting_events) ) {
		$event_obj->supporting_events = $supporting_events;
	}
	return $event_obj;
}

function get_the_intro($day) {
	$event_obj = (object)[];
	$event_obj->intro = true;
	$event_obj->start_time = '12:01am';
	if ( $featured_image = get_field( 'small-medium', $day ) ) {
		$event_obj->featured_image = $featured_image['sizes']['small-medium'];
	}
	$event_obj->title = $day->name;
	$event_obj->excerpt = get_field('excerpt', $day);
	$event_obj->id = $day->term_id;
	$event_obj->date = get_field('event_date', $day);
	$event_obj->hide_from_listing = get_field('hide_from_listing', $day);
	$event_obj->permalink = $day->slug;
	if ( $featured_image = get_field('featured_image', $day) ) {
		$event_obj->featured_image = $featured_image['sizes']['small-medium'];
	}
	return $event_obj;
}

function create_event_item($event, $ongoing=false) {
	ob_start();
	$permalink = $event->permalink;

	$event_classes = [];
	if ( isset($event->event_types) ) {
		foreach ( $event->event_types as $type ) {
			array_push($event_classes, 'type_' . $type->slug);
		}
	}
	if ( isset($event->event_hoods) ) {
		foreach ( $event->event_hoods as $hood ) {
			array_push($event_classes, 'location_' . $hood->slug);
		}
	} ?>
	<div
		class="col-4 event-item new-event <?php echo implode(' ', $event_classes); ?>"
		<?php if($date->slug != 'ongoing-events'){ ?>
			id="event-<?php echo $permalink; ?>"
		<?php } ?>
		data-type="<?php echo $event->event_type ? $event->event_type->slug : ''; ?>"
		data-type-2="<?php echo $event->event_type_2 ? $event->event_type_2->slug : ''; ?>"
		data-location="<?php echo isset($event->event_hoods) ? $event->event_hoods[0]->slug : ''; ?>">
		<?php
		if($date->slug != 'ongoing-events'){ ?>
			<a class="event-anchor" name="<?php echo $permalink; ?>"></a>
		<?php
		}
		if( isset($event->event_type) ) { ?>
			<h6 class="event-type-label"><?php echo $event->event_type->name; ?></h6>
		<?php
		} ?>
		<div class="event-image-container">
			<div class="event-image-content">
				<a
					data-date-id="<?php echo $event->dateID; ?>"
					data-event-id="<?php echo $event->id; ?>"
					data-permalink="<?php echo $permalink; ?>"
					alt="<?php echo $event->title; ?> link"
					href="#"
					class="event-image event-link bg-centered <?php echo isset($event->featured_image) ? 'has-image' : 'no-image'; ?>"
					style="background-image:url(<?php echo isset($event->featured_image) ? $event->featured_image : get_template_directory_uri() . '/images/event-logo.png'; ?>);">

				</a>
			</div>
		</div>
		<?php
		if ( isset($event->event_hoods) ) { ?>
			<h6 class="event-hood"><?php echo $event->event_hoods[0]->name; ?></h6>
		<?php
		} ?>
		<div class="event-title">
			<h3>
				<a
					data-date-id="<?php echo $event->dateID; ?>"
					data-event-id="<?php echo $event->id; ?>"
					data-permalink="<?php echo $permalink; ?>"
					href="#"
					alt="<?php echo $event->title; ?> link"
					class="event-link">
					<span><?php echo $event->title; ?></span>
				</a>
			</h3>
			<?php
			if ( $company = get_user_meta($event->author, 'billing_company') ) { ?>
				<h6><?php echo $company[0]; ?></h6>
			<?php
			} ?>
			<span class="event-loc">
				<?php
				if(isset($event->locText)) {
					echo $event->locText;
				} else {
					echo $event->eventLoc['address'];
				} ?>
			</span>
			<?php
			$event_time = isset($event->altTime) ? $event->altTime : $event->eventTime;
			if ( !$ongoing ) { ?>
				<p style="margin: 0;"><?php echo $event_time; ?></p>
			<?php
			} ?>
		</div>
	</div>
	<?php
	return ob_get_clean();
}

function get_filter_for_day($hoods, $types) {
	ob_start();
	$eventlocs = get_terms([
		'taxonomy' => 'event-location',
		'hide_empty' => true,
	]);
	$eventtypes = get_terms([
		'taxonomy' => 'event-type',
		'hide_empty' => true,
	]); ?>
		<div class="event-filter">
			<div class="event-filter-sentence">
				<a href="#" target="_blank" class="clear-filter">Clear Filter</a>
				<div class="custom-select">
					<h3>Event Type</h3>
					<select class="event-select event-type-select" data-param="type">
						<option value="all">Any event type</option>
						<?php
						foreach($eventtypes as $etype) { ?>
							<option value="<?php echo $etype->slug; ?>"<?php echo !in_array($etype->slug, $types) ? ' disabled' : ''; ?>>
								<?php echo $etype->name; ?>
							</option>
						<?php
						} ?>
					</select>
				</div>
				<div class="custom-select">
					<h3>Neighborhood</h3>
					<select class="event-select event-location-select" data-param="location">
						<option value="all">Any neighborhood</option>
						<?php
						foreach($eventlocs as $eloc) { ?>
							<option value="<?php echo $eloc->slug; ?>"<?php echo !in_array($eloc->slug, $hoods) ? ' disabled' : ''; ?>>
								<?php echo $eloc->name; ?>
							</option>
						<?php
						} ?>
					</select>
				</div>
			</div>
		</div>
	<?php
	echo ob_get_clean();
}

/*-----------------------------*\
  Vendors Functions
\*-----------------------------*/

function get_filterable_content($args) {
	$filterableContent = new WP_Query($args);
	$filterableContentObj = false;
	if($filterableContent->have_posts()) :
		ob_start();
		$filterableContentObj = (object)[ 'more_items' => false, 'args' => $args ];
		$filterableContentArray = [];
		while($filterableContent->have_posts()):
			$filterableObj = (object) [];
			$filterableContent->the_post();
			$slug = slugify(get_the_title());
			$itemImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium');
			$filterableObj->title = get_the_title();
			$filterableObj->slug = $slug;
			$filterableObj->id = get_the_ID();
			$filterableObj->image = $itemImage;
			$filterableObj->link = get_the_permalink();
			if ( $ex_link = get_field('link') ) {
				$filterableObj->ex_link = $ex_link;
			}
			if ( $subtitle = get_field('subtitle') ) {
				$filterableObj->subtitle = $subtitle;
			}
			if ( $gallery = get_field('gallery') ) {
				$filterableObj->gallery = $gallery;
			}
			if ( $gallery = get_field('discover_retail_products') ) {
				$filterableObj->gallery = array_map(function ($item) {
					return $item['image'];
				}, $gallery);
			}
			if ( $gallery = get_field('image_carousel') ) {
				$filterableObj->gallery = $gallery;
			}
			if ( $price = get_field('price') ) {
				$filterableObj->price = $price;
			}
			if ( $materials = get_field('materials') ) {
				$filterableObj->materials = $materials;
			}
			if ( has_excerpt() ) {
				$filterableObj->description = get_the_excerpt();
			}
			if(get_field('vendor_featured')) {
				$filterableObj->featured = true;
			}
			if($locations = get_the_terms( get_the_id(), 'vendor-location' )) {
				$filterableObj->locations = $locations;
			}
			array_push($filterableContentArray, $filterableObj);
		endwhile;
		$filterableContentObj->items = ob_get_clean();
		$filterableContentObj->itemsObj = $filterableContentArray;
		$filterableContentObj->max_pages = $filterableContent->max_num_pages;
		wp_reset_postdata();
	else :
		$filterableContentObj = (object)['args' => $args];
	endif;
	return $filterableContentObj;
}

/*-----------------------------*\
  Team Members
\*-----------------------------*/

function get_team_members($args=false, $desc=true) {
	ob_start();
	$args_terms_id_list = [59,88, 89];
	foreach ($args_terms_id_list as $current_term_id){
    // Get term by id (''term_id'') in Categories taxonomy.
		$m_type = get_term_by('id', $current_term_id , 'member-type');
		$args = array(
			'post_type' => 'team',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => [
				[
					'taxonomy' => 'member-type',
					'field' => 'slug',
					'terms' => $m_type->slug,
					'operator' => 'IN',
				]
			]
		);
		$team_query = new WP_Query($args);
		if( $team_query->have_posts() ) :
			$with_images_array = [];
			$without_images_array = [];
			while( $team_query->have_posts() ): $team_query->the_post();
				$member_object = [
					'name' => get_the_title(),
					'title' => get_field('member_title')
				];
				if( $image = get_field('image_1') ) {
					$member_object['image1'] = $image['sizes']['small-medium'];
					if( $image2 = get_field('image_2') ) {
						$member_object['image2'] = $image2['sizes']['small-medium'];
					}
					array_push($with_images_array, $member_object);
				} else {
					array_push($without_images_array, $member_object);
				}
			endwhile;
			wp_reset_postdata();
			if ( $m_type->slug != 'primary' ) { ?>
				<h2><?php echo $m_type->name; ?></h2>
			<?php
			} ?>
			<div class="team-container team-container-<?php echo $m_type->slug; ?>">
				<?php
				foreach( $with_images_array as $member ) {
					$slug = slugify($member['name']); ?>
					<div class="team-member-container more-info">
						<a class="team-member-anchor" name="<?php echo $member['name']; ?>"></a>
						<div class="team-member-content">
							<div class="member-image-container">
								<a href="<?php echo home_url('the-team'); ?>/#<?php echo $slug; ?>">
									<div class="member-image bg-centered" style="background-image:url(<?php echo $member['image1']; ?>);">
									</div>
									<div class="more-info-cover">
										<span></span>
									</div>
									<?php
									if( $member['image2'] ) { ?>
										<div class="image-2 bg-centered" style="background-image:url(<?php echo $member['image2']; ?>);">
										</div>
									<?php
									} ?>
								</a>
							</div>
							<div class="member-content">
								<a href="<?php echo home_url('the-team'); ?>/#<?php echo $slug; ?>">
									<h3 class="member-name"><?php echo $member['name']; ?></h3>
									<?php
									if( $member['title'] ) { ?>
										<span class="member-title"><?php echo $member['title']; ?></span>
									<?php
									} ?>
								</a>
							</div>
						</div>
					</div>
				<?php
				} ?>
			</div>
			<?php
		endif;
	}

	return ob_get_clean();
}

/*-----------------------------*\
  MAILCHIMP
\*-----------------------------*/

//add_action('init', 'mailchimp_sign_up');

include_once 'mailchimp/MailChimp.php';
use \DrewM\MailChimp\MailChimp;

function mailchimp_sign_up($email){
  $returnObj = (object)array();
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $returnObj->response = get_field('not_real_email', 'option');
    $returnObj->status = 'invalid email';
  }else{ // Is valid email
		$mc_key = get_field('mc_key', 'option');
    $MailChimp = new MailChimp($mc_key);
    $list_id = get_field('mc_list_id', 'option');
    if($MailChimp){
      try {
        $result = $MailChimp->post("lists/" . $list_id . "/members", [
    			'email_address' => $email,
    			'status'        => 'subscribed',
    		]);
        $response = '';
        if($result['status'] == '400') {
          switch($result['title']) {
            case 'Member Exists':
							ob_start(); ?>
							<h2>Thanks for your interest!</h2>
							<p>...it looks like you've already signed up to receive our emails. Please contact us if you think something is wrong: <a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a>.</p>
							<?php
              $returnObj->response = ob_get_clean();
              $returnObj->signedup = 'already signed up';
            break;
            default:
							ob_start(); ?>
							<p>We're sorry, something went wrong during the signup process and we weren't able to add your email address. Please try again!</p>
							<?php
							$returnObj->response = ob_get_clean();
              $returnObj->status = 'something went wrong';
            break;
          }
        }else{
          switch($result['status']) {
            case 'subscribed':
							ob_start(); ?>
							<h1>Thanks for joining our mailing list!</h1>
							<p>You can look forward to receiving all the updates from NYC Jewelry Week!</p>
							<?php
							$returnObj->response = ob_get_clean();
              $returnObj->status = 'successful signup';
            break;
          }
        }
        echo json_encode($returnObj);
      } catch (Exception $e) {
				ob_start(); ?>
				<p>We're sorry, something went wrong during the signup process and we weren't able to add your email address. Please try again!</p>
				<?php
				$returnObj->response = ob_get_clean();
        $returnObj->status = 'invalid email';
        echo json_encode($returnObj);
      }

    }
  }
}

function create_meta_queries($filters) {
	$toolObjs = ['relation' => 'AND'];
	foreach($filters as $filter) {
		$toolID = url_to_postid('/tool/'.$filter);
		$toolObj = [
			'key' => 'tools_x_gardens',
			'compare' => 'LIKE',
			'value' => $toolID
		];
		array_push($toolObjs, $toolObj);
	}
	return $toolObjs;
}

function get_title_length($title) {
	$titleWidth = 'default';
	if(strlen($title) > 12) {
		$titleWidth = 'large-title';
	}
	if(strlen($title) > 15) {
		$titleWidth = 'extra-large-title';
	}
	return $titleWidth;
}

function my_header_add_to_cart_fragment( $fragments ) {

    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php
    }
        ?></a><?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}
// add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

remove_theme_support( 'wc-product-gallery-zoom', 10 );
function clean_commerce_child_custom_woo_fix() {

 		add_filter( 'woocommerce_show_page_title', '__return_true', 1 );
 		add_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 6 );
 }

 add_action( 'init', 'clean_commerce_child_custom_woo_fix' );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

// Gallery action

function gallery_to_slick( $output = '', $atts = null, $instance = null ) {
	$return = $output; // fallback
	$images = $atts['ids'];
	$images = explode(',', $images);
	ob_start(); ?>
	<div class="wp-gallery" data-count="<?php echo count($images); ?>">
		<div class="wp-gallery-images-container">
			<div class="wp-gallery-images">
				<?php
				foreach($images as $image) {
					$image = wp_get_attachment_image_src($image, 'medium'); ?>
					<div class="wp-gallery-img bg-centered" style="background-image:url(<?php echo $image[0]; ?>);">
						<?php
						if($caption = wp_get_attachment_caption($image)) { ?>
							<div class="wp-gallery-img-caption">
								<?php echo $caption; ?>
							</div>
						<?php
						} ?>
					</div>
				<?php
				} ?>
			</div>
		</div>
		<div class="wp-gallery-nav">
			<div class="wp-gallery-arrow arrow-prev">
			</div>
			<div class="wp-gallery-nav-images">
				<?php
				foreach($images as $image) {
					$image = wp_get_attachment_image_src($image, 'thumbnail'); ?>
					<div class="wp-gallery-nav-img">
						<img src="<?php echo $image[0]; ?>"/>
					</div>
				<?php
				} ?>
			</div>
			<div class="wp-gallery-arrow arrow-next">
			</div>
		</div>
	</div>
	<?php
	$return = ob_get_clean();

	return $return;
}

add_filter( 'post_gallery', 'gallery_to_slick', 10, 3 );


// Limit event submission description length

add_filter( 'caldera_forms_field_attributes', function( $attrs, $field, $form ){
	if( 'fld_7683514' === $field[ 'ID' ] && 'paragraph' === Caldera_Forms_Field_Util::get_type( $field, $form ) ){
		$attrs[ 'maxlength' ] = 1500;
	}
	return $attrs;
}, 20, 3 );

// Limit file upload size

add_filter( 'upload_size_limit', function( $size ) {
    //Check if Caldera Forms submission
    if( isset( $_POST, $_POST['_cf_frm_id'] ) ){
        // Set the upload size limit to 10 MB for users lacking the 'manage_options' capability.
        if ( ! current_user_can( 'manage_options' ) ) {
            // 10 MB.
            $size = 1024 * 20000;
        }
    }
    return $size;
} );


add_action('wp_ajax_get_event_by_id', 'get_event_by_id');
add_action('wp_ajax_nopriv_get_event_by_id', 'get_event_by_id');

function get_event_by_id() {
	$post_id = $_REQUEST['post_id'];
	$event = get_post($post_id);
	log_it($event);
	$event_title = $event->post_title;
	$event_subtitle = get_post_meta($post_id, 'event_subtitle', true);
	$event_description = get_post_meta($post_id, 'description', true);
	$event_dates_length = intval(get_post_meta($post_id, 'event_dates', true));
	$virtual_event = get_post_meta($post_id, 'virtual_event', true);
	$event_url = get_post_meta($post_id, 'event_url', true);
	$project_website_url = get_post_meta($post_id, 'project_website_url', true);
	$event_location = get_post_meta($post_id, 'event_location', true);

	$event_dates = [];
	for($i = 0; $i < $event_dates_length; $i++) {
		$meta_key_date = 'event_dates_'.$i.'_date';
		$meta_key_start_time = 'event_dates_'.$i.'_start_time';
		$meta_key_end_time = 'event_dates_'.$i.'_end_time';
		$event_date = get_post_meta($post_id, $meta_key_date, true);
		$event_start_time = get_post_meta($post_id, $meta_key_start_time, true);
		$event_end_time = get_post_meta($post_id, $meta_key_end_time, true);
		array_push($event_dates, array(
			'event_date' => strlen($event_date) > 5 ? (substr($event_date, 0, 4).'-'.substr($event_date, 4, 2).'-'.substr($event_date, 6, 2)) : '',
			'start_time' => $event_start_time,
			'end_time'	=> $event_end_time
		));
	}
	$participants = [];
	$participants_length = intval(get_post_meta($post_id, 'participants', true));
	for($i = 0; $i < $participants_length; $i++) {
		$meta_key_name = 'participants_'.$i.'_name';
		$meta_key_bio = 'participants_'.$i.'_bio';
		$participant_name = get_post_meta($post_id, $meta_key_name, true);
		$participant_bio = get_post_meta($post_id, $meta_key_bio, true);
		array_push($participants, array(
			'name' => $participant_name,
			'bio' => $participant_bio
		));
	}

	$supporting_events = [];
	$supporting_events_length = intval(get_post_meta($post_id, 'supporting_events', true));
	for($i = 0; $i < $supporting_events_length; $i++) {
		$meta_key_support_event_name = 'supporting_events_'.$i.'_supporting_event_name';
		$meta_key_support_event_type = 'supporting_events_'.$i.'_event_type';
		$meta_key_support_event_date = 'supporting_events_'.$i.'_date';
		$meta_key_support_event_start_time = 'supporting_events_'.$i.'_start_time';
		$meta_key_support_event_end_time =  'supporting_events_'.$i.'_end_time';;
		$support_event_name = get_post_meta($post_id, $meta_key_support_event_name, true);
		$support_event_type = get_post_meta($post_id, $meta_key_support_event_type, true);
		$support_event_date = get_post_meta($post_id, $meta_key_support_event_date, true);
		$support_start_time = get_post_meta($post_id, $meta_key_support_event_start_time, true);
		$support_end_time = get_post_meta($post_id, $meta_key_support_event_end_time, true);
		$support_event_term = get_term( $support_event_type );
		array_push($supporting_events, array(
			"event" => $support_event_name,
			"date" =>  strlen($support_event_date) > 5 ? (substr($support_event_date, 0, 4).'-'.substr($support_event_date, 4, 2).'-'.substr($support_event_date, 6, 2)) : '',
			"event_types" => array(
				'id' => $support_event_type,
				'name' => $support_event_term->name
			),
			"start_time" => $support_start_time,
			"end_time" => $support_end_time
		));
	}

	array_push($supporting_events, array(
		"event" => '',
		"date" => '',
		"event_types" => [],
		"start_time" => '',
		"end_time" => ''
	));

	$rsvp_link = get_post_meta($post_id, 'rsvp_link', true);
	$event_image_id = get_post_meta($post_id, 'event_image', true);
	$listing_image_id = get_post_meta($post_id, 'listing_image', true);
	$event_image = wp_get_attachment_image_src($event_image_id);
	$listing_image = wp_get_attachment_image_src($listing_image_id);
	$event_type_id = get_post_meta($post_id, 'event_type', true);
	$event_type = get_term( $event_type_id );
	$event_types = [];
	array_push($event_types, [
		'id' => $event_type_id,
		'name' => $event_type->name
	]);

	$event_data = array(
		'title' => $event_title,
		'subtitle' => $event_subtitle,
		'description' => $event_description,
		'event_dates' => $event_dates,
		'participants' => $participants,
		'event_image' => $event_image,
		'event_types' => $event_types,
		'listing_image' => $listing_image,
		'rsvp_link' => $rsvp_link,
		'supportEvents' => $supporting_events,
		'covid_messaging' => get_field('covid_messaging', 'option'),
		'virtual_event' => intval($virtual_event) == 1,
		'event_url'=> $event_url,
		'project_website_url' => $project_website_url,
		'event_location' => $event_location
	);
	echo json_encode($event_data);
	die();
	// return json_encode($event);
}
