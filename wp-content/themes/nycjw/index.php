<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
				$args = array(
					'numberposts' => -1,
					'post_type' => 'post',
					'meta_query' => array(
						array(
							'key' => 'featured_article',
							'value' => true,
							'compare' => 'IN'
						)
					)
				);
				$the_query = new WP_Query($args);
				if ($the_query->have_posts()): ?>
			<section id="featured-articles">
					<div class="featured_article arrow-prev">
						<svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
							<path d="M27.0119 53L31 49.0123L8.00456 25.9909L30.01 4.01601L25.9936 -4.37672e-07L2.36121e-06 25.9909L27.0119 53Z" fill="#FCF250"/>
						</svg>
					</div>
					<div class="article-repeater">
					<?php	while( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="featured-article-wrapper" style="background-image: url(<?= get_the_post_thumbnail_url();?>)">
							<div class="thumbnail-wrapper">
								<?php
									$categories = get_the_category();
									if (sizeof($categories) > 0):
								?>
									<div class="category-wrapper">
										<span class="category">
											<?=$categories[0]->name;?>
										</span>
									</div>
								<?php
									the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;
								?>

							</div>
						</div>
					<?php endwhile; ?>
					</div>
					<div class="featured_article arrow-next">
						<svg xmlns="http://www.w3.org/2000/svg" width="31" height="53" viewBox="0 0 31 53" fill="none">
							<path d="M3.98814 0L0 3.98773L22.9954 27.0091L0.989963 48.984L5.00639 53L31 27.0091L3.98814 0Z" fill="#FCF250"/>
						</svg>
					</div>
			</section>
			<?php
				endif;
			?>
			<section class="middle-logo">
				<div class="middle-logo-image">
					<img src="<?php echo get_template_directory_uri(); ?>/images/futureheirloom.svg"/>
				</div>
			</section>
			<?php
			if ( have_posts() ) :
			$pw = post_password_required(); ?>
			<section class="section">
				<div class="section-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
					<div id="page-content" class="<?=(is_home() && ! is_front_page()) ? 'blogs-wrapper' : ''; ?>">
						<?php
						$blog_id = get_option( 'page_for_posts' );
						if ( !(is_home() && ! is_front_page()) ) :
							echo get_field( 'blog_content', $blog_id );
						endif;
						/* Start the Loop */
						while ( have_posts() ) : ?>
								<?php
								the_post(); ?>
								<div>
								<?php
								/*
								 * Include the Post-Type-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								 */
								if ( is_home() && ! is_front_page() ) :
									get_template_part( 'template-parts/content-single', get_post_type() );
								else:
									get_template_part( 'template-parts/content', get_post_type() );
								endif;
								?>
							</div>
						<?php
						endwhile;
						if ( is_home() && ! is_front_page() ) :
							the_posts_pagination(
								array(
									'prev_text'          => __( 'Previous', 'cm' ),
									'next_text'          => __( 'Next', 'cm' ),
									'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'cm' ) . ' </span>',
								)
							);
						else:
							the_posts_navigation();
						endif;

						?>
					</div>
				</div>
			</section>
		<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
