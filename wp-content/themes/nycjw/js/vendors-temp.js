jQuery.noConflict();
(function( $ ) {
  $(function() {
    var $vendorsGrid;

    // Vendors Functions

    let vendorFilter = {
      page : 1,
      filter : {},
      more : $('.more-vendors').data('more')
    };

    if($('#vendors').length) {
      $('#vendors').imagesLoaded( {}, function() {
        $vendorsGrid = $('#vendors').isotope({
          // options
          itemSelector: '.vendor',
          percentPosition: true,
          layoutMode: 'packery',
          packery: {
            // use outer width of grid-sizer for columnWidth
            //columnWidth: '.vendor',
            gutter: '.vendors-gutter',
          }
        });
        $('.vendor').removeClass('new-vendor');
        $vendorsGrid.isotope('layout');
      });
    }
    if($('#vendor-filters').length) {

      $('.filter-option').on('mouseenter', function() {
        $('.filter-option').removeClass('active');
        $(this).addClass('active');
      })
      $('body').click(function(evt){
       if(evt.target.id != 'vendor-filters' && $('.filter-options').is(':visible') && $(evt.target).closest('#vendor-filters').length == 0) {
         closeFilterOptions();
       }
      });
      $('.vendor-filter').each(function() {
        let $filter = this;

        // Clear filter
        $('.filter-clear', $filter).on('click', function() {
          clear_filter($filter);
        });

        // Placeholder Click function
        $('.filter-placeholder', $filter).on('click', function() {
          if(!$('.filter-options', $filter).is(':visible')) {
            $('.vendor-filter').each(function() {
              if($('.filter-options', this).is(':visible')) {
                closeFilterOptions(this);
              }
            })
            $('input', $filter).focus();
            $('.filter-options', $filter).slideDown('fast');

            // Set the current filter item as active or the first one
            if(vendorFilter.filter[$($filter).data('filter')]) {
              $('.filter-option', $filter).removeClass('active');
              $('.filter-option.option-'+vendorFilter.filter[$($filter).data('filter')], $filter).addClass('active');
            } else {
              $($filter).find('.filter-option:visible:first').addClass('active')
            }
          } else {
            closeFilterOptions($filter);
          }
        });

        $('input', $filter).keypress(function (e) {
          var key = e.which;
          if(key == 13) { // the enter key code
            $('.filter-option.active', $filter).click()
          }
        });

        $('input', $filter).on('change paste keyup', function(e) {
          if(e.keyCode === 13) {
            return
          }
          let thisInput = this;
          let inputVal = $(this).val();
          // If user has typed something
          if($(this).val() !== '') {
            $('.filter-option', $filter).each(function(f, filteritem) {
              $('.filter-placeholder', $filter).css('opacity', 0);
              $('.filter-value', $filter).hide()
              let itemVal = $(filteritem).data('value')
              let searchIn = $(thisInput).hasClass('search-in')
                ? itemVal.toLowerCase().indexOf(inputVal.toLowerCase()) >= 0
                : itemVal.toLowerCase().startsWith(inputVal)
              if(searchIn) {
                $(filteritem).show();
              } else {
                $(filteritem).hide();
              }
            });
            if( $('.filter-option:visible', $filter).length === 0 ) {
              $('.filter-option-none', $filter).show()
            } else {
              $('.filter-option-none', $filter).hide()
            }
            $($filter).find('.filter-option:visible:first').addClass('active')
          } else { // input is empty
            $('.filter-placeholder', $filter).css('opacity', 1);
            $('.filter-option', $filter).show()
            $('.filter-option-none', $filter).hide()
            $('.filter-clear', $filter).hide()
          }
        });
        $('.filter-option', $filter).on('click', function() {
          vendorFilter.filter[$($filter).data('filter')] = $(this).data('slug')
          vendorFilter.page = 1
          $('.filter-placeholder', $filter).css('opacity', 0);
          $('.filter-clear', $filter).show()
          $('.filter-value', $filter).html($(this).data('value')).show();
          closeFilterOptions();
          filterVendors($filter);
        });
      });
    }

    function clear_filter($filter) {
      $('.filter-option', $filter).removeClass('active')
      $('.filter-placeholder', $filter).css('opacity', 1)
      $('.filter-value', $filter).hide()
      $('.filter-clear', $filter).hide()
      delete vendorFilter.filter[$($filter).data('filter')]
      filterVendors()
    }

    function closeFilterOptions($filter) {
      $('.filter-options', $filter).slideUp('fast');
      $('input', $filter).focusout('fast');
      $('input', $filter).val('')
      if($('.filter-option-none', $filter).is(':visible')) {
        $('.filter-option', $filter).show()
        $('.filter-option-none', $filter).hide()
        // if user has tax selected but looks for another one and its not found and then clicks off, show the original one and keep it in the filters
        if(vendorFilter.filter[$($filter).data('filter')]) {
          $('.filter-value', $filter).show()
          $('.filter-placeholder', $filter).css('opacity', 0)
        }
      }
    }

    function filterVendors() {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'filter_vendors',
          'filter' : vendorFilter
        }
      }).done( function (response) {
        response = $.parseJSON(response);
        vendorFilter.more = response.more_vendors ? true : false;
        if(vendorFilter.page == 1) {
          $vendorsGrid.isotope( 'remove', $vendorsGrid.find('.vendor') );
        }
        if(response.vendors) {
          $('.no-vendors').hide();
          let $newVendors = $($.parseHTML(response.vendors)).filter('*');
          $vendorsGrid.append($newVendors)
          $('#vendors').imagesLoaded( {}, function() {
            $vendorsGrid.isotope( 'appended', $newVendors );
            $vendorsGrid.isotope('layout');
            $('.vendor.new-vendor').removeClass('new-vendor')
          });
        } else { // No Vendors
          $vendorsGrid.isotope('layout');
          $('.no-vendors').show();
        }

        loading = false;
      });
    }

    $(window).scroll(function(){
      if($('#vendors').length) {
        if(($(window).scrollTop() + window.innerHeight > $(document).height() - 100) && vendorFilter.more && !loading) {
          loading = true;
          vendorFilter.page++;
          filterVendors();
        }
      }
    });

  });
})(jQuery);
