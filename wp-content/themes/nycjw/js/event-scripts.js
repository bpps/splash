import $ from 'jquery';

if ( $('#event-owner-events').length ) {
  if ( $('.event-owner-list-item').length ) {
    $('.event-owner-list-item .edit-event').on('click', function() {
      $('#event-owner-instructions').hide();
      $('#event-owner-instructions').hide();
      $('.event-form #message p').hide();
      const eventId = $(this).data('event-id')
      $('.event-owner-list-item').removeClass('active')
      $('.event-list-item-' + eventId).addClass('active')
      $('#new-event-form').hide()
      $('.acf-form').hide()
      $('.event-' + eventId).show();
      $('.event-' + eventId + ' .acf-form').slideDown();
      return
    })
  }
  $('.create-an-event').on('click', () => {
    $('#event-owner-instructions').hide();
    $('.event-form #message p').hide();
    $('.event-owner-list-item').removeClass('active')
    $('.event-owner-event').hide()
    $('#new-event-form').show();
    $('#new-event-form .acf-form').slideDown();
    return
  })
}

if ( $('.event-item').length ) {
  // If site starts with event drawer open, populate the map
  if ( $('#nycjw-event-map').length ) {
    var location = [$('#nycjw-event-map-wrapper').data('lng'), $('#nycjw-event-map-wrapper').data('lat')]
    display_event_location(location)
  }
  $('.event-type-container:first-of-type .event-day-container').slideDown()
  $('.event-type-container h2, .date-arrow').on('click', function() {
    let $eventContainer = $(this).closest('.event-type-container')
    $($eventContainer).toggleClass('active')
    if ( $('.event-day-container', $eventContainer).is(':visible') ) {
      $('.event-day-container', $eventContainer).slideUp()
    } else {
      $('.event-day-container', $eventContainer).slideDown()
    }
  })

  $('#event-drawer-wrapper, #close-events-drawer').on('click', function(e) {
    if ( $(e.target).is('#event-drawer-wrapper') || $(e.target).is('#close-events-drawer') ) {
      $('#event-drawer-wrapper').removeClass('active')
      $('body').css('overflow', 'auto')

      window.history.replaceState('', '', removeParameter(window.location.href, 'nycjw-event'))
    }
  })
  var currentEvent = false;
  $('.event-link').on('click', function(e){
    e.preventDefault();
    $('#event-drawer-wrapper').addClass('active');
    $('body').css('overflow', 'hidden')
    const eventId = $(this).data('event-id')
    const dateId = $(this).data('date-id')
    if ( eventId !== currentEvent ) {
      $('#event-drawer-content').html(`
        <div class="loader-container">
          <div class="jw-submit-loader">
            <div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
          </div>
        </div>
      `);
      window.history.replaceState('', '', updateURLParameter(window.location.href, 'nycjw-event', $(this).data('permalink')))
      currentEvent = eventId
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'get_event_content',
          'id' : eventId,
          'date-id' : dateId,
          'map' : true
        }
      }).done( function (response) {
        response = $.parseJSON(response)
        if(response) {
          $('#event-drawer-content').html(response.event);
          if ( response.location ) {
            display_event_location([response.location.lng, response.location.lat]);
          }
        }
      });
    }
  });
  // $('.event-type-container:first-of-type').addClass('active');
  // if($('.event-type-container:first-of-type').height() < $(window).height()) {
  //   $('.event-type-container:nth-child(2)').addClass('active');
  // }
  //setTimeout(function() {
    $('.new-event').each(function(){
      $(this).removeClass('new-event');
    });
  //}, 200);
  // if ( window.location.hash ) {
  //   let hash = window.location.hash.substring(1);
  //   if($('#event-'+hash).length) {
  //     $('#event-'+hash+' .event-title').click();
  //   }
  // }
}

function display_event_location(location) {
  //$.get( `https://api.mapbox.com/geocoding/v5/mapbox.places/${location}.json?types=address&access_token=pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw`, function( data ) {
    mapboxgl.accessToken = 'pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw';
    const map = new mapboxgl.Map({
      container: 'nycjw-event-map',
      style: 'mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q',
      center: location,
      zoom: 13,
      interactive: false,
    });
    var el = document.createElement('div')
    el.setAttribute('id', 'nycjw-marker')
    $(el).html(`
      <span class="event-icon"><img src="${templateurl}/images/icon-single.png"/></span>
    `)
    map.on('load', function() {
      var $marker = new mapboxgl.Marker(el).setLngLat(location).addTo(map);
    })
  //});
}

function loadMoreEvents($el, postsAdded) {
  var $el = $('.load-more');
  var paged = $($el).data('paged');
  var postCount = $($el).data('count');
  var currentDay = $($el).data('date');
  // var filterArray = null;
  // if($('.filter-btn').length) {
  //   filterArray = [];
  //   $('.filter-btn.active').each(function(){
  //     if($(this).data('filter') != 'all') {
  //       filterArray.push($(this).data('filter'));
  //     }
  //   });
  // }
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'get_more_events',
      'paged' : paged,
      'count' : postCount,
      'currentday' : currentDay,
      //'filter' : filterArray
    }
  }).done( function (response) {
    response = $.parseJSON(response);
    if(response.posts) {
      $('#events-container').append(response.posts);
      $($el).data('paged', paged+1);
      $($el).data('date', response.date);
      postsAdded(response.more);
    }
    if(!response.more) {
      $('.load-more').hide();
    }
  });
}
var loading = false;

// if($('.event-type-container').length) {
//   checkEventPosition();
// }
if($('.event-filter').length) {
  $('.event-select').on('change', function(){
    const $eventContainer = $(this).closest('.event-type-container')
    filterEvents($eventContainer);
  });
  // Clear filter button
  $('.clear-filter').on('click', function(e) {
    e.preventDefault();
    const $eventContainer = $(this).closest('.event-type-container')
    $('.event-select', $eventContainer).each(function(){
      $(this).val('all');
    });
    filterEvents($eventContainer)
  });

}

function filterEvents($container) {
  var filterActive = false;
  $('.event-item', $container).show();
  $('.event-select', $container).each(function() {
    var selectParam = $(this).data('param');
    var selectVal = $(this).val();
    if ( selectVal === 'all') {
      return
    }
    filterActive = true;
    $('.event-item', $container).each(function(){
      if( !$(this).hasClass(selectParam + '_' + selectVal) ) {
        $(this).hide();
      }
    });
  })
  if ( filterActive ) {
    $('.clear-filter', $container).css('opacity', 1)
  } else {
    $('.clear-filter', $container).css('opacity', 0);
  }
}

function removeParameter(url, param) {
  var tempArray = url.split("?");
  var baseURL = tempArray[0];
  var paramURL = tempArray[1];
  var paramArray = paramURL.split("&");
  var newParamURL = '';
  var temp = '';
  if(paramArray.length > 1) {
    for (var i=0; i<paramArray.length; i++) {
      if(paramArray[i].split('=')[0] != param) {
        newParamURL += temp + paramArray[i];
        temp = '&';
      }
    }
    return baseURL + '?' + newParamURL;
  } else {
    return baseURL;
  }
}

function updateURLParameter(url, param, paramVal){
  var newAdditionalURL = "";
  var tempArray = url.split("?");
  var baseURL = tempArray[0];
  var additionalURL = tempArray[1];
  var temp = "";
  if (additionalURL) {
    tempArray = additionalURL.split("&");
    for (var i=0; i<tempArray.length; i++){
      if(tempArray[i].split('=')[0] != param){
        newAdditionalURL += temp + tempArray[i];
        temp = "&";
      }
    }
  }

  var rows_txt = temp + "" + param + "=" + paramVal;
  return baseURL + "?" + newAdditionalURL + rows_txt;
}

// $(window).scroll(function(){
//   if($('.event-type-container').length) {
//     checkEventPosition();
//   }
// });

function checkEventPosition() {
  $('.event-type-container').each(function() {
    var $theseEvents = $(this);
    if(!$($theseEvents).hasClass('active')) {
      var toScroll = $(document).scrollTop() + $(window).height() - 100;
      if($($theseEvents).offset().top < toScroll) {
        $($theseEvents).addClass('active');
      }
    }
  });
}

// function add_event_image_permissions (response) {
//   console.log('anything?')
//   console.log(response)
// }
