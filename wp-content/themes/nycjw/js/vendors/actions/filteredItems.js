export const LOAD_DATA = 'LOAD_DATA'
export const SET_DATA = 'SET_DATA'
export const DATA_ERROR = 'DATA_ERROR'
export const SET_PAGE = 'SET_PAGE'

export const loadData = () => {
  return {
    type: LOAD_DATA,
  }
}

export const setData = (items, hasMore) => {
  return {
    type: SET_DATA,
    items,
    hasMore,
  }
}

export const setPage = (page) => {
  return {
    type: SET_PAGE,
    page
  }
}

export const dataError = (error) =>{
  return {
    type: DATA_ERROR,
    error
  }
}
