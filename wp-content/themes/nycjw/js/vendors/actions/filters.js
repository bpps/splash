export const SET_ACTIVE_FILTER = 'SET_ACTIVE_FILTER'
export const SELECT_TERM = 'SELECT_TERM'
export const CLEAR_TERMS = 'CLEAR_TERMS'

export const clearTerms = () => {
  return {
    type: CLEAR_TERMS
  }
}
export const selectTerm = (tax, term) => {
  return {
    type: SELECT_TERM,
    tax,
    term
  }
}

export const setActiveFilter = (filter) => {
  return {
    type: SET_ACTIVE_FILTER,
    filter
  }
}
