import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Options from './Options'
import { connect } from 'react-redux'
import { selectTerm, setActiveFilter } from '../actions/filters'

const FilterValue = (props) => {
  return (
    <div className="filter-value">
      {props.selected && props.search === '' && props.selected}
    </div>
  )
}

FilterValue.propTypes = {
  search: PropTypes.string.isRequired,
  selected: PropTypes.string
}

const ClearFilter = (props) => {
  return <div onClick={() => props.onClear()} className="filter-clear"></div>
}

ClearFilter.propTypes = {
  onClear: PropTypes.func.isRequired
}

const Placeholder = (props) => {
  return (
    <div onClick={() => props.onClick()} className="filter-placeholder">
      {props.children}
    </div>
  )
}

Placeholder.propTypes = {
  onClick: PropTypes.func.isRequired
}

const Input = (props) => {
  return (
    <input
      value={props.value}
      onChange={(event) => props.onChange(event)}
      ref={(input) => props.setInput(input)} />

  )
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  setupInput: PropTypes.func
}

class Filter extends Component {
  constructor (props) {
    super(props)
    this.state = {
      inputFocus: false,
      search: '',
      activeSelect: null,
      selected: null,
      terms: []
    }
    this.handlePlaceholderClick = this.handlePlaceholderClick.bind(this)
    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.closeOptions = this.closeOptions.bind(this)
    this.handleClear = this.handleClear.bind(this)
    this.setInput = this.setInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  setInput (input) {
    this.textInput = input
  }

  handleChange (e) {
    const searchVal = e.target.value
    let terms = this.props.filter.terms
    if (searchVal != '') {
      switch(this.props.filter.search) {
        case 'search-in':
          terms = terms.filter( term => {
            return term.name.toLowerCase().indexOf(searchVal.toLowerCase()) >= 0
          })
        break;
        case 'search-start':
          terms = terms.filter( term => {
            return term.name.toLowerCase().startsWith(searchVal.toLowerCase())
          })
        break;
      }
    }
    this.setState({
      search: searchVal,
      terms: terms,
      activeSelect: terms.length > 0 && {
        name: terms[0].name,
        slug: terms[0].slug
      }
    })
  }

  handlePlaceholderClick () {
    const terms = this.props.filters[this.props.tax].terms
    this.setState({
      inputFocus: true,
      terms: terms,
      activeSelect: this.state.selected != null ? this.state.selected : terms[0]
    })
    this.textInput.focus()
  }

  handleMouseEnter (term) {
    this.setState({
      activeSelect: {
        name: term.name,
        slug: term.slug
      }
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    this.handleClick()
  }

  handleClick (selected) {
    // tax, term
    this.setState({
      selected: selected,
      search: ''
    }, () => {
      this.props.dispatch(selectTerm(this.props.tax, selected.slug))
      this.closeOptions()
    })
  }

  closeOptions () {
    this.textInput.blur()
    this.setState({
      inputFocus: false
    })
  }

  handleClear () {
    this.setState({
      inputFocus: false,
      search: '',
      activeSelect: null,
      selected: null
    })
    this.closeOptions ()
    // console.log(this.props.filter.tax)
    // this.props.onSelect(this.props.filter.tax, null)
  }

  render () {
    const { tax, filters } = this.props
    return (
      <div className="vendor-filter filter">
        <div className="filter-header">
          <FilterValue
            search={this.state.search}
            selected={filters[tax].selected ? this.state.selected.name : null}
          />

          {this.state.selected && <ClearFilter onClear={this.handleClear}/>}

          <Placeholder onClick={this.handlePlaceholderClick}>
            {this.state.search === '' && filters[tax].selected === null && <span>Choose a {filters[tax].name}</span>}
          </Placeholder>
          <form onSubmit={this.handleSubmit} className="filter-input">
            <Input
              setInput={this.setInput}
              onChange={this.handleChange}
              value={this.state.search}
            />
          </form>

        </div>
        {this.state.inputFocus &&
          <Options
            onMouseEnter={this.handleMouseEnter}
            onSelect={this.handleClick}
            selected={this.state.activeSelect}
            tax={tax}
            terms={filters[tax].terms} />
        }
      </div>
    )
  }
}

Filter.propTypes = {
  active: PropTypes.bool,
  filter: PropTypes.shape({
    terms: PropTypes.array,
    tax: PropTypes.string.isRequired,
    search: PropTypes.string.search
  })
}

export default connect((state) => ({
  items: state.items,
  filterActive: state.filterActive,
  isLoading: state.isLoading,
  filters: state.filters
}))(Filter)
