import React from 'react'

export const Loader = () => {
  return (
    <div style={{width:'100%',textAlign:'center'}} className="vendor-loader"><p>Loading</p></div>
  )
}
