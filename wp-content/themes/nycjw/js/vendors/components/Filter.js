import React, { Component } from 'react'
import Select from 'react-select'
import { connect } from 'react-redux'
import { selectTerm, setActiveFilter } from '../actions/filters'
import { decodeHtml } from '../utils'

class Filter extends Component {

  constructor (props) {
    super(props)
    this.state = {
      inputFocus: false,
      search: '',
      activeSelect: null,
      selected: null,
      terms: []
    }
    this.handleSelect = this.handleSelect.bind(this)
  }

  handleSelect( option ) {
    this.props.dispatch(selectTerm(this.props.tax, option ? option.value : null))
  }

  render () {
    const selectStyles = {
      control: (provided, state) => ({
        // none of react-select's styles are passed to <Control />
        ...provided,
        borderRadius: 0,
        borderColor: filterSettings.alt_design ? 'white' : 'black',
        backgroundColor: filterSettings.alt_design ? 'transparent' : 'white',
        borderWidth: '2px',
        color: filterSettings.alt_design ? 'white' : 'black',
        "&:hover": {
          borderColor: filterSettings.alt_design ? 'rgba(255,255,255,.9)' : 'rgba(0,0,0,.6)'
        }
      }),
      singleValue: (provided) => ({
        ...provided,
        color: filterSettings.alt_design ? 'white' : provided.color
      }),
      placeholder: (provided) => {
        return {
          ...provided,
          color: filterSettings.alt_design ? 'white' : provided.color
        }
      },
      dropdownIndicator: (provided, state) => ({
        ...provided,
        color: filterSettings.alt_design ? 'white' : 'black'
      }),
      clearIndicator: (provided, state) => ({
        ...provided,
        color: filterSettings.alt_design ? 'white' : 'black'
      }),
      indicatorSeparator: (provided, state) => ({
        ...provided,
        display: 'none'
      })
    }
    const { tax, filters } = this.props
    const options = filters[tax]
      ? Object.keys(filters[tax].terms).map( slug => {
          return { value: slug, label: decodeHtml(filters[tax].terms[slug].name) }
        })
      : []
    const selectedOption = filters[tax].selected
      ? { value: filters[tax].selected, label: decodeHtml(filters[tax].terms[filters[tax].selected].name) }
      : null
    return (
      <div className="vendor-filter filter">
        <Select
          options={options}
          value={selectedOption}
          styles={selectStyles}
          isClearable={true}
          onChange={ option => this.handleSelect(option) }
          placeholder={ `Select a ${filters[tax].name === 'event type' ? 'type' : filters[tax].name}` } />
      </div>
    )
  }
}

export default connect((state) => ({
  items: state.items,
  filterActive: state.filterActive,
  isLoading: state.isLoading,
  filters: state.filters
}))(Filter)
