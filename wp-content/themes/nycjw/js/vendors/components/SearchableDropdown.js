import React, { useCallback, useState } from 'react'

const SearchableDropdown = (props) => {
  const { options } = props

  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [searchVal, setSearchVal] = useState('')
  const [results, setResults] = useState([])

  const handleSearch = useCallback((value) => {
    setSearchVal(value);

    if (options.length > 0 && value) {
      setResults(options.filter(v => v.title.toLowerCase().includes(value.toLowerCase())))
    } else {
      setResults([])
    }
  }, [searchVal, options])

  return (
    <div className="searchable-dropdown">
      <input
        placeholder="Search by Title"
        value={searchVal}
        onChange={e => handleSearch(e.target.value)}
        onFocus={() => setDropdownOpen(true)}
        onBlur={() => setDropdownOpen(false)}
      />

      <div className={'dropdown-container' + (dropdownOpen ? ' open' : '')}>
        {results.length > 0 && (
          <div className="options-list">
            {
              results.map((v, index) => {
                const itemLink = v.ex_link ? v.ex_link : v.link
                const linkTarget = v.ex_link ? '_blank' : '_self'
                let imgURL = ''
                if (!v.gallery && v.image) {
                  imgURL = v.image
                } else if (v.gallery) {
                  imgURL = v.gallery[0].url
                }

                return (
                  <a target={linkTarget} href={itemLink}>
                    <div key={index} className="option-item">
                      <img src={imgURL} />
                      <span dangerouslySetInnerHTML={{ __html: v.title }}></span>
                    </div>
                  </a>
                )
              }
            )}
          </div>
        )}
      </div>
    </div>
  )
}

export default SearchableDropdown
