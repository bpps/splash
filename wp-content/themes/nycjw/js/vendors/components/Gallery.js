import React, { Component } from 'react'
import Slider from "react-slick";

class Gallery extends Component {
  render () {
    var settings = {
      dots: false,
      infinite: true,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true
    };
    const { randomNum, images, linkTarget, itemLink } = this.props
    return (
      <Slider {...settings}>
        {
          images.map( image => {
            return <div key={`gallery-image-${image.id}`} className={"item-image-container item-image-" + randomNum}>
              <a target={linkTarget} href={itemLink} className="item-image ratio-1">
                <div style={{ backgroundImage: `url(${image.sizes['small-medium']})`}}>
                </div>
              </a>
            </div>
          })
        }
      </Slider>
    )
  }
}

export default Gallery
