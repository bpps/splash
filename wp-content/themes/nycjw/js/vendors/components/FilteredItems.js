import React, { Component } from 'react'
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry"
import Filter from './Filter'
import Gallery from './Gallery'
import { Loader } from './Loader'
import axios from 'axios'
import { objectToFormData } from '../utils'
import { connect } from 'react-redux'
import { loadData, setData, dataError, setPage } from '../actions/filteredItems'
import { clearTerms } from '../actions/filters'
import SearchableDropdown from './SearchableDropdown'

class FilteredItems extends Component {
  constructor (props) {
    super(props)

    this.loadContent = this.loadContent.bind(this)
    this.onScroll = this.onScroll.bind(this)
    this.clearTerms = this.clearTerms.bind(this)
    this.state = {
      maxPages: true,
    }
  }

  clearTerms () {
    this.props.dispatch(clearTerms())
  }

  onScroll () {
    // Bails early if:
    // * there's an error
    // * it's already loading
    // * there's nothing left to load
    const { error, isLoading, hasMore } = this.props
    if (error || isLoading || !hasMore) return;

    // Checks that the page has scrolled to the bottom
    const container = document.getElementById('filterable-items')
    const containerHieght = document.getElementById('filterable-items')
    // console.log('win height:', window.innerHeight)
    // console.log('doc scroll:', container.offsetTop)
    // console.log('doc offset:', container.offsetHeight)
    if (
      window.innerHeight + document.documentElement.scrollTop
      > (container.offsetTop + container.offsetHeight) - 100
    ) {
      this.props.dispatch(setPage( this.props.page + 1 ))
    }
  }

  clickBody (e) {
    const { filterActive } = this.props
    const element = document.getElementById('filtered-content-filters');
    if (e.target !== element && !element.contains(e.target) && filterActive != null) {
      this.handleActiveFilter(null)
    }
  }

  componentDidMount () {
    this.onScroll()
    document.addEventListener('scroll', this.onScroll)
    document.addEventListener("click", (e) => this.clickBody(e))
    this.loadContent()
    // store.subscribe(() => this.forceUpdate())
  }

  componentWillUnmount () {
    document.removeEventListener('scroll', this.onScroll);
    document.removeEventListener("click", (e) => this.clickBody(e))
  }

  componentDidUpdate(prevProps) {
    // only update chart if the data has changed
    if (prevProps.filters !== this.props.filters || prevProps.page !== this.props.page) {
      this.loadContent()
    }
  }

  loadContent () {
    const { page, items, filters, dispatch } = this.props
    dispatch(loadData())
    let data = {
      action: 'do_ajax',
      fn : 'filter_items',
      page : filterSettings.order_random !== 1 ? page : null,
      post_type : filterSettings.post_type,
      dataType: 'json'
    }
    const selectedTerms = {}
    Object.keys(filters).forEach( tax => {
      if ( filters[tax].selected != undefined ) {
        selectedTerms[tax] = filters[tax].selected
      }
    })
    if ( filterSettings.posts_per_page ) {
      data.posts_per_page = filterSettings.posts_per_page
    }
    if ( Object.keys(selectedTerms).length > 0 ) {
      data.filters = selectedTerms
    }
    if ( filterSettings.term ) {
      selectedTerms[filterSettings.tax] = filterSettings.term
      data.filters = selectedTerms
    }
    if ( page > 1 && filterSettings.order_random === 1 ) {
      data.not_in = items.map(item => {
        return item.id
      })
    }
    axios.post( ajaxurl, objectToFormData(data) )
    .then(results => {
      let newItems = [...items, ...results.data.itemsObj]
      if ( page === 1 ) {
        this.setState({ maxPages: results.data.max_pages })
      }
      dispatch(setData(newItems, page < this.state.maxPages, page + 1, ))
    })
    .catch((error) => dispatch(dataError(error)) )

  }

  render () {
    const masonryOptions = {
      itemSelector: '.item',
      percentPosition: true,
      layoutMode: 'packery',
      columnWidth: '.items-grid-sizer'
    };
    const { items, filterActive, isLoading } = this.props

    return (
      <div>
        {
          ( filterSettings.show_filters && filterObjs ) &&
          <div id="filtered-content-filters">
            {
              filterSettings.alt_design && <h2 style={{ color: 'white', lineHeight: 1, marginRight: '1rem' }}>Explore By: </h2>
            }
            {
              !filterSettings.title_filter_only &&
                Object.keys(filterObjs).map( tax => {
                return (
                  <Filter
                    key={tax}
                    tax={tax}
                  />
                )
              })
            }
            {
              filterSettings.isFilterByTitle && (
                <div id="search-by-title-autosuggest-wrapper">
                  <SearchableDropdown options={items} />
                </div>
              )
            }
            {
              !filterSettings.title_filter_only &&
                <button style={{ textDecoration: 'underline', color: 'white', background: 'transparent', border: 'none', fontSize: '1rem', cursor: 'pointer' }} onClick={this.clearTerms}>All</button>
            }
          </div>
        }
        <div id="filterable-items" className={ `items-per-row-${filterSettings.items_per_row}`}>
          {
            filterSettings.term_label &&
            <h2 style={{ textAlign: 'center' }}>{ filterSettings.term_label }</h2>
          }
          {
            filterSettings.view_type=='tiles' ?
              <Masonry
                className={'items-masonry'} // default ''
                // elementType={'ul'} // default 'div'
                options={masonryOptions} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                //imagesLoadedOptions={imagesLoadedOptions} // default {}
              >
                <div className="items-grid-sizer"></div>
                <div className="items-gutter"></div>
                {
                  items.length > 0 &&
                    items.map(item => {

                      const itemLink = item.ex_link ? item.ex_link : item.link
                      const linkTarget = item.ex_link ? '_blank' : '_self'
                      const randomNum = Math.floor(Math.random() * 3) + 1;
                      return (
                        <div key={item.id} className={"item" + (item.featured ? ' featured' : '')}>
                          <div className="item-content">
                            {
                              !item.gallery && item.image &&
                              <div className={"item-image-container item-image-" + randomNum}>
                                <a target={linkTarget} href={itemLink} className="item-image ratio-1">
                                  <div style={{ backgroundImage: `url(${item.image})`}}/>
                                </a>
                              </div>
                            }
                            {
                              item.gallery &&
                              <Gallery randomNum={randomNum} target={linkTarget} link={itemLink} images={item.gallery} />
                            }
                            <div className="item-info">
                              <div className="item-info-content">
                                <div className="item-name">
                                  <h3>
                                    <a target={linkTarget} href={itemLink} dangerouslySetInnerHTML={{ __html: item.title }}>
                                    </a>
                                  </h3>
                                </div>
                                {
                                  item.subtitle &&
                                  <p className="item-subtitle" dangerouslySetInnerHTML={{ __html: item.subtitle }}>
                                  </p>
                                }
                                {
                                  item.description &&
                                  <div className="item-desc" dangerouslySetInnerHTML={{ __html: item.description }}>
                                  </div>
                                }
                                {
                                  item.materials &&
                                  <div className="item-materials">
                                    { item.materials }
                                  </div>
                                }
                                {
                                  item.price &&
                                  <p className="item-price">
                                    { item.price }
                                  </p>
                                }
                                {item.locations && <p className="item-location">{item.locations[0].name}</p>}
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })
                }
              </Masonry>
              : <div className="item-grid-wrapper" style={{ 'display' : 'flex', 'flexWrap': 'wrap', 'justifyContent': 'center' }}>
               {
                  items.length > 0 &&
                    items.map(item => {
                      const itemLink = item.ex_link ? item.ex_link : item.link
                      const linkTarget = item.ex_link ? '_blank' : '_self'
                      const randomNum = Math.floor(Math.random() * 3) + 1;
                      return (
                        <div key={item.id} className={"item" + (item.featured ? ' featured' : '')}>
                          <div className="item-content">
                            {
                              !item.gallery && item.image &&
                              <div className={"item-image-container item-image-" + randomNum}>
                                <a target={linkTarget} href={itemLink} className="item-image ratio-1">
                                  <div style={{ backgroundImage: `url(${item.image})`}}/>
                                </a>
                              </div>
                            }
                            {
                              item.gallery &&
                              <Gallery randomNum={randomNum} target={linkTarget} link={itemLink} images={item.gallery} />
                            }
                            <div className="item-info">
                              <div className="item-info-content">
                                <div className="item-name">
                                  <h3>
                                    <a target={linkTarget} href={itemLink} dangerouslySetInnerHTML={{ __html: item.title }}>
                                    </a>
                                  </h3>
                                </div>
                                {
                                  item.subtitle &&
                                  <p className="item-subtitle" dangerouslySetInnerHTML={{ __html: item.subtitle }}>
                                  </p>
                                }
                                {
                                  item.description &&
                                  <div className="item-desc" dangerouslySetInnerHTML={{ __html: item.description }}>
                                  </div>
                                }
                                {
                                  item.materials &&
                                  <div className="item-materials">
                                    { item.materials }
                                  </div>
                                }
                                {
                                  item.price &&
                                  <p className="item-price">
                                    { item.price }
                                  </p>
                                }
                                {item.locations && <p className="item-location">{item.locations[0].name}</p>}
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })
                }
              </div>
          }
          {
            (!items.length && !isLoading) &&
              <div className="no-items" style={{ textAlign: 'center', paddingTop: '2rem', paddingBottom: '2rem'}}>
                Sorry, we have a lot of great { filterSettings.post_type_plural ? filterSettings.post_type_plural.toLowerCase() : 'vendors' } but your search may be too specific!
              </div>
          }
          {isLoading && <Loader/>}
        </div>
      </div>
    )
  }
}

export default connect((state) => ({
  items: state.items,
  filters: state.filters,
  filterActive: state.filterActive,
  isLoading: state.isLoading,
  hasMore: state.hasMore,
  page: state.page
}))(FilteredItems)
