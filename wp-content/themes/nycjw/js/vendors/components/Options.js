import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class Options extends Component {
  constructor (props) {
    super(props)

    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleMouseEnter (term) {
    this.props.onMouseEnter(term)
  }

  handleClick (term) {
    this.props.onSelect(term)
  }

  render () {
    const { terms, filters, tax } = this.props
    return (
      <div className="filter-options">
        {
          Object.keys(terms).map( slug => {
            return (
              <div
                key={slug}
                onMouseEnter={this.handleMouseEnter.bind(null, terms[slug])}
                onClick={this.handleClick.bind(null, terms[slug])}
                className={'filter-option ' + (filters[tax].selected === slug ? ' active' : '')}
              >
                {terms[slug].name}
              </div>
            )
          })
      }
      {
        Object.keys(this.props.terms).length === 0 &&
        <div className="filter-option-none">
          No Results Found
        </div>
      }
      </div>
    )
  }
}

Options.propTypes = {
  selected: PropTypes.shape({
    name: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired
  }),
  onSelect: PropTypes.func.isRequired,
  onMouseEnter: PropTypes.func.isRequired
}

export default connect((state) => ({
  filters: state.filters
}))(Options)
