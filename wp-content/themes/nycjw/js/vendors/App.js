import React from 'react'
import FilteredItems from './components/FilteredItems'

function App() {
  return (
    <div className="App">
      <FilteredItems />
    </div>
  );
}

export default App;
