import { DATA_ERROR, LOAD_DATA, SET_DATA } from '../actions/filteredItems'

export const isLoading = (state = true, action) => {
  switch(action.type) {
    case LOAD_DATA :
      return true
    case SET_DATA :
      return false
    case DATA_ERROR :
      return false
    default :
      return state
  }
}

export const error = (state = false, action) => {
  switch(action.type) {
    case DATA_ERROR :
      return action.error
    default :
      return false
  }
}
