import { SET_DATA, LOAD_DATA, SET_PAGE } from '../actions/filteredItems'
import { SELECT_TERM } from '../actions/filters'

export const items = (state = [], action) => {
  switch(action.type) {
    case SET_DATA:
      return action.items
    case SELECT_TERM:
      return []
    default:
      return state
  }
}

export const page = (state = 1, action) => {
  switch(action.type) {
    case SET_PAGE :
      return action.page
    case SELECT_TERM :
      return 1
    default :
      return state
  }
}

export const hasMore = (state = false, action) => {
  switch(action.type) {
    case SET_DATA :
      return action.hasMore
    default :
      return state
  }
}
