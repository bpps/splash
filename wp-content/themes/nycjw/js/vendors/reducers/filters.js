import { SET_ACTIVE_FILTER, SELECT_TERM, CLEAR_TERMS } from '../actions/filters'

export const filters = (state = {}, action) => {
  switch(action.type) {
    case SELECT_TERM:
      return {
        ...state,
        [action.tax]: {
          ...state[action.tax],
          selected: action.term
        }
      }
    break
    case CLEAR_TERMS:
      return filterObjs
    break
    default :
      return state
  }
}

export const filterActive = (state = null, action) => {
  switch(action.type) {
    case SET_ACTIVE_FILTER :
      return action.filter
    default :
      return state
  }
}
