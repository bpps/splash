import { combineReducers } from 'redux'
import { isLoading, error } from './isLoading'
import { items, hasMore, page } from './filteredItems'
import { filters, filterActive } from './filters'
export default combineReducers({
  isLoading,
  items,
  hasMore,
  page,
  filters,
  error,
  filterActive
})
