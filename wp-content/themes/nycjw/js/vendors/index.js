import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reducer from './reducers'
import middleware from './middleware'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const initialState = {
  isLoading: false,
  items: [],
  hasMore: false,
  page: 1,
  filters: filterObjs ? filterObjs : [],
  error: false,
  filterActive: null
}
const store = createStore(reducer, initialState, middleware)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('filtered-content-container'))
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
