var el = wp.element.createElement;

var withClientIdClassName = wp.compose.createHigherOrderComponent( function( BlockListBlock ) {
    return function( props ) {
        var newProps = lodash.assign(
            {},
            props,
            {
                className: "block-" + props.clientId,
            }
        );

        return el(
            BlockListBlock,
            newProps
        );
    };
}, 'withClientIdClassName' );

wp.hooks.addFilter( 'editor.BlockListBlock', 'my-plugin/with-client-id-class-name', withClientIdClassName );
