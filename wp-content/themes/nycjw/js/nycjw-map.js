jQuery.noConflict();
(function( $ ) {
  $(function() {
    var map;
    if(typeof eventsObj !== 'undefined') {
      var events = $.parseJSON(eventsObj);
      mapboxgl.accessToken = 'pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw';
      map = new mapboxgl.Map({
          container: 'nycjw-map',
          style: 'mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q',
          center: [-74.0392709, 40.7590403],
          zoom: 13,
          interactive: true,
      });

      if(!map.loaded()) {

      }
      var nav = new mapboxgl.NavigationControl();
      map.addControl(nav, 'top-left');
      map.on('load', function() {
          // Add a GeoJSON source containing place coordinates and information.
          map.addSource("places", {
              "type": "geojson",
              "data": events
          });
          var bounds = new mapboxgl.LngLatBounds();
          events.features.forEach(function(marker, ind) {
            var el = document.createElement('div');
            el.setAttribute('id', 'marker-'+marker.properties.eid);
            $(el).on('click', function() {
              showEventInfo(marker.properties.eid);
            })
            let eventAddress = encodeURIComponent(marker.properties.address.address);
            //$.get( `https://api.mapbox.com/geocoding/v5/mapbox.places/${eventAddress}.json?types=address&access_token=pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw`, function( data ) {
                el.className = 'marker';
                $(el).html(`
                  <span class="event-icon">${marker.properties.icon}</span>
                  <div class="marker-info">
                    <div class="map-item-img bg-centered" style="background-image:url('${marker.properties.image}');">
                    </div>
                    <div class="map-item-info">
                      <p class="map-item-title">
                        <strong>
                          ${marker.properties.title}
                        </strong>
                      </p>
                      <span class="map-item-loc">
                        ${marker.properties.description}
                      </span>
                    </div>
                  </div>`);

                bounds.extend([marker.lat, marker.lng]);
                // make a marker for each feature and add to the map
                //console.log(marker.geometry.coordinates, marker.properties.title);
                var $marker = new mapboxgl.Marker(el)
                .setLngLat([marker.lat, marker.lng])
                // .setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML(`
                //   <a href="${marker.properties.link}">
                //     <div class="map-item-img bg-centered" style="background-image:url('${marker.properties.image}');">
                //     </div>
                //     <div class="map-item-info">
                //       <p class="map-item-title">
                //         ${marker.properties.title}
                //       </p>
                //       <span class="map-item-loc">
                //         ${marker.properties.description}
                //       </span>
                //     </div>
                //   </a>`))

                .addTo(map);
          	//});

          });
          setTimeout(function() {
            map.fitBounds(bounds, { padding: 40 });
          }, 1000);


      });
      $('#event-drawer-wrapper, #close-events-drawer').on('click', function(e) {
        if ( $(e.target).is('#event-drawer-wrapper') || $(e.target).is('#close-events-drawer') ) {
          $('#event-drawer-wrapper').removeClass('active')
        }
      })
    }
    function showEventInfo(id) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'get_event_content',
          'id' : id,
          'date-id' : false,
          'map' : false
        }
      }).done( function (response) {
        response = $.parseJSON(response);
        $('#event-drawer-inner').html(response.event);
        if(!$('#event-drawer-wrapper').hasClass('active')) {
          $('#event-drawer-wrapper').addClass('active');
        }
        $('.new-event').removeClass('new-event');
        setTimeout(function() {
          map.resize();
          // map.flyTo({
          //   // These options control the ending camera position: centered at
          //   // the target, at zoom level 9, and north up.
          //   center: [-74.0392709, 40.7590403],
          //
          //   // These options control the flight curve, making it move
          //   // slowly and zoom out almost completely before starting
          //   // to pan.
          //   speed: 0.05, // make the flying slow
          //   curve: .1, // change the speed at which it zooms out
          //
          //   // This can be any easing function: it takes a number between
          //   // 0 and 1 and returns another number between 0 and 1.
          //   easing: function (t) {
          //       return t;
          //   }
          // });
        }, 500);

      });
    }
  });
})(jQuery);
