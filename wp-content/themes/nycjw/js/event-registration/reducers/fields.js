import { ADD_FIELD } from '../actions/fields'

export const fields = (state = null, action) => {
  switch(action.type) {
    case ADD_FIELD:
      return {...state, [action.field.name]: { ...state[action.field.name], value: action.value } }
    default:
      return state
  }
}
