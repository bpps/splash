export const ADD_FIELD = 'ADD_FIELD'

export const addFieldValue = (field, value, repeater = false) => {
  return {
    type: ADD_FIELD,
    field,
    value,
    repeater
  }
}
