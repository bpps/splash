import React from 'react';
import { connect } from 'react-redux'
import TextField  from './TextField';
import TextAreaField  from './TextAreaField';
import DateTimeField  from './DateTimeField';
import ImagesField  from './ImagesField';
import SelectField  from './SelectField';
import RepeaterField  from './RepeaterField';

class Field extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field, repeater } = this.props
    switch (field.type) {
      case 'text':
      case 'url':
      case 'email':
        return <TextField field={field} repeater={repeater} />
      break;
      case 'textarea':
        return <TextAreaField field={field} repeater={repeater}/>
      break;
      case 'repeater':
        return <RepeaterField field={field} repeater={repeater}/>
      break;
      case 'date_time_picker':
        const eventDate = new Date()
        return <DateTimeField field={field} eventDate={eventDate} repeater={repeater}/>
      break;
      case 'gallery':
        return <ImagesField field={field} repeater={repeater}/>
      break;
      case 'taxonomy':
        return <SelectField field={field} repeater={repeater}/>
      break;
    }
    return '';
  }
}

export default Field;
