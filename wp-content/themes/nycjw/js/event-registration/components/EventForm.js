import React from 'react';
import { connect } from 'react-redux'
import PreviewEventSlider  from './PreviewEventSlider';
import Field from './Field';
import Multiselect from 'multiselect-react-dropdown';
import axios from 'axios';
import { objectToFormData, eventValidation } from '../../events/utils';
import ImageUploader from 'react-images-upload';
// import DatePicker from "react-datepicker";
// import TimePicker from 'react-time-picker';
import DateTimePicker from 'react-datetime-picker';

// import "react-datepicker/dist/react-datepicker.css";

class EventForm extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      biz_info_business_name: '',
      biz_info_name: '',
      biz_info_location: '',
      biz_info_website_link: '',
      partner_name: '',
      partner_link: '',
      evt_name: '',
      evt_description: '',
      evt_date: new Date(),
      evt_dates: [
        new Date()
      ],
      evt_start_time: '00:00',
      evt_end_time: '12:00',
      evt_link: '',
      social_info_instagram_url: '',
      social_info_clubhouse_url: '',
      social_info_twitter_url: '',
      social_info_facebook_url: '',
      contact_email: '',
      contact_phone: '',
      contact_name: '',
      eventTypes: [],
      selectedEventTypes: [],
      eventLocations: [],
      selectedEventLocations: [],
      eventThemes: [],
      selectedEventThemes: [],
      jewelryTypes: [],
      selectedJewelryTypes: [],
      showPreviewModal: false,
      featuredImages: [],
      participants_name: '',
      participants_bio: '',
      eventContent: null,
      scheduled_or_durational: 1,
      isSaving: false
    };
    this.onClickPreviewButton = this.onClickPreviewButton.bind(this)
    this.onClickPostButton = this.onClickPostButton.bind(this)
    this.hidePreviewModal = this.hidePreviewModal.bind(this)
    this.onDrop = this.onDrop.bind(this);

  }
  componentDidMount() {
    axios.post('/NYC/wp-admin/admin-ajax.php', objectToFormData({
      action: 'get_terms_by_taxonomy'
    })).then(res => {
      let { event_types, event_locations, event_themes, jewelry_types } = res.data;
      event_types.map(event_type=> {
        event_type.id = event_type.term_id
      });
      event_locations.map(event_location=> {
        event_location.id = event_location.term_id
      });
      event_themes.map(event_theme=> {
        event_theme.id = event_theme.term_id
      });
      jewelry_types.map(jewelry_type=> {
        jewelry_type.id = jewelry_type.term_id
      });
      this.setState({
        eventTypes: event_types,
        eventLocations: event_locations,
        eventThemes: event_themes,
        jewelryTypes: jewelry_types
      });
    })
  }
  onClickPreviewButton () {
    let eventContent = {
      featured_image: this.state.featuredImages.length>0 ? this.state.featuredImages[0]: '',
      durational: true,
      title: this.state.evt_name,
      content: this.state.evt_description,
      date: null,
      start_time: null,
      end_time: null,
      event_themes: this.state.selectedEventThemes,
      event_types: this.state.selectedEventTypes,
      images: this.state.featuredImages,
      participants: [
        {
          name: this.state.participants_name,
          bio: this.state.participants_bio
        }
      ],
      subtitle: '',
      link: {
        title: 'website link',
        url: this.state.biz_info_website_link,
        target: '_blank'
      },
      business_information: {
        business_name: this.state.biz_info_business_name,
        name: this.state.biz_info_name,
        location: this.state.biz_info_location,
        website: this.state.biz_info_website_link
      },
      instagram: this.state.social_info_instagram_url,
      facebook: this.state.social_info_facebook_url,
      twitter: this.state.social_info_twitter_url,
      clubhouse: this.state.social_info_clubhouse_url
    }
    this.setState({
      eventContent: eventContent,
      showPreviewModal: true
    })
  }
  onDrop(picture) {
    this.setState({
      featuredImages: []
    });
    for(let i = 0; i < picture.length; i ++) {
      let file = picture[i];
      var reader = new FileReader();
      const self = this;
      reader.onload = function(upload) {
        let imageObj = {
          src: upload.target.result,
          title: file.name,
          image_type: file.type,
          file_name: file.name,
        }
        self.setState({
          featuredImages: self.state.featuredImages.concat(imageObj)
        });
      };
      reader.readAsDataURL(file);
    }
  }

  hidePreviewModal() {
    this.setState({
      showPreviewModal: false
    })
  }

  async onClickPostButton() {
    if (!eventValidation(this.state)) {
      return;
    }
    if (!confirm('Are you sure you want to save this Event?')) {
      return false;
    }
    this.setState({
      isSaving: true
    });
    let featuredImages = this.state.featuredImages;
    let images = [];
    for(let i = 0; i < featuredImages.length; i ++) {
      let image = featuredImages[i];
      let payload = {
        action: 'upload_event_image',
        src: image.src,
        title: image.title,
        image_type: image.image_type,
        image_fileName: image.file_name
      }
      const res = await axios.post('/NYC/wp-admin/admin-ajax.php', objectToFormData(payload));
      if (res.status == 200) {
        let attach_id = res.data.attach_id;
        images.push(attach_id);
      }
    }

    let post_data = {
      post_title    : this.state.evt_name,
      post_type     : 'event',
      post_content  : this.state.evt_description
    }
    let term_taxonomy_ids = [];

    let selectedTerms = this.state.selectedEventLocations.concat(this.state.selectedEventThemes).concat(this.state.selectedEventTypes).concat(this.state.selectedJewelryTypes);
    selectedTerms.forEach(term => {
      term_taxonomy_ids.push(term.term_taxonomy_id);
    })

    let meta_data = {
      event_date: this.state.evt_date,
      start_time: this.state.evt_start_time,
      end_time: this.state.evt_end_time,
      event_link: this.state.evt_link,
      partner_name: this.state.partner_name,
      partner_link: this.state.partner_link,
      scheduled_or_durational: this.state.scheduled_or_durational==1?'scheduled':'durational',
      facebook: this.state.social_info_facebook_url,
      twitter: this.state.social_info_twitter_url,
      instagram: this.state.social_info_instagram_url,
      clubhouse: this.state.social_info_clubhouse_url,
      thumbnail: images[0],
      event_images: images.join(","),
      event_location: this.state.selectedEventLocations.length>0?this.state.selectedEventLocations[0].id:'',
      event_theme: this.state.selectedEventThemes.length>0?this.state.selectedEventThemes[0].id:'',
      event_type: this.state.selectedEventTypes.length>0?this.state.selectedEventTypes[0].id:'',
      jewelry_type: this.state.selectedJewelryTypes.length>0?this.state.selectedJewelryTypes[0].id:'',
      term_taxonomy_ids: term_taxonomy_ids.join(","),
      participants_name: this.state.participants_name,
      participants_bio: this.state.participants_bio
    }

    let payload = {
      action: 'post_event',
      post_data: post_data,
      meta_data: meta_data
    }

    const self = this;
    axios.post('/NYC/wp-admin/admin-ajax.php', objectToFormData(payload)).then(res => {
      if (res.status == 200) {
        alert('Event was created successfully!');
      } else {
        alert('Error occured in creating event');
      }
      self.setState({
        isSaving: false
      });
    })

  }

  render () {
    let { showPreviewModal, eventTypes, eventLocations, eventThemes, jewelryTypes, isSaving } = this.state;
    return (
      <div>
        <div className="controller">
          <h2>Event Registration</h2>
          <button type="button" className="btn btn-preview" onClick={this.onClickPreviewButton}>Preview Event</button>
          <button type="button" className="btn btn-post" onClick={this.onClickPostButton}>Post Event</button>

        </div>
        <div className="form-wrapper">
          {
            eventFormFields.map( fieldGroup => {
              return (
                <div key={fieldGroup.key} className="form-fields-group">
                  <h4>{fieldGroup.label}</h4>
                  {
                    fieldGroup.sub_fields.map( field => {
                      return <Field field={field}/>
                    })
                  }
                </div>
              )
            })
          }
          <div className="left-column">
            <div className="form-fields-group" id="partner-section">
              <h4>Partner Information</h4>
              <div className="form-fluid">
                <label>Partner Name</label>
                <input
                  type="text"
                  value={this.state.partner_name}
                  onChange={(e) => {
                    this.setState({
                      partner_name: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Partner Website</label>
                <input
                  type="text"
                  value={this.state.partner_link}
                  onChange={(e) => {
                    this.setState({
                      partner_link: e.target.value
                    })
                  }}
                />
              </div>
            </div>
            <div className="form-fields-group" id="events-section">
              <h4>Event Detail</h4>
              <div className="form-fluid">
                <div className="checkbox-row">
                  <input type="radio" id="schedule" name="scheduled_or_durational" value="1" onChange={(e)=>{
                    this.setState({
                      scheduled_or_durational: e.target.value
                    })
                  }}/>
                  <label htmlFor="schedule">Schedule</label>
                </div>
                <div className="checkbox-row">
                <input type="radio" id="duration" name="scheduled_or_durational" value="2" onChange={(e)=>{
                    this.setState({
                      scheduled_or_durational: e.target.value
                    })
                  }}/>
                  <label htmlFor="duration">Duration</label>
                </div>
              </div>
              <div className="form-fluid">
                <label>Event Name</label>
                <input
                  type="text"
                  name="event_name"
                  id="event_name"
                  value={this.state.evt_name}
                  onChange={(e)=>{
                    this.setState({
                      evt_name: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Description</label>
                <textarea
                  name="description"
                  id="event_description"
                  onChange={(e)=>{
                    this.setState({
                      evt_description: e.target.value
                    })
                  }}
                  value={this.state.evt_description}
                ></textarea>
              </div>
              <div className="form-fluid">
                <label>Event Link</label>
                <input
                  type="text"
                  name="event_link"
                  value={this.state.evt_link}
                  onChange={(e) => {
                    this.setState({
                      evt_link: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid event_dates">
                <label>Event Date(s)</label>
                {
                  this.state.evt_dates.map((evt_date, index) => {
                    return (
                      <DateTimePicker
                        key={index}
                        value={evt_date}
                        onChange={(e)=>{
                          const cloneDates = JSON.parse(JSON.stringify(this.state.evt_dates));
                          cloneDates[index] = new Date(e);
                          this.setState({
                            evt_dates: cloneDates
                          });
                        }}
                      />
                    )
                  })
                }
                <button
                  type="button"
                  className="btn"
                  onClick={(e)=>{
                    this.setState({
                      evt_dates: this.state.evt_dates.concat(new Date())
                    })
                  }}
                >Add Date</button>
              </div>
              <div className="form-fluid">
                <label>Event Images</label>
                <ImageUploader
                  withIcon={true}
                  buttonText='Choose images'
                  onChange={this.onDrop}
                  imgExtension={['.jpg', 'jpeg', '.gif', '.png', '.gif']}
                  maxFileSize={5242880}
                  withPreview={true}
                />
              </div>
              <div className="form-fluid" id="event_types">
                <label>Event Types</label>
                <Multiselect
                  options={eventTypes}
                  onSelect={(selectedList, item) => {
                    this.setState({
                      selectedEventTypes: selectedList
                    })
                  }}
                  onRemove={(selectedList, item) => {
                    this.setState({
                      selectedEventTypes: selectedList
                    })
                  }}
                  selectedValues={this.state.selectedEventTypes}
                  displayValue="name"
                />
              </div>
              <div className="form-fluid" id="event_locations">
                <label>Event Locations</label>
                <Multiselect
                  options={eventLocations}
                  selectedValues={this.state.selectedEventLocations}
                  displayValue="name"
                  onSelect={(selectedList, item) => {
                    this.setState({
                      selectedEventLocations: selectedList
                    })
                  }}
                  onRemove={(selectedList, item) => {
                    this.setState({
                      selectedEventLocations: selectedList
                    })
                  }}
                />
              </div>
              <div className="form-fluid" id="event_themes">
                <label>Event Themes</label>
                <Multiselect
                  options={eventThemes}
                  selectedValues={this.state.selectedEventThemes}
                  displayValue="name"
                  onSelect={(selectedList, item) => {
                    this.setState({
                      selectedEventThemes: selectedList
                    })
                  }}
                  onRemove={(selectedList, item) => {
                    this.setState({
                      selectedEventThemes: selectedList
                    })
                  }}
                />
              </div>
              <div className="form-fluid" id="jewelry_type">
                <label>Jewlert Types</label>
                <Multiselect
                  options={jewelryTypes}
                  selectedValues={this.state.selectedJewelryTypes}
                  displayValue="name"
                  onSelect={(selectedList, item) => {
                    this.setState({
                      selectedJewelryTypes: selectedList
                    })
                  }}
                  onRemove={(selectedList, item) => {
                    this.setState({
                      selectedJewelryTypes: selectedList
                    })
                  }}
                />
              </div>
            </div>
          </div>
          <div className="right-column">
            <div className="form-fields-group" id="social-section">
              <h4>Social</h4>
              <div className="form-fluid">
                <label>Instagram URL</label>
                <input
                  type="text"
                  id="social_info_instagram_url"
                  value={this.state.social_info_instagram_url}
                  onChange={(e) => {
                    this.setState({
                      social_info_instagram_url: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Clubhouse URL</label>
                <input
                  type="text"
                  id="social_info_clubhouse_url"
                  value={this.state.social_info_clubhouse_url}
                  onChange={(e)=>{
                    this.setState({
                      social_info_clubhouse_url: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Twitter URL</label>
                <input
                  type="text"
                  id="social_info_twitter_url"
                  value={this.state.social_info_twitter_url}
                  onChange = {(e) => {
                    this.setState({
                      social_info_twitter_url: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Facebook URL</label>
                <input
                  type="text"
                  id="social_info_facebook_url"
                  value={this.state.social_info_facebook_url}
                  onChange={(e) => {
                    this.setState({
                      social_info_facebook_url: e.target.value
                    })
                  }}
                />
              </div>
            </div>
            <div className="form-fields-group" id="participants-section">
              <h4>Participants</h4>
              <div className="form-fluid">
                <label>Name</label>
                <input
                  type="text"
                  value={this.state.participants_name}
                  onChange={(e)=>{
                    this.setState({
                      participants_name: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Bio</label>
                <textarea
                  onChange={(e)=>{
                    this.setState({
                      participants_bio: e.target.value
                    })
                  }}
                  value={this.state.participants_bio}
                ></textarea>
              </div>
            </div>
            <div className="form-fields-group" id="contact-info-section">
              <h4>Contact Info (Not public)</h4>
              <div className="form-fluid">
                <label>Email</label>
                <input
                  type="email"
                  value={this.state.contact_email}
                  onChange={(e) => {
                    this.setState({
                      contact_email: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Phone</label>
                <input type="text" value={this.state.contact_phone}
                  onChange={(e) => {
                    this.setState({
                      contact_phone: e.target.value
                    })
                  }}
                />
              </div>
              <div className="form-fluid">
                <label>Name</label>
                <input type="text" value={this.state.contact_name}
                  onChange={(e) => {
                    this.setState({
                      contact_name: e.target.value
                    })
                  }}
                />
              </div>
            </div>

          </div>
        </div>
        {
          showPreviewModal
            ? <PreviewEventSlider eventContent={this.state.eventContent} dismissPreviewModal={this.hidePreviewModal}/>
            : ''
        }
        {
          isSaving
            ? <div id="loading"><p>Saving Event...</p></div>
            : ''
        }
      </div>

    )
  }
}

export default EventForm;
