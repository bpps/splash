import React from 'react';
import { connect } from 'react-redux'
import { addFieldValue } from '../actions/fields'

class TextField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field, dispatch } = this.props
    return (
      <div className="form-fluid">
        <label>{field.label}{ field.required ? <span>*</span> : '' }</label>
        <input
          type={field.type}
          value={field.value}
          name={field.name}
          required={field.required}
          onChange={ e => dispatch( addFieldValue(field, e.target.value) ) }
        />
      </div>
    )
  }
}

export default connect((state) => ({
  fields: state.fields
}))(TextField)
