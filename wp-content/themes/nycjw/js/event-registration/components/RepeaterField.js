import React from 'react';
import { connect } from 'react-redux'
import Field  from './Field';
import { addFieldValue } from '../actions/fields'

class RepeaterField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field } = this.props
    const newSubField = field.sub_fields[0]
    return (
      <div>
        <label>{field.label}{ field.required ? <span>*</span> : '' }</label>
        {
          field.sub_fields.map( subfield => {
            return <Field field={subfield} repeater={field.name}/>
          })
        }
        <button
          type="button"
          className="btn"
          onClick={ e => dispatch( addFieldValue(field, e.target.value) ) }
        >{field.button_label}</button>
      </div>
    )
  }
}

export default RepeaterField;
