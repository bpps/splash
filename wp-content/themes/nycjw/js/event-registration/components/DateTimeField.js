import React from 'react';
import { connect } from 'react-redux'
import DateTimePicker from 'react-datetime-picker';
import { addFieldValue } from '../actions/fields'

class DateTimeField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };
  }

  render () {
    const { field, eventDate, repeater, dispatch } = this.props
    return (
      <div className="form-fluid event_dates">
        {
          repeater == undefined && <label>Event Date(s){ field.required ? <span>*</span> : '' }</label>
        }
        <DateTimePicker
          value={field.value ? field.value : eventDate}
          onChange={ e => {
            dispatch( addFieldValue(field, new Date(e)) )
          } }
        />
      </div>
    )
  }
}

// const cloneDates = JSON.parse(JSON.stringify(this.state.evt_dates));
// cloneDates[index] = new Date(e);
// this.setState({
//   evt_dates: cloneDates
// });

export default connect((state) => ({
  fields: state.fields
}))(DateTimeField)
