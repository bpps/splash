import React, { Component } from 'react'
import facebook from '../../../images/fb.png';
import instagram from '../../../images/instagram.png';
import twitter from '../../../images/twitter.png';
import appleIcon from '../../../images/apple-app-store.png';
import googlePlayIcon from '../../../images/google-play.png';
import Slider from "react-slick";

class PreviewEventSlider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      closing: false
    };
    this.closeSlideout = this.closeSlideout.bind(this)
  }

  componentDidMount() {
    const self = this;
    document.body.classList.add('modal-open');
    setTimeout(function() {
      self.setState({
        loading: false
      })
    }, 10);
  }


  closeSlideout (event) {
    const self = this;
    event.preventDefault()
    const timeout = window.screen.width < 768 ? 0 : 300
    document.body.classList.remove('modal-open');
    this.setState({
      closing: true
    })
    setTimeout(function() {
      self.setState({
        closing: false
      });
      self.props.dismissPreviewModal();

    }, 100);
    return
  }
  render () {
    let { eventContent } = this.props;
    let { loading, closing } = this.state;
    let selectedTestDay = 1;
    let selectedEventType  = 'durational';
    let borderColor = '#FEF200'
    if ( eventContent ) {
      if ( eventContent.event_themes && eventContent.event_themes.length > 0 ) {
        borderColor = eventContent.event_themes[0].color?eventContent.event_themes[0].color:'#FEF200';
      }
    }
    const settings = {
      dots: false,
      infinite: true,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    }
    let eventStatus = 'future'
    let rsvpLink = null
    let rsvpLabel = 'RSVP'
    if ( eventContent ) {
      let today = new Date()
      if ( selectedTestDay !== 1 ) {
        today = new Date(selectedTestDay + `T${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`)
      }
      let eventStartDate = new Date( eventContent.date + 'T09:00:00')
      let eventEndDate = new Date( eventContent.date + 'T23:59:59')
      const weekStartDate = new Date('2020-11-14T09:00:00')
      rsvpLink = eventContent.rsvp ? eventContent.rsvp.url : null
      // If IG event, set as follow us on IG
      if ( eventContent.instagram_event ) {
        rsvpLink = 'https://instagram.com/nycjewelryweek'
        rsvpLabel = 'Follow us to RSVP'
      }
      // If theres an RSVP link, set the label as its text
      if ( eventContent.rsvp ) {
        rsvpLabel = eventContent.rsvp.title ? eventContent.rsvp.title : 'RSVP'
      }
      // If the event is on now
      if ( ( today > eventStartDate && today < eventEndDate ) || ( selectedEventType === 'durational' && today >  weekStartDate ) ) {
        eventStatus = 'present'
        rsvpLabel = 'Watch'
        if ( eventContent.instagram_event ) {
          rsvpLink = 'https://instagram.com/nycjewelryweek/live'
        } else {
          rsvpLink = 'https://youtube.com'
        }
        rsvpLink = eventContent.link ? eventContent.link.url : rsvpLink
        if ( eventContent.link ) {
          rsvpLabel = eventContent.link.title ? eventContent.link.title : rsvpLabel
          rsvpLink = eventContent.link.url
        }

      }
      if ( today > eventEndDate && selectedEventType !== 'durational' ) {
        eventStatus = 'past'
        rsvpLink = true
        rsvpLabel = 'Event over'
      }
    }
    // If todays date is after the event end date
    return (
      <div
        id="event-slideout"
        className={`${loading ? 'loading' : ''}${eventContent ? 'loaded' : ''}${closing ? ' closing' : ''}`}
        // onClick={this.closeSlideout}
      >
        <div id="event-slideout-wrapper" style={{ borderColor: borderColor }} ref={this.slideoutRef}>
          {
            eventContent &&
            <div ref={this.slideoutClose} style={{ borderColor: borderColor }} onClick={this.closeSlideout} id="close-slide">
              <p><span>Events</span></p>
            </div>
          }
          {
            eventContent &&
            <div id="event-slideout-content-wrapper">
              <div id="event-slideout-content">
                <div className="event-slideout-header">
                  {
                    ( eventContent.event_types || eventContent.event_themes ) &&
                    <p className="event-type">
                      {
                        eventContent.event_types &&
                        <span style={{ background: 'black', color: 'white' }}>{ (eventContent.event_types&&eventContent.event_types.length>0)?eventContent.event_types[0].name:'' }</span>
                      }
                      {
                        eventContent.event_themes &&
                        <span style={{ background: borderColor, color: (borderColor.toLowerCase() === '#fef200' || borderColor === 'rgb(255,255,0)') ? 'black' : 'white' }}>{ (eventContent.event_themes&&eventContent.event_themes.length>0)?eventContent.event_themes[0].name:'' }</span>
                      }
                    </p>
                  }
                  <h2 dangerouslySetInnerHTML={{ __html: eventContent.title }}></h2>
                  {
                    eventContent.subtitle &&
                    <h3>{ eventContent.subtitle }</h3>
                  }
                  {
                    eventContent.images &&
                    <div className="item">
                      <Slider className="event-image-gallery"  {...settings}>
                        {
                          eventContent.images.map( image => {
                            return (
                              <div key={image.id} className="event-image-wrapper">
                                <div
                                  className="event-image"
                                  role="img"
                                  // aria-label={ image.alt }
                                  style={{ backgroundImage: `url(${ image.src })` }}>
                                </div>
                              </div>
                            )
                          })
                        }
                      </Slider>
                    </div>
                  }
                  <div className="event-sub-header">
                    {
                      eventContent.date && !eventContent.intro &&
                      <div className="event-date">
                        <p>
                          <strong>{ formatPrettyDate(eventContent.date) }</strong>
                        </p>
                        {
                          eventContent.start_time && <p><span> {`${eventContent.start_time} - ${eventContent.end_time}`}</span></p>
                        }
                      </div>
                    }
                    {
                      ( rsvpLink && !eventContent.intro ) &&
                      <div className={`cta-wrapper ${eventStatus}`}>
                        <a className="btn" target="_blank" href={rsvpLink}>
                          <span>
                            { rsvpLabel }
                          </span>
                        </a>
                      </div>
                    }
                  </div>
                </div>
                {
                  eventContent.content &&
                  <div className="event-content" dangerouslySetInnerHTML={{ __html: eventContent.content }}>
                  </div>
                }
                <div className="partner" style={{ marginTop: '1.5rem' }}>
                  <div className="partner-header">
                    {
                      (eventContent.partnerName || eventContent.partner_website) &&
                      <h3>
                        {
                          eventContent.partner_name && <span>{ eventContent.partner_name }</span>
                        }
                        {
                          eventContent.partner_website &&
                          <a className="uppercase" href={eventContent.partner_website} target="_blank">
                            Website
                          </a>
                        }
                      </h3>
                    }
                    {
                      (eventContent.instagram || eventContent.twitter || eventContent.facebook) &&
                      <div className="event-social">
                        {
                          eventContent.facebook &&
                          <a className="social-link social-facebook" target="_blank" href={eventContent.facebook}>
                            <img src={facebook}/>
                          </a>
                        }
                        {
                          eventContent.instagram &&
                          <a className="social-link social-instagram" target="_blank" href={eventContent.instagram}>
                            <img src={instagram}/>
                          </a>
                        }
                        {
                          eventContent.twitter &&
                          <a className="social-link social-twitter" target="_blank" href={eventContent.twitter}>
                            <img src={twitter}/>
                          </a>
                        }
                      </div>
                    }
                  </div>
                  {
                    (eventContent.partnerName || eventContent.partner_website) && <div className="underlined" style={{ width: '100%' }}></div>
                  }
                  {
                    eventContent.in_gesso &&
                    <div className="partner-info">
                      <div className="partner-info-text">
                        <p>{ eventContent.partner_name ? eventContent.partner_name : 'This event' } is registered with Gesso. Download the app.</p>
                      </div>
                      <div className="partner-info-links-wrapper text-centered">
                        <div className="partner-info-links">
                          <a target="_blank" href="https://play.google.com/store/apps/details?id=app.gesso.android&hl=en_US">
                            <img src={googlePlayIcon}/>
                          </a>
                          <a target="_blank" href="https://apps.apple.com/ca/app/gesso-experiences/id1458855777">
                            <img src={appleIcon}/>
                          </a>
                        </div>
                      </div>
                    </div>
                  }
                  {
                    eventContent.in_shop_week &&
                    <div className="partner-info">
                      <div className="partner-info-text">
                        <p>{ eventContent.partner_name ? eventContent.partner_name : 'This event' } is featured in our 'Shop The Week' page</p>
                      </div>
                      <div className="partner-info-links-wrapper text-centered">
                        <div className="partner-info-links">
                          <a className="btn" href="https://nycjewelryweek.com/shop-the-week">Shop The Week</a>
                        </div>
                      </div>
                    </div>
                  }
                  {
                    eventContent.in_exhib &&
                    <div className="partner-info">
                      <div className="partner-info-text">
                        <p>{ eventContent.partner_name ? eventContent.partner_name : 'This event' } is featured in our 'Exhibitionist' page</p>
                      </div>
                      <div className="partner-info-links-wrapper text-centered">
                        <div className="partner-info-links">
                          <a className="btn" href="https://nycjewelryweek.com/exhibitionist">Exhibitionist</a>
                        </div>
                      </div>
                    </div>
                  }
                </div>
                {
                  eventContent.participants &&
                  <div className="event-participants">
                    <h4 className="underlined">Participants</h4>
                    <ul>
                      {
                        eventContent.participants.map( (participant, index) => {
                          return <li key={`participant-${index}`}>
                            <h5>{ participant.name }</h5>
                            {
                              participant.bio &&
                              <div className="participant-bio" dangerouslySetInnerHTML={{ __html: participant.bio }}>
                              </div>
                            }
                          </li>
                        })
                      }
                    </ul>
                  </div>
                }
                <div className="text-centered follow-nycjw">
                  <a href="https://instagram.com/nycjewelryweek" target="_blank"><img src={instagram}/><span>follow NYCJW on Instagram!</span></a>
                </div>
              </div>
            </div>
          }
          {
            loading &&
            <div className="event-loading-wrapper">
              <div className="event-loading">Loading</div>
            </div>
          }
        </div>
      </div>
    )
  }
}

export default PreviewEventSlider;
