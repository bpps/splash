import React from 'react';
import { connect } from 'react-redux'
import Multiselect from 'multiselect-react-dropdown';
import { addFieldValue } from '../actions/fields'


class SelectField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field, fields, dispatch } = this.props
    const value = fields[field.name].value
    return (
      <div className="form-fluid">
        <label>{field.label}{ field.required ? <span>*</span> : '' }</label>
        <Multiselect
          options={field.terms}
          placeholder="Choose One"
          singleSelect={true}
          onSelect={(selectedList, item) => {
            dispatch( addFieldValue(field, selectedList) )
          }}
          onRemove={(selectedList, item) => {
            this.setState({
              selectedEventTypes: selectedList
            })
          }}
          selectedValues={value}
          displayValue="name"
        />
      </div>
    )
  }
}

export default connect((state) => ({
  fields: state.fields
}))(SelectField)
