import React from 'react';
import { connect } from 'react-redux'
import ImageUploader from 'react-images-upload';

class ImagesField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field, eventDates } = this.props
    return (
      <div className="form-fluid">
        <label>{field.label}{ field.required ? <span>*</span> : '' }</label>
        <ImageUploader
          withIcon={true}
          buttonText='Choose images'
          onChange={this.onDrop}
          imgExtension={['.jpg', 'jpeg', '.gif', '.png', '.gif']}
          maxFileSize={5242880}
          withPreview={true}
        />
      </div>
    )
  }
}

export default ImagesField;
