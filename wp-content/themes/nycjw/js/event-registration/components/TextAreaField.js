import React from 'react';
import { connect } from 'react-redux'
import { addFieldValue } from '../actions/fields'

class TextAreaField extends React.Component {
  constructor (props) {
    super(props);
    this.state = {

    };

  }

  render () {
    const { field, dispatch } = this.props
    return (
      <div className="form-fluid">
        <label>{field.label}{ field.required ? <span>*</span> : '' }</label>
        <textarea
          name={field.name}
          id={field.name}
          onChange={ e => dispatch( addFieldValue(field, e.target.value) ) }
          value={field.value}
        ></textarea>
      </div>
    )
  }
}

export default connect((state) => ({
  fields: state.fields
}))(TextAreaField)
