// import React from 'react';
import React, { Component } from 'react';
import { HashRouter as Router, Route } from 'react-router-dom'

import ReactDOM from 'react-dom';
import EventForm from './components/EventForm';
import EventSlideout from './../events/components/EventSlideout';
import reducer from './reducers'
import { Provider } from 'react-redux'
import middleware from './middleware'
import { createStore } from 'redux'

let event = {};

class App extends Component {
  constructor(props) {
    super(props);
  }
  render()
  {
    return (
      <div className="event-form-wrapper">
        {/* <EventForm /> */}
        {/* <h2>Event Registration</h2> */}
        {/* <Provider> */}
          <Router hashType="noslash">
            <Route path="/:event?" component={EventForm} />
            {/* <Route path="/:event?" component={EventSlideout} /> */}
          </Router>
        {/* </Provider>, */}
        {/* <EventForm />
        <EventSlideout /> */}
      </div>
    );
  }
}

const fields = []
eventFormFields.forEach( fieldGroup => {
  fieldGroup.sub_fields.forEach( field => {
    fields[field.name] = field
  })
})
const initialState = {
  fields
}

const store = createStore(reducer, initialState, middleware)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('event-registration-wrapper')
)
