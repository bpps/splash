import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import App from './App';
import reducer from './reducers'
import middleware from './middleware'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { formatDate } from './utils'
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#000'
    },
    secondary: {
      main: '#f44336',
    },
  },
});

let today = new Date();
let firstEventDate = new Date(eventIntros[0]);
let lastEventDate = new Date(eventIntros[eventIntros.length - 1]);
// If todays date is less than the first day of the event week, use the first day. Otherwise, use today's date.
let startDate = today < firstEventDate || today > lastEventDate ? formatDate(firstEventDate) : formatDate(today)
const initialState = {
  events: null,
  loadingEvents: true,
  selectedCat: eventCats.length ? eventCats[0].slug : null,
  selectedDay: startDate,
  selectedEvent: null,
  selectedEventType: nycjwEventSettings.selected_event_type,
  selectedTestDay: null,
  today: new Date,
  mapToggled: false,
  mapDate: false
}

const store = createStore(reducer, initialState, middleware)

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter basename={`${process.env.NODE_ENV === 'production' ? 'events' : 'nycjw/events'}`}>
            <Routes>
                <Route path="*" element={<App/>} />
                <Route path="/" exact element={<App/>}/>
                <Route path="/:type" exact element={<App/>}/>
                <Route path="/:type/:cat" exact element={<App/>}/>
                <Route path="/:type/:cat/:event" exact element={<App/>}/>
                <Route path="/:type/:cat/:event/:supporting" exact element={<App/>}/>
            </Routes>
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
