import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import DayTab from './DayTab'
import CatTab from './CatTab'
import { useDispatch, useSelector } from 'react-redux';
// import { selectEventType } from '../actions/eventType'
import Button from '@mui/material/Button';
import MapIcon from '@mui/icons-material/Map';
import { toggleMap } from '../actions/map'

const Header = () => {

  const events = useSelector( state => state.events );
  const mapToggled = useSelector( state => state.mapToggled );
  // const eventType = useSelector( state => state.eventType );
  const [eventCatsFiltered, setEventCatsFiltered] = useState([])
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();

  useEffect( () => {
    if ( !events ) return
    if ( !params.cat && params.type !== 'virtual' ) {
      const selectedType = params.type ? params.type : 'one-time'
      const selectedCat = selectedType === 'one-time'
        ? eventIntros[0]
        : eventCatsFiltered[0].slug
      // navigate({ pathname: `/${selectedType}/${selectedCat}/`})
      navigate({ pathname: `/virtual/`})
    }
    setEventCatsFiltered((Object.keys(eventCats).length && events) 
      ? eventCats.filter( eventCat => {
          return eventCat.slug in events && events[eventCat.slug].filter( event => event.durational && !event.virtual ).length
        })
      : [])
  }, [events])

  const hasEventsByday = day => {
    let dayData = new Date(day);
    let fullYear = dayData.getFullYear();
    let fullMonth = (dayData.getMonth() + 1) < 10 ? `0${dayData.getMonth() + 1}` : (dayData.getMonth() + 1);
    let fullDate =  (dayData.getDate()) < 10 ? `0${dayData.getDate()}` : (dayData.getDate());
    let dateString = `${fullYear}-${fullMonth}-${fullDate}`;
    let eventsByDate = events[dateString];
    if (eventsByDate?.length > 0 ) {
      return true;
    } else {
      return false;
    }
  }

  // const eventDaysFiltered = eventIntros.length && events
  // ? eventIntros.filter( event => {
  //     return hasEventsByDay(event)
  //   })
  // : []

  // console.log(eventDaysFiltered)

  // useEffect(() => {
  //   if (location.search.includes('?eventType') && events) {
  //     const urlSearchParams = new URLSearchParams(location.search);
  //     const intro = urlSearchParams.get('intro') || false
  //     if (eventType !== eventType) {
  //       if (eventType !== 'one-time') {          
  //         let index = eventCats.findIndex(it => {
  //           return it.slug == eventType;
  //         })
  //         if ( index < 0) {
  //           let eventCat = eventCats.find( eventCat => {
  //             return eventCat.count > 0 && events[eventCat.slug]?.length > 0
  //           });
  //           navigate({
  //             to: `/${eventCat ? eventCat.slug : 'on-going'}`,
  //           });
  //         }
  //       }

  //       changeEventType(eventType, intro);
  //     }
  //   }
  // }, [events])

  return (
    <div id="events-header">
      {
        nycjwEventSettings.watch_live_link &&
        <div className="watch-live-link text-centered">
          <a className="btn watch-live" target={nycjwEventSettings.watch_live_link.target} href={ nycjwEventSettings.watch_live_link.url }>
            <span>{ nycjwEventSettings.watch_live_link.title }</span>
          </a>
        </div>
      }
      <div className="sub-nav sub-nav-events" style={{ 'display': 'none' }}>
        {
          <ul className="menu">
            {
              eventsSubMenu.map( menuItem => {
                return <li key={`menu-${menuItem.ID}`}>
                  <Button
                  variant="contained" 
                  size="small"
                  sx={{
                    color: '#000',
                    fontSize: '1.2rem',
                    fontFamily: 'Bebas Neue',
                    background: 'transparent',
                    border: 'none',
                    '&:hover, &.Mui-focusVisible': {
                      background: 'transparent'
                    },
                  }}
                  onClick={() => window.location.replace(menuItem.url)}
                  >{ menuItem.title }</Button>
                </li>
              })
            }
            <li>
              <Button 
                variant="contained" 
                endIcon={<MapIcon/>}
                size="small"
                disabled={ params.type === 'virtual' }
                sx={{
                  color: '#000',
                  fontSize: '1.2rem',
                  fontFamily: 'Bebas Neue',
                  background: 'transparent',
                  border: 'none',
                  '&:hover, &.Mui-focusVisible': {
                    background: 'transparent'
                  },
                }}
                onClick={ () => dispatch(toggleMap(!mapToggled)) }>Map View</Button>
            </li>
          </ul>
        }
      </div>
      <div id="events-sub-header">
        <div className={`event-type-menu-wrapper${ events ? '' : ' disabled'}`}>
          {
            eventIntros.length > 0 &&
            <div className="event-type-menu">
              <div className="event-type-button-wrapper">
                <Button
                  variant="contained"
                  size="large"
                  className="event-type"
                  //startIcon={<DevicesIcon sx={{ color: 'black' }} />}
                  sx={{
                    color: '#000',
                    background: params.type === 'virtual' ? '#FEF200' : '#fff',
                    border: '3px solid black',
                    borderRadius: '6px',
                    '&:hover, &.Mui-focusVisible': {
                      background: params.type === 'virtual' ? '#FEF200' : 'rgba(0,0,0,.05)',
                      borderColor: 'black'
                    },
                  }}
                  onClick={() => navigate({
                    pathname: `/virtual/`,
                  })}
                >
                  <div className="btn-content">Virtual Events</div>
                </Button>
              </div>
              <div className="event-type-button-wrapper">
                <Button
                  variant="contained"
                  size="large"
                  className="event-type"
                  // startIcon={<TodayIcon sx={{ color: 'black' }} />}
                  sx={{
                    color: '#000',
                    background: (params.type === 'one-time' || params.type === undefined) ? '#FEF200' : '#fff',
                    border: '3px solid black',
                    borderRadius: '6px',
                    '&:hover, &.Mui-focusVisible': {
                      background: (params.type === 'one-time' || params.type === undefined) ? '#FEF200' : 'rgba(0,0,0,.05)',
                      borderColor: 'black'
                    },
                  }}
                  onClick={() => navigate({
                    pathname: `/one-time/${eventIntros[0]}/`
                  })}
                >
                  <div className="btn-content">one-time Events</div>
                </Button>
              </div>
              <div className="event-type-button-wrapper">
                <Button
                  variant="contained"
                  size="large"
                  className="event-type"
                  //startIcon={<CalendarMonthIcon sx={{ color: 'black' }} />}
                  sx={{
                    color: '#000',
                    background: params.type === 'on-going' ? '#FEF200' : '#fff',
                    border: '3px solid black',
                    borderRadius: '6px',
                    '&:hover, &.Mui-focusVisible': {
                      background: params.type === 'on-going' ? '#FEF200' : 'rgba(0,0,0,.05)',
                      borderColor: 'black'
                    },
                  }}
                  onClick={() => navigate({
                    pathname: `/on-going/${eventCatsFiltered[0].slug}/`,
                  })}
                >
                  <div className="btn-content">on-going Events</div>
                </Button>
              </div>
            </div>
          }
        </div>
      </div>
      <section id="events-tabs-wrapper">
          {
              params.type === 'virtual' && 
              <div>
                <div id="virtual-events-section">
                  <div id="virtual-header">
                    <h2>Virtual Events</h2>
                  </div>
                </div>
                <div id="events-header-tabs" style={{ 'display': 'none' }}>
                    <div style={{ 'pointerEvents': 'none' }} onClick={ () => navigate({ pathname: `/virtual`}) } className={ `header-tab ${(params.type === 'virtual' && params.cat !== 'instagram') ? ' active' : ''}` }>
                      <h3>Virtual Events</h3>
                    </div>
                    <div style={{ 'display': 'none' }} onClick={ () => navigate({ pathname: `/virtual/instagram`}) } className={ `header-tab ${params.cat === 'instagram' ? ' active' : ''}` }>
                      <h3>Instagram Events</h3>
                    </div>
                </div>
              </div>
          }
          {
              (params.type === 'one-time' || params.type === undefined) &&
              <div id="events-header-tabs">
                  {
                      eventIntros.length > 1 &&
                      eventIntros.map( day => {
                          return <DayTab key={day} day={day} />
                      })
                  }
              </div>
          }
          {
              params.type === 'on-going' && 
              <div id="events-header-tabs">
                  {
                      events && (
                          Object.keys(eventCatsFiltered).length > 1 &&
                          Object.keys(eventCatsFiltered).map( cat => {
                              return <CatTab key={eventCatsFiltered[cat].slug} cat={eventCatsFiltered[cat]} />
                          })
                      )
                  }
              </div>
          }
      </section>
      <div id="events-tab-select-wrapper">
          {
              params.type === 'virtual' && 
              // Temporarily hide this selector after the event week is over
              <div>
                <select
                    id="events-tab-select"
                    value={`/${params.type}${ params.cat === 'instagram' ? '/instagram' : '' }`}
                    onChange={ e => navigate({ pathname: e.target.value }) }>
                    <option value='/virtual' key="cat-virtual">
                      Virtual Events
                    </option>
                    <option value='/virtual/instagram' key="cat-instagram">
                      Instagram Events
                    </option>
                </select>
              </div>
          }
          {
              params.type === 'one-time' && 
              <select
                  id="events-tab-select"
                  value={params.cat}
                  onChange={ e => navigate({ pathname: `/${params.type ? params.type : 'one-time'}/${e.target.value}/`}) }>
                  {
                      eventIntros.map( day => {
                          return <DayTab option={true} key={day} day={day} />
                      })
                  }
              </select>
          }
          {
              params.type === 'on-going' && 
              <select
                  id="events-tab-select"
                  value={params.cat}
                  onChange={ e => navigate({ pathname: `/${params.type}/${e.target.value}/`}) }>
                  {
                      Object.keys(eventCatsFiltered).map( cat => {
                          return <CatTab option={true} key={eventCatsFiltered[cat].slug} cat={eventCatsFiltered[cat]} name={eventCatsFiltered[cat]} />
                      })
                  }
              </select>
          }
      </div>
    </div>
  )
}

Header.propTypes = {

}

export default Header
