import React, { useRef, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import { useNavigate, useParams } from "react-router-dom";
import closeButton from "../../../images/x.png";
import iconTalk from "../../../images/map/talk.svg";
import iconExhibit from "../../../images/map/exhibition.svg";
import iconPanel from "../../../images/map/panel.svg";
import iconRetail from "../../../images/map/retail.svg";
import iconSlideControl from "../../../images/map/slide-control.svg";
import { toggleMap } from "../actions/map";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

const icons = [
  { name: "talk", image: iconTalk },
  { name: "exhibit", image: iconExhibit },
  { name: "panel", image: iconPanel },
  { name: "retail", image: iconRetail },
];

const accessToken =
  "pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw";
mapboxgl.accessToken = accessToken;

const Dates = ({ dates, isOpen, onChange }) => {
  const renderDates = (date, i) => {
    const weekday = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ];
    const dayOfWeek = new Date(date).getDay();
    return (
      <FormControlLabel
        key={`date-${i}`}
        className="nycjw-map-date-item"
        control={
          <Checkbox
            checked={dates[date]}
            onChange={() => onChange(date)}
            size="small"
            sx={{
              padding: "0 .5rem 0 0",
              "&.Mui-checked": {
                color: "#000",
              },
            }}
          />
        }
        label={weekday[dayOfWeek]}
      />
    );
  };

  return (
    <div id="nycjw-map-dates" className={`${isOpen ? "opened" : ""}`}>
      <h3>Filter By Dates</h3>
      <FormGroup>{Object.keys(dates).map(renderDates)}</FormGroup>
    </div>
  );
};

const Legend = (props) => {
  const renderCategoryKeys = (category, i) => {
    return (
      <div key={i} className="nycjw-map-legend-item">
        <img src={props.categories[category].icon} />
        <span>{props.categories[category].name}</span>
      </div>
    );
  };

  return (
    <div id="nycjw-map-legend">
      <h3>Categories</h3>
      {Object.keys(props.categories).map(renderCategoryKeys)}
    </div>
  );
};

const Marker = ({ onClick, onEnter, onLeave, children, event }) => {
  const _onClick = () => {
    onClick(event);
  };

  const _onEnter = () => {
    onEnter(event);
  };

  const _onLeave = () => {
    onLeave();
  };

  let icon = null;
  if (event.event_types) {
    switch (event.event_types[0].slug) {
      case "exhibition":
      case "reception":
        icon = iconExhibit;
        break;
      case "panel":
      case "workshop":
        icon = iconPanel;
        break;
      case "artist-talk":
      case "talk":
        icon = iconTalk;
        break;
      case "retail-event":
        icon = iconRetail;
        break;
      default:
        icon = iconExhibit;
        break;
    }
  }

  return (
    <button onClick={_onClick} onMouseEnter={_onEnter} className="marker">
      {icon && <img src={icon} />}
    </button>
  );
};

const Tooltip = ({ event, clickDetails }) => {
  const { id, location, title } = event;
  var address = location.address.replace(/\,/g, "");
  var directionsUrl = address.replace(/\ /g, "%20");
  return (
    <div className="map-tooltip" id={`tooltip-${id}`}>
      <h3 dangerouslySetInnerHTML={{ __html: title }} />
      {location && (
        <p dangerouslySetInnerHTML={{ __html: location.address }}></p>
      )}
      <button className="btn small details" onClick={() => clickDetails()}>
        Details
      </button>
      <button
        className="btn small directions"
        onClick={() =>
          window.open(`https://maps.google.com/?q=${directionsUrl}`, "_blank")
        }
      >
        Get Directions
      </button>
    </div>
  );
};

const Map = () => {
  const mapToggled = useSelector((state) => state.mapToggled);
  const mapDate = useSelector((state) => state.mapDate);
  const events = useSelector((state) => state.events);

  const mapContainer = useRef(null);
  const mapWrapper = useRef(null);
  const map = useRef(null);
  const tooltipRef = useRef(new mapboxgl.Popup({ offset: 10 }));

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const params = useParams();

  const [mapDates, setMapDates] = useState({});
  const [allEvents, setAllEvents] = useState([]);
  const [openDateFilter, setOpenDateFilter] = useState(true);

  const [lng, setLng] = useState(-73.97915);
  const [lat, setLat] = useState(40.7590403);
  const [zoom, setZoom] = useState(12);
  const [bounds, setBounds] = useState([]);

  useEffect(() => {
    if (map.current || !events) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q",
      center: [lng, lat],
      zoom: zoom,
    });
  }, [events]);

  useEffect(() => {
    if (!events || !map.current) return;
    let boundsArray = bounds;

    map.current.on("load", () => {
      const eventLocations = [];

      Object.keys(events).forEach((eventDate) => {
        const dayEvents = events[eventDate];
        dayEvents.forEach((dayEvent) => {
          if (!dayEvent.location) return;
          if (!dayEvent.location.lat) return;
          let datesArray = [];
          if (dayEvent.date) {
            datesArray.push(dayEvent.date);
          }
          // If multiple dates, add all dates into array
          if (dayEvent.dates) {
            datesArray = dayEvent.dates.map((date) => date.event_date);
          }
          // If range of dates, add all dates into array
          if (dayEvent.start_date) {
            const startDate = dayEvent.start_date.split("-")[2];
            const endDate = dayEvent.end_date.split("-")[2];
            for (let i = startDate; i <= endDate; i++) {
              datesArray.push(`2022-11-${i}`);
            }
          }

          boundsArray.push([dayEvent.location.lng, dayEvent.location.lat]);
          eventLocations.push({
            id: dayEvent.location.place_id,
            lng: dayEvent.location.lng,
            lat: dayEvent.location.lat,
            dates: datesArray,
            ...dayEvent,
            permalink: dayEvent.durational 
              ? `/on-going/${dayEvent.event_types[0].slug}/${dayEvent.permalink}`
              : dayEvent.date 
                ? `/one-time/${dayEvent.date}/${dayEvent.permalink}`
                : `/one-time/${dayEvent.start_date}/${dayEvent.permalink}`
          });
        });
      });

      setAllEvents(eventLocations);

      icons.forEach((icon) => {
        const iconImage = new Image(40, 40);
        iconImage.src = icon.image;
        map.current.addImage(icon.name, iconImage);
      });

      addSourceAndLayersToMap(eventLocations);
    });
  }, [events, map]);
  
  const addSourceAndLayersToMap = (eventLocations) => {
    map.current.addSource("events", {
      type: "geojson",
      // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes
      // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
      // data: 'https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson',
      data: {
        type: "FeatureCollection",
        features: eventLocations.map((item) => ({
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: [item.lng, item.lat],
          },
          properties: {
            ...item,
            id: item.id,
            geometry: {
              type: "Point",
              coordinates: [item.lng, item.lat],
            }
          }
        }))
      },
      cluster: true,
      clusterMaxZoom: 14, // Max zoom to cluster points on
      clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
    });

    map.current.addLayer({
      id: "clusters",
      type: "circle",
      source: "events",
      filter: ["has", "point_count"],
      paint: {
        // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
        // with three steps to implement three types of circles:
        //   * Blue, 20px circles when point count is less than 100
        //   * Yellow, 30px circles when point count is between 100 and 750
        //   * Pink, 40px circles when point count is greater than or equal to 750
        "circle-color": [
          "step",
          ["get", "point_count"],
          "#000",
          100,
          "#000",
          750,
          "#000",
        ],
        "circle-radius": ["step", ["get", "point_count"], 14, 100, 14, 750, 14],
      },
    });

    map.current.addLayer({
      id: "cluster-count",
      type: "symbol",
      source: "events",
      filter: ["has", "point_count"],
      layout: {
        "text-field": "{point_count_abbreviated}",
        "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
        "text-size": 12,
      },
      paint: {
        "text-color": "#fff",
      },
    });

    map.current.addLayer({
      id: "unclustered-point",
      type: "circle",
      source: "events",
      filter: ["!", ["has", "point_count"]],
      paint: {
        "circle-color": "#000",
        "circle-radius": 10,
        "circle-stroke-width": 1,
        "circle-stroke-color": "#fff",
      },
    });

    map.current.addLayer({
      id: "unclustered-count",
      type: "symbol",
      filter: ["!", ["has", "point_count"]],
      source: "events",
      layout: {
        "icon-image": [
          "match", // Use the 'match' expression: https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-match
          ["get", "icon"],
          "exhibition",
          "exhibit",
          "talk",
          "talk",
          "panel",
          "panel",
          "retail-event",
          "retail",
          "",
          "exhibit",
          "exhibit",
        ],
        "icon-size": 0.5,
      },
    });

    // inspect a cluster on click
    map.current.on("click", "clusters", (e) => {
      const features = map.current.queryRenderedFeatures(e.point, {
        layers: ["clusters"],
      });
      const clusterId = features[0].properties.cluster_id;
      map.current
        .getSource("events")
        .getClusterExpansionZoom(clusterId, (err, zoom) => {
          if (err) return;

          map.current.easeTo({
            center: features[0].geometry.coordinates,
            zoom: zoom,
          });
        });
    });
    
    // When a click event occurs on a feature in
    // the unclustered-point layer, open a popup at
    // the location of the feature, with
    // description HTML from its properties.
    map.current.on("click", "unclustered-point", (e) => {
      const coordinates = JSON.parse(e.features[0].properties.geometry).coordinates.slice();
      let { id, title, location, permalink } = e.features[0].properties;
      location = JSON.parse(location);
      var address = location.address.replace(/\,/g, "");
      var directionsUrl = address.replace(/\ /g, "%20");

      // Ensure that if the map is zoomed out such that
      // multiple copies of the feature are visible, the
      // popup appears over the copy being pointed to.
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      const popups = document.getElementsByClassName("mapboxgl-popup");
      if (popups.length) {
        popups[0].remove();
      }
      new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(
          `<div class="map-tooltip" id="tooltip-${id}">
            <h3>${title}</h3>
            ${location && `<p>${location.address}</p>`}
            <button class="btn small details" id="details-${id}">
              Details
            </button>
            <button
              type="button"
              class="btn small directions"
              onClick="window.open('https://maps.google.com/?q=${directionsUrl}', '_blank')">
              Get Directions
            </button>
          </div>`
        )
        .addTo(map.current);
        console.log('hohooho')
        document.getElementById(`details-${id}`).addEventListener("click", () => navigateTo(permalink));
    });

    map.current.on("mouseenter", "clusters", () => {
      map.current.getCanvas().style.cursor = "pointer";
    });
    map.current.on("mouseleave", "clusters", () => {
      map.current.getCanvas().style.cursor = "";
    });
  };

  const navigateTo = permalink => {
    navigate(permalink, { replace: true });
  }

  useEffect(() => {
    if (!map.current || bounds.length === 0) return;
    var maxBounds = bounds.reduce(function (bound, coord) {
      return bound.extend(coord);
    }, new mapboxgl.LngLatBounds(bounds[0], bounds[0]));
    map.current.fitBounds(maxBounds, {
      padding: 60,
    });
  }, [bounds, map]);

  useEffect(() => {
    let mapDates = {};
    eventIntros.forEach((intro) => {
      mapDates[intro] = true;
    });
    if (mapDate) {
      Object.keys(mapDates).forEach((mapDate) => {
        mapDates[mapDate] = mapDate === params.cat;
      });
      let filteredEvents = [];
      filteredEvents = allEvents.filter((eventItem) =>
        eventItem.dates.includes(params.cat)
      );
      map.current.removeLayer("clusters");
      map.current.removeLayer("cluster-count");
      map.current.removeLayer("unclustered-point");
      map.current.removeLayer("unclustered-count");
      map.current.removeSource("events");
      addSourceAndLayersToMap(filteredEvents);
    }
    setMapDates(mapDates);
  }, [mapDate]);

  const handleChangeDate = (date) => {
    const cloneMapDates = { ...mapDates, [date]: !mapDates[date] };
    setMapDates(cloneMapDates);
    map.current.removeLayer("clusters");
    map.current.removeLayer("cluster-count");
    map.current.removeLayer("unclustered-point");
    map.current.removeLayer("unclustered-count");
    map.current.removeSource("events");

    const activeDates = Object.keys(cloneMapDates).filter(
      (keyDate) => cloneMapDates[keyDate]
    );
    let filteredEvents = [];
    if (activeDates.length > 0) {
      filteredEvents = allEvents.filter((eventItem) =>
        activeDates.some((activeDate) => eventItem.dates.includes(activeDate))
      );
    }
    addSourceAndLayersToMap(filteredEvents);
  };

  const handleCloseMap = (event) => {
    if (mapWrapper && mapWrapper.current === event.target) {
      event.preventDefault();
      dispatch(toggleMap(false));
    }
  };

  const categories = {
    exhibit: {
      name: "Exhibit/Receiption",
      icon: iconExhibit,
    },
    panel: {
      name: "Panel/Workshop",
      icon: iconPanel,
    },
    talk: {
      name: "Talk",
      icon: iconTalk,
    },
    retail: {
      name: "Retail Event",
      icon: iconRetail,
    },
  };

  return (
    <div
      id="nycjw-map-container"
      ref={mapWrapper}
      onClick={handleCloseMap}
      className={mapToggled ? "active" : ""}
    >
      <div id="nycjw-map-wrapper">
        <div
          ref={mapContainer}
          id="nycjw-map"
          onClick={() => setOpenDateFilter(false)}
        />
        <button id="close-map" onClick={() => dispatch(toggleMap(false))}>
          <img src={closeButton} />
        </button>
        <button id="slide-control-map" onClick={() => setOpenDateFilter(true)}>
          <img src={iconSlideControl} alt="slide control" />
        </button>
        <Dates
          dates={mapDates}
          onChange={handleChangeDate}
          isOpen={openDateFilter}
        />
        <Legend categories={categories} />
      </div>
    </div>
  );
};

export default Map;
