import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import Event from './Event'
import { fetchAllEvents, by_start_date_time, by_end_date_time, moveElementToEndOfArray, moveElementToStartOfArray } from '../utils'
import { setEvents } from '../actions/events'
import { setTodaysDate } from '../actions/today'
import { mapDate } from '../actions/map'

const Events = props => {
  const dispatch = useDispatch();
  const loadingEvents = useSelector( state => state.loadingEvents );
  const events = useSelector( state => state.events );
  const selectedTestDay = useSelector( state => state.selectedTestDay );
  let renderEvents = []
  const params = useParams();

  useEffect( () => {
    console.log('Welcome to Jewelry Week!')
    setInterval(() => dispatch(setTodaysDate(new Date)), 60000);

    fetchAllEvents()
    .then( events => {
      dispatch( setEvents(events) )
    })
    .catch( error => console.log(error) )
  }, [])

  if ( events ) {
    if ( !events ) return
    if ( params.type !== 'virtual' && params.cat ) {
      renderEvents = events[params.cat].filter( event => !event.virtual )
    } 

    if ( params.type === 'virtual' ) {
      Object.keys(events).forEach( eventsKey => {
        events[eventsKey].forEach( event => {
          if ( event.virtual ) {
            if ( params.cat === 'instagram' ) {
              if ( event.instagram ) {
                renderEvents.push(event)
              }
            } else {
              if ( !event.instagram ) {
                renderEvents.push(event)
              }
            }
          }
        })
      })
    }

    // renderEvents.sort(alphabetically);
    renderEvents.sort(by_start_date_time);

    // Move events with no start time to the end
    renderEvents.forEach( (renderEvent, i) => {
      if ( 'date' in renderEvent && (!('start_time' in renderEvent) || renderEvent.start_time === null) ) {
        renderEvents = moveElementToEndOfArray(renderEvents, i)
      }
    })

    // move joy of collecting to the top
    renderEvents.forEach( (renderEvent, i) => {
      if ( renderEvent.id === 19948 ) {
        renderEvents = moveElementToStartOfArray(renderEvents, i)
      }
    })
    
    // renderEvents.sort(by_end_date_time);
    // if (params.cat == 'scheduled-event' && params.type == 'ongoing') {
    //   let allScheduled = [];
    //   // console.log(Object.keys(events));
    //   // console.log(events);
    //   let allKeys = Object.keys(events);

    //   allKeys.forEach(it => {
    //     let catIndex = eventCats.findIndex(cat => {
    //       return cat.slug == it
    //     });
    //     if (catIndex < 0) {
    //       let dayEvents = events[it];
    //       dayEvents.forEach(event => {
    //         let dayEventIndex = allScheduled.findIndex(it1 => it1.id == event.id);
    //         if (dayEventIndex < 0) {
    //             allScheduled.push(event);
    //         }
    //       })
    //     }
    //   });
    //   renderEvents = allScheduled;
    // }
  }

  // console.log(events);
  let today = new Date

  const todaysDate = selectedTestDay ? selectedTestDay : `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`
  const weekEndDate = new Date(eventIntros[eventIntros.length - 1]);
  
  return (
    <div>
      {
        loadingEvents
        ? <div className="event-loading-wrapper">
            <div className="event-loading">Loading</div>
          </div>
        : <div>
            <div id="events-wrapper">
              {
                (renderEvents.length > 0 && params.type === 'one-time') &&
                <div style={{ display: 'none' }} id="map-current-date"><button className="btn" onClick={() => dispatch(mapDate())}>Map this date</button></div>
              }
              {
                renderEvents.length > 0
                ? renderEvents.map( (nycjwEvent, index) => {
                    if ( nycjwEvent.hide_from_listing ) return <div key={nycjwEvent.id}></div>
                    if ( nycjwEvent.intro && (todaysDate < params.cat || today > weekEndDate) ) return <div key={nycjwEvent.id}></div>
                    return <Event key={`event-${index}`} nycjwEvent={nycjwEvent} />
                  })
                : <div className="no-events"><p>No events</p></div>
              }
            </div>
          </div>
      }
    </div>
  )
}

export default Events
