import React from 'react'
import { useNavigate, useParams } from 'react-router-dom';

const CatTab = props => {
  const navigate = useNavigate();
  const params = useParams();
  const { cat, option } = props
  if ( !option ) {
    return (
      <div onClick={ () => navigate({ pathname: `/${params.type}/${cat.slug}`}) } className={ `header-tab ${params.cat === cat.slug ? ' active' : ''}` }>
        <h3>{cat.plural ? cat.plural : cat.name}</h3>
      </div>
    )
  } else {
    return (
      <option value={cat.slug} key={`cat-${cat}`}>
        {cat.name}
      </option>
    )
  }
}

export default CatTab
