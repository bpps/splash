import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { formatPrettyDate, fetchEventContentById, beautyTime } from '../utils'
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import ReactDOM from 'react-dom/client';
import facebook from '../../../images/fb.png';
import instagram from '../../../images/instagram.png';
import twitter from '../../../images/twitter.png';
import tiktok from '../../../images/tiktok.png';
import iconTalk from '../../../images/map/talk.svg'
import iconExhibit from '../../../images/map/exhibition.svg'
import iconPanel from '../../../images/map/panel.svg'
import iconRetail from '../../../images/map/retail.svg'
import websiteIcon from '../../../images/event-website.png';
import Slider from "react-slick";
const socialIcons = { facebook, instagram, tiktok, twitter }

const accessToken = 'pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw';
mapboxgl.accessToken = accessToken;

const EventSlideout = props => {
  const navigate = useNavigate();
  
  const params = useParams();
  
  const curEventType = params.type;
  let currentPath = params.type === 'virtual' ? params.cat : params.event
  let supportingUrl = params.type === 'virtual' 
    ? params.cat === 'instagram'
      ? null
      : params.event
    : params.supporting || null
  if ( params.cat === 'instagram' ) {
    currentPath = params.event || ''
  }

  const mapContainer = useRef(null);
  const map = useRef(null);
  const slideoutRef = useRef(null);
  const slideoutClose = useRef()

  const selectedTestDay = useSelector( state => state.selectedTestDay )
  const [today, setToday] = useState(new Date)
  const [eventContent, setEventContent] = useState(null)
  const [scrollPos, setScrollPos] = useState(null)
  const [loading, setLoading] = useState(false)
  const [closing, setClosing] = useState(false)
  const [showInstructions, setShowInstructions] = useState(false)
  const [mapMarkers, setMapMarkers] = useState([]);
  const [locationURL, setLocationURL] = useState(null)
  
  const [lng, setLng] = useState(-73.9791500);
  const [lat, setLat] = useState(40.7590403);
  const [zoom, setZoom] = useState(12);
  const [bounds, setBounds] = useState([]);

  const intro = params.intro ? params.intro : false;

  useEffect(() => {
    if ( currentPath !== '' ) {
      getEventContent(true, intro)
    }
    setToday(selectedTestDay ? new Date(selectedTestDay + ` ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`) : new Date)
  }, [])

  useEffect(() => {
    if ( map.current ) {
      populateMarker()
    } else {
      if ( !mapContainer.current || !eventContent.location_address ) return
      map.current = new mapboxgl.Map({
        container: mapContainer.current,
        style: 'mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q',
        center: [lng, lat],
        zoom: zoom
      });
      map.current.scrollZoom.disable()
      populateMarker()
    }
    let directionsUrl = null
    if ( eventContent && 'location_address' in eventContent && eventContent.location_address !== "" ) {
      var address = eventContent.location_address.address.replace(/\,/g, '');
      var addressUrl = address.replace(/\ /g, '%20');
      directionsUrl = `https://www.google.com/maps/dir//${addressUrl}`
    }
    setLocationURL(directionsUrl)
    window.onpopstate = e => {
      closeSlideout(e)
    }
  }, [eventContent])

  const populateMarker = () => {
    if ( !eventContent || !('location_address' in eventContent) || eventContent.location_address === "" ) return
    const ref = React.createRef();
    // Create a new DOM node and save it to the React ref
    ref.current = document.createElement("div");
    const markerRoot = ReactDOM.createRoot(ref.current);
    let icon = iconPanel
    if ( eventContent.event_types ) {
      switch( eventContent.event_types[0].slug ) {
          case 'exhibition':
          case 'reception':
              icon = iconExhibit
          break;
          case 'panel':
          case 'workshop':
              icon = iconPanel
          break;
          case 'artist-talk':
          case 'talk':
              icon = iconTalk
          break;
          case 'retail-event':
              icon = iconRetail
          break;
          default:
              icon = iconExhibit
          break;
      }
  }
    markerRoot.render(<div className="map-event-marker"><img src={ icon }/></div>);
    // Render a Marker Component on our new DOM node
    const coordinates = new mapboxgl.LngLat(eventContent.location_address.lng, eventContent.location_address.lat);
    // Create a Mapbox Marker at our new DOM node
    new mapboxgl.Marker(ref.current)
      .setLngLat(coordinates)
      .addTo(map.current);
    const bounds = [ 
      { lat: coordinates.lat - .0008, lng: coordinates.lng - .0008 }, 
      { lat: coordinates.lat + .0008, lng: coordinates.lng + .0008 }
    ]
    map.current.fitBounds(bounds, { padding: 80 });
      
  }

  const toggleShowInstructions = () => {
    setShowInstructions(!showInstructions)
  }


  const getEventContent = (update, intro) => {
    setLoading(true)
    setScrollPos(window.scrollY)
    document.body.classList.add('modal-open');
    fetchEventContentById(currentPath, intro, supportingUrl).then( content => {
      if ( content ) {

        const timeout = window.screen.width < 768 ? 0 : 250
        setTimeout( () => {
          setEventContent(content)
          setLoading(false)
        }, timeout)
      } else {
        document.body.classList.remove('modal-open');
        setLoading(false)
        setScrollPos(window.scrollY)
        
      }

    })
  }

  const closeSlideout = event => {
    if ( event.type === 'popstate' || slideoutRef && !slideoutRef.current.contains(event.target) || slideoutClose.current.contains(event.target) ) {
      event.preventDefault()
      const timeout = window.screen.width < 768 ? 0 : 300
      document.body.classList.remove('modal-open');
      setClosing(true)
      const closePath = curEventType === 'virtual'
        ? params.cat === 'instagram' 
          ? `/${curEventType}/instagram`
          : `/${curEventType}`
        : `/${curEventType}/${params.cat}`
      setTimeout( () => {
        setClosing(false)
        setEventContent(null)
        navigate(closePath)
        window.scrollTo(0, scrollPos);
      }, timeout)
    }
    return
  }

  let borderColor = '#FEF200'
  if ( eventContent ) {
    if ( eventContent.event_themes && eventContent.event_themes.length > 0 ) {
      borderColor = eventContent.event_themes[0].color?eventContent.event_themes[0].color:'#FEF200';
    }
  }

  useEffect( () => {
    if ( selectedTestDay !== null ) {
      setToday(new Date(selectedTestDay + ` ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`))
    }
  }, [selectedTestDay])

  const settings = {
    dots: false,
    infinite: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  }
  let eventStatus = 'future'
  let rsvpLink = null
  let rsvpLabel = ''
  if ( eventContent ) {
    let eventStartDate = new Date( eventContent.event_date + 'T09:00:00')
    const weekStartDate = new Date('2022-11-14T07:00:00')
    const eventEndTime = eventContent.end_time
    let eventEndDate = new Date( eventContent.event_date + 'T' + eventEndTime)
    rsvpLink = null
    rsvpLabel = null
    if ( params.type === 'one-time' ) {
      eventStartDate = (eventContent.event_date && eventContent.event_date !== '')
        ? eventContent.event_date
        : eventContent.start_date
          ? eventContent.start_date
          : eventContent.dates[0].event_date
          console.log(typeof eventContent.start_time)
      let eventStartTime = eventContent.start_time 
        ? typeof eventContent.start_time === 'object' 
          ? eventContent.start_time.reduce(
            (acc, loc) =>
              acc.split(':')[0] < loc.split(':')[0]
                ? acc
                : loc
            )
          : eventContent.start_time
            ? eventContent.start_time
            :  '06:00:00'
        : '06:00:00'
      // if ( typeof eventContent.start_time === 'object' ) {
      //   eventStartTime = 
      // }
      eventStartDate = new Date( eventStartDate + 'T' + eventStartTime)
      if ( today >= eventStartDate ) {
        rsvpLabel = eventContent.link 
          ? eventContent.link.title === '' 
            ? 'View Now' 
            : eventContent.link.title  
          : null
        rsvpLink = eventContent.link ? eventContent.link.url : null
      } else {
        rsvpLink = eventContent.rsvp ? eventContent.rsvp.url : null
        rsvpLabel = ( eventContent.rsvp && eventContent.rsvp.title )
          ? eventContent.rsvp.title
          : 'RSVP'
      }
    }

    if ( params.type === 'on-going' || params.type === 'virtual' ) {
      if ( eventContent.start_date ) {
        console.log('START')
        let eventStartTime = eventContent.start_time ? eventContent.start_time : '06:00:00'
        eventStartDate = new Date( eventContent.start_date + 'T' + eventStartTime)
        let eventEndTime = eventContent.end_time ? eventContent.end_time : '11:59:00'
        eventEndDate = eventContent.end_date 
          ? new Date( eventContent.end_date + 'T' + eventEndTime )
          : new Date( eventContent.start_date + 'T' + eventEndTime )
      }

      if ( eventContent.dates && eventContent.dates.length ) {
        let eventStartTime = eventContent.dates[0].start_time ? eventContent.dates[0].start_time : '06:00:00'
        eventStartDate = new Date( eventContent.dates[0].event_date + 'T' + eventStartTime)
        let eventEndTime = eventContent.dates[eventContent.dates.length - 1].end_time ? eventContent.dates[eventContent.dates.length - 1].end_time : '11:59:00' 
        eventEndDate = new Date(eventContent.dates[eventContent.dates.length - 1].event_date + 'T' + eventEndTime )
      }

      if ( eventContent.date && typeof eventContent.date !== 'object' ) {
        let eventStartTime = eventContent.start_time ? eventContent.start_time : '06:00:00'
        eventStartDate = typeof eventContent.date === 'object' ? new Date( eventContent.date[0] + 'T' + eventStartTime) : new Date( eventContent.date + 'T' + eventStartTime)
        let eventEndTime = eventContent.date.end_time ? eventContent.date.end_time : '11:59:00' 
        eventEndDate = typeof eventContent.date === 'object' ? new Date(eventContent.date[eventContent.date[0] - 1] + 'T' + eventEndTime ) : new Date(eventContent.date + 'T' + eventEndTime )
        console.log(eventContent.title, eventContent, 'start date: ', eventStartTime, 'start time: ', eventContent.start_time)
      }

      if ( curEventType === 'on-going' ) {
        if ( today >= eventStartDate ) {
          rsvpLabel = eventContent.event_url ? 'View Now' : null
          rsvpLink = eventContent.event_url ? eventContent.event_url : null
        } else {
          rsvpLabel = ( eventContent.rsvp && eventContent.rsvp.title ) 
            ? eventContent.rsvp.title
            : 'Get Notified'
          rsvpLink = eventContent.rsvp ? eventContent.rsvp.url : 'https://nycjewelryweek.com/join-us/'
        }
      }
      
      console.log(today, eventStartDate)
      if ( curEventType === 'virtual' ) {
        if ( today >= eventStartDate || Object.prototype.toString.call(eventStartDate) === '[object Date]' ) {
          rsvpLabel = null 
          rsvpLink = null
          if ( nycjwEventSettings.watch_live_link ) {
            rsvpLabel = nycjwEventSettings.watch_live_link.title ? nycjwEventSettings.watch_live_link.title : 'Watch Now'
            rsvpLink = nycjwEventSettings.watch_live_link.url
          }
          if ( eventContent.link ) {
            rsvpLabel = eventContent.link.title ? eventContent.link.title : 'Watch Now'
            rsvpLink = eventContent.link.url
          }
          if ( eventContent.instagram ) {
            rsvpLabel = nycjwEventSettings.watch_live_link.title ? nycjwEventSettings.watch_live_link.title : 'Watch On Instagram'
            rsvpLink = 'https://instagram.com/nycjewelryweek'
          }
        } else {
          rsvpLabel = ( eventContent.rsvp && eventContent.rsvp.title ) 
            ? eventContent.rsvp.title
            : 'Get Notified'
          rsvpLink = eventContent.rsvp ? eventContent.rsvp.url : 'https://nycjewelryweek.com/join-us/'
        }
      }
    }
  }
  console.log(eventContent)

  return (
    <div
      id="event-slideout"
      className={`${loading ? 'loading' : ''}${eventContent ? 'loaded' : ''}${closing ? ' closing' : ''}`}
      onClick={closeSlideout}>
      <div id="event-slideout-wrapper" style={{ borderColor: borderColor }} ref={slideoutRef}>
        {
          eventContent &&
          <div ref={slideoutClose} style={{ borderColor: borderColor }} onClick={closeSlideout} id="close-slide">
            <p><span>Events</span></p>
          </div>
        }
        {
          eventContent &&
          <div id="event-slideout-content-wrapper" style={{paddingTop: 60}}>
            <div id="event-slideout-content">

              <div className="event-slideout-header">
                {
                  ( eventContent.event_types || eventContent.event_themes ) &&
                  <div className="top-wrapper">
                    <p className="event-type">
                      <span style={{ background: 'black', color: 'white' }}>
                        { eventContent.virtual_event === 'irl' ? 'In-person' : 'virtual' }
                      </span>
                      {
                        eventContent.event_types &&
                        <span style={{ background: '#fef200', color : 'black' }}>{ ( eventContent.event_types && eventContent.event_types.length > 0 ) ? eventContent.event_types[0].name : '' }</span>
                      }
                    </p>
                    {
                    rsvpLink &&
                      <>
                        {
                          rsvpLink === 'mailinglist' 
                          ? <a style={{ cursor: 'pointer' }} className="btn rsvp-link mailinglist" onClick={ e => {
                              e.preventDefault()
                              document.body.classList.add('signup-active');
                              document.body.classList.remove('modal-open');
                              navigate('/virtual')
                              setEventContent(null)
                              window.scrollTo(0, scrollPos);
                            }}>{ rsvpLabel }</a>
                          : <a target="_blank" className="btn rsvp-link" href={rsvpLink}>{ rsvpLabel }</a>
                        }
                      </>
                    }
                  </div>
                }
                <h2 dangerouslySetInnerHTML={{ __html: eventContent.title ? eventContent.title : 'EVENT TITLE' }}></h2>
                {
                  eventContent.subtitle &&
                  <h3 style={{ marginTop: '.75rem' }}>{ eventContent.subtitle }</h3>
                }
                {
                  eventContent.images &&
                  <div className="item">
                    <Slider className="event-image-gallery"  {...settings}>
                      {
                        eventContent.images.map( image => {
                          return (
                            <div key={image.id} className="event-image-wrapper">
                              <div
                                className="event-image"
                                role="img"
                                // aria-label={ image.alt }
                                style={{ backgroundImage: `url(${ image.src })` }}>
                              </div>
                            </div>
                          )
                        })
                      }
                    </Slider>
                  </div>
                }
              </div>
              {
                (( eventContent.dates && eventContent.dates.length > 0 ) || eventContent.start_date ) &&
                <div className="event-date" style={{ marginBottom: '2rem', 'display': 'none' }}>
                  <h3 className="underlined" style={{ marginBottom: '1rem' }}>
                    {eventContent.dates.length == 1 ? 'Event Date' : 'Event Dates'}
                  </h3>
                  {
                    eventContent.dates.length > 0 &&
                    <div>
                      {
                        eventContent.dates.map( (date, ind) => {
                          return (
                            <div key={`date-${ind}`}>
                              <p style={{ marginBottom: 0, marginTop: 0 }}>
                                <strong>
                                  { formatPrettyDate(date.event_date) }
                                </strong>
                                {
                                  date.start_time &&
                                  <span style={{ marginLeft: '1rem' }}>
                                  <strong>
                                  {beautyTime(date.start_time)}
                                  {
                                    date.end_time && `- ${beautyTime(date.end_time)}`
                                  }</strong>
                                  </span>
                                }
                              </p>
                            </div>
                          )
                        })
                      }
                    </div>
                  }
                  {
                    eventContent.start_date &&
                    <div>
                      <p style={{ marginBottom: 0 }}>
                        <strong>
                          {formatPrettyDate(eventContent.start_date)}
                          {
                            eventContent.end_date &&
                            <span>{` - ${formatPrettyDate(eventContent.end_date)}`}</span>
                          }
                        </strong>
                      </p>
                      {
                        eventContent.start_time &&
                        <p style={{ marginTop: 0 }}>
                          {beautyTime(eventContent.start_time)}
                          {
                            eventContent.end_time &&
                            <span>{` - ${beautyTime(eventContent.end_time)}`}</span>
                          }
                        </p>
                      }
                    </div>
                  }
                </div>
              }
              {
                (eventContent.location_name || eventContent.location_address || eventContent.access_instructions) &&
                <div style={{ marginBottom: '2rem' }}>
                  <h3 className="underlined" style={{ marginBottom: '1rem' }}>Location</h3>
                  {
                    ( eventContent.location_name && eventContent.virtual_event == 'irl' ) &&
                    <p style={{ marginBottom: 0 }}><strong>{ eventContent.location_name }</strong></p>
                  }

                  {
                    ( eventContent.location_address && eventContent.virtual_event == 'irl' ) &&
                    <p style={{ marginTop: 0, marginBottom: 0 }}>{ eventContent.location_address.address }</p>
                  }
                  {
                    ( eventContent.access_instructions && eventContent.virtual_event == 'irl' ) &&
                    <div className="special-instructions">
                      <sub style={{ textTransform: 'uppercase', marginBottom: '1rem', marginTop: '1rem', fontWeight: '600', cursor: 'pointer' }} className="underlined">Special Instructions:</sub>
                      <p>{ eventContent.access_instructions }</p>
                    </div>
                  }
                </div>
              }
              <div id="nycjw-event-map-wrapper" style={eventContent.location_address === "" ? { display: 'none' } : {} }>
                {
                  locationURL && 
                  <button className="btn small directions" onClick={ () => window.open( locationURL, '_blank' )}>Get Directions</button>
                }
                <div ref={mapContainer} id="nycjw-event-map"></div>
              </div>
              {
                eventContent.content &&
                <div>
                  {
                    !eventContent.intro &&
                    <h3 className="underlined" style={{ marginBottom: '1rem' }}>Description</h3>
                  }
                  <div className="event-content" dangerouslySetInnerHTML={{ __html: eventContent.content }}/>
                  {
                    params.type === 'virtual' && <p><i>Click <b>GET NOTIFIED</b> to be notified when this program is viewable.</i></p>
                  }
                  {
                    (eventContent.social && eventContent.social.length > 0) &&
                    <div className="event-social">
                      {
                        eventContent.project_url &&
                        <a target="_blank" title="website" className="project-url bold uppercase text-button social-link" href={eventContent.project_url}>
                          <img src={websiteIcon}/>
                        </a>
                      }
                      {
                        eventContent.social.map( social => {
                          return (
                            <a key={`social-${social.type}`} title={social.type} className={`social-link social-${social.type}`} target="_blank" href={social.link}>
                              <img src={socialIcons[social.type]}/>
                            </a>
                          )
                        })
                      }
                    </div>
                  }
                </div>
              }
              {
                (
                  (
                    eventContent.event_types && eventContent.event_types.length &&
                    (
                      eventContent.event_types[0].name.toLowerCase() == 'talk' ||
                      eventContent.event_types[0].name.toLowerCase() == 'performance' ||
                      eventContent.event_types[0].name.toLowerCase() == 'panel')) &&
                      eventContent.virtual_event == 'virtual' &&
                      !eventContent.hide_virtual
                    ) &&
                <div style={{ marginTop: '2rem', marginBottom: '2rem' }}>
                  <p style={{ marginBottom: '.25rem', marginTop: 0 }}><strong>NYCJW22 VIRTUAL PROGRAM</strong></p>
                  <p style={{ marginBottom: '.25rem', marginTop: 0 }}>Sections of this program will be released daily on YouTube and RSVP is required to access the stream. Remember to <a style={{ color: 'black', fontWeight: '600' }} target="_blank" href="https://www.youtube.com/c/nycjewelryweek">subscribe to NYC Jewelry Week’s YouTube channel</a> for access to all of our content.</p>
                </div>
              }
              {
                (eventContent.participants && eventContent.participants.length > 0) ?
                <div className="event-participants">
                  <h4 className="underlined">Participants</h4>
                  <ul>
                    {
                      eventContent.participants.map( (participant, index) => {
                        return <li key={`participant-${index}`}>
                          <h5>{ participant.name }</h5>
                          {
                            participant.bio &&
                            <div className="participant-bio" dangerouslySetInnerHTML={{ __html: participant.bio }}>
                            </div>
                          }
                        </li>
                      })
                    }
                  </ul>
                </div> : ''
              }
              {
                (eventContent.sponsor && eventContent.sponsor.name.length > 0) ?
                <div className="event-participants sponsor_layout">
                  <h4 className="underlined">Sponsored By</h4>
                  <div className="event-sponsors">
                    <a href={eventContent.sponsor.link} target="_blank">
                      {(eventContent.sponsor.image && eventContent.sponsor.image.length > 0) 
                        ? <img src={eventContent.sponsor.image} className="sponsor_logo" /> 
                        : ''
                      }
                      <span>{eventContent.sponsor.name}</span>
                    </a>
                    {
                      eventContent.sponsors && eventContent.sponsors.map( sponsor => (
                        <a href={sponsor.link} target="_blank">
                          {
                            (sponsor.image && sponsor.image.length > 0) 
                            ? <img src={sponsor.image} className="sponsor_logo" /> 
                            : ''
                          }
                          <span>{sponsor.name}</span>
                        </a>
                      ))
                    }
                  </div>
                </div> : ''
              }
              {
                eventContent.is_discover &&
                <div>
                  <h4 className="underlined">DISCOVER</h4>
                  <p><strong>Find Jewelry from this event at <a target="_blank" style={{ color: 'black' }} href={`${homeurl}/discover`}>DISCOVER</a></strong></p>
                </div>
              }
              {
                eventContent.virtual_event === 'In-person' && eventContent.covid_messaging &&
                <div style={{ marginBottom: '4rem' }}>
                  {
                    eventContent.covid_messaging.heading && 
                    <h4 className="underlined">{ eventContent.covid_messaging.heading }</h4>
                  }
                  {
                    eventContent.covid_messaging.content && 
                    <div dangerouslySetInnerHTML={{ __html: eventContent.covid_messaging.content }}></div>
                  }
                </div>
              }
              {
                (eventContent.supportEvents && eventContent.supportEvents.length > 0) ?
                <div className="event-participants">
                  <h4 className="underlined">Supporting Events</h4>
                  <ul>
                    {
                      eventContent.supportEvents.map( (supportEvent, index) => {
                        return <li key={`support-event-${index}`}>
                          {
                            supportEvent.event_types &&
                            <p className="event-type">
                              {
                                supportEvent.event_types.name &&
                                <span style={{ background: 'black', color: 'white' }}>{ supportEvent.event_types.name === 'irl' ? 'In-person' : supportEvent.event_types.name }</span>
                              }
                            </p>
                          }
                          {
                            (supportEvent.event && supportEvent.event.length > 0) ?
                            <h5 dangerouslySetInnerHTML={{ __html: supportEvent.event }}/>
                            : ''
                          }
                          {
                            (supportEvent.location && supportEvent.location.address) &&
                            <p><strong>{supportEvent.location.address}</strong></p>
                          }
                          {
                            supportEvent.date && supportEvent.date.length > 1 &&
                            <div className="event-date" style={{ 'display': 'none' }}>
                              <p>
                                <strong>{ formatPrettyDate(supportEvent.date) }</strong>
                              </p>
                              {
                                supportEvent.start_time && <p><span><strong>{`${beautyTime(supportEvent.start_time)} - ${beautyTime(supportEvent.end_time)}`}</strong></span></p>
                              }
                            </div>
                          }
                          {
                            supportEvent.rsvp &&
                            <a target="_blank" href={supportEvent.rsvp.url}>More Info</a>
                          }
                        </li>
                      })
                    }
                  </ul>
                </div> : ''
              }
              <div className="text-centered follow-nycjw">
                <a href="https://instagram.com/nycjewelryweek" target="_blank"><img src={instagram}/><span>follow NYCJW on Instagram!</span></a>
              </div>
            </div>
          </div>
        }
        {
          loading &&
          <div className="event-loading-wrapper">
            <div className="event-loading">Loading</div>
          </div>
        }
      </div>
    </div>
  )
}

export default EventSlideout
