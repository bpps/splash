import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { formatPrettyDate, beautyTime } from '../utils'
import moment from 'moment'

const Event = props => {
  
  const { nycjwEvent } = props
  const eventEndTime = nycjwEvent.end_time
    ? moment(nycjwEvent.end_time, ["h:mm A"]).format("HH:mm:ss")
    : moment('23:59:59', ["h:mm A"]).format("HH:mm:ss")

  const navigate = useNavigate();
  const params = useParams();
  const selectedTestDay = useSelector( state => state.selectedTestDay );
  const todaysDate = useSelector( state => state.todaysDate );
  const [dateActive, setDateActive] = useState('')
  const [buttonLabel, setButtonlabel] = useState(params.type === 'virtual' ? 'Learn More' : 'RSVP')
  const [today, setToday] = useState(new Date)

  const curEventType = params.type ? params.type : 'scheduled';
  let weekEndDate = new Date(eventIntros[eventIntros.length - 1]);
  weekEndDate.setDate(weekEndDate.getDate() + 1)

  useEffect( () => {
    if ( selectedTestDay !== null ) {
      setToday(new Date(selectedTestDay + ` ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`))
    }
  }, [selectedTestDay, todaysDate])

  useEffect( () => {
    setToday(selectedTestDay ? new Date(selectedTestDay + ` ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`) : new Date)
  }, [nycjwEvent])

  useEffect( () => {
    
    let eventStartDate = new Date( nycjwEvent.date + 'T09:00:00')
    let eventEndDate = new Date( nycjwEvent.date + 'T' + eventEndTime)
    let dateActiveText = today > eventStartDate ? 'event-active' : ''
    let buttonLabelText = buttonLabel
    const ongoingStartTime = nycjwEvent.start_time
      ? moment(nycjwEvent.start_time, ["h:mm A"]).format("HH:mm:ss")
      : moment('07:00:00', ["h:mm A"]).format("HH:mm:ss")
    let eventDate = nycjwEvent.intro ? new Date(nycjwEvent.date + 'T23:59:59') : new Date( nycjwEvent.date + 'T' + ongoingStartTime)
    if ( curEventType === 'one-time' ) {
      dateActiveText = '';
      const weekStartDate = new Date('2022-11-14T07:00:00')
      if ( today >= eventDate ) {
        buttonLabelText = 'Learn More'
      } else {
        buttonLabelText = nycjwEvent.listing_button_label ? nycjwEvent.listing_button_label : 'RSVP'
        if ( !nycjwEvent.rsvp ) {
          
        }
      }
    }
    
    if ( today < eventDate && nycjwEvent.listing_button_label ) {
      buttonLabelText = nycjwEvent.listing_button_label
    }

    if ( curEventType === 'on-going' || curEventType === 'virtual' ) {
      dateActiveText = '';
      if ( (nycjwEvent.start_date && nycjwEvent.start_date !== 'Invalid Date') || nycjwEvent.date ) {
        let eventStartTime = nycjwEvent.start_time ? nycjwEvent.start_time : '06:00:00'
        // console.log(nycjwEvent, eventStartTime)
        eventStartDate = nycjwEvent.start_date ? new Date( nycjwEvent.start_date + 'T' + eventStartTime) : new Date( nycjwEvent.date + 'T' + eventStartTime)
        let eventEndTime = nycjwEvent.end_time ? nycjwEvent.end_time : '11:59:00'
        eventEndDate = nycjwEvent.end_date 
          ? new Date( nycjwEvent.end_date + 'T' + eventEndTime )
          : new Date( nycjwEvent.start_date + 'T' + eventEndTime )
      }
      if ( nycjwEvent.dates ) {
        let eventStartTime = nycjwEvent.dates[0].start_time || '06:00:00'
        eventStartDate = new Date( nycjwEvent.dates[0].event_date + 'T' + eventStartTime)
        let eventEndTime = nycjwEvent.dates[nycjwEvent.dates.length - 1].end_time || '11:59:00' 
        eventEndDate = new Date(nycjwEvent.dates[nycjwEvent.dates.length - 1].event_date + 'T' + eventEndTime )
      }
      if ( curEventType === 'on-going' ) {
        if ( today >= eventStartDate ) {
          buttonLabelText = 'Learn More'
        } else {
          buttonLabelText = nycjwEvent.listing_button_label ? nycjwEvent.listing_button_label : 'Learn more'
        }
      }
      // console.group('EVENT')
      // console.log('today:', today)
      // console.log('start date: ', eventStartDate)
      // console.log('name:', nycjwEvent.title )
      // console.groupEnd()
      if ( curEventType === 'virtual' ) {
        if ( today >= eventStartDate ) {
          buttonLabelText = 'watch now'
        } else {
          buttonLabelText = 'Learn More'
        }
      }
    }
  
    // If todays date is after the event end date
    if (today > eventEndDate && today < weekEndDate && params.type !== 'virtual' ) {
      dateActiveText = ' event-over'
    } 
    setDateActive(dateActiveText)
    setButtonlabel(buttonLabelText)
  }, [today])
  
  let borderColor = nycjwEvent.event_types ? nycjwEvent.event_types[0].color ? nycjwEvent.event_types[0].color : '#FEF200' : '#FEF200'
  
  let displayButton = true

  
  let eventType = nycjwEvent.event_types ? nycjwEvent.event_types[0].name : null;
  let permalink = nycjwEvent.permalink.split('/')
  permalink = permalink[permalink.length - 1]
  const eventPermalink = nycjwEvent.supporting 
  ? curEventType === 'virtual' 
    ? params.cat === 'instagram' 
      ? `/${curEventType}/instagram/${permalink}/${nycjwEvent.supporting_permalink}`
      : `/${curEventType}/${permalink}/${nycjwEvent.supporting_permalink}`
    : `/${curEventType}/${params.cat}/${permalink}/${nycjwEvent.supporting_permalink}`
  : curEventType === 'virtual'
    ? params.cat === 'instagram' 
      ? `/${curEventType}/instagram/${permalink}`
      : `/${curEventType}/${permalink}`
    : `/${curEventType}/${params.cat}/${permalink}`
  
  return (
    <>
      <div 
        className={`event-listing${dateActive}${nycjwEvent.intro ? ' intro' : ''} ${nycjwEvent.scheduled_or_durational}`}
        onClick={ () => navigate({
          pathname: eventPermalink
        }) }>
          <div className="event-image-wrapper">
            <div className="event-image-content">
              <div className="event-image bg-centered" style={{ backgroundImage: `url(${nycjwEvent.listing_image})` }}>

              </div>
            </div>
          </div>
          <div className="event-info">
            <div className="event-title">
              {
                !nycjwEvent.intro &&
                  <p className="event-type">
                    <span style={{ background: 'black', color: 'white' }}>
                    { nycjwEvent.virtual ? 'Virtual' : 'In-person' }
                    </span>
                    {
                      eventType &&
                      <span style={{ background: borderColor, color: (borderColor.toLowerCase() === '#fef200' || borderColor === 'rgb(255,255,0)') ? 'black' : 'white' }}>{ eventType }</span>
                    }
                  </p>
              }
              <h4
                dangerouslySetInnerHTML={{ __html: nycjwEvent.title }}>
              </h4>
              {
                nycjwEvent.subtitle &&
                <h5 dangerouslySetInnerHTML={{ __html: nycjwEvent.subtitle }}></h5>
              }
              {
                nycjwEvent.excerpt &&
                <div className="event-exc" dangerouslySetInnerHTML={{ __html: nycjwEvent.excerpt }}></div>
              }
            </div>
            <div className="event-details">
            <div className="event-time" style={{ 'display': 'none' }}>
              {
                (nycjwEvent.date && !nycjwEvent.start_date && curEventType !== 'one-time') &&
                <p style={{ margin: 0, fontFamily: 'Bebas Neue' }}>{formatPrettyDate(nycjwEvent.date)}</p>
              }
              {
                nycjwEvent.dates && 
                <p style={{ margin: 0, fontFamily: 'Bebas Neue' }}>{`${formatPrettyDate(nycjwEvent.dates[0].event_date)} - ${formatPrettyDate(nycjwEvent.dates[nycjwEvent.dates.length -1].event_date)}`}</p>
              }
              {
                (nycjwEvent.start_date && !nycjwEvent.date && curEventType !== 'one-time') &&
                <p style={{ margin: 0, fontFamily: 'Bebas Neue' }}>{formatPrettyDate(nycjwEvent.start_date)} {
                  nycjwEvent.end_date && ` - ${formatPrettyDate(nycjwEvent.end_date)}`
                }</p>
              }
              {
                nycjwEvent.start_time &&
                <>
                  <p style={{ margin: 0 }}>
                    { nycjwEvent.start_time }
                    { nycjwEvent.end_time && <span> - </span> }
                    { nycjwEvent.end_time }
                  </p>
                </>
              }
            </div>
            <div className="cta-wrapper">
              <button 
                className="btn"
                onClick={ () => navigate({
                  pathname: eventPermalink
                }) }>
                  { buttonLabel }
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Event
