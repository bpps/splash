import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { selectTestDay } from '../actions/selectedTestDay'

class DateSimulation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false
    }
    this.handleSelectDay = this.handleSelectDay.bind(this)
  }

  handleSelectDay (day) {
    const { dispatch, selectedTestDay } = this.props
    if ( day.target.value === selectedTestDay ) {
      return
    }
    dispatch( selectTestDay(day.target.value) )
    this.setState({
      visible: false
    })
  }

  render () {
    const { selectedTestDay } = this.props
    const { visible } = this.state
    const options = eventIntros.map( day => {
      const label = new Intl.DateTimeFormat('en-US', {
        weekday: 'long',
        month: 'long',
        day: '2-digit',
        timeZone: 'UTC'
      }).format(new Date(day))
      return <option key={`option-${day}`} value={day}>{label}</option>
    })
    let toggleLabel = visible ? 'close' : 'Simulate today\'s date'
    if ( selectedTestDay ) {
      toggleLabel = `today: ${new Intl.DateTimeFormat('en-US', {
        weekday: 'long',
        month: 'long',
        day: '2-digit',
        timeZone: 'UTC'
      }).format(new Date(selectedTestDay))}`
    }
    return (
      <div className={ `sim-wrapper${ visible ? ' visible' : '' }`}>
        <div className="sim-container">
          <p>Choose a day to simulate the current day of the week. Click around to see what the event days will look like before and during that day.</p>
          <p className="sim-sub"><sub>*This only shows when you're logged in as admin</sub></p>
          <select
            value={selectedTestDay || 1}
            onChange={ value => this.handleSelectDay(value)}>
            <option value={1} disabled>Select day to test</option>
            { options }
          </select>
        </div>
        <div className="sim-toggle" onClick={ () => this.setState({ visible: !visible })}>
          <span>{ toggleLabel }</span>
        </div>
      </div>
    )
  }
}

DateSimulation.propTypes = {

}

export default connect((state) => ({
  selectedTestDay: state.selectedTestDay
}))(DateSimulation)
