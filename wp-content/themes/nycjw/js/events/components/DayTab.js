import React from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { mapDate } from '../actions/map'
import MapIcon from '@mui/icons-material/Map';

const DayTab = props => {
  const navigate = useNavigate();
  const params = useParams();
  const dispatch = useDispatch();
  const { day, option } = props
  
  if ( !option ) {
    return (
      <div onClick={ () => navigate({ pathname: `/${params.type ? params.type : 'scheduled'}/${day}`}) } className={ `header-tab ${params.cat === day ? ' active' : ''}` }>
        <h3>{new Intl.DateTimeFormat('en-US', {
          weekday: 'long',
          timeZone: 'UTC'
        }).format(new Date(day))}</h3>
        <p>
          {new Intl.DateTimeFormat('en-US', {
            month: 'long',
            day: '2-digit',
            timeZone: 'UTC'
          }).format(new Date(day))}
        </p>
        {
          params.cat === day &&
          <button id="map-current-date-tab" className="btn" onClick={() => dispatch(mapDate())}><MapIcon/><span>MAP</span></button>
        }
      </div>
    )
  } else {
    return (
      <option value={day} key={`day-option-${day}`}>
          {
            new Intl.DateTimeFormat('en-US', {
              weekday: 'long',
              month: 'long',
              day: '2-digit',
              timeZone: 'UTC'
            }).format(new Date(day))
        }
      </option>
    )
  }
}

export default DayTab
