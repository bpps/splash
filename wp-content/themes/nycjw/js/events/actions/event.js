export const SELECT_EVENT = 'SELECT_EVENT'

export const selectEvent = id => {
  return {
    type: SELECT_EVENT,
    id
  }
}
