export const SELECT_EVENT_TYPE = 'SELECT_EVENT_TYPE'

export const selectEventType = selectedEventType => {
  return {
    type: SELECT_EVENT_TYPE,
    selectedEventType
  }
}
