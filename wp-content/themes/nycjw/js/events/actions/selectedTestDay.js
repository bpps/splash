export const SELECT_TEST_DAY = 'SELECT_TEST_DAY'

export const selectTestDay = selectedTestDay => {
  return {
    type: SELECT_TEST_DAY,
    selectedTestDay
  }
}
