export const SELECT_CAT = 'SELECT_CAT'

export const selectCat = selectedCat => {
  return {
    type: SELECT_CAT,
    selectedCat
  }
}
