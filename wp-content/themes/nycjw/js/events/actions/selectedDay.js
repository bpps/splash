export const SELECT_DAY = 'SELECT_DAY'

export const selectDay = selectedDay => {
  return {
    type: SELECT_DAY,
    selectedDay
  }
}
