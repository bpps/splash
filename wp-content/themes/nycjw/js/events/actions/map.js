export const TOGGLE_MAP = 'TOGGLE_MAP'
export const MAP_DATE = 'MAP_DATE'

export const toggleMap = toggled => {
  return {
    type: TOGGLE_MAP,
    toggled
  }
}

export const mapDate = () => {
  return {
    type: MAP_DATE
  }
}
