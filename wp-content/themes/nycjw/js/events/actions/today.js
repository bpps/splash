export const SET_TODAYS_DATE = 'SET_TODAYS_DATE'

export const setTodaysDate = todaysDate => {
  return {
    type: SET_TODAYS_DATE,
    todaysDate
  }
}
