export const SET_EVENTS = 'SET_EVENTS'

export const setEvents = events => {
  return {
    type: SET_EVENTS,
    events
  }
}
