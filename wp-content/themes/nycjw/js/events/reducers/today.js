import { SET_TODAYS_DATE } from '../actions/today'

export const today = (state = new Date, action) => {
  switch(action.type) {
    case SET_TODAYS_DATE:
      return action.todaysDate
    default:
      return state
  }
}
