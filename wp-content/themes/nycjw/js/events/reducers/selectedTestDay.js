import { SELECT_TEST_DAY } from '../actions/selectedTestDay'

export const selectedTestDay = (state = null, action) => {
  switch(action.type) {
    case SELECT_TEST_DAY:
      return action.selectedTestDay
    default:
      return state
  }
}
