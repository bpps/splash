import { SELECT_DAY } from '../actions/selectedDay'
import { SELECT_TEST_DAY } from '../actions/selectedTestDay'

export const selectedDay = (state = null, action) => {
  switch(action.type) {
    case SELECT_DAY:
      return action.selectedDay
    case SELECT_TEST_DAY:
      return action.selectedTestDay
    default:
      return state
  }
}
