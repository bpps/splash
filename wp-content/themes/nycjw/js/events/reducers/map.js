import { TOGGLE_MAP, MAP_DATE } from '../actions/map'

export const mapToggled = (state = false, action) => {
  switch(action.type) {
    case TOGGLE_MAP:
      return action.toggled
    case MAP_DATE:
      return true
    default:
      return state
  }
}

export const mapDate = (state = false, action) => {
  switch(action.type) {
    case MAP_DATE:
      return true
    case TOGGLE_MAP:
      return !action.toggled ? false : state
    default:
      return state
  }
}
