import { SET_EVENTS } from '../actions/events'

export const events = (state = [], action) => {
  switch(action.type) {
    case SET_EVENTS:
      return action.events
    default:
      return state
  }
}

export const loadingEvents = (state = false, action) => {
  switch(action.type) {
    case SET_EVENTS:
      return false
    default:
      return state
  }
}
