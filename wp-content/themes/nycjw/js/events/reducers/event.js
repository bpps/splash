import { SELECT_EVENT } from '../actions/event'

export const selectedEvent = (state = null, action) => {
  switch(action.type) {
    case SELECT_EVENT:
      return action.id
    default:
      return state
  }
}
