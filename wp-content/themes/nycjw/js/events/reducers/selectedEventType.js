import { SELECT_EVENT_TYPE } from '../actions/selectedEventType'
import { SELECT_CAT } from '../actions/selectedCat'

export const selectedEventType = (state = null, action) => {
  switch(action.type) {
    case SELECT_EVENT_TYPE:
      return action.selectedEventType
    case SELECT_CAT:
      if ( state !== 'ongoing' ) {
        return 'ongoing'
      }
      return state
    default:
      return state
  }
}
