import { SELECT_CAT } from '../actions/selectedCat'

export const selectedCat = (state = null, action) => {
  switch(action.type) {
    case SELECT_CAT:
      return action.selectedCat
    default:
      return state
  }
}
