import { combineReducers } from 'redux'
import { events, loadingEvents } from './events'
import { selectedCat } from './selectedCat'
import { selectedDay } from './selectedDay'
import { selectedTestDay } from './selectedTestDay'
import { selectedEvent } from './event'
import { selectedEventType } from './selectedEventType'
import { today } from './today'
import { mapToggled, mapDate } from './map'

export default combineReducers({
  events,
  loadingEvents,
  selectedCat,
  selectedDay,
  selectedEvent,
  selectedEventType,
  selectedTestDay,
  today,
  mapToggled,
  mapDate
})
