import { SELECT_CAT } from '../actions/selectedCat'
import { SELECT_DAY } from '../actions/selectedDay'
import { SELECT_TEST_DAY } from '../actions/selectedTestDay'
import { SELECT_EVENT_TYPE } from '../actions/selectedEventType'
import { SET_EVENTS } from '../actions/events'
import { fetchEvents } from '../utils'

const getEvents = store => next => action => {
  next(action)
  switch (action.type) {
    // case SELECT_TEST_DAY:
    //   if ( store.getState().selectedDay != action.selectedDay ) {
    //     next(action)
    //     break
    //   }
    // case SELECT_DAY:
    //   fetchEvents( action.selectedDay, 'scheduled' ).then( events => {
    //     next({
    //       type: SET_EVENTS,
    //       events
    //     })
    //   })
    // break
    // case SELECT_CAT:
    //   fetchEvents( action.selectedCat, 'ongoing' ).then( events => {
    //     next({
    //       type: SET_EVENTS,
    //       events
    //     })
    //   })
    // break
    case SET_EVENTS:
      if ( store.getState().selectedEventType == 'ongoing' ) {
        let firstCat = null
        const events = store.getState().events
        eventCats.slice().reverse().forEach( eventCat => {
          if ( events[eventCat.slug] !== undefined ) {
            firstCat = eventCat.slug
            return
          }
        })
        if ( firstCat ) {
          next({
            type: SELECT_CAT,
            selectedCat: firstCat
          })
        }
      }
    break
    // case SELECT_EVENT_TYPE:
    //   const param = action.selectedEventType === 'scheduled'
    //     ? store.getState().selectedDay
    //     : store.getState().selectedCat
    //   fetchEvents( param, action.selectedEventType ).then( events => {
    //     next({
    //       type: SET_EVENTS,
    //       events
    //     })
    //   })
    // break
    default:
    break
  }
}

export default getEvents
