import React from 'react'
import Header from './components/Header'
import Events from './components/Events'
import DateSimulation from './components/DateSimulation'
import { useParams } from 'react-router-dom';
import Map from './components/Map'
import EventSlideout from './components/EventSlideout'

const App = props => {
  const params = useParams();
  const eventType = params.type ? params.type : nycjwEventSettings.selected_event_type
  return (
    <div className="App">
      {
        nycjwEventSettings.content &&
        <div className="event-main-content">
          <div className="inner-content" dangerouslySetInnerHTML={{ __html: nycjwEventSettings.content }}>
          </div>
        </div>
      }
      {
        nycjwEventSettings.is_admin && <DateSimulation/>
      }
      <Header {...props} />
      <Events {...props} />
      {
        ( params.event 
          || (params.type === 'virtual' && params.cat && params.cat !== 'instagram')
          || (params.type === 'virtual' && params.cat === 'instagram' && params.event)) && <EventSlideout/>
      }
      <Map/>
    </div>
  );
}

export default App
